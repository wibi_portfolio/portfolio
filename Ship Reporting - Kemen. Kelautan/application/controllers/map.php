<?php
class map extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
	}
    
    public function index()
    {
		$this->load->view('map');
    }
	
    public function detail()
    {
		$data['content']="detail";
		$id=$this->uri->segment(3);
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('REQUEST_ID',$id);
        $data['vacan'] = $this->db->get()->row();
		$set = array('viewed'=>($data['vacan']->viewed+1));
		$where = array('REQUEST_ID'=>$id);
		$this->crud->edit_trans('crr_vacancy',$set, $where);
		$this->load->view('home_template',$data);
    }
	
}
?>