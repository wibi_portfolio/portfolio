<?php
class Applicant extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
	}
    
    public function index()
    {
		$data['content']="home";
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('END_STATUS',1);
        $data['vacan'] = $this->db->get()->result();
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$this->load->view('home_template',$data);
    }
	public function activation()
    {
		$data['content']="home";
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('END_STATUS',1);
        $data['vacan'] = $this->db->get()->result();
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$this->load->view('home_template',$data);
    }
	
	public function jobs()
    {
		$data['content']="jobs";
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('END_STATUS',1);
        $data['vacan'] = $this->db->get()->result();
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$this->load->view('home_template',$data);
    }
	
    public function apply()
    {
		
		$appid=$this->session->userdata('appid');
		$reqid=$this->input->post('req_id');
		$cek  	= $this->crud->msrwhere('crr_applied',array('app_id'=>$appid,'req_id'=>$reqid),'APPLIED_ID','asc')->num_rows();
		if($cek>0){
			echo 1;
		}else{
			$data = array(
			   'APP_ID' => $appid ,
			   'REQ_ID' => $reqid
			);
			$this->db->insert('crr_applied', $data);
			echo 0;
		}
		/*
		$data['content']="home";
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('END_STATUS',1);
        $data['vacan'] = $this->db->get()->result();
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$this->load->view('home_template',$data);
		*/
    }
    public function register()
    {
		$data['content']	="register";
		$data['privacy']  	= $this->crud->msrwhere('app_config',array('config_key'=>'privacy_statement'),'config_key','asc')->row()->config_value;
		$data['religion']  	= $this->crud->msrwhere('crr_religion',array('religion_enabled'=>0),'religion_name_en','asc')->result();
		$data['country']  	= $this->crud->msr('country','country_name','asc')->result();
		$data['edulev']  	= $this->crud->msrwhere('crr_education',array('actived'=>1),'status','asc')->result();
		$data['major']  	= $this->crud->msrwhere('crr_major',array('view'=>0),'major_name_en','asc')->result();
		$this->load->view('home_template',$data);
    }
    public function save_register()
    {
		$error='';
		$email=$this->input->post('email');
		$kata=explode('@',$email);
		$password=$kata[0].date('Ymd');
		$params=$kata[0].$password;
		$cek = $this->crud->msrwhere('crr_app',array('username'=>$email),'id','asc')->row();
			$row=array('username'=>$email,
						   'password'=>md5($password),
						   'params'=>md5($params));
			if(count($cek)<1){
				$input=$this->crud->insert_trans('crr_app',$row);
				$status=0;
				$appid = $this->crud->msrwhere('crr_app',array('username'=>$email),'id','asc')->row();
				echo json_encode(array('status'=>$status,'errorM'=>$error,'appid'=>$appid->id));
			}else
			{
				if($cek->activation==0){
					$status=1;
					$error='Username Not Active Yet. Continue register information.';
					echo json_encode(array('status'=>$status,'errorM'=>$error,'appid'=>$cek->id));
				}else{
					$status=2;
					$error='Username Already Exist';
					echo json_encode(array('status'=>$status,'errorM'=>$error,'username'=>$email));
				}
			}
    }
	 public function save_personal()
    {
		$error='';
		$appid=$_COOKIE['appid'];
		
		$first=$this->input->post('first');
		$middle=$this->input->post('middle');
		$last=$this->input->post('last');
		$pob=$this->input->post('pob');
		$dob=tglDb2($this->input->post('dob'));
		$marital=$this->input->post('marital');
		$gender=$this->input->post('gender');
		$religion=$this->input->post('religion');
		$address=nl2br($this->input->post('address'));
		$zipcode=$this->input->post('zipcode');
		$city=$this->input->post('city');
		$country=$this->input->post('country');
		$phone=$this->input->post('phone');
		$mobile=$this->input->post('mobile');
				
		$cek = $this->crud->msrwhere('crr_personal',array('app_id'=>$appid),'app_id','asc')->row();
			$personal=array('FIRST_NAME'=>$first,
					   'MIDDLE_NAME'=>$middle,
					   'LAST_NAME'=>$last,
					   'POB'=>$pob,
					   'DOB'=>$dob,
					   'MARITAL'=>$marital,
					   'GENDER'=>$gender,
					   'RELIGION'=>$religion,
					   'MOBILE'=>$mobile,
					   'APP_ID'=>$appid);
			if(count($cek)<1){
				if($this->crud->insert_trans('crr_personal',$personal)){
					$rowaddress=array('ADDRESS'=>$address,
					   'ZIPCODE'=>$zipcode,
					   'CITY'=>$city,
					   'COUNTRY'=>$country,
					   'PHONE'=>$phone,
					   'APP_ID'=>$appid);
					if($this->crud->insert_trans('crr_address',$rowaddress))
						$status=0;
					else
						$status=1;
				}
				else{
					$status=1;
				}
			}else
			{
				$personal=array('FIRST_NAME'=>$first,
			   'MIDDLE_NAME'=>$middle,
			   'LAST_NAME'=>$last,
			   'POB'=>$pob,
			   'DOB'=>$dob,
			   'MARITAL'=>$marital,
			   'GENDER'=>$gender,
			   'RELIGION'=>$religion,
			   'MOBILE'=>$mobile);
			   $this->crud->edit_trans('crr_personal',$personal,array('APP_ID'=>$appid));
			   $rowaddress=array('ADDRESS'=>$address,
			   'ZIPCODE'=>$zipcode,
			   'CITY'=>$city,
			   'COUNTRY'=>$country,
			   'PHONE'=>$phone);
			   $this->crud->edit_trans('crr_address',$rowaddress,array('APP_ID'=>$appid));
			   $status=2;
			}
			echo json_encode(array('status'=>$status,'errorM'=>$error));
		
    }
	
	 public function save_education()
    {
		$error='';
		$appid=$_COOKIE['appid'];
		
		$edulev=$this->input->post('edulev');
		$edustat=$this->input->post('edustat');
		$startdate=tglDb2($this->input->post('startdate'));
		$enddate=tglDb2($this->input->post('enddate'));
		$institution=$this->input->post('institution');
		$city=$this->input->post('city');
		$country=$this->input->post('country');
		$major=$this->input->post('major');
		$gpa=$this->input->post('gpa');
				
		$cek = $this->crud->msrwhere('crr_appedu',array('app_id'=>$appid),'app_id','asc')->row();
			$education=array('EDULEV'=>$edulev,
					   'EDUSTAT'=>$edustat,
					   'STARTDATE'=>$startdate,
					   'ENDDATE'=>$enddate,
					   'INSTITUTION'=>$institution,
					   'CITY'=>$city,
					   'COUNTRY'=>$country,
					   'MAJOR'=>$major,
					   'GPA'=>$gpa,
					   'APP_ID'=>$appid);
			if(count($cek)<1){
				if($this->crud->insert_trans('crr_appedu',$education)){
						$status=0;
				}
				else{
					$status=1;
				}
			}else
			{
				$education=array('EDULEV'=>$edulev,
					   'EDUSTAT'=>$edustat,
					   'STARTDATE'=>$startdate,
					   'ENDDATE'=>$enddate,
					   'INSTITUTION'=>$institution,
					   'CITY'=>$city,
					   'COUNTRY'=>$country,
					   'MAJOR'=>$major,
					   'GPA'=>$gpa);
			   $this->crud->edit_trans('crr_appedu',$education,array('APP_ID'=>$appid));
			   $status=2;
			}
			echo json_encode(array('status'=>$status,'errorM'=>$error));
		
    }
	
	 public function save_experience()
    {
		$error='';
		$appid=$_COOKIE['appid'];
		
		$company=$this->input->post('company');
		$industry=$this->input->post('industry');
		$position=$this->input->post('position');
		$currently=$this->input->post('currently');
		$startdate=tglDb2($this->input->post('startdate'));
		$enddate=tglDb2($this->input->post('enddate'));
		$city=$this->input->post('city');
		$country=$this->input->post('country');
		$description=nl2br($this->input->post('description'));
				
		$cek = $this->crud->msrwhere('crr_appexp',array('app_id'=>$appid),'app_id','asc')->row();
			$experience=array('COMPANY'=>$company,
					   'INDUSTRY'=>$industry,
					   'POSITION'=>$position,
					   'CURRENT'=>$startdate,
					   'STARTDATE'=>$startdate,
					   'ENDDATE'=>$enddate,
					   'CITY'=>$city,
					   'COUNTRY'=>$country,
					   'DESCRIPTION'=>$description,
					   'APP_ID'=>$appid);
			if(count($cek)<1){
				if($this->crud->insert_trans('crr_appexp',$experience)){
						$status=0;
				}
				else{
					$status=1;
				}
			}else
			{
				$experience=array('COMPANY'=>$company,
					   'INDUSTRY'=>$industry,
					   'POSITION'=>$position,
					   'CURRENT'=>$startdate,
					   'STARTDATE'=>$startdate,
					   'ENDDATE'=>$enddate,
					   'CITY'=>$city,
					   'COUNTRY'=>$country,
					   'DESCRIPTION'=>$description);
			   $this->crud->edit_trans('crr_appexp',$experience,array('APP_ID'=>$appid));
			   $status=2;
			}
			echo json_encode(array('status'=>$status,'errorM'=>$error));
		
    }
	 public function upload()
    {
		$error='';
		$appid=@$_COOKIE['appid'];
		$tipe=$this->uri->segment(3);
		 $this->load->library('image_lib');  
        
        if($tipe=='photo'){
			$url = './uploads/photo/';    //path image 
			$config['upload_path'] = $url;      
			$this->load->library('upload');
			$this->upload->initialize($config);
			if( $this->upload->do_upload('photofile') )
			{    
				$files = $this->upload->data();
				$fileNameResize = $config['upload_path'].$files['file_name'];
				$size =  array(                
							array('name'    => 'thumb','width'    => 100, 'height'    => 100, 'quality'    => '100%')
						);
				$resize = array();
				foreach($size as $r){                
					$resize = array(
						"width"            => $r['width'],
						"height"        => $r['height'],
						"quality"        => $r['quality'],
						"source_image"    => $fileNameResize,
						"new_image"        => $url.$r['name'].'/'.$files['file_name']
					);
					$this->image_lib->initialize($resize); 
					if(!$this->image_lib->resize())                    
						die($this->image_lib->display_errors());
				}            
				echo '1';
			} 
			else 
				echo $this->upload->display_errors();
		}
		 if($tipe=='cv'){
			$url = './uploads/cv/';    //path image 
			$config['upload_path'] = $url;      
			$this->load->library('upload');
			$this->upload->initialize($config);
			if( $this->upload->do_upload('cv_file') )
				echo '1';
			else 
				echo $this->upload->display_errors();
		}
    }
	
    public function register_success()
    {
		$data['content']="home";
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$data['notification']="Thank you for your registration in Career website. Now you can apply to available positions within Group. Please continue your Login and change your Password.";
		$this->load->view('home_template',$data);
    }
	
     public function cek_login_json()
    {
		$this->load->model('login_app_model');
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        
        $auth = $this->login_app_model->loginAuth($username, $pass);
        switch($auth)
        {
            case 1:
                echo 0;
				break;
            case 0 :
                echo 1;
				break;
            case -1 :
                echo 2;
				break;
        }
    }
	public function logout()
    {
		$data = array(
			   'id' => '' ,
			   'type' => 'logout' ,
			   'table_' => 'app',
			   'date_' => date('Y-m-d h:i:s') ,
			   'username' => $this->session->userdata('userapp')
			);
			$this->db->insert('user_activity', $data);
        $this->session->sess_destroy();
        redirect(base_url());
    }
	
    public function resume()
    {
		$data['content']="resume";
		$id=$this->session->userdata('appid');
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('REQUEST_ID',$id);
        $data['vacan'] = $this->db->get()->row();
		$data['personal']  	= $this->crud->msrwhere('crr_personal',array('APP_ID'=>$id),'APP_ID','asc')->row();
		$data['address']  	= $this->crud->msrwhere('crr_address',array('APP_ID'=>$id),'APP_ID','asc')->row();
		$data['education']  = $this->crud->msrwhere('crr_appedu',array('APP_ID'=>$id),'APP_ID','asc')->result();
		$data['experience'] = $this->crud->msrwhere('crr_appexp',array('APP_ID'=>$id),'APP_ID','asc')->result();
		
		$data['religion']  	= $this->crud->msrwhere('crr_religion',array('religion_enabled'=>0),'religion_name_en','asc')->result();
		$data['country']  	= $this->crud->msr('country','country_name','asc')->result();
		$data['edulev']  	= $this->crud->msrwhere('crr_education',array('actived'=>1),'status','asc')->result();
		$data['major']  	= $this->crud->msrwhere('crr_major',array('view'=>0),'major_name_en','asc')->result();
		
		$this->load->view('home_template',$data);
    }
}
?>