<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Use For    : GIS KMLH
 * @author 	  : -
 * How To Use : 
 */
class Xml extends CI_Controller {
	
	/**
	 * Use For	: 
	 * Fungsi Construct untuk memanggil semua helper, model dll
	 */
	public function __construct() {
		parent::__construct();
		// Your own constructor code
	}
	
	/**
	 * Use For	: membentuk file xml 
	 */
	 function awal(){
		$id=str_replace("%20"," ",$this->uri->segment(5));
		$jenis=$this->uri->segment(3);
		if($jenis==''){
			$lokasi=$this->m_gis->geocode2()->result();
		}
       	$dom = new DOMDocument("1.0");
		$node = $dom->createElement("markers");
		$parnode = $dom->appendChild($node); 
		
		header("Content-type: text/xml"); 

		// Iterate through the rows, adding XML nodes for each
		foreach ($lokasi as $row){  
		  // ADD TO XML DOCUMENT NODE  
			$node = $dom->createElement("marker");  
			$newnode = $parnode->appendChild($node);   
			$icon='GrDot.gif';$analisa=0;
			$tdsnya=0;
			$suhunya=0;
			$condnya=0;
			$salinitasnya=0;
			$donya=0;
			$phnya=0;
			if(@$analisa>0)
			$icon='RedDot.gif';
			$html='<div id="infowindow"> 
			  <div class="title"> 
				  <div class="icon" style="background:url('.$this->config->item('imgpath').$icon.') no-repeat center;"></div> 
				  <div class="text">'.$row->no_spt.'</div> 
			  </div> 
			  <table style="font-size:10px;"> 
			  <tr><td width="100"><b>Tanggal SPT</b></td><td width="10">:</td><td>'.tgltampil($row->tgl_spt).'</td></tr>
			  <tr><td ><b>Latitude</b></td><td width="10">:</td><td>'.$row->lat.'</td></tr>
			  <tr><td ><b>Longitude</b></td><td width="10">:</td><td>'.$row->lng.'</td></tr>
			  </table><br>';
			  //<tr><td ><b>Analisa asumsi</b></td><td width="10"></td><td></td></tr>
			  //<tr><td ><b></b></td><td width="10">:</td><td>'.@$analisa->analisa.'</td></tr>
			$newnode->setAttribute("id",$row->id);  
			$newnode->setAttribute("kel_data",$row->kel_data);  
			$newnode->setAttribute("nama",$row->id_stasiun);
			$newnode->setAttribute("html",$html);
			$newnode->setAttribute("lat", $row->lat);  
			$newnode->setAttribute("lng", $row->lng);  
			$newnode->setAttribute("type", $row->id_stasiun);
			$newnode->setAttribute("ico", $icon);
		} 
		echo $dom->saveXML();
    }  
	
	 function history(){
		$keldata=explode(',',$this->uri->segment(3));
		$lokasi=$this->main_mod->msrwherein('geocode',$keldata,'id')->result();
       	$dom = new DOMDocument("1.0");
		$node = $dom->createElement("markers");
		$parnode = $dom->appendChild($node); 
		
		header("Content-type: text/xml"); 

		// Iterate through the rows, adding XML nodes for each
		foreach ($lokasi as $row){  
		  // ADD TO XML DOCUMENT NODE  
			$node = $dom->createElement("marker");  
			$newnode = $parnode->appendChild($node);   
			$icon='GrDot.gif';$analisa=0;
			$tdsnya=$this->main_mod->cektds($row->tds);
			if(count($tdsnya)>0)
			$analisa++;
			$suhunya=$this->main_mod->ceksuhu($row->suhu,$row->salinitas);
			if(count($suhunya)>0)
			$analisa++;
			$condnya=$this->main_mod->cekcond($row->cond,$row->salinitas);
			if(count($condnya)>0)
			$analisa++;
			$salinitasnya=$this->main_mod->ceksal($row->salinitas);
			if(count($salinitasnya)>0)
			$analisa++;
			$donya=$this->main_mod->cekdo($row->do,$row->salinitas);
			if(count($donya)>0)
			$analisa++;
			$phnya=$this->main_mod->cekph($row->ph,$row->salinitas);
			if(count($phnya)>0)
			$analisa++;
			//$analisa=$this->main_mod->msrwhere('analisa',array('id_sensor'=>$row->id),'id','asc')->row();
			if(@$analisa>0)
			$icon='RedDot.gif';
			$html='<div id="infowindow"> 
			  <div class="title"> 
				  <div class="icon" style="background:url('.ico().$icon.') no-repeat center;"></div> 
				  <div class="text">'.$row->id_stasiun.'</div> 
			  </div> 
			  <table style="font-size:10px;"> 
			  <tr><td width="100"><b>Suhu</b></td><td width="10">:</td><td>'.$row->suhu.' &deg;C</td></tr>
			  <tr><td><b>DHL</b></td><td width="10">:</td><td>'.$row->cond.'</td></tr>
			  <tr><td></td><td height="10"></td><td></td></tr>
			  <tr><td><b>TDS</b></td><td width="10">:</td><td>'.$row->tds.' mg/l</td></tr>
			  <tr><td ><b>Salinitas</b></td><td width="10">:</td><td>'.$row->salinitas.' </td></tr>
			  <tr><td></td><td height="10"></td><td></td></tr>
			  <tr><td ><b>Ph</b></td><td width="10">:</td><td>'.$row->ph.'</td></tr>
			  <tr><td ><b>Turbiniti</b></td><td width="10">:</td><td>'.$row->etc.'</td></tr>
			  </table><br>';
			$newnode->setAttribute("id",$row->id);  
			$newnode->setAttribute("nama",$row->id_stasiun);
			$newnode->setAttribute("html",$html);
			$newnode->setAttribute("lat", $row->lat);  
			$newnode->setAttribute("lng", $row->lng);  
			$newnode->setAttribute("type", $row->id_stasiun);
			$newnode->setAttribute("ico", $icon);
		} 
		echo $dom->saveXML();
    }  
	
	function liatpeta(){
		$keldata=$this->uri->segment(3);
		$row=$this->main_mod->msrwhere('geocode',array('id'=>$keldata),'id','asc')->row();
       	$dom = new DOMDocument("1.0");
		$node = $dom->createElement("markers");
		$parnode = $dom->appendChild($node); 
		
		header("Content-type: text/xml"); 

		// Iterate through the rows, adding XML nodes for each
		  // ADD TO XML DOCUMENT NODE  
			$node = $dom->createElement("marker");  
			$newnode = $parnode->appendChild($node);   
			$icon='GrDot.gif';$analisa=0;
			$tdsnya=$this->main_mod->cektds($row->tds);
			if(count($tdsnya)>0)
			$analisa++;
			$suhunya=$this->main_mod->ceksuhu($row->suhu,$row->salinitas);
			if(count($suhunya)>0)
			$analisa++;
			$condnya=$this->main_mod->cekcond($row->cond,$row->salinitas);
			if(count($condnya)>0)
			$analisa++;
			$salinitasnya=$this->main_mod->ceksal($row->salinitas);
			if(count($salinitasnya)>0)
			$analisa++;
			$donya=$this->main_mod->cekdo($row->do,$row->salinitas);
			if(count($donya)>0)
			$analisa++;
			$phnya=$this->main_mod->cekph($row->ph,$row->salinitas);
			if(count($phnya)>0)
			$analisa++;
			//$analisa=$this->main_mod->msrwhere('analisa',array('id_sensor'=>$row->id),'id','asc')->row();
			if(@$analisa>0)
			$icon='RedDot.gif';
			$html='<div id="infowindow"> 
			  <div class="title"> 
				  <div class="icon" style="background:url('.ico().$icon.') no-repeat center;"></div> 
				  <div class="text">'.$row->id_stasiun.'</div> 
			  </div> 
			  <table style="font-size:10px;"> 
			  <tr><td width="100"><b>Suhu</b></td><td width="10">:</td><td>'.$row->suhu.' &deg;C</td></tr>
			  <tr><td><b>DHL</b></td><td width="10">:</td><td>'.$row->cond.'</td></tr>
			  <tr><td></td><td height="10"></td><td></td></tr>
			  <tr><td><b>TDS</b></td><td width="10">:</td><td>'.$row->tds.' mg/l</td></tr>
			  <tr><td ><b>Salinitas</b></td><td width="10">:</td><td>'.$row->salinitas.' </td></tr>
			  <tr><td></td><td height="10"></td><td></td></tr>
			  <tr><td ><b>Ph</b></td><td width="10">:</td><td>'.$row->ph.'</td></tr>
			  <tr><td ><b>Turbiniti</b></td><td width="10">:</td><td>'.$row->etc.'</td></tr>
			  </table><br>';
			$newnode->setAttribute("id",$row->id);  
			$newnode->setAttribute("nama",$row->id_stasiun);
			$newnode->setAttribute("html",$html);
			$newnode->setAttribute("lat", $row->lat);  
			$newnode->setAttribute("lng", $row->lng);  
			$newnode->setAttribute("type", $row->id_stasiun);
			$newnode->setAttribute("ico", $icon);
		echo $dom->saveXML();
    }  
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>
