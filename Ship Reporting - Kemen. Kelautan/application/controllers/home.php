<?php
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
	}
    
    public function index()
    {
		$data['content']="home";
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('END_STATUS',1);
        $data['vacan'] = $this->db->get()->result();
		$data['city']  	= $this->crud->msrwhere('countrystate',array('view'=>1),'STATE_NAME','asc')->result();
		$data['major']  = $this->crud->msrwhere('crr_jobspec',array('JOBSPEC_CATEGORY'=>'SKL'),'DESCRIPTION_EN','asc')->result();
		$this->load->view('home_template',$data);
    }
	
    public function detail()
    {
		$data['content']="detail";
		$id=$this->uri->segment(3);
		$this->db->select('a.*,a.request_id as req_id,b.position_name_en,c.company_name,d.city_name');
        $this->db->from('crr_vacancy a');
        $this->db->join('crr_position b','a.request_posid = b.position_id');
        $this->db->join('crr_company c','a.compbranch_code = c.company_id');
        $this->db->join('countrycity d','a.worklocation = d.city_id');
        $this->db->where('REQUEST_ID',$id);
        $data['vacan'] = $this->db->get()->row();
		$set = array('viewed'=>($data['vacan']->viewed+1));
		$where = array('REQUEST_ID'=>$id);
		$this->crud->edit_trans('crr_vacancy',$set, $where);
		$this->load->view('home_template',$data);
    }
    public function login()
    {
		$data['content']="home";
		$this->load->view('login',$data);
    }
	
	public function do_login()
    {
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        $this->load->model('home_model');
        $auth = $this->home_model->loginAuth($username, $pass);
        switch($auth)
        {
            case 1:
                echo 0;
                break;
            case 0 :
                echo 1;
                break;
        }
    }
	
	public function logout()
    {
		$data = array(
			   'id' => '' ,
			   'type' => 'logout' ,
			   'table_' => 'member',
			   'date_' => date('Y-m-d h:i:s') ,
			   'username' => $this->session->userdata('username_member')
			);
			$this->db->insert('user_activity', $data);
        $this->session->sess_destroy();
        redirect(base_url());
    }
	
	
}
?>