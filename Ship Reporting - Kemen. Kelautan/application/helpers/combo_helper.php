<?php

function romawi($bln){
	$ico='';
	if($bln=='01' or $bln==1)
		$ico='I';
	if($bln=='02' or $bln==2)
		$ico='II';
	if($bln=='03' or $bln==3)
		$ico='III';
	if($bln=='04' or $bln==4)
		$ico='IV';
	if($bln=='05' or $bln==5)
		$ico='V';
	if($bln=='06' or $bln==6)
		$ico='VI';
	if($bln=='07' or $bln==7)
		$ico='VII';
	if($bln=='08' or $bln==8)
		$ico='VIII';
	if($bln=='09' or $bln==9)
		$ico='IX';
	if($bln=='10' or $bln==10)
		$ico='X';
	if($bln=='11' or $bln==11)
		$ico='XI';
	if($bln=='12' or $bln==12)
		$ico='XII';
	return $ico;
}


function hari_spell($hari){
	$ico='';
	if($hari=='Mon')
		$ico='Senin';
	if($hari=='Tue')
		$ico='Selasa';
	if($hari=='Wed')
		$ico='Rabu';
	if($hari=='Thu')
		$ico='Kamis';
	if($hari=='Fri')
		$ico='Jumat';
	if($hari=='Sat')
		$ico='Sabtu';
	if($hari=='Sun')
		$ico='Minggu';
	return $ico;
}

if ( ! function_exists('tgl_spell')){
	function tgl_spell($tgl){
		$exp=explode('-',$tgl);
		$tanggal = $exp[2];
		$bulan = bulan_en($exp[1]);
		$tahun = $exp[0];
		/*$tanggal = substr($tgl,8,2);
		$bulan = bulan_en(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);*/
		if($tanggal<10)
		return str_replace('0','',$tanggal).' '.$bulan.' '.$tahun;
		else
		return $tanggal.' '.$bulan.' '.$tahun;
	}
}

function tgl_db($tgl){
		$exp=explode('-',$tgl);
		/*$tanggal = substr($tgl,8,2);
		$bulan = bulan_en(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);*/
		if(count($exp)>1)
		return $exp[2].'-'.$exp[1].'-'.$exp[0];
		else
		return '';
	}
function tgl_v($tgl){
		$exp=explode('-',$tgl);
		/*$tanggal = substr($tgl,8,2);
		$bulan = bulan_en(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);*/
		if(count($exp)>1)
		return $exp[2].'-'.$exp[1].'-'.$exp[0];
		else
		return '';
	}

if ( ! function_exists('bulan_en')){
	function bulan_en($bln){
		switch ($bln){
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}


function sub_module2($id,$lev,$selectid=array(),$par=null)
{
    $_CI =& get_instance();
    $sql = "SELECT * from blck_admin_menu where parent=".$id." order by sort asc";
        
    $result = $_CI->db->query($sql)->result_array();
    
    echo "<ul>";
	$n=$lev;
    foreach($result as $row)
    {
		$chk='';
		$sql2 = "SELECT * from blck_admin_menu where parent=".$row['modul_id']." order by sort asc";
		$cek = $_CI->db->query($sql2)->num_rows();
		$sql3 = "SELECT * from blck_user_modul where modul_id=".$row['modul_id']." and role_id=".$lev." order by modul_id asc";
		$cek2 = $_CI->db->query($sql3)->num_rows();
		
		if($cek2>0)
		$chk='checked';
		echo '<li><span><i class="icon-minus-sign"></i> <input type="checkbox" name="disposisi[]" style="margin-top:-2px;vertical-align: middle;" value="'.$row['modul_id'].'" '.$chk.'> '.$row['label'].'</span>';
		//$lev++;
		if($cek>0)
		sub_module2($row['modul_id'],$lev,$selectid);
		echo '</li>';
    }
    echo "</ul>";
}
?>