<?php

function sublevel_menu($parent, $level,$uri)
{
    $_CI =& get_instance();
     $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url,c.modul_name, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND d.role_id = '" . $_CI->session->userdata('role_id') ."' and a.sort<8
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo '';
	$isiuri=explode('/',$uri);
    foreach($result as $row)
    {
		$actv="";$ico="";
		$tabid=$row['label'];
		if($uri==$row['modul_url']){
			$actv="class='active'";
			$tabid="opened";
		}
		$isiuri2=explode('/',$row['modul_url']);
		if(in_array($isiuri2[0],$isiuri)){
			$actv="class='active'";
			$tabid="opened";
		}
		/*
		$ico='<i class="fa fa-dashboard fa-fw"></i> ';
		if(strpos($row['label'],'Surat') !== false)
		$ico='<i class="fa fa-inbox fa-fw"></i> ';
		if(strpos($row['label'],'Entri') !== false)
		$ico='<i class="fa fa-pencil fa-fw"></i> ';
		if(strpos($row['label'],'Agenda') !== false)
		$ico='<i class="fa fa-calendar fa-fw"></i> ';
		if(strpos($row['label'],'Arsip') !== false)
		$ico='<i class="fa fa-folder-open fa-fw"></i> ';
		*/
		echo '<li '.$actv.'><a href="'. site_url($row['modul_url']) .'">'.$ico.$row['label'].'</a></li>';
    }
    echo "";
}
function sublevel_menu_side($parent, $level)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, a.ico, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    foreach($result as $row)
    {
		$ico="fa-navicon";
		if($row['label']!='SPT'){
			if ($row['ico']!='')
			$ico=$row['ico'];
			if ($row['Count'] > 0) {
				echo "<li class=\"has-sub\"><a href=\"#\"><i class=\"".$ico."\"></i><span class=\"title\">" . $row['label'] . "</span></a>";
				echo "<ul>";
				sublevel_menu_side2($row['modul_id'], $level + 1);
				echo "</li>";
				echo "</ul>";
			} elseif ($row['Count']==0) {
				echo "<li class=\"opened active\"><a href='" . site_url($row['modul_url']) . "'><i class=\"".$ico."\"></i><span class=\"title\">" . $row['label'] . "</span></a></li>";
			} else;
		}
    }
}
function sublevel_menu_side2($parent, $level)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    foreach($result as $row)
    {
        if ($row['Count'] > 0) {
            echo "<li><a href=\"#\">" . $row['label'] . "</a>";
			echo "</li>";
        } elseif ($row['Count']==0) {
            echo "<li><a href='" . site_url($row['modul_url']) . "'>" . $row['label'] . "</a></li>";
        } else;
    }
}

function sublevel_menu_top($parent, $level,$uri)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url,c.modul_name, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND a.sort < 3 and d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "";
    foreach($result as $row)
    {
		if($uri==$row['modul_url'])
		$actv="class='active'";
		$ico='<i class="fa fa-dashboard fa-fw"></i> ';
		if(strpos($row['label'],'Surat') !== false)
		$ico='<i class="fa fa-inbox fa-fw"></i> ';
		if(strpos($row['label'],'Entri') !== false)
		$ico='<i class="fa fa-pencil fa-fw"></i> ';
		if(strpos($row['label'],'Agenda') !== false)
		$ico='<i class="fa fa-calendar fa-fw"></i> ';
		if(strpos($row['label'],'Arsip') !== false)
		$ico='<i class="fa fa-folder-open fa-fw"></i> ';
        if ($row['Count'] > 0  && $row['modul_id']!='1') {
            echo "<li class='active' ><a href='#menu1' data-toggle='dropdown' class='dropdown-toggle'>" . $ico.$row['label'] . "<span class=\"fa arrow\"></span></a>";
			sublevel_menu_top2($row['modul_id'], $level + 1);
			echo "</li>";
        } elseif ($row['Count']==0 or $row['modul_id']=='1') {
            echo "<li ".$actv."><a href='" . site_url($row['modul_url']) . "'>" . $ico.$row['label'] . "</a></li>";
        } else;
    }
    echo "";
}

function sublevel_menu_top2($parent, $level)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "<ul class='nav nav-second-level'>";
    foreach($result as $row)
    {
        if ($row['Count'] > 0) {
            echo "<li><a href='" . site_url($row['modul_url']) . "'>" . $row['label'] . "</a>";
			//sublevel_menu_top2($row['modul_id'], $level + 1);
			echo "</li>";
        } elseif ($row['Count']==0) {
            echo "<li><a href='" . site_url($row['modul_url']) . "'>" . $row['label'] . "</a></li>";
        } else;
    }
    echo "</ul>";
}
function sublevel_menu_top3($parent, $level,$uri)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url,c.modul_name, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND a.sort > 7 and d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "";
	$actv="";
    foreach($result as $row)
    {
		$actv="";
		if($uri==$row['modul_url'])
		$actv="class='active'";
		$ico='<i class="fa fa-wrench fa-fw"></i> ';
		if(strpos($row['label'],'User') !== false)
		$ico='<i class="fa fa-users fa-fw"></i> ';
        if ($row['Count'] > 0  && $row['modul_id']!='1') {
            echo "<li class='dropdown' ><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">" . $ico.$row['label'] . " <i class=\"fa fa-caret-down\"></i></a>";
			sublevel_menu_top4($row['modul_id'], $level + 1);
			echo "</li>";
        } elseif ($row['Count']==0 or $row['modul_id']=='1') {
            echo "<li ".$actv."><a href='" . site_url($row['modul_url']) . "'>" . $ico.$row['label'] . "</a></li>";
        } else;
    }
    echo "";
}

function sublevel_menu_top4($parent, $level)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.parent='". $parent . "' AND d.role_id = '" . $_CI->session->userdata('role_id') ."'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "<ul class=\"dropdown-menu dropdown-user\">";
    foreach($result as $row)
    {
        if ($row['Count'] > 0) {
            echo "<li><a href='" . site_url($row['modul_url']) . "'>" . $row['label'] . "</a>";
			//sublevel_menu_top2($row['modul_id'], $level + 1);
			echo "</li>";
        } elseif ($row['Count']==0) {
            echo "<li><a href='" . site_url($row['modul_url']) . "'>" . $row['label'] . "</a></li>";
        } else;
    }
    echo "</ul>";
}
function breadcrumb($uri)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, a.parent, a.level, c.modul_url
        FROM blck_admin_menu a
        LEFT OUTER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        WHERE c.modul_url='". $uri . "'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "";
    foreach($result as $row)
    {
		//print_r($row);
		if($row['level']>0)
		breadcrumb_par($row['parent']);
		if($row['level']==0)
		echo "<li class=\"active\"><i class=\"fa fa-home\"></i> ".$row['label']."</li>";
		else
		echo "<li class=\"active\"><i class=\"fa fa-desktop\"></i> ".$row['label']."</li>";
    }
    echo "";
}

function breadcrumb_par($uri)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, a.parent, a.level, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.modul_id='". $uri . "'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "";
    foreach($result as $row)
    {
		if($row['level']>1)
		breadcrumb_par2($row['parent']);
		if($row['level']==0)
		echo "<li><i class=\"fa fa-home\"></i> ".$row['label']."</li>";
		else
		echo "<li><i class=\"fa fa-desktop\"></i> ".$row['label']."</li>";
    }
    echo "";
}

function breadcrumb_par2($uri)
{
    $_CI =& get_instance();
    $sql = "
        SELECT a.modul_id, a.label, a.sort, c.modul_url, Deriv1.Count
        FROM blck_admin_menu a
        LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count
        FROM blck_admin_menu GROUP BY parent) Deriv1 ON a.modul_id = Deriv1.parent
        INNER JOIN blck_master_modul c
            ON a.modul_id = c.modul_id
        INNER JOIN blck_user_modul d
            ON a.modul_id = d.modul_id
        WHERE a.modul_id='". $uri . "'
		GROUP BY a.label
        ORDER BY a.sort ASC ";
        
    $result = $_CI->db->query($sql)
                  ->result_array();
    
    echo "";
    foreach($result as $row)
    {
		echo "<li><i class=\"fa fa-home\"></i>".$row['label']."</li>";
    }
    echo "";
}


?>