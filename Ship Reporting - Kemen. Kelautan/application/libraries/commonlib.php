<?php

class commonlib
{
    public $_CI;
    public function __construct()
    {
        $this->_CI =& get_instance();
    }
    
    /*
        Retrieve application information
    */
    public function getAppConfig($configId=null)
    {        
        if(!is_null($configId))
        {
            if(is_array($configId))
            {
                $this->_CI->db->where_in('config_id',$configId);
            }else{
                $this->_CI->db->where(array('config_id'=>$configId));   
            }
        }        
        return $this->_CI->db->get('app_config');
    }
    
    /*
        Validate entire page with user's module
    */
    
    public function checkPagesPrivileges($ignoredUri=null)
    {        
        if(is_null($ignoredUri))
        {
            $currentUri = uri_string();
        }else{
            $currentUri = '';
            $uri = $this->_CI->uri->segment_array();
            
            if(is_array($ignoredUri))
            {                
                foreach($ignoredUri as $xuri)
                {
                    unset($uri[$xuri]);   
                }
            }else{
                unset($uri[$ignoredUri]);   
            }
            
            foreach($uri as $segment)
            {
                $currentUri .= $segment.'/';
            }
            $currentUri = rtrim($currentUri,'/');
        }
                
        $urljo=explode('/',$currentUri);
		//print_r($urljo);
        $roleId = $this->_CI->session->userdata('role_id');
        $this->_CI->db->select('a.modul_id,b.modul_url');
        $this->_CI->db->from('user_modul a');
        $this->_CI->db->join('master_modul b','a.modul_id = b.modul_id');
		if(count($urljo)==0){
        $this->_CI->db->not_like('modul_name', 'Add');
        $this->_CI->db->not_like('modul_name', 'Edit');
        $this->_CI->db->not_like('modul_name', 'Delete');        
		}
		if($urljo[1]=='lists'){
	    $this->_CI->db->where(array(
					'b.modul_url'=>$currentUri,
					'a.role_id'=>$roleId)
				);
		}else{
	    $this->_CI->db->where(array(
					'b.modul_url like'=>'%'.$urljo[0].'%',
					'a.role_id'=>$roleId)
				);
	    if($urljo[1]=='add')
	    $this->_CI->db->where(array('a.insert_role'=>1));
	    if($urljo[1]=='edit')
	    $this->_CI->db->where(array('a.update_role'=>1));
		}
		
        $found = $this->_CI->db->count_all_results();
        if($found<1)   
        {
			echo "<script>alert('You can\'t access this module');</script>";
			header('Location: '.base_url().'login');
			return false;
        }
    }
}
?>