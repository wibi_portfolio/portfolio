<?php
/*
    Class of geshi (generic syntax highlighting)
*/
class syn_highlight
{
    protected $_CI;
    
    public function __construct()
    {
        $this->_CI =& get_instance();
        $this->_CI->load->library('3rdparty/geshi/geshi');
    }
    
    //For html language
    public function geshi_php($source)
    {
        $syntax = new GeSHi($source, 'php');
        $syntax->set_header_type(GESHI_HEADER_PRE);
        $syntax->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS);
        return $syntax->parse_code();
    }
}
?>