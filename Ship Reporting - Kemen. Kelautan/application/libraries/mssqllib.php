<?php
/*
    Class for MSSQL DBMS
*/
class mssqllib
{
    public $_CI;
    public function __construct()
    {
        $this->_CI =& get_instance();
        return $this;
    }
    
    //to get next id for auto increment value (use for sqlserver)
    public function get_ai($table,$field,$prefix=null)
    {
        if(is_array($prefix))
        {
            $this->_CI->db->dbprefix = $prefix[0] . '_'; 
        }
        $this->_CI->db->select_max($field);
        $qr = $this->_CI->db->get($table)
                            ->row();
        if(!isset($qr->$field))
        {
            $max = 0;
        }else{
            $max = $qr->$field;
        }
        $nextValue = (int)$max + 1;
        
        if(is_array($prefix))
        {
            $this->_CI->db->dbprefix = $prefix[1] . '_'; 
        }
        return $nextValue;
    }
}
?>