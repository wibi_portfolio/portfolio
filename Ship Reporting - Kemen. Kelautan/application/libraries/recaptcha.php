<?php
include "recaptchalib.php";
class recaptcha
{
    // Get a key from https://www.google.com/recaptcha/admin/create
    private $publickey = "6LfOtcoSAAAAAB6R9OL3Y2IzjGcCId5X_EELwaP7";
    private $privatekey = "6LfOtcoSAAAAAGOCFdpmNUPQSjSx36Bd01eMaHYB";
    
    //Function untuk membuat captcha
    public function create($error=null)
    {
        return recaptcha_get_html($this->publickey, $error);
    }
    
    //Function untuk mengecek captcha
    public function validate_captcha($challenge_field,$response_field)
    {
        $resp = recaptcha_check_answer( $this->privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $challenge_field,
                                        $response_field);
        if ($resp->is_valid) {
                return TRUE;
        } else {
                # set the error code so that we can display it
                $error = $resp->error;
                return $error;
        }
        
    }
}
?>