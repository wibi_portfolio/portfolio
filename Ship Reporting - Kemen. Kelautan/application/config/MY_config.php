<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['app_base_dir'] = $_SERVER['DOCUMENT_ROOT'] .'/taskList/taskListProject/' ;
$config['app_upload_path'] = $config['app_base_dir'] . 'assets/img/';
$config['app_base_path'] = "http://".$_SERVER['HTTP_HOST'] . '/taskList/taskListProject/';
$config['app_img_path'] = $config['app_base_path'] .'assets/img/';
$config['app_img_dir'] = $config['app_base_dir'] . 'assets/img/';
$config['app_css_dir'] = $config['app_base_dir'] . 'assets/css/';
$config['app_js_dir'] = $config['app_base_dir'] . 'assets/js/';
$config['app_layout_dir'] = $config['app_base_dir'] . 'application/views/layout/';
$config['app_config_file'] = $config['app_base_dir'] . 'application/config/plugins_config' . EXT;
$config['languages'] = array(
                             'indonesia'=>'Bahasa Indonesia',
                             'english'=>'English'
                        );
?>