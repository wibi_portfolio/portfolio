
<script type="text/javascript">
jQuery(document).ready(function($)
{
	var notif="<?=@$notification?>";
	$("#wl").select2();
	$("#jf").select2();
	if(notif!='')
	toastr.info(notif);
});
</script>
<div class="page-title"> 
	<div class="title-env"> 
	<h1 class="title"><?= $this->config->item('apl_title') ?></h1> 
	<p class="description"></p> 
	</div> 
</div> 
<div class="panel panel-inverted"> 
	<div class="panel-body"> 
		<div class="col-md-6"> 
			<div class="panel panel-inverted panel-border panel-shadow">
				<div class="panel-heading"> <h3 class="panel-title">Jobs Timeline</h3></div> 
				<div class="panel-body"> 
					<ul class="cbp_tmtimeline"> 
					<?PHP
						foreach($vacan as $rowvac){
					?>						
						<li> 
							<time class="cbp_tmtime" datetime="2015-08-06T18:30">
								<span class="hidden">06/08/2015</span> 
								<span style="font-size:11px;"><?=tgltampil(@$rowvac->REQUEST_DATE)?></span> 
							</time> 
							<div class="cbp_tmicon timeline-bg-success"> <i class="fa-paper-plane-o"></i> </div> 
							<div class="cbp_tmlabel"> 
								<h2><a href="<?=base_url()?>home/detail/<?=@$rowvac->req_id?>"><?=kata(@$rowvac->position_name_en)?></a></h2>
								<h2><span>Location : <?=kata(@$rowvac->city_name)?></span></h2>
							</div> 
						</li> 
					<?php
						}
					?>
					</ul>
				</div> 
			</div> 
		</div>
		<div class="col-sm-6"> 
			<div class="panel panel-default panel-border panel-shadow">
				<div class="panel-heading"> <h3 class="panel-title">Search</h3></div> 
				<div class="panel-body"> 
					<form method="post" role="form" id="searchjob" class="login-form fade-in-effect in" novalidate="novalidate">  
						<div class="form-group"> 
							<label class="control-label" for="username">Keyword</label> 
							<input type="text" class="form-control input-dark" name="keyword" id="keyword" autocomplete="off"> 
						</div> 
						<div class="form-group"> 
							<label class="control-label" for="passwd">Work Location </label> 
							<select name="wl" id="wl" class="form-control">
							<?php
								foreach($city as $rowkat){
									$selected='';
							?>
								<option value="<?=$rowkat->STATE_ID?>" <?=$selected?>><?=kata($rowkat->STATE_NAME)?></option>
							<?php
								}
							?>
							</select>
						</div> 
						<div class="form-group"> 
							<label class="control-label" for="passwd">Job Function </label> 
							<select name="jf" id="jf" class="form-control" data-validate="required" data-message-required="This is required field.">
							<?php
								foreach($major as $rowkat){
									$selected='';
							?>
								<option value="<?=$rowkat->JOBSPEC_CODE?>" <?=$selected?>><?=kata($rowkat->DESCRIPTION_EN)?></option>
							<?php
								}
							?>
							</select>
						</div>
						<div class="form-group"> 
							<button type="submit" class="btn btn-dark btn-block"> <i class="fa-search"></i> Search</button> 
						</div> 
					</form> 
				</div> 
			</div>		
		</div>		
		<?php
		if(@$this->session->userdata('appid')=='')
		{
		?>
		<div class="col-sm-6" style="margin-top: -33px;"> 
		<div class="panel panel-default panel-border panel-shadow">
			<div class="panel-heading"> <h3 class="panel-title">Login</h3></div> 
				<div class="panel-body"> 
					<script type="text/javascript">					
					function login(){
						var isi=$("#login").serialize();
						$.post( "applicant/cek_login_json", isi).done(function( balik,r ) {
							//var rtn=JSON.parse(data);
							//console.log(balik);
							if(balik==='0')
							{
							// Redirect after successful login page (when progress bar reaches 100%)
							location.reload();
							}
							else
							{
							toastr.error("You have entered wrong password, please try again. ", "Invalid Login!");
							}
						});
					}
					</script> 
					<div class="errors-container"> </div> 
					<form method="post" role="form" id="login" class="login-form fade-in-effect in" novalidate="novalidate">  
						<div class="form-group"> 
							<label class="control-label" for="username">Username</label> 
							<input type="text" class="form-control input-dark" name="username" id="username" autocomplete="off"> 
						</div> 
						<div class="form-group"> 
							<label class="control-label" for="passwd">Password</label> 
							<input type="password" class="form-control input-dark" name="password" id="passwd" autocomplete="off"> 
						</div> 
						<div class="form-group"> 
							<button type="button" class="btn btn-dark btn-block text-left" onclick="login()"> <i class="fa-lock"></i> Log In</button> 
						</div> 
						<div class="login-footer"> 
							<a href="#">Forgot your password?</a> 
							<div class="info-links"> Not a member? <a href="<?=base_url()?>applicant/register">Please Register</a>  </div> 
							<div class="info-links">If you found difficulty to Register/ Login, please click <a href="#">Help</a> </div> 
						</div> 
					</form> 
				</div>
			</div> 
		</div>	
		<?php
		}
		?>	
	</div> 
</div>
