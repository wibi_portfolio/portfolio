
<script type="text/javascript">
jQuery(document).ready(function($)
{
	var notif="<?=@$notification?>";
	$("#wl").select2();
	$("#jf").select2();
	if(notif!='')
	toastr.info(notif);
});
function apply(reqid){
	var appid="<?=$this->session->userdata('appid')?>"
	confirm('Are you sure you want to apply for this job ?');
	if(appid!=''){
		$.post( "<?=base_url()?>applicant/apply", {req_id:reqid}).done(function( balik,r ) {
			//var rtn=JSON.parse(data);
			console.log(balik);
			if(balik==='0')
			{
			// Redirect after successful login page (when progress bar reaches 100%)
			//location.reload();
				toastr.info("Thank you for your interest in a career. You have applied for <?=kata(@$vacan->position_name_en)?> position.We will contact you should you meet the qualification of this position.");
			}
			else if(balik==='1')
			{
				toastr.error("You already applied for <?=kata(@$vacan->position_name_en)?> position.");
			}
			else
			{
				toastr.error("You failed to apply for <?=kata(@$vacan->position_name_en)?> position. Please wait a minute and try again later.");
			}
		});	
	}else{
		toastr.error("Please Login First for Apply The Position");
	}
}
</script>
<div class="page-title"> 
	<div class="title-env"> 
	<h1 class="title"><?= $this->config->item('apl_title') ?></h1> 
	<p class="description"></p> 
	</div> 
</div> 
<div class="row"> 
	<div class="col-md-10"> 
		<div class="panel panel-default"> 
			<div class="panel-body"> 
				<div class="vacant-container">				
					<h2 style="color:#004437"><?=kata(@$vacan->position_name_en)?></h4>
						<br><br>
					<div style="color: #474747;font-size: 14px;">
						<div><i class="fa-list"></i> <b>Job responsibilities</b></div>
						<?=@$vacan->DETAIL?>
						<br><br>
						<div><i class="fa-list"></i> <b>Job requirement</b></div>
						<?=@$vacan->JOBSPECIFICATION?>
						<p></p>
					</div>
				</div>
			</div>
		</div>
	</div>		
	<div class="col-md-2"> 
		<div class="one_fourth last text-align-left">
			<div class=" box " style="padding:10px 15px;background-color:#333;color:#fff;;margin:0px">
				<div class="boxcontent">
					<h6>Other Details</h6>
					<div class="divider shortcode-divider thin"></div>
					<div class="spacer" style="height:10px;"></div>
					<p><b>Work location</b><br><?=kata(@$vacan->city_name)?></p>
					<p><b>Years of experience</b><br><?=@$vacan->YEARSEXPERIANCE?> year(s)</p>
					<p><b>Status</b><br><?=kata(@$vacan->EMPSTS)?></p>
					<p><b>Closing date</b><br><?=tgltampil(@$vacan->END_DATE)?></p>
					<p><b>Available Position</b><br><?=@$vacan->REQUEST_TOTSTAFF?> position(s)</p>
					<br><p><b>
					<a style="padding:5px 10px;background:#00994a;margin-top:10px;color:#fff;" href="#" onclick="apply(<?=@$vacan->req_id?>)">Apply Now</a></b></p><b>
					</b>
				</div>
			</div>
		</div>		
		<div class="clearboth"></div>
	</div>
</div>