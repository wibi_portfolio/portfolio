<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="description" content="<?= $this->config->item('apl_title') ?></ Admin Panel"> 
<meta name="author" content="<?= $this->config->item('apl_title') ?></"> 
<title><?= $this->config->item('apl_title') ?></title> 
<!--
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic" id="style-resource-1"> 
-->
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/font-awesome.min.css" id="style-resource-3"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/bootstrap.css" id="style-resource-4"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-core.css" id="style-resource-5"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-forms.css" id="style-resource-6"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-components.css" id="style-resource-7"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-skins.css" id="style-resource-8"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/custom.css" id="style-resource-9"> 
<script src="<?= $this->config->item('jspath') ?>cr/jquery-1.11.1.min.js"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/bootstrap.min.js" id="script-resource-1"></script> 
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> 
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
<![endif]--> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/dataTables.bootstrap.css" id="style-resource-1"> 
	<link href="<?= $this->config->item('csspath') ?>plugins/select2/select2.css" rel="stylesheet"/>
	
    <script type="text/javascript" src="<?= $this->config->item('jspath') ?>plugins/select2/select2.js"></script>

<style>
#logo_blakang{
background-color:#fff;
display:block;
margin:auto;
width:65px;
padding:5px;
margin-top: -10px;
margin-bottom:-7px;
-webkit-border-radius: 99px;
-moz-border-radius: 99px;
border-radius: 99px;
border:5px solid #DBF7FF;
background-color:#FFFFFF;
-webkit-box-shadow: #B3B3B3 2px 2px 2px;
-moz-box-shadow: #B3B3B3 2px 2px 2px; 
box-shadow: #B3B3B3 2px 2px 2px;
}
</style>
</head> 
<body class="page-body"> 
<nav class="navbar horizontal-menu navbar-fixed-top"> 
			<div class="navbar-inner"> 
				<div class="navbar-brand"> 
					<a href="<?=base_url()?>" class="logo"> 
					<img src="<?= $this->config->item('imgpath') ?>logo.png" width="150" alt="" class="hidden-xs"> 
					<img src="<?= $this->config->item('imgpath') ?>logo.png" width="150" alt="" class="visible-xs"> </a> 
					<a href="#" data-toggle="settings-pane" data-animate="true"><i class="linecons-cog"></i> </a> 
				</div> 
				<ul class="nav nav-userinfo navbar-right"> 
					<li class="search-form"> 
					<form method="get" action=""> 
						<input type="text" name="s" class="form-control search-field" placeholder="Type to search..."> 
						<button type="submit" class="btn btn-link"> <i class="linecons-search"></i> </button> 
					</form> 
					</li> 
					<?php
					if($this->session->userdata('appid')!='')
					{
					?>
					<li class="dropdown xs-left"> 
						<a href="#" data-toggle="dropdown" class="notification-icon notification-icon-messages"> <i class="fa-bell-o"></i> <span class="badge badge-purple">7</span> </a> 
						<ul class="dropdown-menu notifications"> 
							<li class="top"> 
								<p class="small"> <a href="#" class="pull-right">Mark all Read</a>You have <strong>3</strong> new notifications.</p> 
							</li> 
							<li> 
								<ul class="dropdown-menu-list list-unstyled ps-scrollbar ps-container"> 
									<li class="active notification-success"> 
										<a href="#"> <i class="fa-user"></i> 
											<span class="line"> <strong>New user registered</strong> </span> <span class="line small time">30 seconds ago</span> 
										</a> 
									</li> 
									<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
										<div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
									</div>
									<div class="ps-scrollbar-y-rail" style="top: 0px; right: 2px;">
										<div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
									</div>
								</ul> 
							</li> 
							<li class="external"> <a href="#"> <span>View all notifications</span> <i class="fa-link-ext"></i> </a> </li>
						</ul> 
					</li> 
					<li class="dropdown user-profile"> 
						<a href="#" data-toggle="dropdown"> 
							<img src="<?=base_url()?>/uploads/photo/thumb/<?=(@$this->session->userdata('pic')==''?'user.jpg':$this->session->userdata('pic'))?>" alt="user-image" class="img-circle img-inline userpic-32" width="28"> 
							<span><?=$this->session->userdata('name')?> <i class="fa-angle-down"></i> </span> 
						</a> 
						<ul class="dropdown-menu user-profile-menu list-unstyled"> 
							<li> <a href="<?=base_url()?>applicant/resume"> <i class="fa-edit"></i>Resume</a> </li> 
							<li> <a href="#settings"> <i class="fa-wrench"></i>Settings</a> </li> 
							<li> <a href="<?=base_url()?>applicant/jobs"> <i class="fa-list"></i>Job Index</a> </li> 
							<li> <a href="#help"> <i class="fa-info"></i>Help</a> </li> 
							<li class="last"> <a href="<?=base_url()?>applicant/logout"> <i class="fa-lock"></i>Logout</a> </li> 
						</ul> 
					</li> 
					<?php
					}
					?>
				</ul> 
			</div> 
		</nav>
		
<div class="page-container"> 
	<div class="main-content" style=""> 
		<?= $this->load->view($content); ?>
      
   <!-- Main Footer --> 
<footer class="main-footer sticky footer-type-1" style=""> 
<div class="footer-inner"> 
<div class="footer-text">
&copy; 2015
<strong><?=@$this->session->userdata('company_website')?>
</div> 
<div class="go-up"> 
<a href="#" rel="go-top"> <i class="fa-angle-up"></i> </a> 
</div> 
</div> 
</footer>
</div> 

</div> 
<div class="page-loading-overlay loaded"> <div class="loader-2"></div> </div> 
<script src="<?= $this->config->item('jspath') ?>cr/TweenMax.min.js" id="script-resource-2"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/resizeable.js" id="script-resource-3"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/joinable.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-api.js" id="script-resource-5"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-toggles.js" id="script-resource-6"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-widgets.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/globalize.min.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/toastr.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/jquery.validate.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-custom2.js" id="script-resource-11"></script>   

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="buatnomorLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:70% !important;">
			<div class="modal-content" id="result">
			
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>	
	
	<div class="modal fade" id="loading">
		<div class="modal-dialog" style="width:20% !important;">
			<div class="modal-content" id="result" style="border-radius:20px;">
				<img src="<?= $this->config->item('imgpath') ?>loading.gif" style="width:50% !important;margin: auto;display: block;">
			</div>
		</div>
	</div>
	<!-- /.modal -->
	
	<div class="page-loading-overlay">
		<div class="loader-2"></div>
	</div>
</body>
</html>

<script src="<?= $this->config->item('jspath') ?>cr/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/datatables/dataTables.bootstrap.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/datatables/jquery.dataTables.yadcf.js" id="script-resource-9"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/datatables/dataTables.tableTools.min.js" id="script-resource-10"></script> 