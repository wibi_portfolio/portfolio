
<script type="text/javascript">
jQuery(document).ready(function($)
{
	var notif="<?=@$notification?>";
	$("#wl").select2();
	$("#jf").select2();
	if(notif!='')
	toastr.info(notif);
});
function apply(reqid){
	var appid="<?=$this->session->userdata('appid')?>"
	confirm('Are you sure you want to apply for this job ?');
	if(appid!=''){
		$.post( "<?=base_url()?>applicant/apply", {req_id:reqid}).done(function( balik,r ) {
			//var rtn=JSON.parse(data);
			console.log(balik);
			if(balik==='0')
			{
			// Redirect after successful login page (when progress bar reaches 100%)
			//location.reload();
				toastr.info("Thank you for your interest in a career. You have applied for <?=kata(@$vacan->position_name_en)?> position.We will contact you should you meet the qualification of this position.");
			}
			else if(balik==='1')
			{
				toastr.error("You already applied for <?=kata(@$vacan->position_name_en)?> position.");
			}
			else
			{
				toastr.error("You failed to apply for <?=kata(@$vacan->position_name_en)?> position. Please wait a minute and try again later.");
			}
		});	
	}else{
		toastr.error("Please Login First for Apply The Position");
	}
}
</script>
<div class="page-title"> 
	<div class="title-env"> 
	<h1 class="title"><?= $this->config->item('apl_title') ?></h1> 
	<p class="description"></p> 
	</div> 
</div> 
<div class="panel panel-inverted"> 
	<div class="panel-body"> 
		<div class="col-md-9"> 
			<div class="panel panel-default panel-border panel-shadow">
				<div class="panel-heading"> <h3 class="panel-title">Jobs Index</h3></div> 
				<div class="panel-body"> 
					<div class="table-wrapper">
					<div class="table-responsive" data-pattern="priority-columns" data-focus-btn-icon="fa-asterisk" data-sticky-table-header="true" data-add-display-all-btn="true" data-add-focus-btn="true"> 
					<table cellspacing="0" class="table table-small-font table-bordered table-striped"> 
						<thead> 
							<tr> 
								<th id="id86719b76-col-0">Company</th> 
								<th data-priority="1" id="id86719b76-col-1">Position Name</th> 
								<th data-priority="3" id="id86719b76-col-2">Work Location</th> 
								<th data-priority="1" id="id86719b76-col-3">Experience</th> 
								<th data-priority="3" id="id86719b76-col-4">Closing Date</th> 
								<th data-priority="3" id="id86719b76-col-5">Status</th> 
							</tr> 
						</thead> 
						<tbody> 
							<?PHP
								foreach($vacan as $rowvac){
									$cek = $this->crud->msrwhere('crr_applied',array('app_id'=>@$this->session->userdata('appid'),'req_id'=>@$rowvac->req_id),'APPLIED_ID','asc')->row();
									$status='';
									if(@$cek->APP_STS==0)
									$status='Applied';
									if(@$cek->APP_STS==3)
									$status='Reject';
							?>	
							<tr> 
								<th colspan="1" data-columns="id86719b76-col-0"><span class="co-name"><?=kata(@$rowvac->company_name)?></span></th> 
								<td data-priority="1" colspan="1" data-columns="id86719b76-col-1">
									<a href="home/detail/<?=@$rowvac->req_id?>" target="_blank"><?=kata(@$rowvac->position_name_en)?></a>
								</td> 
								<td data-priority="3" colspan="1" data-columns="id86719b76-col-2"><?=kata(@$rowvac->city_name)?></td> 
								<td data-priority="1" colspan="1" data-columns="id86719b76-col-3"><?=@$rowvac->YEARSEXPERIANCE?> year(s)</td> 
								<td data-priority="3" colspan="1" data-columns="id86719b76-col-4"><?=tgltampil(@$rowvac->END_DATE)?></td> 
								<td data-priority="3" colspan="1" data-columns="id86719b76-col-5"><?=$status?></td> 
							</tr>
							<?php
								}
							?>
						</tbody> 
					</table> 
					</div>
					</div>
				</div> 
			</div> 
		</div>
		<div class="col-sm-3"> 
			<div class="panel panel-default panel-border panel-shadow">
				<div class="panel-heading"> <h3 class="panel-title">Search</h3></div> 
				<div class="panel-body"> 
					<form method="post" role="form" id="searchjob" class="login-form fade-in-effect in" novalidate="novalidate">  
						<div class="form-group"> 
							<label class="control-label" for="username">Keyword</label> 
							<input type="text" class="form-control input-dark" name="keyword" id="keyword" autocomplete="off"> 
						</div> 
						<div class="form-group"> 
							<label class="control-label" for="passwd">Work Location </label> 
							<select name="wl" id="wl" class="form-control">
							<?php
								foreach($city as $rowkat){
									$selected='';
							?>
								<option value="<?=$rowkat->STATE_ID?>" <?=$selected?>><?=kata($rowkat->STATE_NAME)?></option>
							<?php
								}
							?>
							</select>
						</div> 
						<div class="form-group"> 
							<label class="control-label" for="passwd">Job Function </label> 
							<select name="jf" id="jf" class="form-control" data-validate="required" data-message-required="This is required field.">
							<?php
								foreach($major as $rowkat){
									$selected='';
							?>
								<option value="<?=$rowkat->JOBSPEC_CODE?>" <?=$selected?>><?=kata($rowkat->DESCRIPTION_EN)?></option>
							<?php
								}
							?>
							</select>
						</div>
						<div class="form-group"> 
							<button type="submit" class="btn btn-dark btn-block"> <i class="fa-search"></i> Search</button> 
						</div> 
					</form> 
				</div> 
			</div>		
		</div>		
		
	</div> 
</div>
