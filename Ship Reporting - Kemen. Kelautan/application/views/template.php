<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="description" content="<?= $this->config->item('apl_title') ?></ Admin Panel"> 
<meta name="author" content="<?= $this->config->item('apl_title') ?></"> 
<title><?= $this->config->item('apl_title') ?></title> 
<!--
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic" id="style-resource-1"> 
-->
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/font-awesome.min.css" id="style-resource-3"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/bootstrap.css" id="style-resource-4"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/xenon-core.css" id="style-resource-5"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/xenon-forms.css" id="style-resource-6"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/xenon-components.css" id="style-resource-7"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/xenon-skins.css" id="style-resource-8"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/custom.css" id="style-resource-9"> 
<script src="<?= $this->config->item('jspath') ?>novtmp/jquery-1.11.1.min.js"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/bootstrap.min.js" id="script-resource-1"></script> 
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> 
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
<![endif]--> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/dataTables.bootstrap.css" id="style-resource-1"> 
	<link href="<?= $this->config->item('csspath') ?>plugins/select2/select2.css" rel="stylesheet"/>
	
    <script type="text/javascript" src="<?= $this->config->item('jspath') ?>plugins/select2/select2.js"></script>

<style>
#logo_blakang{
background-color:#fff;
display:block;
margin:auto;
width:65px;
padding:5px;
margin-top: -10px;
margin-bottom:-7px;
-webkit-border-radius: 99px;
-moz-border-radius: 99px;
border-radius: 99px;
border:5px solid #DBF7FF;
background-color:#FFFFFF;
-webkit-box-shadow: #B3B3B3 2px 2px 2px;
-moz-box-shadow: #B3B3B3 2px 2px 2px; 
box-shadow: #B3B3B3 2px 2px 2px;
}
</style>
</head> 
<body class="page-body right-sidebar skin-navy"> 

<div class="page-container">
	<div class="sidebar-menu toggle-others fixed" style=""> 
		<div class="sidebar-menu-inner ps-container ps-active-y"> 
			<header class="logo-env" style="padding-top:25px !important;">
				<div class="logo"> 
					<a href="" class="logo-expanded"> 
						<font style="color:#0099ff;font-size:16px;font-weight:bold;text-shadow: 1px 1px 7px rgba(3, 107, 168, 0.65);"><?= $this->config->item('apl_name') ?></font> 
					</a> 
					<a href="" class="logo-collapsed"> 
						<div id="logo_blakang">
							<img src="<?= $this->config->item('imgpath') ?>logo.png" width="40" alt="" style="margin-top:-3px;"> 
						</div>
					</a> 
				</div> 
				<div class="mobile-menu-toggle visible-xs"> 
					<a href="#" data-toggle="user-info-menu"> <i class="fa-bell-o"></i> <span class="badge badge-success">7</span> </a> 
					<a href="#" data-toggle="mobile-menu"> <i class="fa-bars"></i> </a> 
				</div> 
			</header>
			<section class="sidebar-user-info"> 
				<div class="sidebar-user-info-inner"> 
					<a href="" class="user-profile"> 
						<img src="<?= $this->config->item('imgpath') ?>user-4.png" width="60" height="60" class="img-circle img-corona" alt="user-pic"> 
						<span> <strong><?=$this->session->userdata('username')?></strong></span> 
					</a> 
					<ul class="user-links list-unstyled"> 
					<li> <a href="#" title="Edit profile"> <i class="linecons-user"></i>Edit profile</a> </li> 
					<li> <a href="<?=base_url()?>spt/lists" title="Mailbox"> <i class="linecons-mail"></i>SPT</a> </li> 
					<li class="logout-link"> <a href="<?=base_url()?>login/logout" title="Log out"> <i class="fa-power-off"></i> </a> </li> 
					</ul> 
				</div> 
			</section>			
			<ul id="main-menu" class="main-menu"> 
				<?=sublevel_menu_side(0,0)?>
			</ul> 
			<div class="ps-scrollbar-x-rail" style="display: block; width: 340px; left: 0px; bottom: 3px;">
				<div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
			</div>
			<div class="ps-scrollbar-y-rail" style="display: inherit; top: 0px; height: 596px; right: 2px;">
				<div class="ps-scrollbar-y" style="top: 0px; height: 446px;"></div>
			</div>
		</div> 
	</div> 
	<div class="main-content" style=""> 
	<nav class="navbar user-info-navbar" role="navigation"> 
		<ul class="user-info-menu right-links list-inline list-unstyled"> 
			<li class="hidden-sm hidden-xs" style="min-height: 76px;"> <a href="#" data-toggle="sidebar"> <i class="fa-bars"></i> </a> </li> 
			<!--
			<li class="dropdown hover-line" style="min-height: 76px;"> 
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa-bell-o"></i> <span class="badge badge-purple">7</span> </a> 
			<ul class="dropdown-menu notifications"> 
				<li class="top"> <p class="small"> <a href="#" class="pull-right">Mark all Read</a>You have <strong>3</strong> new notifications.</p> </li> 
				<li> 
					<ul class="dropdown-menu-list list-unstyled ps-scrollbar ps-container"> 
						<li class="active notification-success"> 
							<a href="#"> <i class="fa-user"></i> 
							<span class="line"> <strong>New user registered</strong> </span> 
							<span class="line small time">30 seconds ago</span> 
							</a> 
						</li> 
						<div class="ps-scrollbar-x-rail" style="display: block; width: 0px; left: 0px; bottom: 3px;">
							<div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
						</div>
						<div class="ps-scrollbar-y-rail" style="display: block; top: 0px; height: 0px; right: 2px;">
							<div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
						</div>
					</ul> 
				</li> 
				<li class="external"> 
					<a href="#"> <span>View all notifications</span> <i class="fa-link-ext"></i> </a> 
				</li>
			</ul> 
		</li>
-->		
	</ul> 
	<ul class="user-info-menu left-links list-inline list-unstyled"> 
		<!--
		<li class="search-form" style="min-height: 76px;"> 
			<form name="userinfo_search_form" method="get" action=""> 
			<input type="text" name="s" class="form-control search-field" placeholder="Type to search..."> 
			<button type="submit" class="btn btn-link"> <i class="fa-search"></i> </button> 
			</form> 
		</li> 
		<li class="dropdown user-profile" style="min-height: 76px;"> 
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			<img src="<?= $this->config->item('imgpath') ?>user-4.png" alt="user-image" class="img-circle img-inline userpic-32" width="28"> <span><?=$this->session->userdata('username')?>  <i class="fa-angle-down"></i> </span> </a>
			<ul class="dropdown-menu user-profile-menu list-unstyled"> 
				<!--
				<li> <a href="#edit-profile"> <i class="fa-edit"></i>New Post</a> </li>
				<li> <a href="#settings"> <i class="fa-wrench"></i>Settings</a> </li>  
				<li> <a href="#profile"> <i class="fa-user"></i>Profile</a> </li> 
				<li> <a href="#help"> <i class="fa-info"></i>Help</a> </li> 
				<li class="last"> <a href="<?=base_url()?>login/logout"> <i class="fa-lock"></i>Logout</a> </li> 
			</ul> 
		</li> 
		-->
	</ul> 
</nav>
		<?= $this->load->view($content); ?>
      
   <!-- Main Footer --> 
<footer class="main-footer sticky footer-type-1" style=""> 
<div class="footer-inner"> 
<div class="footer-text">
&copy; 2015
<strong><?= $this->config->item('apl_title') ?> - <?= $this->config->item('apl_name') ?>
</div> 
<div class="go-up"> 
<a href="#" rel="go-top"> <i class="fa-angle-up"></i> </a> 
</div> 
</div> 
</footer>
</div> 

</div> 
<div class="page-loading-overlay loaded"> <div class="loader-2"></div> </div> 
<script src="<?= $this->config->item('jspath') ?>novtmp/TweenMax.min.js" id="script-resource-2"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/resizeable.js" id="script-resource-3"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/joinable.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/xenon-api.js" id="script-resource-5"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/xenon-toggles.js" id="script-resource-6"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/xenon-widgets.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/globalize.min.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/toastr.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/jquery.validate.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/xenon-custom.js" id="script-resource-11"></script>   
<script src="<?= $this->config->item('jspath') ?>novtmp/bootstrap-datepicker.js" id="script-resource-11"></script>   

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="buatnomorLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:70% !important;">
			<div class="modal-content" id="result">
			
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<div class="page-loading-overlay">
		<div class="loader-2"></div>
	</div>
</body>
</html>

<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/dataTables.bootstrap.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.yadcf.js" id="script-resource-9"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/dataTables.tableTools.min.js" id="script-resource-10"></script> 