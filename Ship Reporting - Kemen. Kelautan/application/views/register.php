<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/fileinput.css" id="style-resource-1"> 
<script src="<?= $this->config->item('jspath') ?>novtmp/fileinput.js" id="script-resource-7"></script>
<script src="<?= $this->config->item('jspath') ?>novtmp/bootstrap-datepicker.js" id="script-resource-9"></script>
<script type="text/javascript">
jQuery(document).ready(function($)
{
	$("#major").select2();
	$("#country").select2();
	$("#country2").select2();
	$("#country3").select2();
	$('#photofile').fileinput({
        uploadUrl: 'upload/photo',
        maxFileSize: 500,
        maxFileCount: 1,
        allowedFileExtensions : ['jpg', 'png','gif'],
    });
	$('#cvfile').fileinput({
        uploadUrl: 'upload/cv',
        maxFileSize: 500,
        maxFileCount: 1,
        allowedFileExtensions : ['pdf','doc','docx'],
    });
});
	function register(){
		$("#frmregister").validate();
		var isi=$( "#frmregister" ).serialize();
		$('#loading').modal({ keyboard: false,backdrop: 'static' });
		$.post( "save_register", isi)
		.done(function( data,r ) {
			var rtn=JSON.parse(data);
			//console.log(rtn.status);
			if(rtn.status==0){
				$('#loading').modal('hide');
				gopersonal();
				toastr.info('Register Success. Automatic sending authentification email to you email account.');
				document.cookie="appid="+rtn.appid;
			}else if(rtn.status===1){
				$('#loading').modal('hide');
				gopersonal();
				toastr.info(rtn.errorM);
				document.cookie="appid="+rtn.appid;
			}else if(rtn.status===2){
				$('#loading').modal('hide');
				toastr.info(rtn.errorM);
			}else{
				$('#loading').modal('hide');
				toastr.info('Register Failed.');
			}
		});
	}
	function personal(){
		var appid=getCookie('appid');
		//alert(appid);
		$("#frmpersonal").validate();
		var isi=$( "#frmpersonal" ).serialize();
		$('#loading').modal({ keyboard: false,backdrop: 'static' });
		$.post( "save_personal/", isi)
		.done(function( data ) {
			var rtn=JSON.parse(data);
			if(rtn.status==0){
				$('#loading').modal('hide');
				goeducation();
			}else if(rtn.status===1){
				$('#loading').modal('hide');
				toastr.info('Register Failed.');
			}else{
				$('#loading').modal('hide');
				goeducation();
			}
		});			
	}
	function education(){
		var appid=getCookie('appid');
		//alert(appid);
		
		$("#frmeducation").validate();
		var isi=$( "#frmeducation" ).serialize();
		$('#loading').modal({ keyboard: false,backdrop: 'static' });
		$.post( "save_education/", isi)
		.done(function( data ) {
			var rtn=JSON.parse(data);
			if(rtn.status==0){
				$('#loading').modal('hide');
				goexperience();
			}else if(rtn.status===1){
				$('#loading').modal('hide');
				toastr.info('Register Failed.');
			}else{
				$('#loading').modal('hide');
				goexperience();
			}
		});					
	}
	
	function experience(){
		var appid=getCookie('appid');
		//alert(appid);
		
		$("#frmexperience").validate();
		var isi=$( "#frmexperience" ).serialize();
		$('#loading').modal({ keyboard: false,backdrop: 'static' });
		$.post( "save_experience/", isi)
		.done(function( data ) {
			var rtn=JSON.parse(data);
			if(rtn.status==0){
				$('#loading').modal('hide');
				goupload();
			}else if(rtn.status===1){
				$('#loading').modal('hide');
				toastr.info('Register Failed.');
			}else{
				$('#loading').modal('hide');
				goupload();
			}
		});					
	}
	function gopersonal(){
		$( "#tmblregister" ).removeClass( "active" );
		$( "#fwv-1" ).removeClass( "active" );
		$( "#tmblregister" ).addClass( "completed" );
		$( "#tmblpersonal" ).addClass( "active" );
		$( "#fwv-2" ).addClass( "active" );
		$( "#tmbleducation" ).addClass( "ms-hover" );
		$( ".progress-indicator" ).css( "width", "20%" );
	}
	
	function goeducation(){
		$( "#tmblpersonal" ).removeClass( "active" );
		$( "#fwv-2" ).removeClass( "active" );
		$( "#tmblpersonal" ).addClass( "completed" );
		$( "#tmbleducation" ).addClass( "active" );
		$( "#fwv-3" ).addClass( "active" );
		$( "#tmblexperience" ).addClass( "ms-hover" );
		$( ".progress-indicator" ).css( "width", "40%" );
	}
	function goexperience(){
		$( "#tmbleducation" ).removeClass( "active" );
		$( "#fwv-3" ).removeClass( "active" );
		$( "#tmbleducation" ).addClass( "completed" );
		$( "#tmblexperience" ).addClass( "active" );
		$( "#fwv-4" ).addClass( "active" );
		$( "#tmblupload" ).addClass( "ms-hover" );
		$( ".progress-indicator" ).css( "width", "60%" );
	}
	function goupload(){
		$( "#tmblexperience" ).removeClass( "active" );
		$( "#fwv-4" ).removeClass( "active" );
		$( "#tmblexperience" ).addClass( "completed" );
		$( "#tmblupload" ).addClass( "active" );
		$( "#fwv-5" ).addClass( "active" );
		$( ".progress-indicator" ).css( "width", "80%" );
	}
	function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
</script>
<div class="page-title"> 
	<div class="title-env"> 
	<h1 class="title"><?= $this->config->item('apl_title') ?></h1> 
	<p class="description"></p> 
	</div> 
</div> 
<div class="panel panel-inverted"> 
	<div class="panel-body"> 
		<div class="col-md-12"> 
			<div class="panel panel-border panel-shadow">
				<div class="panel-heading"> <h3 class="panel-title">Register</h3></div> 
				<div class="panel-body"> 
						<form role="forl" class="form-wizard validate" novalidate="novalidate"> 
						<ul class="tabs"> 
							<li class="active ms-hover" id="tmblregister"> <a href="#fwv-1">Register </a> </li> 
							<li class="ms-hover" id="tmblpersonal"> <a href="#fwv-2">Personal Info </a> </li> 
							<li id="tmbleducation"> <a href="#fwv-3">Education </a> </li> 
							<li id="tmblexperience"> <a href="#fwv-4">Experience </a> </li> 
							<li id="tmblupload"> <a href="#fwv-5">Upload </a> </li> 
						</ul> 
						<div class="progress-indicator" style="width: 0%;"> <span></span> </div> 
						</form> 
						<div class="tab-content no-margin" style="margin-top:25px !important;"> 
							<div class="tab-pane with-bg active" id="fwv-1"> 
								<form role="forl" id="frmregister" class="form-wizard validate" novalidate="novalidate"> 
								<div class="row"> 
									<div class="col-md-12"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Email</label> 
											<input class="form-control" name="email" id="email" data-validate="required,email" placeholder="Your email"> 
										</div> 
									</div> 
								</div> 
								<div class="row"> 
									<div class="col-md-12"> 
										<div class="form-group"> 
											<div class="privacyStatement" style="overflow:auto;">
											<?=$privacy?>
											</div>
										</div> 
									</div> 
								</div> 
								<div class="row"> 
									<div class="col-md-12"> 
										<div class="form-group"> 
											<div class="checkbox" style="padding-left:20px;">
												<input type="checkbox" id="check" name="agree" value="1"><strong> I have read and accepted Career's Privacy Statement and Terms and Conditions. </strong>
											</div> 
										</div> 
									</div> 
								</div>
								<div class="row"> 
									<div class="col-md-11"> 
									</div> 
									<div class="col-md-1"> 
										<div class="btn-group"> 
											<button type="button" class="btn btn-success btn-small" onclick="register()">Next</button> 
										</div>
									</div> 
								</div> 
							</form>
							</div> 
							<div class="tab-pane with-bg" id="fwv-2">
								<form role="forl" id="frmpersonal" class="form-wizard validate" novalidate="novalidate"> 
								<div class="row"> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">First Name</label> 
											<input class="form-control" name="first" id="first" > 
										</div> 
									</div> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Middle Name</label> 
											<input class="form-control" name="middle" id="middle"> 
										</div> 
									</div> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Last Name</label> 
											<input class="form-control" name="last" id="last"> 
										</div> 
									</div> 
								</div> 
								<div class="row"> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Place Of Birth</label> 
											<input class="form-control" name="pob" id="pob" > 
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Date Of Birth</label> 
											<input class="form-control datepicker" name="dob" id="dob"> 
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Marital Status</label> 
											<select name="marital" class="form-control" >
												<option value="0" selected="selected">Single</option>
												<option value="1" selected="selected">Married</option>
												<option value="2" selected="selected">Widow</option>
												<option value="3" selected="selected">Widower</option>
											</select>
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Gender</label> 											
											<select name="gender" class="form-control" >
												<option value="1" selected="selected">Male</option>
												<option value="0" selected="selected">Female</option>
											</select>
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Religion</label> 
											<select name="religion" class="form-control">
											<?php
												foreach($religion as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->RELIGION_ID?>" <?=$selected?>><?=kata($rowkat->RELIGION_NAME_EN)?></option>
											<?php
												}
											?>
											</select>
										</div> 
									</div> 
								</div> 
								<div class="row"> 
									<div class="col-md-12"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Address</label> 
											<textarea class="form-control" name="address"></textarea>
										</div> 
									</div> 
								</div> 
								<div class="row"> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Zipcode</label> 
											<input class="form-control" name="zipcode" id="zipcode" > 
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">City</label> 
											<input class="form-control" name="city" id="city"> 
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Country</label> 
											<select name="country" id="country" class="form-control">
											<?php
												foreach($country as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->COUNTRY_ID?>" <?=$selected?>><?=kata($rowkat->COUNTRY_NAME)?></option>
											<?php
												}
											?>
											</select>
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Home Phone</label>
											<input class="form-control" name="phone" id="phone" > 
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Mobile Phone</label> 
											<input class="form-control" name="mobile" id="mobile"> 
										</div> 
									</div>  
								</div> 
								<div class="row"> 
									<div class="col-md-11"> 
									</div> 
									<div class="col-md-1"> 
										<button type="button" class="btn btn-success btn-small" onclick="personal()">Next</button> 
									</div> 
								</div> 
								</form>
							</div> 
							<div class="tab-pane with-bg" id="fwv-3">
								<form role="forl" id="frmeducation" class="form-wizard validate" novalidate="novalidate"> 
								<div class="row"> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Latest Education Level</label> 
											<select name="edulev" class="form-control">
											<?php
												foreach($edulev as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->EDUTYPE_ID?>" <?=$selected?>><?=kata($rowkat->EDUTYPE_NAME_EN)?></option>
											<?php
												}
											?>
											</select> 
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Education Status</label> 											
											<select name="edustat" class="form-control" >
												<option value="0" selected="selected">Completed</option>
												<option value="1" selected="selected">On Going</option>
												<option value="2" selected="selected">Incomplete</option>
											</select>
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Time Period</label> 
											<input class="form-control datepicker" name="startdate" id="startdate"> 
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">To</label> 
											<input class="form-control datepicker" name="enddate" id="enddate"> 
										</div> 
									</div> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Institution Name</label> 
											<input class="form-control" name="institution" id="institution"> 
										</div> 
									</div> 
								</div> 
								<div class="row">  
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">City</label> 
											<input class="form-control" name="city" id="city"> 
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Country</label> 
											<select name="country" id="country2"  class="form-control">
											<?php
												foreach($country as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->COUNTRY_ID?>" <?=$selected?>><?=kata($rowkat->COUNTRY_NAME)?></option>
											<?php
												}
											?>
											</select>
										</div> 
									</div> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Major</label> 
											<select name="major" id="major" class="form-control">
											<?php
												foreach($major as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->MAJOR_ID?>" <?=$selected?>><?=kata($rowkat->MAJOR_NAME_EN)?></option>
											<?php
												}
											?>
											</select>
										</div> 
									</div> 
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">GPA</label> 
											<input class="form-control" name="gpa" id="gpa" > 
										</div> 
									</div>
								</div> 
								<div class="row"> 
									<div class="col-md-11"> 
									</div> 
									<div class="col-md-1"> 
										<button type="button" class="btn btn-success btn-small" onclick="education()">Next</button> 
									</div> 
								</div> 
								</form>
							</div>  
							<div class="tab-pane with-bg" id="fwv-4">
								<form role="forl" id="frmexperience" class="form-wizard validate" novalidate="novalidate"> 
								<div class="row"> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Company Name</label> 
											<input class="form-control" name="company" id="company"> 
										</div> 
									</div> 
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Industry</label> 
											<select name="industry" id="industry" class="form-control">
											<?php
												foreach($industry as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->id_industry?>" <?=$selected?>><?=kata($rowkat->industry)?></option>
											<?php
												}
											?>
											</select> 
										</div> 
									</div>
									<div class="col-md-4"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Position</label> 
											<input class="form-control" name="position" id="position"> 
										</div> 
									</div> 
									
								</div> 
								<div class="row">  
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Time Period</label> 											
											<div class="checkbox" style="padding-left:20px;">
												<input type="checkbox" id="check" name="agree" value="1"><strong> I currently work here </strong>
											</div> 
										</div> 
									</div>
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Start Date</label> 
											<input class="form-control datepicker" name="startdate" id="startdateexp"> 
										</div> 
									</div>
									<div class="col-md-2"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">End Date</label> 
											<input class="form-control datepicker" name="enddate" id="enddateexp"> 
										</div> 
									</div>
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">City</label> 
											<input class="form-control" name="city" id="city"> 
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Country</label> 
											<select name="country"  id="country3" class="form-control">
											<?php
												foreach($country as $rowkat){
													$selected='';
											?>
												<option value="<?=$rowkat->COUNTRY_ID?>" <?=$selected?>><?=kata($rowkat->COUNTRY_NAME)?></option>
											<?php
												}
											?>
											</select>
										</div> 
									</div> 
								</div> 
								<div class="row">  
									<div class="col-md-12"> 
										<div class="form-group"> 
											<label class="control-label" for="full_name">Description</label> 
											<textarea name="description" class="form-control"></textarea>
										</div> 
									</div>
								</div> 
								<div class="row"> 
									<div class="col-md-11"> 
									</div> 
									<div class="col-md-1"> 
										<button type="button" class="btn btn-success btn-small" onclick="experience()">Next</button> 
									</div> 
								</div> 
								</form>
							</div>  
							<div class="tab-pane with-bg" id="fwv-5">
								<div class="row">  
									<div class="col-md-3"> 
										<div class="form-group">
											<label class="control-label" for="full_name">Photo</label>
											<form enctype="multipart/form-data">
												<input id="photofile" name="photofile" class="file" type="file">
											</form>
										</div> 
									</div> 
									<div class="col-md-3"> 
										<div class="form-group">
											<label class="control-label" for="full_name">CV</label>
											<form enctype="multipart/form-data">
												<input id="cvfile" name="cv_file" class="file" type="file" multiple data-min-file-count="1">
											</form>
										</div> 
									</div>
								</div> 
								<div class="row"> 
									<div class="col-md-1"> 
										<a href="<?=base_url()?>applicant/register_success"type="button" class="btn btn-success btn-small"  onclick="return confirm('Thank You For Registering. Email activation will automaticly send to your email account.');" >finish</a> 
									</div> 
								</div> 
							</div> 
							<!--
							<ul class="pager wizard"> 
								<li class="previous disabled"><a href="#"><i class="entypo-left-open"></i> Previous</a> </li> 
								<li class="next"> <a href="#">Next <i class="entypo-right-open"></i></a> </li> 
							</ul> 
							-->
						</div> 
				</div> 
			</div> 
		</div>
	</div> 
</div>