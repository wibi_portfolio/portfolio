
<script type="text/javascript">
jQuery(document).ready(function($)
{
	var notif="<?=@$notification?>";
	$("#wl").select2();
	$("#jf").select2();
	if(notif!='')
	toastr.info(notif);
});
</script>
<div class="page-title"> 
	<div class="title-env"> 
	<h1 class="title"><?= $this->config->item('apl_title') ?></h1> 
	<p class="description"></p> 
	</div> 
</div> 
<div class="row"> 
	<div class="col-md-10"> 
		<ul class="nav nav-tabs nav-tabs-justified"> 
			<li class="active"> <a href="#resume-1" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa-user"></i></span> <span class="hidden-xs">Personal Information</span> </a> </li> 
			<li class=""> <a href="#resume-2" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa-university"></i></span> <span class="hidden-xs">Education Information</span> </a> </li> 
			<li class=""> <a href="#resume-3" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa-university"></i></span> <span class="hidden-xs">Experience Information</span> </a> </li> 
			<!--
			<li class=""> <a href="#resume-4" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a> </li> 
			<li class=""> <a href="#resume-5" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa-bell-o"></i></span> <span class="hidden-xs">Inbox</span> </a> </li> 
			-->
		</ul>
		<div class="tab-content"> 
			<div class="tab-pane active" id="resume-1"> 
				<form role="forl" id="frmpersonal" class="form-wizard validate" novalidate="novalidate"> 
					<div class="row"> 
						<div class="col-md-4"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">First Name</label> 
								<input class="form-control" name="first" id="first" value="<?=@$personal->FIRST_NAME?>" > 
							</div> 
						</div> 
						<div class="col-md-4"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Middle Name</label> 
								<input class="form-control" name="middle" id="middle" value="<?=@$personal->MIDDLE_NAME?>" > 
							</div> 
						</div> 
						<div class="col-md-4"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Last Name</label> 
								<input class="form-control" name="last" id="last" value="<?=@$personal->LAST_NAME?>" > 
							</div> 
						</div> 
					</div> 
					<div class="row"> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Place Of Birth</label> 
								<input class="form-control" name="pob" id="pob"  value="<?=@$personal->POB?>" > 
							</div> 
						</div> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Date Of Birth</label> 
								<input class="form-control datepicker" name="dob" id="dob" value="<?=tgltampil(@$personal->DOB)?>" > 
							</div> 
						</div> 
						<div class="col-md-3"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Marital Status</label> 
								<?php
								if(@$personal->MARITAL==0)
								$selecmar1='selected="selected"';
								if(@$personal->MARITAL==1)
								$selecmar2='selected="selected"';
								if(@$personal->MARITAL==2)
								$selecmar3='selected="selected"';
								if(@$personal->MARITAL==3)
								$selecmar4='selected="selected"';
								?>
								<select name="marital" class="form-control" >
									<option value="0" <?=@$selectmar1?>>Single</option>
									<option value="1" <?=@$selectmar2?>>Married</option>
									<option value="2" <?=@$selectmar3?>>Widow</option>
									<option value="3" <?=@$selectmar4?>>Widower</option>
								</select>
							</div> 
						</div> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Gender</label> 
								<?php
								if(@$personal->GENDER==1)
								$selgen1='selected="selected"';
								if(@$personal->MARITAL==0)
								$selgen2='selected="selected"';
								?>											
								<select name="gender" class="form-control" >
									<option value="1" <?=@$selgen1?>>Male</option>
									<option value="0" <?=@$selgen2?>>Female</option>
								</select>
							</div> 
						</div> 
						<div class="col-md-3"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Religion</label> 
								<select name="religion" class="form-control">
								<?php
									foreach($religion as $rowkat){
										$selected='';
										if(@$personal->RELIGION==$rowkat->RELIGION_ID)
										$selected='selected="selected"';
								?>
									<option value="<?=$rowkat->RELIGION_ID?>" <?=@$selected?>><?=kata($rowkat->RELIGION_NAME_EN)?></option>
								<?php
									}
								?>
								</select>
							</div> 
						</div> 
					</div> 
					<div class="row"> 
						<div class="col-md-12"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Address</label> 
								<textarea class="form-control" name="address"><?=@$address->ADDRESS?></textarea>
							</div> 
						</div> 
					</div> 
					<div class="row"> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Zipcode</label> 
								<input class="form-control" name="zipcode" id="zipcode" VALUE="<?=@$address->ZIPCODE?>"> 
							</div> 
						</div> 
						<div class="col-md-3"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">City</label> 
								<input class="form-control" name="city" id="city" VALUE="<?=@$address->CITY?>"> 
							</div> 
						</div> 
						<div class="col-md-3"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Country</label> 
								<select name="country" id="country" class="form-control">
								<?php
									foreach($country as $rowkat){
										$selected='';
										if(@$address->COUNTRY==$rowkat->COUNTRY_ID)
										$selected='selected="selected"';
								?>
									<option value="<?=$rowkat->COUNTRY_ID?>" <?=$selected?>><?=kata($rowkat->COUNTRY_NAME)?></option>
								<?php
									}
								?>
								</select>
							</div> 
						</div> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Home Phone</label>
								<input class="form-control" name="phone" id="phone"  VALUE="<?=@$address->PHONE?>"> 
							</div> 
						</div> 
						<div class="col-md-2"> 
							<div class="form-group"> 
								<label class="control-label" for="full_name">Mobile Phone</label> 
								<input class="form-control" name="mobile" id="mobile" VALUE="<?=@$personal->MOBILE?>"> 
							</div> 
						</div>  
					</div> 
					<div class="row"> 
						<div class="col-md-11"> 
						</div> 
						<div class="col-md-1"> 
							<button type="button" class="btn btn-success btn-small" onclick="personalupdate()">Update</button> 
						</div> 
					</div> 
					</form>
			</div> 
			<div class="tab-pane active" id="resume-2"> 
			</div> 
			<div class="tab-pane active" id="resume-3"> 
			</div> 
			<!--
			<div class="tab-pane active" id="resume-4"> 
			</div> 
			<div class="tab-pane active" id="resume-5"> 
			</div> 
			-->
		</div> 
	</div>		
	<div class="col-md-2"> 
		<div class="one_fourth last text-align-left">
			<div class=" box " style="padding:10px 15px;background-color:#333;color:#fff;;margin:0px">
				<div class="boxcontent">
					<h6>Other Details</h6>
					<div class="divider shortcode-divider thin"></div>
					<div class="spacer" style="height:10px;"></div>
					<p><b>Work location</b><br><?=kata(@$vacan->city_name)?></p>
					<p><b>Years of experience</b><br><?=@$vacan->YEARSEXPERIANCE?> year(s)</p>
					<p><b>Status</b><br><?=kata(@$vacan->EMPSTS)?></p>
					<p><b>Closing date</b><br><?=tgltampil(@$vacan->END_DATE)?></p>
					<p><b>Available Position</b><br><?=@$vacan->REQUEST_TOTSTAFF?> position(s)</p>
					<br><p><b>
					<a style="padding:5px 10px;background:#00994a;margin-top:10px;color:#fff;" href="<?=base_url()?>applicant/apply/" onclick="return confirm('Are you sure you want to apply for this job ?');" target="_blank">Apply Now</a></b></p><b>
					</b>
				</div>
			</div>
		</div>		
		<div class="clearboth"></div>
	</div>
</div>