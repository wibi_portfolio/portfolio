<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_kota()
    {
		$this->db->order_by('kota','asc');
        $qr = $this->db->get('kps_kota')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkroles[]\" value=\"'.$row->id_kota.'\" />",
                "'.kata($row->kota).'",
                "'.$row->dms_lat.', '.$row->dms_lng.'",
                "<a href=\"'.site_url('kota/edit/' . $row->id_kota).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a onclick=\"del('.$row->id_kota.')\" id=\"del_role\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;</a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>