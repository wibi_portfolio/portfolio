<?php
class kota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'kota List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'kota';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_kota';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('id_kota');
        $roleName = $this->input->post('kota');
        $username = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'kota'=>$roleName
                            );
                if($this->crud->insert_trans('kps_kota',$values))
                {
                    redirect(site_url('kota/lists')); 
                }else{
                    $data['content'] = 'forms/modif_kota';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('id_kota'=>$roleId);
                $set = array(
                            'kota'=>$roleName
                        );
                if($this->crud->edit_trans('kps_kota',$set,$where))
                {
                    redirect(site_url('kota/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->crud->msrwhere('kps_kota',array('id_kota'=>$roleId),'id_kota','asc')->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_kota';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id_kota'=>$roleId);
        $this->crud->delete_trans('kps_kota',$where);
        redirect(site_url('kota/lists'));
    }
    
    public function delete_stack()
    {
        $kota = $this->input->post('kota');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$kota);
    }
}
?>