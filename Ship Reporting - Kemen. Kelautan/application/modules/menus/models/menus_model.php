<?php
class menus_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getMenuInfo($modulId)
    {
        $this->db->where(array('modul_id'=>$modulId));
        $qr = $this->db->get('admin_menu');
        return $qr;
    }
}
?>