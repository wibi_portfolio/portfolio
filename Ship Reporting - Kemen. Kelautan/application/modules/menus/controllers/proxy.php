<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json()
    {
        $this->db->order_by('sort','ASC');
        $this->db->where('level',0);
        $qr = $this->db->get('admin_menu')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
			$mn = $this->crud->msrwhere('admin_menu',array('parent'=>$row->parent),'sort','asc')->row();
			if($row->sort==0)
			$sort='<a href=\"'.site_url('menus/sort_/down/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-down\"></i>&nbsp;&nbsp;</a>';
            else
			$sort='<a href=\"'.site_url('menus/sort_/up/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-up\"></i>&nbsp;&nbsp;</a><a href=\"'.site_url('menus/sort_/down/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-down\"></i>&nbsp;&nbsp;</a>';
            echo '
            [
                "<input type=\"checkbox\" name=\"chkmodule[]\" value=\"'.$row->modul_id.'\" />",
                "'.$row->label.'",
                "'.@$mn->label.'",
                "'.$row->level.'",
				"'.$sort.'",
                "<a href=\"'.site_url('menus/edit/' . $row->modul_id).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a  onclick=\"del('.$row->modul_id.')\" id=\"del_modul\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;"
            ]';
			$cekmn = $this->crud->msrwhere('admin_menu',array('parent'=>$row->modul_id),'sort','asc')->num_rows();
            if($i<$total or $cekmn>0)
            {
                echo ',';
            }
			if($cekmn)
			$this->sub_json($row->modul_id,$i,$total);
            $i++;
        }
        
        echo "]}";
    }
	
	public function sub_json($id,$i2,$total2){
		$this->db->order_by('sort','ASC');
        $this->db->where('level',1);
        $this->db->where('parent',$id);
        $qr = $this->db->get('admin_menu')->result();
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
			$mn = $this->crud->msrwhere('admin_menu',array('parent'=>$row->parent),'sort','asc')->row();
			if($row->sort==0)
			$sort='<a href=\"'.site_url('menus/sort_/down/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-down\"></i>&nbsp;&nbsp;</a>';
            else
			$sort='<a href=\"'.site_url('menus/sort_/up/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-up\"></i>&nbsp;&nbsp;</a><a href=\"'.site_url('menus/sort_/down/' . $row->modul_id.'/'.$row->sort).'\"><i class=\"fa-arrow-down\"></i>&nbsp;&nbsp;</a>';
             echo '
            [
                "<input type=\"checkbox\" name=\"chkmodule[]\" value=\"'.$row->modul_id.'\" />",
                "---'.$row->label.'",
                "'.@$mn->label.'",
                "'.$row->level.'",
				"'.$sort.'",
                "<a href=\"'.site_url('menus/edit/' . $row->modul_id).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;<a  onclick=\"del('.$row->modul_id.')\" id=\"del_modul\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;"
            ]';
            if($i<$total or $i2<$total2)
            {
                echo ',';
            }
            $i++;
        }
	}
}
?>