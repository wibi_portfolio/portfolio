<?php
class menus extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('menus_model');
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'menus';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'forms/modif_menus';
        $data['formType'] = 'add';
        $data['title'] = 'Add new menu';
        $data['modulnya'] = $this->crud->msr('master_modul','modul_name','asc')->result();
        $this->load->view('template',$data);
    }
    
    public function edit($modulId=null,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(array(3,4));
        $data['modul'] = $this->menus_model->getMenuInfo($modulId)
                                              ->row();
	    $par=$data['modul']->parent;
		if($par!==0){
			$data['parent']=$this->crud->msrwhere('admin_menu',array('modul_id'=>$par),'label','asc')->row();
		}
        $data['message'] = $err;
        $data['modulnya'] = $this->crud->msr('master_modul','modul_name','asc')->result();
        $data['content'] = 'forms/modif_menus';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit menu';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');
        $modulId = $this->input->post('modul_id');
        $label = $this->input->post('label');
        $level = $this->input->post('level');
        $parent = $this->input->post('parent');
        $user = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
				$sort=$this->crud->msrwhere('admin_menu',array('level'=>$level,'parent'=>$parent),'label','asc')->num_rows();
				if($level==0)
				$sort=$this->crud->msrwhere('admin_menu',array('level'=>$level),'label','asc')->num_rows();
                $values = array(
                            'modul_id'=>$modulId,
                            'label'=>$label,
                            'parent'=>$parent,
                            'sort'=>$sort++,
                            'level'=>$level                       
                          );
                if($this->crud->insert_trans('admin_menu',$values))
                {
                    redirect(complete_url('menus/lists')); 
                }else{
                    $data['content'] = 'forms/modif_menus';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('modul_id'=>$modulId);
                $set = array(
                            'modul_id'=>$modulId,
                            'label'=>$label,
                            'parent'=>$parent,
                            'level'=>$level                       
                          );
                if($this->crud->edit_trans('admin_menu',$set,$where))
                {
                    redirect(site_url('menus/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($modulId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function sort_()
    {
        //$this->commonlib->checkPagesPrivileges(3);
		$type=$this->uri->segment(3);
		$modid=$this->uri->segment(4);
		$sort=$this->uri->segment(5);
		if($type=='up')
		$sort2=$sort-1;
		else
		$sort2=$sort+1;
		$mn=$this->crud->msrwhere('admin_menu',array('modul_id'=>$modid),'label','asc')->row();
		$mn2=$this->crud->msrwhere('admin_menu',array('level'=>$mn->level,'parent'=>$mn->parent,'sort'=>$sort2),'label','asc')->row();
		$where = array('modul_id'=>$modid);
		$set = array(
					'sort'=>$sort2                  
				  );
        $this->crud->edit_trans('admin_menu',$set,$where);
		//print_r( $mn2);
		//exit;
		$where2 = array('modul_id'=>$mn2->modul_id);
		$set2 = array(
					'sort'=>$sort                  
				  );
        $this->crud->edit_trans('admin_menu',$set2,$where2);
        redirect(site_url('menus/lists'));
    }
    
    public function delete($id)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('modul_id'=>$id);
        $this->crud->delete_trans('admin_menu',$where);
        redirect(site_url('menus/lists'));
    }
    
    public function delete_stack()
    {
        $modules = $this->input->post('modules');
        $field = 'modul_id';
        echo $this->crud->delete_in('admin_menu',$field,$modules);
    }
	
	
	function lvl()
	{
		$id=$this->uri->segment(3);
		$modid=$this->uri->segment(4);
		if(($id-1)>0)
		$isi=$this->crud->msrwhere('admin_menu',array('level'=>($id-1),'parent'=>$modid),'sort','asc')->result();
		else
		$isi=$this->crud->msrwhere('admin_menu',array('level'=>($id-1)),'sort','asc')->result();
		
		if(count($isi>0)){
		$res='';
			foreach($isi as $isi2){
				$res.="<option value='".$isi2->modul_id."'>".$isi2->label."</option>";
			}
		}else{
			$res['error']='Data tidak ditemukan';	
		}
		$res.='';
		die($res);
	}
}
?>