<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 
<div class="panel panel-default"> 
	<div class="panel-heading"> 
	</div> 
	<div class="panel-body"> 
	<form role="form" class="form-horizontal" action="<?=base_url();?>menus/save" method="post"> 
		<?= isset($message)?show_errors($message):'' ?>
		<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
				<input type="hidden" name="modul_id" id="modul_id" value="<?= isset($modul->parent)?$modul->parent:'' ?>" />
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Menu name</label> 
			<div class="col-sm-10"> 
				<input type="text" class="form-control" name="label" id="label" placeholder="Menu name" <?= isset($modul->label)?'value="'.$modul->label.'"':'' ?>> 
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Module</label> 
			<div class="col-sm-10"> 
				<input type="radio" name="modul_id" id="modul_id" value="0" <?= @$modul->modul_id==0?'checked':'' ?>/> None<br>
				<?php
					foreach($modulnya as $modval){
				?>
						<input type="radio" name="modul_id" id="modul_id" value="<?=$modval->modul_id?>" <?= @$modul->modul_id==$modval->modul_id?'checked':'' ?>/> <?=$modval->modul_name?><br>
				<?php
					}
				?>
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Level</label> 
			<div class="col-sm-10"> 
				<select name="level" id="level" onchange="parentnya()" class="form-control" >
					<option value="-">Choose Level</option>
					<option value="0" <?= @$modul->level==0?'selected':'' ?>>0</option>
					<option value="1" <?= @$modul->level==1?'selected':'' ?>>1</option>
					<option value="2" <?= @$modul->level==2?'selected':'' ?>>2</option>
					<option value="3" <?= @$modul->level==3?'selected':'' ?>>3</option>
				</select>	
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Level</label> 
			<div class="col-sm-10"> 
				<select name="parent" id="parent" class="form-control">
					<option value="-">Choose Parent</option>
				</select>
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<button type="submit" class="btn btn-success">Save</button> 
			<button type="reset" class="btn btn-white">Reset</button>
		</div> 
	</form>
	</div> 
</div>
<script>
			$( document ).ready(function() {
				parentnya();
			});
				function parentnya(){	
					$("#parent").empty();
					var level = $("#level").val(); 
					var modul_id = $("#modul_id").val(); 
					if(level>0){
						$.ajax({
							type:"GET",
							url:"<?= base_url() ?>menus/lvl/" + level +"/"+modul_id,
							dataType:"html",
							success:function(html){
								$("#parent").append(html.valueOf());
														  
							},
							error:function(){                                
								//jAlert("FAIL");
							}
					   }); 
				   }
				} 
			</script>