<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<?=breadcrumb($this->uri->uri_string)?>
			</ol>
			<div class="mws-panel grid_8">
				<div class="mws-panel-header">
				<span class="mws-i-24 i-list">Edit rules</span>
			</div>
			<div class="mws-panel-body">
				<?= isset($message)?show_errors($message):'' ?>
				<?= form_open(site_url('user_modul/save_rules'),array('class'=>'mws-form')) ?>
				<input type="hidden" name="modul_id" value="<?= isset($role->modul_id)?$role->modul_id:'' ?>" />
				<input type="hidden" name="role_id" value="<?= isset($role->modul_id)?$role->role_id:'' ?>" />
				<div class="mws-form-inline">
					<div class="mws-form-row">
						<label>Modul name</label>
						<div class="mws-form-item small">
							<input type="text" class="mws-textinput validate[required]" value="<?= isset($role->modul_name)?$role->modul_name:'' ?>" disabled="disabled" />
						</div>
					</div>
					<div class="mws-form-row">
						<label>Role</label>
						<div class="mws-form-item small">
							<input type="text" class="mws-textinput validate[required]" value="<?= isset($role->role_name)?$role->role_name:'' ?>" disabled="disabled" />
						</div>
					</div>
					<div class="mws-form-row">
						<label>Rules</label>
						<div class="mws-form-item clearfix">
							<ul class="mws-form-list inline">
								<li><input type="checkbox" name="chkinsert" onclick="if($(this).is(':checked')){$(this).val(1)}else{$(this).val(0)}" <?= $role->insert_role=='1'?'value="1" checked':'value="0"' ?>  /> <label>Insert</label></li>
								<li><input type="checkbox" name="chkupdate" onclick="if($(this).is(':checked')){$(this).val(1)}else{$(this).val(0)}" <?= $role->update_role=='1'?'value="1" checked':'value="0"' ?> /> <label>Update</label></li>
								<li><input type="checkbox" name="chkdelete" onclick="if($(this).is(':checked')){$(this).val(1)}else{$(this).val(0)}" <?= $role->delete_role=='1'?'value="1" checked':'value="0"' ?> /> <label>Delete</label></li>
								<li><input type="checkbox" name="chkprint" onclick="if($(this).is(':checked')){$(this).val(1)}else{$(this).val(0)}" <?= $role->print_role=='1'?'value="1" checked':'value="0"' ?> /> <label>Print</label></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="mws-button-row">
					<input type="submit" value="Submit" class="mws-button red">
					<input type="reset" value="Reset" class="mws-button gray">
				</div>
				<?= form_close() ?> 	
			</div>
		</div>
	</div>
</div>