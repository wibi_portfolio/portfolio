<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<?=breadcrumb($this->uri->uri_string)?>
			</ol>
			<div class="mws-panel grid_8">    
				<div class="mws-panel-header">
					<span class="mws-i-24 i-table-1">Assign module</span>
				</div>    
				<div class="mws-panel-body">
					<form class="mws-form">            
						<div class="mws-form-inline">
							<div class="mws-form-row">
								<label>Role</label>
								<div class="mws-form-item small">
									<input type="text" class="mws-textinput validate[required]" value="<?= isset($role->role_name)?$role->role_name:'' ?>" disabled="disabled" />
								</div>
							</div>
						</div>
					</form>
				</div>
					<table class="mws-datatable-fn mws-table">
						<thead>
							<tr>                    
								<th>Modul name</th>
								<th>Site url</th>
								<th>Insert</th>
								<th>Edit</th>
								<th>Delete</th>
								<th>Print</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"> 
function assign(id){
           // e.preventDefault();
            var roleId = <?= $role->role_id ?>;
            var modulId = id;            
            var chkinsert = $('input[name="chkinsert"][data-id="'+modulId+'"]').val();
            var chkupdate = $('input[name="chkedit"][data-id="'+modulId+'"]').val();
            var chkdelete = $('input[name="chkdelete"][data-id="'+modulId+'"]').val();
            var chkprint = $('input[name="chkprint"][data-id="'+modulId+'"]').val();
            var postData =  {
                                'role_id':roleId,
                                'modul_id':modulId,
                                'chkinsert':chkinsert,
                                'chkupdate':chkupdate,
                                'chkdelete':chkdelete,
                                'chkprint':chkprint
                            }
            $.post("<?= site_url('user_modul/assign_save') ?>",postData,function(r){
                if(r=='1')
                {
                    oTable.fnReloadAjax("<?= site_url('user_modul/proxy/json_assign_modul/'.$role->role_id)  ?>") ;
                }
            })
        }
   
        var oTable = $(".mws-datatable-fn").dataTable({
            sPaginationType: "full_numbers",
            "bProcessing": true,
            "sAjaxSource": "<?= site_url('user_modul/proxy/json_assign_modul/'.$role->role_id)  ?>",
            "bSort": false
        });
        
    $(function(){
        $('a#add_modul').on('click',function(e){
            e.preventDefault();
            var roleId = <?= $role->role_id ?>;
            var modulId = $(this).attr('data-id');            
            var chkinsert = $('input[name="chkinsert"][data-id="'+modulId+'"]').val();
            var chkupdate = $('input[name="chkedit"][data-id="'+modulId+'"]').val();
            var chkdelete = $('input[name="chkdelete"][data-id="'+modulId+'"]').val();
            var chkprint = $('input[name="chkprint"][data-id="'+modulId+'"]').val();
            var postData =  {
                                'role_id':roleId,
                                'modul_id':modulId,
                                'chkinsert':chkinsert,
                                'chkupdate':chkupdate,
                                'chkdelete':chkdelete,
                                'chkprint':chkprint
                            }
            $.post("<?= site_url('user_modul/assign_save') ?>",postData,function(r){
                if(r=='1')
                {
                    oTable.fnReloadAjax("<?= site_url('user_modul/proxy/json_assign_modul/'.$role->role_id)  ?>") ;
                }
            })
        })
    })
	$('#add_modul').on('click',function(e){
            e.preventDefault();
            var roleId = <?= $role->role_id ?>;
            var modulId = $(this).attr('data-id');            
            var chkinsert = $('input[name="chkinsert"][data-id="'+modulId+'"]').val();
            var chkupdate = $('input[name="chkedit"][data-id="'+modulId+'"]').val();
            var chkdelete = $('input[name="chkdelete"][data-id="'+modulId+'"]').val();
            var chkprint = $('input[name="chkprint"][data-id="'+modulId+'"]').val();
            var postData =  {
                                'role_id':roleId,
                                'modul_id':modulId,
                                'chkinsert':chkinsert,
                                'chkupdate':chkupdate,
                                'chkdelete':chkdelete,
                                'chkprint':chkprint
                            }
            $.post("<?= site_url('user_modul/assign_save') ?>",postData,function(r){
                if(r=='1')
                {
                    oTable.fnReloadAjax("<?= site_url('user_modul/proxy/json_assign_modul/'.$role->role_id)  ?>") ;
                }
            })
        })
</script>