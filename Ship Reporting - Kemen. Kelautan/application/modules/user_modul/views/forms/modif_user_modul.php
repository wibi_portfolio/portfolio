<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<?=breadcrumb($this->uri->uri_string)?>
			</ol>
			<div class="mws-panel grid_8">
				<div class="mws-panel-header">
				<span class="mws-i-24 i-list"><?= $title ?></span>
			</div>
			<div class="mws-panel-body">
				<?= isset($message)?show_errors($message):'' ?>
				<?= form_open(complete_url('module/save'),array('class'=>'mws-form')) ?>
				<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
				<input type="hidden" name="modul_id" value="<?= isset($modul->modul_id)?$modul->modul_id:'' ?>" />
				<div class="mws-form-inline">
					<div class="mws-form-row">
						<label>Modul name</label>
						<div class="mws-form-item small">
							<input type="text" class="mws-textinput validate[required]" name="modul_name" id="modul_name" value="<?= isset($modul->modul_name)?$modul->modul_name:'' ?>" />
						</div>
					</div>
					<div class="mws-form-row">
						<label>Site url</label>
						<div class="mws-form-item small">
							<span><?= base_url() ?></span><input type="text" class="mws-textinput validate[required]" name="modul_url" id="modul_url" value="<?= isset($modul->modul_url)?$modul->modul_url:'' ?>" />
						</div>
					</div>
				</div>
				<div class="mws-button-row">
					<input type="submit" value="Submit" class="mws-button red">
					<input type="reset" value="Reset" class="mws-button gray">
				</div>
				<?= form_close() ?> 	
			</div>
		</div>
	</div>
</div>