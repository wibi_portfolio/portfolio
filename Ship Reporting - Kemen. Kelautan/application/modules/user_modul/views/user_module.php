<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 
<style>
.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>
<script>

	function disposisi(level,id){	
		//$("#parentsub").remove();
		//var id = $("#parent").val(); 
		//if(level>0){
		$("#disposisinya").empty();
			$.ajax({
				type:"GET",
				url:"<?= base_url() ?>user_modul/chk/"+level+"/"+id,
				dataType:"html",
				success:function(html){
					$("#disposisinya").append(html.valueOf());
				},
				error:function(){                                
					//jAlert("FAIL");
				}
		   }); 
	   //}
	} 
</script>
<form method="post" action="save">
<div class="panel panel-default"> 
	<div class="panel-heading"> 
		<h3 class="panel-title"></h3> 
		<div class="panel-options">
			<label>Pilih Role : </label>
			<select class="form-control" name="role" id="select_role" style="float:left;" onchange="disposisi(this.value,this.value)">
				<?php foreach($roles->result() as $role): ?>
				<option value="<?= $role->role_id ?>"><?= $role->role_name ?></option>
				<?php endforeach; ?>
			</select>
		</div> 
	</div> 
	<div class="panel-body"> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Module</label> 
			<div class="col-sm-10"> 
				<div class="tree well" style="display: inline-block;width:100%;overflow:auto;height:400px;" id="disposisinya">
					<ul>
						<?php
						foreach($so as $row){
							$cek=$this->crud->msrwhere('user_modul',array('modul_id'=>@$row->modul_id,'role_id'=>$this->session->userdata('role_id')),'modul_id','asc')->num_rows();
							$check='';
							if($cek>0)
							$check='checked';
						?>
							<li><span><i class="icon-folder-open"></i> <input type="checkbox" name="disposisi[]" value="<?=$row->modul_id?>" style="margin-top:-2px;vertical-align: middle;" <?=$check?>> <?=$row->label?></span>
							<?=sub_module2($row->modul_id,$this->session->userdata('role_id'))?></li>
						<?php
						}
						?>
					</ul>
				</div> 
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<input type="submit" value="Save" class="btn btn-success">
		</div> 
		<div class="form-group-separator"></div> 
	</div> 
</div>
</form>