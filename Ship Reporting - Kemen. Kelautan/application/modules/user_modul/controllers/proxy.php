<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_modul($roleId=null)
    {        
        $this->db->select('a.modul_id, a.role_id, a.insert_role, a.update_role, a.delete_role, a.print_role, b.modul_name, b.modul_url');
        $this->db->from('user_modul a');
        $this->db->join('master_modul b','a.modul_id=b.modul_id');   
        $this->db->not_like('modul_name', 'Add');
        $this->db->not_like('modul_name', 'Edit');
        $this->db->not_like('modul_name', 'Delete');     
        $this->db->where(array('a.role_id'=>$roleId));
        $this->db->group_by('b.modul_name');
        $this->db->order_by('modul_id','ASC');
        $qr = $this->db->get()->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            $insert = $row->insert_role=='1'?'checked':'';
            $edit = $row->update_role=='1'?'checked':'';
            $delete = $row->delete_role=='1'?'checked':'';
            $print = $row->print_role=='1'?'checked':'';
            echo '
            [
                "<input type=\"checkbox\" name=\"chkmodule[]\" value=\"'.$row->modul_id.'\" />",
                "'.$row->modul_name.'",
                "'.$row->modul_url.'",
                "<input type=\"checkbox\" name=\"chkinsert\" '.$insert.' disabled />",
                "<input type=\"checkbox\" name=\"chkedit\" '.$edit.' disabled />",
                "<input type=\"checkbox\" name=\"chkdelete\" '.$delete.' disabled />",
                "<input type=\"checkbox\" name=\"chkprint\" '.$print.' disabled />",
                "<ul class=\"clearfix\" id=\"list_action\"><a href=\"'.site_url('user_modul/edit_rules/' . $row->modul_id .'/' .$row->role_id ).'\" id=\"edit_rules\" title=\"Edit rules\"><li class=\"mws-ic-16 ic-edit\"></li></a><a href=\"#\" onclick=\"del('.$row->modul_id.')\" data-id=\"'.$row->modul_id.'\" id=\"del_modul\" title=\"Remove module\"><li class=\"mws-ic-16 ic-trash\"></li></a></ul>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
    
    public function json_assign_modul($roleId)
    {
        $this->db->select('a.modul_id, a.modul_name, a.modul_url, b.modul_id as sign');
        $this->db->from('master_modul a');
        $this->db->join('user_modul b','a.modul_id=b.modul_id AND b.role_id='.$roleId,'left');
        $this->db->where('b.modul_id IS NULL');
        $qr = $this->db->get()->result();
        
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [            
                "'.$row->modul_name.'",
                "'.$row->modul_url.'",
                "<input type=\"checkbox\" name=\"chkinsert\" data-id=\"'.$row->modul_id.'\" value=\"0\" onclick=\"if($(this).is(\':checked\')){$(this).val(1)}else{$(this).val(0)}\" />",
                "<input type=\"checkbox\" name=\"chkedit\" data-id=\"'.$row->modul_id.'\" value=\"0\" onclick=\"if($(this).is(\':checked\')){$(this).val(1)}else{$(this).val(0)}\" />",
                "<input type=\"checkbox\" name=\"chkdelete\" data-id=\"'.$row->modul_id.'\" value=\"0\" onclick=\"if($(this).is(\':checked\')){$(this).val(1)}else{$(this).val(0)}\" />",
                "<input type=\"checkbox\" name=\"chkprint\" data-id=\"'.$row->modul_id.'\" value=\"0\" onclick=\"if($(this).is(\':checked\')){$(this).val(1)}else{$(this).val(0)}\" />",
                "<ul class=\"clearfix\" id=\"list_action\"><a href=\"#\" onclick=\"assign('.$row->modul_id.')\" id=\"add_modul\" data-id=\"'.$row->modul_id.'\" title=\"Add modul\"><li class=\"mws-ic-16 ic-add\"></li></a></ul>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>