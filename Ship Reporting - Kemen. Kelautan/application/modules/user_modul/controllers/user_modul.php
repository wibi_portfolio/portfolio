<?php
class user_modul extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_modul_model');
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'user_modul/user_module';
        $data['roles'] = $this->user_modul_model->getRoles();
        $data['so'] = $this->crud->msrwhere('admin_menu',array('parent'=>0),'sort','asc')->result();
        $this->load->view('template',$data);
    }
    
    public function edit($modulId=null,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['modul'] = $this->modul_model->getModulInfo($modulId)
                                              ->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_module';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit modul';
        $this->load->view('template',$data);
    }
    
	/*
    public function save()
    {
        $type = $this->input->post('form_type');
        $modulId = $this->input->post('modul_id');
        $modulName = $this->input->post('modul_name');
        $modulUrl = $this->input->post('modul_url');
        $user = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                            'modul_name'=>$modulName,
                            'modul_url'=>$modulUrl,
                            'create_user'=>$user,
                            'create_date'=>$date                            
                          );
                if($this->crud->insert_trans('master_modul',$values))
                {
                    redirect(complete_url('module/lists')); 
                }else{
                    $data['content'] = 'forms/modif_module';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('modul_id'=>$modulId);
                $set = array(
                            'modul_name'=>$modulName,
                            'modul_url'=>$modulUrl,
                            'modif_user'=>$user,
                            'modif_date'=>$date                            
                        );
                if($this->crud->edit_trans('master_modul',$set,$where))
                {
                    redirect(site_url('module/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($modulId,$msg);
                }
                break;
            default :
                break;
        }
    }
	*/
	
    function save()
	{
		$role_id = $this->input->post('role');
		$module_id = $this->input->post('disposisi');
        $user = $this->session->userdata('username');
		
		$sql="DELETE FROM `blck_user_modul` WHERE `role_id` =".$role_id." ";
		$del_all=$this->db->query($sql);
		for($i=0;$i<count($module_id);$i++){
			$values = array(
                            'modul_id'=>$module_id[$i],
                            'role_id'=>$role_id,
                            'update_role'=>1,
                            'insert_role'=>1,
                            'delete_role'=>1,
                            'print_role'=>1,
                            'create_user'=>$user                           
                          );
             $this->crud->insert_trans('user_modul',$values);
		}
		redirect(complete_url('user_modul/lists')); 
	}
    function chk()
	{
		$level=$this->uri->segment(3);
		$id=$this->uri->segment(4);
		$so=$this->crud->msrwhere('admin_menu',array('parent'=>0),'sort','asc')->result();
		
		echo '<ul>';
		foreach($so as $isi2){
			$cek=$this->crud->msrwhere('user_modul',array('modul_id'=>@$isi2->modul_id,'role_id'=>$id),'modul_id','asc')->num_rows();
			$check='';
			if($cek>0)
			$check='checked';
			echo '<li><span><i class="icon-folder-open"></i> <input type="checkbox" name="disposisi[]" style="margin-top:-2px;vertical-align: middle;" value="'.$isi2->modul_id.'" '.$check.'> '.$isi2->label.'</span>';
			sub_module2($isi2->modul_id,$id);
			echo '</li>';
		}
		echo '</ul>';
		//return($res);
	}
    public function delete($modulId, $roleId)
    {
        $this->commonlib->checkPagesPrivileges(array(3,4));
        $where = array('modul_id'=>$modulId,'role_id'=>$roleId);
        $this->crud->delete_trans('user_modul',$where);
        redirect(site_url('user_modul/lists'));
    }
    
    public function remove_stack()
    {
        $this->commonlib->checkPagesPrivileges();
        $roleId = $this->input->post('role_id');
        $modules = $this->input->post('modules');
        $field = 'modul_id';
        $where = array('role_id'=>$roleId);
        echo $this->crud->delete_in('user_modul',$field,$modules,$where);                
    }
    
    public function edit_rules($modulId, $roleId, $err = "")
    {
        $this->commonlib->checkPagesPrivileges(array(3,4));
        $data['content'] = 'forms/edit_rules';
        $data['role'] = $this->user_modul_model->getRoleModul($modulId,$roleId)
                                               ->row();
        $this->load->view('template',$data);
    }
    
    public function save_rules()
    {
        $roleId = $this->input->post('role_id');
        $modulId = $this->input->post('modul_id');
        $insert = $this->input->post('chkinsert');
        $update = $this->input->post('chkupdate');
        $delete = $this->input->post('chkdelete');
        $print = $this->input->post('chkprint');
        
        $set = array(
                    'insert_role'=>$insert,
                    'update_role'=>$update,
                    'delete_role'=>$delete,
                    'print_role'=>$print
                );
        $where = array(
                    'modul_id'=>$modulId,
                    'role_id'=>$roleId
                );
        
        if($this->crud->edit_trans('user_modul',$set,$where))
        {
            redirect(site_url('user_modul/lists'));
        }else{
            $msg = "Error when editing data, please try again";
            $this->edit_rules($modulId,$roleId,$msg);
        }
    }
    
    public function assign_to($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $this->load->model('roles/roles_model');
        $data['role'] = $this->roles_model->getRoleInfo($roleId)
                                          ->row();
        $data['modules'] = $this->user_modul_model->getRoleModul()
                                                  ->result();
        $data['content'] = 'user_modul/forms/assign_modul';
        
        $this->load->view('template',$data);
    }
    
    public function assign_save()
    {
        $roleId = $this->input->post('role_id');
        $modulId = $this->input->post('modul_id');
        $insert = $this->input->post('chkinsert');
        $update = $this->input->post('chkupdate');
        $delete = $this->input->post('chkdelete');
        $print = $this->input->post('chkprint');
        $date = date('Y-m-d h:i:s');
        $user = $this->session->userdata('username');
        $values = array(
                    'modul_id'=>$modulId,
                    'role_id'=>$roleId,
                    'update_role'=>$update,
                    'insert_role'=>$insert,
                    'delete_role'=>$delete,
                    'print_role'=>$print,
                    'create_date'=>$date,
                    'create_user'=>$user
                  );
        echo $this->crud->insert_trans('user_modul',$values);
    }
}
?>