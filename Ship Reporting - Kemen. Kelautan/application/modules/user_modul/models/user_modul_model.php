<?php
class user_modul_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getRoles()
    {
        $this->db->select('role_id, role_name');
        $qr = $this->db->get('user_role');
        return $qr;
    }
    
    public function getRoleModul($modulId=null, $roleId=null)
    {
        
        $this->db->select('a.modul_id, a.modul_name, c.role_id, b.insert_role, b.update_role, b.delete_role, b.print_role,c.role_name');
        $this->db->from('master_modul a');
        $this->db->join('user_modul b','a.modul_id=b.modul_id');
        $this->db->join('user_role c','b.role_id=c.role_id');
        
        if((!is_null($roleId)) && (!is_null($modulId)))
        {
            $this->db->where(array(
                            'a.modul_id'=>$modulId,
                            'b.role_id'=>$roleId
                            )
                        );
        }
        
        $qr = $this->db->get();
        
        return $qr;
    }
}
?>