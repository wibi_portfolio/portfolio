<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_kapal()
    {
		$this->db->order_by('kapal','asc');
        $qr = $this->db->get('kps_kapal')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkroles[]\" value=\"'.$row->id_kapal.'\" />",
                "'.@$row->kapal.'",
                "<a href=\"'.site_url('kapal/edit/' . $row->id_kapal).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a onclick=\"del('.$row->id_kapal.')\" id=\"del_role\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;</a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>