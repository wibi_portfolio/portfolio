<?php
class kapal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'kapal List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'kapal';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_kapal';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('id_kapal');
        $roleName = $this->input->post('kapal');
        $username = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'kapal'=>$roleName
                            );
                if($this->crud->insert_trans('kps_kapal',$values))
                {
                    redirect(site_url('kapal/lists')); 
                }else{
                    $data['content'] = 'forms/modif_kapal';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('id_kapal'=>$roleId);
                $set = array(
                            'kapal'=>$roleName
                        );
                if($this->crud->edit_trans('kps_kapal',$set,$where))
                {
                    redirect(site_url('kapal/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->crud->msrwhere('kps_kapal',array('id_kapal'=>$roleId),'id_kapal','asc')->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_kapal';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id_kapal'=>$roleId);
        $this->crud->delete_trans('kps_kapal',$where);
        redirect(site_url('kapal/lists'));
    }
    
    public function delete_stack()
    {
        $kapal = $this->input->post('kapal');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$kapal);
    }
}
?>