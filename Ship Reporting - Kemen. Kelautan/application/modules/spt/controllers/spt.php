<?php
class spt extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'spt List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'spt';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_spt';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('id_spt');
        $roleName = $this->input->post('spt');
        $username = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'spt'=>$roleName
                            );
                if($this->crud->insert_trans('kps_spt',$values))
                {
                    redirect(site_url('spt/lists')); 
                }else{
                    $data['content'] = 'forms/modif_spt';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('id_spt'=>$roleId);
                $set = array(
                            'spt'=>$roleName
                        );
                if($this->crud->edit_trans('kps_spt',$set,$where))
                {
                    redirect(site_url('spt/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->crud->msrwhere('kps_spt',array('id_spt'=>$roleId),'id_spt','asc')->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_spt';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id_spt'=>$roleId);
        $this->crud->delete_trans('kps_spt',$where);
        redirect(site_url('spt/lists'));
    }
    
    public function delete_stack()
    {
        $spt = $this->input->post('spt');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$spt);
    }
}
?>