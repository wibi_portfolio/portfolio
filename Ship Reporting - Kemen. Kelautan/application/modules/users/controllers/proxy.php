<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_users()
    {
        $this->db->select('a.id,a.email, a.name, b.role_name, a.phone, a.is_blocked, a.last_login');
        $this->db->from('user a');
        $this->db->join('user_role b','a.role_id = b.role_id');
        $qr = $this->db->get()
                   ->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            $bloked = $row->is_blocked=='0'?'No':'Yes';
			$lock = $row->is_blocked=='0'?'lock':'lock-unlock';
			$lockLink = $row->is_blocked=='0'?'lock':'unlock';
            echo '
            [
                "<input type=\"checkbox\" name=\"chkuser[]\" value=\"'.$row->id.'\" />",
                "'.$row->email.'",
                "'.$row->name.'",
                "'.$row->role_name.'",
                "'.date('d F Y \o\n h:i:s',strtotime($row->last_login)).'",
                "'.$bloked.'",
                "<a href=\"'.site_url('users/edit/' . $row->id).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;<a onclick=\"del('.$row->id.')\" id=\"del_user\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;<a href=\"'.site_url('users/'.$lockLink.'/'.$row->id).'\" id=\"'.$lockLink.'\"><i class=\"fa-lock\"></i></a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>