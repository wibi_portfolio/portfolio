<?php
class users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'User List';
        $data['content'] = 'users';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'forms/modif_users';
        $data['formType'] = 'add';
        $data['title'] = 'Add new user';
        $data['roles'] = $this->users_model->getAllRoles();
        $this->load->view('template',$data);
    }
    
    public function edit($id=null,$username=null,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['user'] = $this->users_model->getUserInfo($id,$username)
                                              ->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_users';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit user';
        $data['roles'] = $this->users_model->getAllRoles();
        $this->load->view('template',$data);
    }
    
    public function save($profile=FALSE)
    {
        $type = $this->input->post('form_type');        
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $phone = @$this->input->post('phone');
        $mobile = @$this->input->post('mobile');
        $address = @$this->input->post('address');
        $roleId = @$this->input->post('role');
        switch($type)
        {
            case 'add' :
                $values = array(
                            'id'=>$this->mssqllib->get_ai('user','id'),
                            'email'=>$username,
                            'password'=>md5($password),
                            'name'=>$name,
                            'address'=>$address,
                            'phone'=>$phone,
                            'mobile'=>$mobile,
                            'role_id'=>$roleId
                          );
                if($this->crud->insert_trans('user',$values))
                {
                    redirect(complete_url('users/lists')); 
                }else{
                    $data['content'] = 'forms/modif_users';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('email'=>$username);
                $set = array(
                            'name'=>$name,
                            'address'=>$address,
                            'phone'=>$phone,
                            'mobile'=>$mobile,
                            'role_id'=>$roleId
                        );
                $set = trim($password)!=''?array_merge($set, array('password'=>md5($password))):$set;                
                if($this->crud->edit_trans('user',$set,$where))
                {
                    if($profile)
                    {
                        redirect(site_url('users/my_profile'));   
                    }else{
                        redirect(site_url('users/lists'));   
                    }
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit(null,$username,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function delete($id)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id'=>$id);
        $this->crud->delete_trans('user',$where);
        redirect(site_url('users/lists'));
    }
	
	public function lock($id)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id'=>$id);
		$set = array('is_blocked'=>'1');
		$this->crud->edit_trans('user',$set,$where);
        redirect(site_url('users/lists'));
    }
	
	public function unlock($id)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id'=>$id);
		$set = array('is_blocked'=>'0');
		$this->crud->edit_trans('user',$set,$where);
        redirect(site_url('users/lists'));
    }
    
    public function delete_stack()
    {
        $users = $this->input->post('users');
        $field = 'id';
        $in = $users;
		echo $this->crud->delete_in('user',$field,$in);
    }
    
    public function my_profile()
    {
        $username = $this->session->userdata('username');
        $data['user'] = $this->users_model->getUserInfo(0,$username)
                                          ->row();
        $data['content'] = 'forms/my_profile';
        $this->load->view('template',$data);
    }
    
    public function save_profile()
    {
        
    }
}
?>