<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 

<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>novtmp/dataTables.bootstrap.css" id="style-resource-1"> 
<div class="panel panel-default"> 
	<div class="panel-heading"> 
		<h3 class="panel-title"></h3> 
		<div class="panel-options">
			<a href="<?=base_url()?>users/add"> <i class="fa-plus"></i> </a> 
		</div> 
	</div> 
 <div class="panel-body"> 
 <script type="text/javascript">
jQuery(document).ready(function($)
{
	$("#example-4").dataTable({
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"sAjaxSource": "<?= complete_url('users/proxy/json_users') ?>",
		"bSort": false
	});
});

function del(id) {
	var data = "<?= base_url() ?>users/delete/"+id;
	e =confirm('Delete Users ?');
	if(e)
	{
		//console.log(data);
		location.href = data;
	}
	
}
</script> 
<table id="example-4" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th><input type="checkbox" class="checkall" /></th>
			<th>Username</th>
			<th>Name</th>
			<th>Role</th>
			<th>Last loged in</th>
			<th>Lock</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

</div> 
</div>
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.min.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/dataTables.bootstrap.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/jquery.dataTables.yadcf.js" id="script-resource-9"></script> 
<script src="<?= $this->config->item('jspath') ?>novtmp/datatables/dataTables.tableTools.min.js" id="script-resource-10"></script> 