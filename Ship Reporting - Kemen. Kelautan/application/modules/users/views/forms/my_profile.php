<div class="mws-panel grid_8">
    <div class="mws-panel-header">
        <span class="mws-i-24 i-list">My Profile</span>
    </div>
    <div class="mws-panel-body">
        <?= form_open(site_url('users/save/1'),array('class'=>'mws-form')) ?>
        <input type="hidden" value="edit" name="form_type" />
        <input type="hidden" value="<?= $user->role_id ?>" name="role" />
        <div class="mws-form-inline">
            <div class="mws-form-row">
                <label>Username/Email</label>
                <div class="mws-form-item small">
                    <input type="text" class="mws-textinput" name="username" id="username" value="<?= $user->email ?>" readonly="readonly" />
                </div>
            </div>
            <div class="mws-form-row">
                <label>Name</label>
                <div class="mws-form-item small">
                    <input type="text" class="mws-textinput" name="name" id="name" value="<?= $user->name ?>" />
                </div>
            </div>
            <div class="mws-form-row">
                <label>Password</label>
                <div class="mws-form-item small">
                    <input type="password" class="mws-textinput" name="password" id="password" value="" />
                    <div class="mws-error">Leave it blank if you won't change password</div>
                </div>
            </div>
            <div class="mws-form-row">
                <label>Confirm password</label>
                <div class="mws-form-item small">
                    <input type="password" class="mws-textinput validate[equals[password]]" name="repassword" id="repassword" value="" />
                    <div class="mws-error">Leave it blank if you won't change password</div>
                </div>
            </div>
            <div class="mws-form-row">
                <label>Address</label>
                <div class="mws-form-item small">
                    <textarea name="address"><?= $user->address ?></textarea>
                </div>
            </div>
            <div class="mws-form-row">
                <label>Phone</label>
                <div class="mws-form-item small">
                    <input type="text" class="mws-textinput" name="phone" id="phone" value="<?= $user->phone ?>" />
                </div>
            </div>
            <div class="mws-form-row">
                <label>Mobile</label>
                <div class="mws-form-item small">
                    <input type="text" class="mws-textinput" name="mobile" id="mobile" value="<?= $user->mobile ?>" />
                </div>
            </div>
        </div>
        <div class="mws-button-row">
            <input type="submit" value="Submit" class="mws-button red">
            <input type="reset" value="Reset" class="mws-button gray">
        </div>
        <?= form_close() ?>
    </div>
</div>