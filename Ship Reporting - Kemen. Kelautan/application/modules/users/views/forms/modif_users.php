<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 
<div class="panel panel-default"> 
	<div class="panel-heading"> 
	</div> 
	<div class="panel-body"> 
	<form role="form" class="form-horizontal" action="<?=base_url();?>users/save" method="post"> 
		<?= isset($message)?show_errors($message):'' ?>
		<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Username/Email</label> 
			<div class="col-sm-10"> 
				<input type="text" class="form-control" name="username" id="username" placeholder="Username/Email" <?= isset($user->email)?'value="'.$user->email.'" readonly':'' ?>> 
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Password</label> 
			<div class="col-sm-10"> 
				<input type="password" class="form-control" name="password" id="password" placeholder="Password" <?= isset($user->email)?'value="'.$user->email.'" readonly':'' ?>> 
				<?= isset($user->password)?'<div class="mws-error">Leave it blank if you won\'t change password</div>':'' ?>
			</div> 
		</div>  
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Confirm Password</label> 
			<div class="col-sm-10"> 
				<input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Confirm Password" <?= isset($user->email)?'value="'.$user->email.'" readonly':'' ?>> 
				<?= isset($user->password)?'<div class="mws-error">Leave it blank if you won\'t change password</div>':'' ?>
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Name</label> 
			<div class="col-sm-10"> 
				<input type="text" class="form-control" name="name" id="name" placeholder="Name" <?= isset($user->name)?'value="'.$user->name.'"':'' ?>> 
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Role</label> 
			<div class="col-sm-10"> 
				<select class="form-control" name="role"> 
					<?php foreach($roles->result() as $role): 
						$selected = "";
						if(isset($user->role_id))
						{
							if($user->role_id==$role->role_id)
							{
								$selected = ' selected="selected"';
							}
						}
					?>
					<option value="<?= $role->role_id ?>" <?= $selected ?>><?= $role->role_name ?></option>
					<?php endforeach; ?>
				</select>			
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<button type="submit" class="btn btn-success">Save</button> 
			<button type="reset" class="btn btn-white">Reset</button>
		</div> 
	</form>
	</div> 
</div>
