<?php
class users_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getAllRoles()
    {
        $this->db->select('role_id,role_name');
        $this->db->from('user_role');
        return $this->db->get();
    }
    
    public function getUserInfo($id,$username=null)
    {
        if(is_null($username))
        {
            $this->db->where(array('id'=>$id));
        }else{
            $this->db->where(array('email'=>$username));
        }
        return $this->db->get('user');
    }
}
?>