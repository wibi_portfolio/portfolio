<?php  
class login_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function loginAuth($username,$password)
    {
        $where = array(
                    'email'=>$username,
                    'password'=>md5($password),
                    'is_blocked'=>'0'
                );
        $qr = $this->db->get_where('user',$where)->row();
        if(count($qr)>0)
        {
            $userdata = array(
                            'username'=>$username,
                            'id_user'=>$qr->id,
                            'role_id'=>$qr->role_id,
                            'name'=>$qr->name,
                            'pic'=>$qr->pic
                        );
            $this->session->set_userdata($userdata);
            
            //Set last login
            $set = array('last_login'=>date('Y-m-d h:i:s'));
            $where = array('email'=>$username);
            $this->crud->edit_trans('user',$set, $where);
			$data = array(
			   'id' => '' ,
			   'type' => 'login' ,
			   'table_' => 'user',
			   'date_' => date('Y-m-d h:i:s') ,
			   'username' => $username
			);
			$this->crud->insert_trans('user_activity',$data);
            return 1;
        }else{
            return 0;
        }
    }
    
    
}
?>