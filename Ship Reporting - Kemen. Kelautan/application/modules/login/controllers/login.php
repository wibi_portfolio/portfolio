<?php
class login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }
    
    public function index()
    {
        if($this->session->userdata('username')!='')
        {
            redirect(site_url('dashboard/index'));
        }
        $this->load->view('login');
    }
    
    public function do_login()
    {
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        
        $auth = $this->login_model->loginAuth($username, $pass);
        switch($auth)
        {
            case 1:
                $this->apply_config();  //Applying web configuration
                redirect(site_url('dashboard/index'));
                break;
            case 0 :
                redirect(site_url('login'));
                break;
            case -1 :
                redirect(site_url());
                break;
        }
    }
	
	
    public function do_login_json()
    {
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        
        $auth = $this->login_model->loginAuth($username, $pass);
        switch($auth)
        {
            case 1:
                $this->apply_config();  //Applying web configuration
                echo 0;
                break;
            case 0 :
                //redirect(site_url('login'));
                echo 1;
            case -1 :
                //redirect(site_url());
                echo 2;
        }
    }
    
    public function logout()
    {
		$data = array(
			   'id' => '' ,
			   'type' => 'logout' ,
			   'table_' => 'user',
			   'date_' => date('Y-m-d h:i:s') ,
			   'username' => $this->session->userdata('username')
			);
			$this->db->insert('user_activity', $data);
        $this->session->sess_destroy();
        redirect(site_url());
    }
    
    protected function apply_config()
    {
        $this->load->library('commonlib');
        $configId = array(1,3,4);
        $config = $this->commonlib->getAppConfig($configId);
        $sess_val = array();
        foreach($config->result_array() as $conf)
        {
            $sess_val = array_merge($sess_val, array(
                                                $conf['config_key']=>$conf['config_value']
                                              )
                                   );
        }
        $this->session->set_userdata($sess_val);
        
    }
}
?>