
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="description" content="<?= $this->config->item('apl_title') ?>/ Admin Panel"> 
<meta name="author" content="<?= $this->config->item('apl_title') ?>/"> 
<title><?= $this->config->item('apl_title') ?></title> 
<!--
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic" id="style-resource-1"> 
-->
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/font-awesome.min.css" id="style-resource-3"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/bootstrap.css" id="style-resource-4"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-core.css" id="style-resource-5"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-forms.css" id="style-resource-6"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-components.css" id="style-resource-7"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/xenon-skins.css" id="style-resource-8"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/custom.css" id="style-resource-9"> 
<link rel="stylesheet" href="<?= $this->config->item('csspath') ?>cr/dataTables.bootstrap.css" id="style-resource-1"> 
<link href="<?= $this->config->item('csspath') ?>plugins/select2/select2.css" rel="stylesheet"/>
<script src="<?= $this->config->item('jspath') ?>cr/jquery-1.11.1.min.js"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/bootstrap.min.js" id="script-resource-1"></script> 
</head>
<body class="page-body lockscreen-page"> 

<div class="login-container"> 
<div class="row"> 
<div class="col-sm-7"> 
<script type="text/javascript">
jQuery(document).ready(function($)
{
// Reveal Login form
setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);

// Clicking on thumbnail will focus on password field
$(".user-thumb a").on('click', function(ev)
{
ev.preventDefault();
$("#passwd").focus();
});

// Set Form focus
$("form#lockscreen .form-group:has(.form-control):first .form-control").focus();
});
function login(){
	var isi=$("#lockscreen").serialize();
	$.post( "login/do_login_json", isi)
	.done(function( data,r ) {
		//var rtn=JSON.parse(data);
		//console.log(rtn.status);
		if(data==='0')
		{
		// Redirect after successful login page (when progress bar reaches 100%)
		location.reload();
		}
		else
		{
		toastr.error("You have entered wrong password, please try again. ", "Invalid Login!");
		}
	});
}
</script> 
<form role="form" id="lockscreen" class="lockcreen-form fade-in-effect"> 
<div class="user-thumb"> 
<a href="#"> <img src="<?= $this->config->item('imgpath') ?>user.jpg" class="img-responsive img-circle" /> </a> 
</div> 
<div class="form-group"> 
<h3>Welcome</h3> 
<div class="input-group"> 
<input type="text" class="form-control input-dark" name="username" id="username" placeholder="Username" />
</div> 
<div class="input-group"> 
<input type="password" class="form-control input-dark" name="password" id="passw" placeholder="Password" /> 
<span class="input-group-btn"> <button type="button" class="btn btn-primary" onclick="login()">Log In</button> </span> 
</div> 
</div> 
</form> 
</div> 
</div> 
</div> 
<script src="<?= $this->config->item('jspath') ?>cr/TweenMax.min.js" id="script-resource-2"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/resizeable.js" id="script-resource-3"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/joinable.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-api.js" id="script-resource-5"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-toggles.js" id="script-resource-6"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-widgets.js" id="script-resource-7"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/globalize.min.js" id="script-resource-8"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/toastr.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/jquery.validate.min.js" id="script-resource-10"></script> 
<script src="<?= $this->config->item('jspath') ?>cr/xenon-custom.js" id="script-resource-11"></script>

</body> 
</html>