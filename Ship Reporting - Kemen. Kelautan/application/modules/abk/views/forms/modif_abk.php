<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 
<div class="panel panel-default"> 
	<div class="panel-heading"> 
	</div> 
	<div class="panel-body"> 
	<form role="form" class="form-horizontal" action="<?=base_url();?>abk/save" method="post"> 
		<?= isset($message)?show_errors($message):'' ?>
		<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
		<input type="hidden" name="id_abk" value="<?= isset($role->id_abk)?$role->id_abk:'' ?>" />
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Nama ABK</label> 
			<div class="col-sm-10"> 
				<input type="text" class="form-control" name="abk" id="abk" placeholder="Nama ABK" <?= isset($role->abk)?'value="'.$role->abk.'"':'' ?>> 
			</div> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<button type="submit" class="btn btn-success">Simpan</button> 
		</div> 
	</form>
	</div> 
</div>