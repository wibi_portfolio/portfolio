<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 

<div class="panel panel-default"> 
	<div class="panel-heading"> 
		<h3 class="panel-title"></h3> 
		<div class="panel-options">
			<a href="<?=base_url()?>abk/add"> <i class="fa-plus"></i> </a> 
		</div> 
	</div> 
 <div class="panel-body"> 
 <script type="text/javascript">
jQuery(document).ready(function($)
{
	$("#example-4").dataTable({
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"sAjaxSource": "<?= complete_url('abk/proxy/json_abk') ?>",
		"bSort": false
	});
	 $('.checkall').click(function () {
            $(this).parents('table:eq(0)').find(':checkbox').attr('checked', this.checked);
        });
        
        $('a#del_role').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href')
            confirm('Delete Role ?',{'verify':true},function(e){
                if(e)
                {
                    location.href = url;
                }
            });
        })
        $('a#remove-selected').on('click',function(){
             confirm('Delete all selected abk ?',{'verify':true},function(e){
                if(e)
                {
                    //Sent abk id as array
                    var abkelected = [];
                    $('input[name="chkabk[]"]').each(function(){
                        if($(this).is(":checked"))
                        {
                            abkelected = $.merge(abkelected,[$(this).val()]);
                        }
                    });
                    $.post("<?= site_url('abk/delete_stack') ?>",{'abk':abkelected},function(data){
                        if(data=='1')
                        {
                            location.href="<?= site_url('abk/lists') ?>"
                        }else{
                             toastr.info('Unknown error : ' + data);
                        }
                    });
                }
            });
        })
});
function del(id) {
	var data = "<?= base_url() ?>abk/delete/"+id;
	e =confirm('Hapus abk ?');
	if(e)
	{
		//console.log(data);
		location.href = data;
	}
	
}
</script> 
<table id="example-4" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th><input type="checkbox" class="checkall" /></th>
			<th>Nama ABK</th>
			<th></th>
		</tr>
	</thead>
</table>

</div> 
</div>