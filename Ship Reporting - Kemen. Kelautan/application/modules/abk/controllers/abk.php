<?php
class abk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'abk List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'abk';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_abk';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('id_abk');
        $roleName = $this->input->post('abk');
        $username = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'abk'=>$roleName
                            );
                if($this->crud->insert_trans('kps_abk',$values))
                {
                    redirect(site_url('abk/lists')); 
                }else{
                    $data['content'] = 'forms/modif_abk';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('id_abk'=>$roleId);
                $set = array(
                            'abk'=>$roleName
                        );
                if($this->crud->edit_trans('kps_abk',$set,$where))
                {
                    redirect(site_url('abk/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->crud->msrwhere('kps_abk',array('id_abk'=>$roleId),'id_abk','asc')->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_abk';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id_abk'=>$roleId);
        $this->crud->delete_trans('kps_abk',$where);
        redirect(site_url('abk/lists'));
    }
    
    public function delete_stack()
    {
        $abk = $this->input->post('abk');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$abk);
    }
}
?>