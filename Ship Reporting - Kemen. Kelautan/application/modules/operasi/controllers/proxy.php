<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_operasi()
    {
		$this->db->order_by('id_operasi','asc');
        $qr = $this->db->get('kps_operasi')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkroles[]\" value=\"'.$row->id_operasi.'\" />",
                "'.@$row->no_spt.'",
                "<a href=\"'.site_url('operasi/edit/' . $row->id_operasi).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a onclick=\"del('.$row->id_operasi.')\" id=\"del_role\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;</a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
	public function json_operasi_kapal()
    {
		$this->db->order_by('id_riksa','asc');
        $qr = $this->db->get('kps_riksa')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkroles[]\" value=\"'.$row->id_operasi.'\" />",
                "'.@$row->no_spt.'",
                "<a href=\"'.site_url('operasi/edit/' . $row->id_operasi).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a onclick=\"del('.$row->id_operasi.')\" id=\"del_role\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;</a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>