<?php
class operasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'operasi List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'operasi';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_operasi';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
		$data['addid'] =$this->session->userdata('id_user').'__'.date('Ymdhis');
        $this->load->view('template',$data);
    }
	public function addriksa()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_operasi';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $data['country'] = $this->crud->msr('country','country_name','asc')->result();
		$data['addid'] =$this->session->userdata('id_user').'__'.date('Ymd');
        $this->load->view('forms/addriksa',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('id_operasi');    
        $addid = $this->input->post('addid');
        $no_spt = $this->input->post('no_spt');
        $tgl_spt = $this->input->post('tgl_spt');
        $periode = $this->input->post('periode');
        $lokasi_awal = $this->input->post('lokasi_awal');
        $lokasi_akhir = $this->input->post('lokasi_akhir');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $username = $this->session->userdata('user_id');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'no_spt'=>$no_spt,
                                'tgl_spt'=>$tgl_spt,
                                'addid'=>$addid,
                                'periode'=>$periode,
                                'lokasi_awal'=>$lokasi_awal,
                                'lokasi_akhir'=>$lokasi_akhir,
                                'tgl_awal'=>$tgl_awal,
                                'tgl_akhir'=>$tgl_akhir,
                                'created_by'=>$username,
                                'created_date'=>$date
                            );
                if($this->crud->insert_trans('kps_operasi',$values))
                {
                    redirect(site_url('operasi/lists')); 
                }else{
                    $data['content'] = 'forms/modif_operasi';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('id_operasi'=>$roleId);
                $set = array(                            
                                'no_spt'=>$no_spt,
                                'tgl_spt'=>$tgl_spt,
                                'addid'=>$addid,
                                'periode'=>$periode,
                                'lokasi_awal'=>$lokasi_awal,
                                'lokasi_akhir'=>$lokasi_akhir,
                                'tgl_awal'=>$tgl_awal,
                                'tgl_akhir'=>$tgl_akhir,
                                'updated_by'=>$username,
                                'updated_date'=>$date
                        );
                if($this->crud->edit_trans('kps_operasi',$set,$where))
                {
                    redirect(site_url('operasi/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->crud->msrwhere('kps_operasi',array('id_operasi'=>$roleId),'id_operasi','asc')->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_operasi';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('id_operasi'=>$roleId);
        $this->crud->delete_trans('kps_operasi',$where);
        redirect(site_url('operasi/lists'));
    }
    
    public function delete_stack()
    {
        $operasi = $this->input->post('operasi');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$operasi);
    }
}
?>