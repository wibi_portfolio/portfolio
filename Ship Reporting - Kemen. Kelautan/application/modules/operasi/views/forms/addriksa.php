
<form role="form" class="form-horizontal" action="#" method="post"> 
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
  <h4 class="modal-title" id="myModalLabel">Tambah Hasil Pemeriksaan</h4>
</div>
<div class="modal-body">
		<?= isset($message)?show_errors($message):'' ?>
		<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
		<input type="hidden" name="item_id" value="<?= isset($role->item_id)?$role->item_id:'' ?>" />
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1"><b>Informasi Kapal</b></label> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Nama Kapal</label> 
			<div class="col-sm-6"> 
				<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Kapal"> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">Tonase Kapal</label> 
			<div class="col-sm-2"> 
				<input type="text" class="form-control" name="ton" id="ton" placeholder="Tonase Kapal"> 
			</div> 
		</div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Jenis Kapal</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="jenis" id="jenis" placeholder="Jenis Kapal"> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">Alat</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="alat" id="alat" placeholder="Alat"> 
			</div> 
		</div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Bendera</label> 
			<div class="col-sm-4"> 
				<select name="parent" id="parent" class="form-control">
					<option value="0">Pilih Bendera</option>
				<?php
					foreach($country as $rcountry){
						
				?>
					<option value="<?=$rcountry->COUNTRY_ID?>"><?=kata($rcountry->COUNTRY_NAME)?></option>
				<?php
					}
				?>
				</select>
			</div> 
		</div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Pemilik Kapal</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="pemilik" id="pemilik" placeholder="Pemilik Kapal"> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">Nakhoda Kapal</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="nakhoda" id="nakhoda" placeholder="Nakhoda Kapal"> 
			</div> 
		</div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">SIUP</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="siup" id="siup" placeholder="SIUP"> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">SIPI</label> 
			<div class="col-sm-4"> 
				<input type="text" class="form-control" name="sipi" id="sipi" placeholder="SIPI"> 
			</div> 
		</div> 
		<div class="form-group-separator"></div>
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1"><b>ABK</b></label> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">WNI</label> 
			<div class="col-sm-2"> 
				<input type="text" class="form-control" name="wni" id="wni" placeholder="WNI"> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">WNA</label> 
			<div class="col-sm-2"> 
				<input type="text" class="form-control" name="wna" id="wna" placeholder="WNA"> 
			</div> 
		</div> 
		<div class="form-group-separator"></div>
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1"><b>Posisi</b></label> 
		</div> 
		<div class="form-group-separator"></div> 
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Latitude</label> 
			<div class="col-sm-2"> 
				<input type="text" class="form-control" name="lat" id="lat" placeholder=""> 
			</div> 
			<label class="col-sm-2 control-label" for="field-1">Longitude</label> 
			<div class="col-sm-2"> 
				<input type="text" class="form-control" name="lng" id="lng" placeholder=""> 
			</div> 
		</div> 
		<div class="form-group-separator"></div>
		<div class="form-group"> 
			<label class="col-sm-2 control-label" for="field-1">Dugaan Pelanggaran</label> 
			<div class="col-sm-10"> 
				<input type="text" class="form-control" name="dugaan" id="dugaan" placeholder="Dugaan Pelanggaran"> 
			</div> 
		</div>  
</div>
<div class="modal-footer">
	
		<div class="form-group"> 
			<button type="button" class="btn btn-success">Simpan</button> 
			<button type="reset" class="btn btn-white">Kosongkan</button>
		</div> 
	</form>
</div>