
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		$("#tgl_spt").datepicker();
		$("#tgl_awal").datepicker();
		$("#tgl_akhir").datepicker();
		$("#s2example-1").select2({
			placeholder: 'Select your country...',
			allowClear: true,
			dropdownAutoWidth: true
		}).on('select2-open', function()
		{
			// Adding Custom Scrollbar
			$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
		});
	});
</script>
<div class="page-title"> 
	<div class="title-env"> 
		<h1 class="title"><?=ucfirst($this->uri->segment(1))?></h1> 
		<p class="description"></p> 
	</div> 
	<div class="breadcrumb-env"> 
		<ol class="breadcrumb bc-1"> 
			<?=breadcrumb($this->uri->uri_string)?>
			<!--
			<li> <a href="./"><i class="fa-home"></i>Report</a> </li> 
			<li class="active"> <strong>Queue List</strong> </li> 
			-->
		</ol> 
	</div> 
</div> 
<div class="panel panel-default"> 
	<div class="panel-heading"> 
	</div> 
	<div class="panel-body"> 
	<div class="col-md-12"> 
		<?= isset($message)?show_errors($message):'' ?>
		<form role="form" class="form-horizontal" action="<?=base_url();?>operasi/save" method="post"> 
		<ul class="nav nav-tabs nav-tabs-justified"> 
			<li class="active"> 
				<a href="#tab1" data-toggle="tab"> <span class="hidden-xs">Operasi</span> </a> 
			</li> 
			<li class=""> 
				<a href="#tab2" data-toggle="tab"> <span class="hidden-xs">Hasil Riksa</span> </a> 
			</li> 
			<li class=""> 
				<a href="#tab3" data-toggle="tab"> <span class="hidden-xs">Upload Dokumen</span> </a> 
			</li> 
		</ul> 
		<div class="tab-content"> 
			<div class="tab-pane active" id="tab1"> 
				<input type="hidden" name="form_type" id="form_type" value="<?= $formType  ?>" />
				
				<input type="hidden" name="addid" id="addid" value="<?= @$addid ?>" />
				<input type="hidden" name="id_operasi" value="<?= isset($role->id_operasi)?$role->id_operasi:'' ?>" />
				<div class="form-group"> 
					<label class="col-sm-2 control-label" for="field-1">No SPT</label> 
					<div class="col-sm-5"> 
						<input type="text" class="form-control" name="no_spt" id="no_spt" placeholder="Nomor SPT"> 
					</div> 
					<label class="col-sm-2 control-label" for="field-1">Tanggal SPT</label> 
					<div class="col-sm-3"> 
						<input type="text" class="form-control datepicker" name="tgl_spt" id="tgl_spt" placeholder="Tanggal SPT"> 
					</div> 
				</div>				
				<div class="form-group"> 
					<label class="col-sm-2 control-label" for="field-1">Periode</label> 
					<div class="col-sm-2"> 
						<input type="text" class="form-control" name="periode" id="periode" placeholder="Periode" <?= isset($role->periode)?'value="'.$role->periode.'"':'' ?>> 
					</div> 				
					<label class="col-sm-2 control-label" for="field-1">Tanggal Awal</label> 
					<div class="col-sm-2"> 
						<input type="text" class="form-control" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal" <?= isset($role->tgl_awal)?'value="'.$role->tgl_awal.'"':'' ?>> 
					</div> 				
					<label class="col-sm-2 control-label" for="field-1">Tanggal Akhir</label> 
					<div class="col-sm-2"> 
						<input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir" <?= isset($role->tgl_akhir)?'value="'.$role->tgl_akhir.'"':'' ?>> 
					</div> 
				</div>
				<div class="form-group"> 
					<label class="col-sm-2 control-label" for="field-1">Lokasi Awal</label> 
					<div class="col-sm-10"> 
						<input type="text" class="form-control" name="no_spt" id="no_spt" placeholder="Lokasi Awal" <?= isset($role->no_spt)?'value="'.$role->no_spt.'"':'' ?>> 
					</div> 
				</div> 
				<div class="form-group"> 
					<label class="col-sm-2 control-label" for="field-1">Lokasi Akhir</label> 
					<div class="col-sm-10"> 
						<input type="text" class="form-control" name="no_spt" id="no_spt" placeholder="Lokasi Akhir" <?= isset($role->no_spt)?'value="'.$role->no_spt.'"':'' ?>> 
					</div> 
				</div> 
				
			</div> 
			<div class="tab-pane" id="tab2"> 
				<div class="form-group"> 
					<label class="col-sm-2 control-label" for="field-1"><a href="javascript:void(0);" onclick="addriksa()" class="btn btn-success btn-xs">Tambah Hasil Riksa</a></label> 
					<div class="col-sm-10"> 
					</div> 
				</div>
				<div class="form-group-separator"></div> 				
				 <script type="text/javascript">
				jQuery(document).ready(function($)
				{
					var addid=$("#addid").val();
					$("#example-4").dataTable({
						"sPaginationType": "full_numbers",
						"bProcessing": true,
						"sAjaxSource": '<?=base_url()?>operasi/proxy/json_operasi_kapal/'+addid,
						"bSort": false
					});
				});
				function addriksa(){				
					var addid=$("#addid").val();				
					$.post( "addriksa/", { addid: addid})
						.done(function( data ) {
						
						$( "#result" ).empty();
						$( "#result" ).html(data);
						$('#myModal').modal({ keyboard: false });
						
					});
				}
				function del(id) {
					var data = "<?= base_url() ?>operasi/delete_kapal/"+id;
					e =confirm('Hapus operasi?');
					if(e)
					{
						//console.log(data);
						location.href = data;
					}					
				}
				</script>
				<table id="example-4" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kapal</th>
							<th>Bendera</th>
							<th>Jumlah ABK</th>
							<th>Posisi</th>
							<th>Dugaan Pelanggaran</th>
						</tr>
					</thead>
				</table>
			</div> 
			<div class="tab-pane" id="tab3"> 
				<div class="form-group"> 
						<label class="col-sm-2 control-label" for="field-1">Perintah Gerak</label> 
						<div class="col-sm-10"> 
							<input type="file" class="form-control" name="perintah_gerak" id="perintah_gerak"> 
						</div> 
					</div> 
				</div> 
			</div>
	</div>
	<div class="form-group-separator"></div> 
	<div class="form-group"> 
		<button type="submit" class="btn btn-success">Simpan</button> 
	</div> 
	</form>
	</div> 
</div>