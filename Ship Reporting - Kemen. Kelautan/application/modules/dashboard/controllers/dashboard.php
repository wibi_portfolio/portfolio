<?php
class dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->commonlib->checkPagesPrivileges();
		$notx=$this->input->post('no_tx');
        $data['content'] = 'dashboard';
		$ttlreg=array();$negara=array();$ttllog=array();$tgl=array();
		$ttlp1=array();$ttlp2=array();$ttlp3=array();$tgl2=array();
		$tglnya=date('Y-m-d');
		$tgls=explode('-',$tglnya);
		
		for($i=1;$i<=date('d',strtotime($tglnya));$i++){
			$tgli=$i;
			if($i<10)
			$tgli='0'.$i;
			$tgl1[]=$i;
			$qr = $this->crud->msrwhere('kps_operasi',array('tgl_spt'=>$tgls[0].'-'.$tgls[1].'-'.$tgli),'tgl_spt','asc')->num_rows();
			$ttlreg[] = @$qr;
		}
		/*
		
		$sql = 'select count(id) as n,registerdate from blck_crr_app group by MONTH(registerdate),YEAR(registerdate) order by id desc';
		$qr = $this->crud->msrquery($sql)->result();
				
		foreach($qr as $row)
        {
			$ttlreg[] = @$row->n;
			$tgl[]= date('M y',strtotime(@$row->registerdate));
		}
		$data['ttlreg'] =$ttlreg;			
		$data['tgl'] =$tgl;
		
		$sql = 'select count(a.APPLIED_ID) as n,a.APPLIED_ID,b.POSITION_NAME_EN from blck_crr_applied a, (select c.REQUEST_ID,d.POSITION_NAME_EN from blck_crr_vacancy c,blck_crr_position d where c.REQUEST_POSID=d.POSITION_ID) b where a.REQ_ID=b.REQUEST_ID group by b.POSITION_NAME_EN order by a.APPLIED_ID desc';
		$qr = $this->crud->msrquery($sql)->result();
				
		foreach($qr as $row)
        {
			$ttlneg[] = @$row->n;
			$negara[]= kata(@$row->POSITION_NAME_EN);
		}
		*/
		$data['tgl'] =@$tgl1;
		
		$data['ttlreg'] =@$ttlreg;			
		
        $this->load->view('template',$data);
    }
	
}
?>