<div class="row">
                <div class="col-lg-7">
                    <h1>Dashboard <small>Statistics and more</small></h1> 
                </div>
            </div>
	<div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa-globe"></i> Peta Operasi</h3>
				</div>
				<div class="panel-body">
					<iframe src="<?=base_url()?>map" width="100%" height="400"></iframe>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> </h3>
				</div>
				<div class="panel-body">
					<div id="graph"></div>
				</div>
			</div>
		</div>
	</div>
	<script type='text/javascript' src='<?= $this->config->item('jspath') ?>highcharts.js'></script>
    <script type="text/javascript">
	$(function () {
		$('#graph').highcharts({
			chart: {
				type: 'line'
			},
			title: {
				text: 'Total SPT bulan <?=date('M Y')?>'
			},
			xAxis: {
				categories: [<?php
								for($i=0;$i<count($tgl);$i++){
									echo "'".$tgl[$i]."',";
								}
							?>]
			},
			yAxis: {
				min: 0,
				title: {
					text: ' '
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [
			{
				name: 'SPT',
				data: [<?php
							for($i=0;$i<count($tgl);$i++){
								echo @$ttlreg[@$i].',';
							}
						?>]

			}]
		});
		/*
		// Radialize the colors
		Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
			return {
				radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
				stops: [
					[0, color],
					[1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
				]
			};
		});
		
		// Build the chart
		$('#donut2').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: 'Applicant Position Applied'
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						style: {
							color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
						},
						connectorColor: 'silver'
					}
				}
			},
			series: [{
				type: 'pie',
				name: ' Applicant',
				data: [<?php
							for($i=0;$i<count($negara);$i++){
								echo "['".@$negara[$i]."',".$ttlneg[$i]."],";
							}
						?>]				
			}]
		});
		*/
	});
    </script>