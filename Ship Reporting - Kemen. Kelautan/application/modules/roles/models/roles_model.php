<?php
class roles_model extends CI_Model  
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getRoleInfo($roleId)
    {
        $this->db->where(array('role_id'=>$roleId));
        return $this->db->get('user_role');
    }
}
?>