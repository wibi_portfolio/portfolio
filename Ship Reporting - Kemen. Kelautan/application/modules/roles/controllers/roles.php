<?php
class roles extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('roles_model');
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['title'] = 'Roles List';
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'roles';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
		$data['uri'] = $this->uri->uri_string;
        $data['content'] = 'forms/modif_roles';
        $data['formType'] = 'add';
        $data['title'] = 'Add new role';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');        
        $roleId = $this->input->post('role_id');
        $roleName = $this->input->post('role_name');
        $username = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                                'role_name'=>$roleName,
                                'create_user'=>$username,
                                'create_date'=>$date
                            );
                if($this->crud->insert_trans('user_role',$values))
                {
                    redirect(site_url('roles/lists')); 
                }else{
                    $data['content'] = 'forms/modif_roles';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('role_id'=>$roleId);
                $set = array(
                            'role_name'=>$roleName,
                            'modif_user'=>$username,
                            'modif_date'=>$date
                        );
                if($this->crud->edit_trans('user_role',$set,$where))
                {
                    redirect(site_url('roles/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($roleId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function edit($roleId,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $data['role'] = $this->roles_model->getRoleInfo($roleId)
                                              ->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_roles';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit role';
        $this->load->view('template',$data);
    }
    
    public function delete($roleId)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('role_id'=>$roleId);
        $this->crud->delete_trans('user_role',$where);
        redirect(site_url('roles/lists'));
    }
    
    public function delete_stack()
    {
        $roles = $this->input->post('roles');
        $field = 'role_id';
        echo $this->crud->delete_in('user_role',$field,$roles);
    }
}
?>