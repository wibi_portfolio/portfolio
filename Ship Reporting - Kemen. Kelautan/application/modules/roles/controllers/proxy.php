<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_roles()
    {
        $qr = $this->db->get('user_role')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkroles[]\" value=\"'.$row->role_id.'\" />",
                "'.$row->role_name.'",
                "'.$row->create_user.'",
                "'.$row->create_date.'",
                "<a href=\"'.site_url('roles/edit/' . $row->role_id).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;</a><a onclick=\"del('.$row->role_id.')\" id=\"del_role\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;</a>"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>