<?php
class modul_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getModulInfo($modulId)
    {
        $this->db->where(array('modul_id'=>$modulId));
        $qr = $this->db->get('master_modul');
        return $qr;
    }
}
?>