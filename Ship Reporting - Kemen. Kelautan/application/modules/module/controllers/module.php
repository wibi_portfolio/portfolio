<?php
class module extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('modul_model');
    }
    
    public function lists()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'modules';
        $this->load->view('template',$data);
    }
    
    public function add()
    {
        $this->commonlib->checkPagesPrivileges();
        $data['content'] = 'forms/modif_module';
        $data['formType'] = 'add';
        $data['title'] = 'Add new modul';
        $this->load->view('template',$data);
    }
    
    public function edit($modulId=null,$err=null)
    {
        $this->commonlib->checkPagesPrivileges(array(3,4));
        $data['modul'] = $this->modul_model->getModulInfo($modulId)
                                              ->row();
        $data['message'] = $err;
        $data['content'] = 'forms/modif_module';
        $data['formType'] = 'edit';
        $data['title'] = 'Edit modul';
        $this->load->view('template',$data);
    }
    
    public function save()
    {
        $type = $this->input->post('form_type');
        $modulId = $this->input->post('modul_id');
        $modulName = $this->input->post('modul_name');
        $modulUrl = $this->input->post('modul_url');
        $user = $this->session->userdata('username');
        $date = date('Y-m-d h:i:s');
        switch($type)
        {
            case 'add' :
                $values = array(
                            'modul_id'=>$this->mssqllib->get_ai('master_modul','modul_id'),
                            'modul_name'=>$modulName,
                            'modul_url'=>$modulUrl,
                            'create_user'=>$user,
                            'create_date'=>$date                            
                          );
                if($this->crud->insert_trans('master_modul',$values))
                {
                    redirect(complete_url('module/lists')); 
                }else{
                    $data['content'] = 'forms/modif_module';
                    $data['message'] = 'Error when saving data, please try again';
                    $this->load->view('template',$data);
                }
                break;
            case 'edit' :
                $where = array('modul_id'=>$modulId);
                $set = array(
                            'modul_name'=>$modulName,
                            'modul_url'=>$modulUrl,
                            'modif_user'=>$user,
                            'modif_date'=>$date                            
                        );
                if($this->crud->edit_trans('master_modul',$set,$where))
                {
                    redirect(site_url('module/lists'));
                }else{
                    $msg = "Error when editing data, please try again";
                    $this->edit($modulId,$msg);
                }
                break;
            default :
                break;
        }
    }
    
    public function delete($id)
    {
        $this->commonlib->checkPagesPrivileges(3);
        $where = array('modul_id'=>$id);
        $this->crud->delete_trans('master_modul',$where);
        redirect(site_url('module/lists'));
    }
    
    public function delete_stack()
    {
        $modules = $this->input->post('modules');
        $field = 'modul_id';
        echo $this->crud->delete_in('master_modul',$field,$modules);
    }
}
?>