<?php
class proxy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        //If the request not as xhr do exit
        if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])
           || (strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest')){
           exit('No direct access allowed');
        }
    }
    
    public function json_modules()
    {
        $this->db->order_by('modul_id','ASC');
        $qr = $this->db->get('master_modul')->result();
        echo '{ "aaData": [';
        $total = count($qr);
        $i = 1;
        foreach($qr as $row)
        {
            echo '
            [
                "<input type=\"checkbox\" name=\"chkmodule[]\" value=\"'.$row->modul_id.'\" />",
                "'.$row->modul_name.'",
                "'.$row->modul_url.'",
                "'.$row->create_user.'",
                "'.date('d F Y',strtotime($row->create_date)).'",
                "<a href=\"'.site_url('module/edit/' . $row->modul_id).'\"><i class=\"fa-edit\"></i></a>&nbsp;&nbsp;<a  onclick=\"del('.$row->modul_id.')\" id=\"del_modul\"><i class=\"fa-trash\"></i></a>&nbsp;&nbsp;"
            ]';
            if($i<$total)
            {
                echo ',';
            }
            $i++;
        }
        
        echo "]}";
    }
}
?>