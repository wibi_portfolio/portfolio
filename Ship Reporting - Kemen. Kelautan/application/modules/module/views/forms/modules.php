<div class="mws-panel grid_8">
    <div class="mws-panel-header">
        <span class="mws-i-24 i-table-1">Module Lists</span>
    </div>
    <div class="mws-panel-body">
        <div class="mws-panel-toolbar top clearfix">
            <ul>
                <li><a href="<?= site_url('module/add') ?>" class="mws-ic-16 ic-add" title="Accept">Add New</a></li>
                <li><a href="#" id="remove-selected" class="mws-ic-16 ic-bin-closed" title="Accept">Remove All Selected</a></li>
            </ul>
        </div>
        <table class="mws-datatable-fn mws-table">
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall" /></th>
                    <th>Modul name</th>
                    <th>Site url</th>
		    <th>Create user</th>
                    <th>Create date</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(function(){
	$(".mws-datatable-fn").dataTable({
            sPaginationType: "full_numbers",
            "bProcessing": true,
            "sAjaxSource": "<?= site_url('module/proxy/json_modules') ?>",
            "bSort": false
        });
        
        $('a#del_modul').live('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            apprise('Delete Module ?',{'verify':true},function(e){
                if(e)
                {
                    location.href = url;
                }
            });
        })
        
        $('.checkall').click(function () {
            $(this).parents('table:eq(0)').find(':checkbox').attr('checked', this.checked);
        });
        
        $('a#remove-selected').live('click',function(){
            apprise('Delete all selected modules ?',{'verify':true},function(e){
                if(e)
                {
                    //Sent module id as array
                    var modulSelected = [];
                    $('input[name="chkmodule[]"]').each(function(){
                        if($(this).is(":checked"))
                        {
                            modulSelected = $.merge(modulSelected,[$(this).val()]);
                        }
                    });
                    $.post("<?= site_url('module/delete_stack') ?>",{'modules':modulSelected},function(data){
                        if(data=='1')
                        {
                            location.href="<?= site_url('module/lists') ?>"
                        }else{
                            apprise('Unknown error : ' + data);
                        }
                    });
                }
            });
        })
    })
</script>