<?php
class crud extends CI_Model
{
	
	function edit($table, $set,$field, $id){
		$this->db->where($field, $id);
		$this->db->update($table, $set);
	}
	
    public function insert_trans($table,$values)
    {
        $this->db->trans_start();
        $this->db->insert($table,$values);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return = FALSE;
        }else
        {
            $this->db->trans_commit();
            $return = TRUE;
        }
        $this->db->trans_complete();
        return $return;
    }
    
    public function delete_trans($table,$where=null)
    {
        $this->db->trans_start();
        if(!is_null($where))
        {
            $this->db->where($where);
        }
        $this->db->delete($table);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return = FALSE;
        }else
        {
            $this->db->trans_commit();
            $return = TRUE;
        }
        $this->db->trans_complete();
        return $return;
    }
    
    public function edit_trans($table, $set, $where=null)
    {
        $this->db->trans_start();
        if(!is_null($where))
        {
            $this->db->where($where);
        }        
        $this->db->set($set);
        $this->db->update($table);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return = FALSE;
        }else
        {
            $this->db->trans_commit();
            $return = TRUE;
        }
        $this->db->trans_complete();
        return $return;
    }
    
    public function query_trans($sql)
    {
        $this->db->trans_start();
        $this->db->query($sql);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return = FALSE;
        }else
        {
            $this->db->trans_commit();
            $return = TRUE;
        }
        $this->db->trans_complete();
        return $return;
    }
    
    public function delete_in($table, $field, $whereIn, $where = null)
    {
		$n=count($whereIn);
		for($i=0;$i<$n;$i++){
			$this->db->where($field, $whereIn[$i]);
			$this->db->delete($table); 
		}
		return 1;
        // $this->db->where_in($field,$whereIn);
        // if(!is_null($where))
        // {
            // $this->db->where($where);
        // }
        // return $this->db->delete($table);
    }
	
	public function msrquery($sql){
		return $this->db->query($sql);
	}
	
	public function msr($table,$field,$sb){
		$this->db->order_by($field,$sb);
		return $this->db->get($table);
	}
	
	public function msrgp($table,$field,$sb,$gb){
		$this->db->group_by($gb); 
		$this->db->order_by($field,$sb);
		return $this->db->get($table);
	}
	
	public function msrpag($table,$field,$sb,$limit,$offset){
		$this->db->order_by($field,$sb, $limit, $offset);
		return $this->db->get($table);
	}
	
	//mencari semua record berdasarkan kondisi 
	public function msrwhere($table,$com,$field,$sb){
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	//mencari semua record berdasarkan kondisi 
	public function msrwherein($table,$where,$key,$field,$sb){
		$this->db->where_in($where, $key);
		$this->db->order_by($field,$sb);
		return $this->db->get($table);
	}
	
	public function msrwhere_select($table,$com,$field,$sb,$select){
		$this->db->select($select);
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwheregp($table,$com,$field,$sb,$gb){
		$this->db->group_by($gb);
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com);
	}
	
	public function msrwherepag($table,$com,$field,$sb,$limit,$offset){
		$this->db->order_by($field,$sb);
		return $this->db->get_where($table, $com, $limit, $offset);
	}
	
	public function msrwherepaggr($table,$com,$limit,$offset,$gb){
		$this->db->group_by($gb);
		return $this->db->get_where($table, $com, $limit, $offset);
	}
}
?>