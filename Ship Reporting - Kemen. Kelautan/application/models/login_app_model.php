<?php  
class login_app_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function loginAuth($username,$password)
    {
        $where = array(
                    'username'=>$username,
                    'password'=>md5($password),
                    'is_blocked'=>'0'
                );
        $qr = $this->db->get_where('crr_app',$where)->row();
        if(count($qr)>0)
        {
			$personal = $this->db->get_where('crr_personal',array('APP_ID'=>$qr->id))->row();
            $userdata = array(
                            'userapp'=>$username,
                            'appid'=>$qr->id,
                            'name'=>@$personal->FIRST_NAME.' '.@$personal->LAST_NAME,
                            'pic'=>$qr->user_photo
                        );
            $this->session->set_userdata($userdata);
            
            //Set last login
            $set = array('lastvisitDate'=>date('Y-m-d h:i:s'));
            $where = array('username'=>$username);
            $this->crud->edit_trans('crr_app',$set, $where);
			$data = array(
			   'id' => '' ,
			   'type' => 'login' ,
			   'table_' => 'app',
			   'date_' => date('Y-m-d h:i:s') ,
			   'username' => $username
			);
			$this->crud->insert_trans('user_activity',$data);
            return 1;
        }else{
            return 0;
        }
    }
    
    
}
?>