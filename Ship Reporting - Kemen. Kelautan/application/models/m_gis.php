<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class M_gis extends CI_Model
{
	private $menu;
	
    function  __construct() {
        parent::__construct();
    }
    
	function geocode2(){
	$this->db->select('*');
		$this->db->from('geocode a');
		$this->db->join('kps_operasi c', 'a.id = c.id_geo');
		$this->db->where('a.lat !=', 0); 
		$this->db->where('c.published', 1); 

		$query = $this->db->get();
		//get_where('mytable', array('id' => $id), $limit, $offset);
		return $query;
    }
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>