<?php
class Dash{

	public function get_sales_month(){
		$result = 0;

        $text = "SELECT DATE_FORMAT(po_date,'%b' ) as SalesMonth, SUM(po_totalAmount) AS TotalSales
    			FROM t_po where YEAR(po_date) = DATE_FORMAT(NOW(),'%Y')
				GROUP BY  MONTH(po_date) ORDER BY MONTH(po_date)";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

	}

	public function get_sales_year(){
		$result = 0;

        $text = "SELECT DATE_FORMAT(po_date,'%Y' ) as SalesYear, SUM(po_totalAmount) AS TotalSales
    			FROM t_po GROUP BY  YEAR(po_date) ORDER BY YEAR(po_date) ASC";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

	}

    public function get_approve_po(){
        $result = 0;

        $text = "SELECT  COUNT(po_ID) AS TotalPo
                FROM t_po WHERE po_status = 1";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

    }

    public function get_verify_po(){
        $result = 0;

        $text = "SELECT  COUNT(po_ID) AS TotalPo
                FROM t_po WHERE po_status = 2";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

    }

    public function get_ship_po(){
        $result = 0;

        $text = "SELECT  COUNT(po_ID) AS TotalPo
                FROM t_po WHERE po_status = 3";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

    }

    public function get_exp_pro(){
        $result = 0;

        $text = "SELECT  COUNT(at_id) AS TotalPo
                FROM at_brg_masuk WHERE DATE_SUB(at_expired,INTERVAL 6 MONTH) < NOW()";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

    }

	public function get_stok(){
		$result = 0;

        $text = "SELECT * FROM t_product WHERE product_ID != 0";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

	}

	public function sale_by_product(){
		$result = 0;

        $text = "SELECT SUM(atpp_qty) AS TotalSales , product_name,	product_satuan FROM at_po_product 
        LEFT JOIN t_product ON atpp_productID = product_ID GROUP BY atpp_productID";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;

	}




}

?>