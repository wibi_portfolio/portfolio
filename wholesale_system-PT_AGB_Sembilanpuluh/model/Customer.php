<?php
class Customer{
	private $table = "t_customer";
	private $itemPerPageAdmin = 10;

//START FUNCTION FOR ADMIN PAGE

	public function get_data_by_page($page=1){
        $result = 0;  
        //get total data
        $text_total = "SELECT customer_ID FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT customer_ID, customer_name,  customer_phone, customer_email, customer_company, customer_country,
        	(SELECT po_date FROM t_po WHERE po_custID = customer_ID ORDER BY UNIX_TIMESTAMP(po_modifyDate) DESC LIMIT 0,1) AS po_date 
        	FROM $this->table ORDER BY customer_ID DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }        

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function get_data_detail($id){
        $result = 0;

        $text = "SELECT * FROM $this->table WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        return $result;
    }



    public function update_data($id, $name, $phone, $email, $company, $alamat, $city, $state, $country, $zip){
        $result = 0;
        $text = "UPDATE $this->table SET customer_name = '$name', customer_phone = '$phone', customer_email='$email', 
        customer_company='$company', customer_address = '$alamat', customer_city = '$city', customer_state='$state',
        customer_country = '$country', customer_zip = '$zip'
        WHERE customer_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        //return  $text;
        return $result;
    }

    public function update_data_customer($id, $name, $phone){
        $result = 0;
        $text = "UPDATE $this->table SET customer_fname = '$name', customer_phone = '$phone' WHERE customer_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

   

//END FUNCTION FOR ADMIN PAGE

//START FUNCTION FOR CLIENT PAGE
    public function insert_cust($name, $email, $phone, $company, $alamat, $city, $state, $country, $zip){
        $result = 0;    
               
        $text = "INSERT INTO $this->table ( customer_name, customer_phone, customer_email, customer_company, customer_address, customer_city, customer_state, customer_country,  customer_zip) 
        VALUES('$name', '$phone', '$email', '$company', '$alamat', '$city', '$state', '$country', '$zip')";

        $query = mysql_query($text);

        if($query){
            $result = mysql_insert_id();
        }        
        return $result;
    }

    public function login($email, $password){        
        $result = 0; //FAILED
        $pass = md5($password) ;
        $text = "SELECT customer_ID, customer_fname, customer_email FROM $this->table WHERE customer_email = '$email' AND customer_password = '$pass' LIMIT 0,1";

        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
            $row = mysql_fetch_array($query,MYSQL_ASSOC);

            $_SESSION['customer_email'] = $email;
            $_SESSION['customer_name'] = $row['customer_fname'];
            $_SESSION['customer_id'] = $row['customer_ID'];
            $result = 1;
        }       
        return $result;

    }
    public function check_pass($id,$password){        
        $result = 0; //FAILED
        $pass = md5($password) ;
        $text = "SELECT customer_password FROM $this->table WHERE customer_ID = '$id' AND customer_password = '$pass'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $result = 1;
        }        
        return $result;

    }
    public function update_pass($id, $password){
        $result = 0;
        $pass = md5($password) ;
        $text = "UPDATE $this->table SET customer_password = '$pass' WHERE customer_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function customer_check_login(){
        $result = 0; 

        if(isset($_SESSION['customer_id'])){
            $result = 1;
        }
        return $result;
    }    

    public function customer_logout(){
        unset($_SESSION['customer_email']);
        unset($_SESSION['customer_name']);
        unset($_SESSION['customer_id']);
        $result = 1;
        return $result;
    }


}

?>