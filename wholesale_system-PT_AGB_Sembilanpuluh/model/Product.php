<?php

class Product{
	private $table = "t_product";
	private $itemPerPageAdmin = 10;
       

  
    public function generate_kode_brg(){
        $result = 0;
        $text = "SELECT product_ID FROM t_product";
        $query = mysql_query($text);
        if($query){
            $total_data = mysql_num_rows($query);
            if($total_data > 0){
                $num = $total_data + 1;
                $result = "KHP0".$num;
            } else {
                $result = "KHP01";
            }
        } else {
             $result = "KHP01";
        }
        return $result;
    }
  
    //START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
             
        //get total data

        $text_total = "SELECT product_ID FROM $this->table WHERE product_ID != 0";

        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);

        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM $this->table  GROUP BY product_ID ORDER BY product_ID DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;
                $result[$loop]['sell_price'] = $row['product_price'] + ($row['product_price'] * $row['product_persentase_profit'] / 100);
                $loop++ ;
            }
        }       

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;

    }



	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table  WHERE product_ID = '$id'";

		$query = mysql_query($text);

		if(mysql_num_rows($query) >= 1){
			$result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
		}
        //$result = $text;
		return $result;
	}

   



	public function insert_data($name, $stok, $satuan, $price, $profit, $publish){

		$result = 0;
        $kode = $this->generate_kode_brg() ;
		$text = "INSERT INTO $this->table (product_name, product_code, product_stok, product_satuan, product_price, product_persentase_profit,  product_publish) VALUES('$name', '$kode', '$stok', '$satuan', '$price', '$profit', '$publish')";

		$query = mysql_query($text);

		if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
		return $result;	
    }

    public function sold_stok($id, $jumlah){
        $result = 0;
        $text = "UPDATE $this->table SET product_stok = product_stok - $jumlah WHERE product_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

     public function reduce_stock($id){
        $result = 0;

        $text = "SELECT * FROM at_po_product WHERE atpp_poID= '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            
            while($row = mysql_fetch_assoc($query)){
                $this->sold_stok($row['atpp_productID'], $row['atpp_qty']);
            }
        }    
        return $result;
    }

    public function add_stok($id, $jumlah){
        $result = 0;
        $text = "UPDATE $this->table SET product_stok = product_stok + $jumlah WHERE product_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }


	public function update_data($id, $name, $satuan, $price, $profit, $publish){

		$result = 0;
		$text = "UPDATE $this->table SET product_name = '$name', product_price = '$price', product_publish = '$publish', 
         product_satuan = '$satuan', product_persentase_profit = '$profit'  WHERE product_ID = '$id'";

		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}



	public function delete_data($id){
		$result = 0;      
		$text = "DELETE FROM $this->table WHERE product_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

     public function get_product_list(){
        $result = "";

        $text = "SELECT * FROM $this->table WHERE product_publish = 'Publish' AND product_stok > 0 ORDER BY product_code ASC  ";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
         
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result .= "<option value='".$row['product_ID']."'>".$row['product_code']."-".$row['product_name']."</option>"  ;

                $loop++;
            }
        }
        //$result = $text;
        return $result;
    }

    public function get_price($id){
        $result = 0;
        $text = "SELECT product_price, product_persentase_profit FROM t_product WHERE product_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){              
            $row = mysql_fetch_assoc($query);
            $result = $row['product_price'] + ($row['product_price'] * $row['product_persentase_profit'] / 100);              
                         
        }
        //$result = $text;
        return $result;
    }




//END FUNCTION FOR ADMIN PAGE

}

?>