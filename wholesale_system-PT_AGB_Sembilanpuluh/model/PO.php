<?php
class PO{

	private $table = "t_po";
	private $itemPerPageAdmin = 10;
    private $itemPerPageAdminOrder = 20;
    private $itemPerPageClient = 10;
//START FUNCTION FOR CLIENT PAGE
    public function get_order_po($id){
        $result = 0;

        $text = "SELECT address_ID, address_street1 AS po_addressStreet1, address_street2 AS po_addressStreet2, address_city AS po_addressCity, address_state AS po_addressState, address_country AS po_addressCountry, address_zip AS po_addressZip ,po_ID, po_name, po_date, po_isPaid, po_status, po_note, po_resi, po_amount, po_service, po_totalAmount, po_shippingCost, po_note, (SELECT COUNT(atpp_ID) FROM AT_PO_PRODUCT WHERE atpp_poID = '$id') AS counter_product, customer_ID, customer_fname, customer_phone, customer_email, confirm_ID, confirm_status, confirm_amount FROM $this->table LEFT JOIN T_CUSTOMER ON po_custID = customer_ID LEFT JOIN T_CONFIRM ON confirm_poID = po_ID LEFT JOIN T_ADDRESS ON po_addressID = address_ID WHERE po_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price, atpp_weight, product_weight FROM at_po_product LEFT JOIN t_product ON atpp_productID = product_ID WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }

     public function get_sale_month($month){
        $result = 0;

        $text = "SELECT address_ID, address_street1 AS po_addressStreet1, address_street2 AS po_addressStreet2, address_city AS po_addressCity, address_state AS po_addressState, address_country AS po_addressCountry, address_zip AS po_addressZip ,po_ID, po_name, po_date, po_isPaid, po_status, po_note, po_resi, po_amount, po_service, po_totalAmount, po_shippingCost, po_note, (SELECT COUNT(atpp_ID) FROM AT_PO_PRODUCT WHERE atpp_poID = '$id') AS counter_product, customer_ID, customer_fname, customer_phone, customer_email, confirm_ID, confirm_status, confirm_amount FROM $this->table LEFT JOIN T_CUSTOMER ON po_custID = customer_ID LEFT JOIN T_CONFIRM ON confirm_poID = po_ID LEFT JOIN T_ADDRESS ON po_addressID = address_ID WHERE po_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price, atpp_weight, product_weight FROM at_po_product LEFT JOIN t_product ON atpp_productID = product_ID WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }


    public function get_order_customer($id, $page=1){
        $result = 0;    

        //get total data
        $text_total = "SELECT * FROM t_po LEFT JOIN t_confirm ON confirm_poID = po_ID AND confirm_customerID = po_custID LEFT JOIN t_customer ON customer_ID = po_custID WHERE po_custID = '$id'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageClient);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageClient;
        }

        $text = "SELECT * FROM t_po LEFT JOIN t_confirm ON confirm_poID = po_ID AND confirm_customerID = po_custID LEFT JOIN t_customer ON customer_ID = po_custID WHERE po_custID = '$id' ORDER BY po_date DESC LIMIT $limitBefore, $this->itemPerPageClient";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }

        return $result;
    }
    //function for set as paid/not in module/order/edit.php
    
    public function get_po_order($customer_id){
        $result = 0;

        $text = "SELECT * FROM t_po WHERE po_status = '1' AND po_custID = '$customer_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        return $result;
    }
    public function get_by_po($po_name){
        $result = 0;

        $text = "SELECT * FROM t_po WHERE po_name = '$po_name'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        return $result;
    }
    public function get_product_by_idpo($po_id){
        $result = 0;

        $text = "SELECT atpp_ID, atpp_qty, atpp_weight, atpp_price, product_ID, product_name FROM `at_po_product LEFT JOIN t_product ON atpp_productID = product_ID WHERE atpp_poID = '$po_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }

        return $result;
    }

    public function generate_po_code(){
        $result = 0;
        
        $rand = strtoupper(substr(md5(microtime()),rand(0,26),4));

        $text = "SELECT po_ID FROM t_po";
        $query = mysql_query($text);
        if($query){
            $total_data = mysql_num_rows($query);
            if($total_data > 0){
                $num = $total_data + 1;
                $result = "PO/".$rand."/".$num;
            } else {
                $result = "PO/".$rand."/1";
            }
        } else {
            $result = "PO/".$rand."/1";
        }
        return $result;
    }
   
    public function create_atpo($po_ID, $product_ID, $product_qty, $subtotal){
        $result = 0;
        $text = "INSERT INTO at_po_product (atpp_poID, atpp_productID, atpp_qty, atpp_price) VALUES('$po_ID', '$product_ID', '$product_qty', '$subtotal')";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function create_po($po_name, $po_custID, $term, $pay_term){
        $result = 0;        
        $text = "INSERT INTO $this->table (po_name, po_custID, po_term, po_pay_term, po_status, po_date) 
        VALUES('$po_name', '$po_custID', '$term', '$pay_term', '1', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }

    public function add_total($id, $total){
        $result = 0;
        $text = "UPDATE $this->table SET po_totalAmount = '$total' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }


    public function get_price($id){
        $result = 0;
        $text = "SELECT product_price, product_persentase_profit FROM t_product WHERE product_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){              
            $row = mysql_fetch_assoc($query);
            $result = $row['product_price'] + ($row['product_price'] * $row['product_persentase_profit'] / 100);              
                         
        }
        //$result = $text;
        return $result;
    }

//START FUNCTION FOR ADMIN PAGE

    //function for get data order in module/order/edit.php
    public function get_order_detail($id){
        $result = 0;

        $text = "SELECT po_ID, po_name, po_date,  po_status, po_term, po_pay_term, po_totalAmount, (SELECT COUNT(atpp_ID) FROM at_po_product WHERE atpp_poID = '$id') AS counter_product, customer_ID, customer_name, customer_phone, customer_email, 
        customer_company, customer_address, customer_city, customer_state, customer_country, customer_zip,
        bill_of_lading, pengiriman_fob, pengiriman_packing, pengiriman_date,
        payment_LC, payment_applicant, payment_bank
        FROM $this->table LEFT JOIN t_customer ON po_custID = customer_ID
        LEFT JOIN t_pengiriman ON  pengiriman_ref_po = po_ID
        LEFT JOIN t_payment ON  pay_rev_po_id = po_ID
        WHERE po_ID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;
+
                $text_child = "SELECT * FROM at_po_product LEFT JOIN t_product ON atpp_productID = product_ID WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][$loop2] = $row_child;
                        $result[$loop][$loop2]['product_sell'] = $this->get_price($row_child['atpp_productID']);
                        $loop2++ ;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }

    public function update_po_status($id, $status){
        $result = 0;      
        $text = "UPDATE $this->table SET po_status = '$status' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
  
    //function for set as paid/not in module/order/edit.php
    public function update_paid($id, $paid){
        $result = 0;

        if($paid == 1){
            $po_status = 3 ;
        }
        else{
            $po_status = 2 ;
        }

        $text = "UPDATE $this->table SET po_isPaid = '$paid', po_status = $po_status, po_resi='0'  WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function for cancel order in module/order/edit.php
    public function cancel_order($id){
        $result = 0;

        $text = "UPDATE $this->table SET po_status = 6 , po_resi='0' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    
    //function for update note in module/order/edit.php
    public function update_note($id, $note){
        $result = 0;

        $text = "UPDATE $this->table SET po_note='$note' WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //function for get data order in module/order/index.php
    public function update_resi($id, $resi, $status){
        $result = 0;

        $text = "UPDATE $this->table SET po_resi='$resi', po_status= $status WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    public function get_order_history($page=1){
        $result = 0;    

        //get total data
        $text_total = "SELECT Po_ID FROM t_po LEFT JOIN t_payment ON pay_rev_po_id = po_ID  LEFT JOIN t_customer ON customer_ID = po_custID";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdminOrder);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdminOrder;
        }

        $text = "SELECT * FROM t_po LEFT JOIN t_payment ON pay_rev_po_id = po_ID  LEFT JOIN t_customer ON customer_ID = po_custID ORDER BY po_date DESC LIMIT $limitBefore, $this->itemPerPageAdminOrder";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }

        return $result;
    }

    //function for get data order in module/customer/edit.php
	public function get_po_by_customer($page=1, $cust_id){
        $result = 0;    

        //get total data
        $text_total = "SELECT po_ID FROM $this->table WHERE po_custID = '$cust_id'";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM $this->table WHERE po_custID = '$cust_id' ORDER BY UNIX_TIMESTAMP(po_modifyDate) DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    //function for get data order in module/customer/order.php
    public function get_data_order($id){
        $result = 0;

        $text = "SELECT po_ID, po_name, po_terms, po_date, po_status, po_note, po_resi, po_amount, po_totalAmount, po_shippingCost, 
            po_addressName, po_addressType, po_addressStreet1, po_addressStreet2, po_addressCity, po_addressState, po_addressZip,
            (SELECT COUNT(atpp_ID) FROM at_po_product WHERE atpp_poID = '$id') AS counter_product, po_addressCountry, customer_ID,
            customer_fname, customer_phone, customer_email FROM $this->table LEFT JOIN t_customer ON po_custID = customer_ID 
            WHERE po_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                $text_child = "SELECT atpp_productID, atpp_productName, atpp_qty, atpp_price FROM at_po_product WHERE 
                    atpp_poID = '$id' ORDER BY UNIX_TIMESTAMP(atpp_modifyDate) DESC";
                $query_child = mysql_query($text_child);
                if(mysql_num_rows($query_child) >= 1){
                    while($row_child = mysql_fetch_assoc($query_child)){
                        $result[$loop][] = $row_child;
                    }
                }
                $loop++;
            }
        }    
        return $result;
    }   
//END FUNCTION FOR ADMIN PAGE
}
?>