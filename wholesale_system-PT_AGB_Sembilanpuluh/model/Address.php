<?php

class Address{
	private $table = "T_ADDRESS";
	private $itemPerPageAdmin = 10;

//START FUNCTION FOR ADMIN PAGE

    //function for get data address in module/customer/address.php

    public function get_data_address($cust_id){
        $result = 0;
        $text = "SELECT customer_ID, customer_fname, address_ID, address_name, address_type, address_street1, address_street2, address_cityID,
            address_city, address_zip, address_country, address_stateID, address_state FROM $this->table LEFT JOIN T_CUSTOMER ON
            address_customerID = customer_ID WHERE address_customerID = '$cust_id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }



    public function insert_data($cust_id, $name, $address1, $address2, $cityid, $city, $state, $stateid, $zip, $country){

        $result = 0;



        $text = "INSERT INTO $this->table (address_customerID, address_name, address_street1, address_street2, address_cityID, address_city, address_state, address_stateID, address_zip, address_country) VALUES('$cust_id', '$name', '$address1', '$address2', '$cityid', '$city', '$state', '$stateid', '$zip', '$country')";

        $query = mysql_query($text);

        if($query){

            $result = 1;

        }

        return $result;

    }



    public function update_data($id, $name, $address1, $address2, $cityid, $city, $state, $stateid, $zip, $country){

        $result = 0;



        $text = "UPDATE $this->table SET address_name = '$name', address_street1 = '$address1', address_street2 = '$address2', address_cityID = '$cityid', address_city = '$city', address_state = '$state', address_stateID = '$stateid', address_zip = '$zip', address_country = '$country' WHERE address_ID = '$id'";

        $query = mysql_query($text);

        if(mysql_affected_rows() == 1){

            $result = 1;

        }

        return $result;

    }



    public function delete_data($id){

        $result = 0;
        $text = "DELETE FROM $this->table WHERE address_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

//END FUNCTION FOR ADMIN PAGE

//START FUNCTION FOR CLIENT PAGE
    public function insert_address($cust_id, $name, $address1, $cityid, $city, $state, $stateid, $zip, $country){

        $result = 0;

        $text = "INSERT INTO $this->table (address_customerID, address_name, address_street1, address_cityID, address_city, address_state, address_stateID, address_zip, address_country, address_createDate) VALUES('$cust_id', '$name', '$address1', '$cityid', '$city', '$state', '$stateid', '$zip', '$country', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }

        return $result;

    }
    public function get_address($id){
        $result = 0;
        $text = "SELECT * FROM $this->table WHERE address_customerID = '$id'";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
}

?>