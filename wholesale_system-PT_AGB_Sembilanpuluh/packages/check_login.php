<?php
require_once('../../model/User.php'); 
$obj_user = new User();

if($obj_user->admin_check_login() == 0){
    header("Location:{$path['login']}?notLogin");
}

if(isset($_GET['action']) && $_GET['action'] == 'logout'){
    if($obj_user->admin_logout()){
		header("Location:{$path['login']}?logout");
    }
}
?>