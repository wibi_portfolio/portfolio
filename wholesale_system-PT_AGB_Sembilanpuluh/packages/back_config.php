<?php 
$global['absolute-url'] = "http://localhost/wholesale/";
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['img-url'] = $global['absolute-url'];
$global['absolute-url-admin'] = $global['absolute-url'];
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url-admin']."assets/images/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."images/logo/logo-mobile.png";
$global['logo-desktop'] = $global['absolute-url']."images/logo/logo-desktop.png";
$global['logo-agb']=$global['absolute-url']."assets/images/logo.jpg";
$global['logo-t']=$global['absolute-url']."assets/images/logo_trans.png";

$path['home'] = $global['absolute-url'];
$path['login'] = $global['absolute-url-admin']."index.php";
$path['logout'] = "?action=logout";

$seo['company-name'] = "AGB";
$global['copyright'] = "&copy; AGB ".date('Y')." All rights reserved.";
$global['powered'] = date('Y')." &copy; Powered by <a id='copyright-link' href='' target='_blank'>Informatika 2013 ITI</a>.";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";



// module user
$title['user'] = $seo['company-name']." | Module User Admin";

$path['user'] = $global['absolute-url-admin']."module/user/index.php";
$path['user-edit'] = $global['absolute-url-admin']."module/user/edit.php?id=";
// dashboard
$title['dash'] = $seo['company-name']." | Dashboard";

$path['dash'] = $global['absolute-url-admin']."module/dashboard/index.php";

// module forwarding
$title['forwarding'] = $seo['company-name']." | Module Forwarding";

$path['forwarding'] = $global['absolute-url-admin']."module/forwarding/index.php";
$path['forwarding-edit'] = $global['absolute-url-admin']."module/forwarding/edit.php?id=";

// module farm
$title['farm'] = $seo['company-name']." | Module Farm";

$path['farm'] = $global['absolute-url-admin']."module/farm/index.php";
$path['farm-edit'] = $global['absolute-url-admin']."module/farm/edit.php?id=";
// drop
$title['drop'] = $seo['company-name']." | Discard Barang Masuk";

$path['drop'] = $global['absolute-url-admin']."module/drop/index.php";

// module barang masuk
$title['incoming'] = $seo['company-name']." | Module Barang Masuk";

$path['incoming'] = $global['absolute-url-admin']."module/incoming/index.php";
$path['incoming-edit'] = $global['absolute-url-admin']."module/incoming/edit.php?id=";
$path['incoming-drop'] = $global['absolute-url-admin']."module/incoming/drop.php?id=";

// module customer
$title['customer'] = $seo['company-name']." | Module Customer";

$path['customer'] = $global['absolute-url-admin']."module/customer/index.php";
$path['customer-edit'] = $global['absolute-url-admin']."module/customer/edit.php?id=";
$path['customer-address'] = $global['absolute-url-admin']."module/customer/address.php?id=";
$path['customer-order'] = $global['absolute-url-admin']."module/customer/order.php?id=";
$path['customer-wishlist'] = $global['absolute-url-admin']."module/customer/wishlist.php?id=";

// module category
$title['category'] = $seo['company-name']." | Module Category";

$path['category'] = $global['absolute-url-admin']."module/category/index.php";
$path['category-edit'] = $global['absolute-url-admin']."module/category/edit.php?id=";

// module coupon
$title['coupon'] = $seo['company-name']." | Module Coupon";
$path['coupon'] = $global['absolute-url-admin']."module/coupon/index.php";
$path['coupon-edit'] = $global['absolute-url-admin']."module/coupon/edit.php?id=";


// module product
$title['product'] = $seo['company-name']." | Module Product";

$path['product'] = $global['absolute-url-admin']."module/product/index.php";
$path['product-edit'] = $global['absolute-url-admin']."module/product/edit.php?id=";
$path['product-add'] = $global['absolute-url-admin']."module/product/insert.php";

// module order history
$title['order'] = $seo['company-name']." | Module Order History";

$path['order'] = $global['absolute-url-admin']."module/order/index.php";
$path['order-add'] = $global['absolute-url-admin']."module/order/insert.php";
$path['order-edit'] = $global['absolute-url-admin']."module/order/edit.php?id=";
$path['order-addpo'] = $global['absolute-url-admin']."module/order/insertpo.php?id=";

// module confirmation
$title['confirmation'] = $seo['company-name']." | Module Payment Confirmation";

$path['confirmation'] = $global['absolute-url-admin']."module/confirmation/index.php";

// module social
$title['social'] = $seo['company-name']." | Module Social Media";

$path['social'] = $global['absolute-url-admin']."module/social/index.php";

// module shipping
$title['shipping'] = $seo['company-name']." | Module Shipping";

$path['shipping'] = $global['absolute-url-admin']."module/shipping/index.php";
$path['shipping-edit'] = $global['absolute-url-admin']."module/shipping/edit.php?id=";



// module content
$title['content'] = $seo['company-name']." | Module Content";

$path['content'] = $global['absolute-url-admin']."module/content/index.php";
$path['content-return'] = $global['absolute-url-admin']."module/content/return.php";
$path['content-order'] = $global['absolute-url-admin']."module/content/order.php";
$path['content-faq'] = $global['absolute-url-admin']."module/content/faq.php";
$path['content-email'] = $global['absolute-url-admin']."module/content/email.php";

// module profile
$title['profile'] = $seo['company-name']." | Module Profile";

$path['profile'] = $global['absolute-url-admin']."module/profile/index.php";

// module user
$title['user'] = $seo['company-name']." | Module User Administrator";

$path['user'] = $global['absolute-url-admin']."module/user/index.php";
$path['user-edit'] = $global['absolute-url-admin']."module/user/edit.php?id=";
?>