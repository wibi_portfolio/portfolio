<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1'); 
session_start();
require_once("back_config.php");
require_once("check_input.php");
   function convert_status($status){
        $result = "";
        if($status==6){
            $result='<span class="label label-danger">CANCELED</span>' ;
        }elseif($status == 5){
            $result='<span class="label label-success">COMPLETED</span>' ;
        }elseif($status == 4){
            $result='<span class="label label-success">SHIPPED</span>' ;                                               
        }elseif($status == 3) {
            $result='<span class="label label-info">PAYMENT VERIFIED</span>' ;             
        }elseif($status == 2) {
            $result='<span class="label label-warning">PO APPROVED</span>' ;                            
        }else {
            $result='<span class="label label-default">AWAITING APPROVAL</span>' ;
        } 
        return $result ;                                       
    }

     function convert_role($status){
        $result = "";
        if($status == 5){
            $result='Warehouse Operator' ;
        }elseif($status == 4){
            $result='Sales Operator' ;                                               
        }elseif($status == 3) {
            $result='Sales Manager' ;             
        }elseif($status == 2) {
            $result='Stakeholder' ;                            
        }else {
            $result='Administrator' ;
        } 
        return $result ;                                       
    }

    function check_expired($tgl){
        $result = false ;
        $exp = new DateTime($tgl);
        $now = new DateTime();
        $exp->modify('-6 month');

        $result = ($exp < $now);
        return $result ;
    }
?>