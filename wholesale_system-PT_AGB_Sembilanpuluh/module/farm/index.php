<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_farm.php");
	$curpage="farm";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['farm'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Management Tempat Produksi
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<?php if($_SESSION['user_status'] == 1) { ?>
										<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
											<i class="fa fa-plus"></i> Add New Data
										</a>
										<?php } ?>
									</div>
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th class="text-center">#</th>
											  		<th>Kota</th>
											  		<th>PIC</th>											  		
											  		<th>Phone</th>
											  		<?php if($_SESSION['user_status'] == 1 or $_SESSION['user_status'] == 2) { ?>
											  		<th class="text-center">Action</th>
											  		<?php } ?>
											  	</tr>
											  	<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											  	<tr>
											  		<td class="text-center"><?=$num;?>.</td>
											  		<td><?=$data['farm_kota'];?></td>
											  		<td><?=$data['farm_pic'];?></td>
											  		<td><?=$data['farm_telp'];?></td>
											  		<?php if($_SESSION['user_status'] == 1 or $_SESSION['user_status'] == 2) { ?>											  		
											  		<td>
											  			<div class="text-center">
															<a href="<?=$path['farm-edit'].$data['farm_id'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['farm_id'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														</div>
											  		</td>
											  		<?php } ?>
											  	</tr>
											  	<?php $num++;} } else { ?>
											  	<tr class="warning">
											  		<td colspan="4">There is no data right now!</td>
											  	</tr>
											  	<?php } ?>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add Tempat Produksi</h4>
								</div>
								<form name="addPortfolio" action="index.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>PIC <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-title" name="title" type="text" class="form-control" placeholder="nama pic" />
		                                    	<div id="error-title" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>kota <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-title" name="kota" type="text" class="form-control" placeholder="kota" />
		                                    	<div id="error-title" class="is-error"></div>
		                                    </div>
		                                </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Alamat <span class="symbol required"></span></strong></div>
                                            <div class="col-sm-6 col-xs-12 up1">
                                                <textarea id="input-desc" name="alamat" class="tinymce form-control" rows="5" placeholder="alamat"></textarea>
                                                <div id="error-desc" class="is-error"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Telpon <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <input id="input-title" name="telpon" type="text" class="form-control" placeholder="telpon" />
                                                <div id="error-title" class="is-error"></div>
                                            </div>
                                        </div>

		                                
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>
		<script type="text/javascript">
			function validateForm(){
				var title = $("#input-title").val();
				
				if(title != ""){
					$("#error-title").html("");
					$("#error-title").hide();
					$("#input-title").removeClass("input-error");
				} else {
					$("#error-title").show();
					$("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-title").addClass("input-error");
					return false;
				}
			}
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>