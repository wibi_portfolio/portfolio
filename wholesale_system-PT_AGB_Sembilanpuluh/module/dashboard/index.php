<?php 
	include("../../packages/require.php");
	include("../../packages/productFunction.php");//RECURSIVE CATEGORY
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_dashboard.php");
	$curpage="dashboard";
	$page_name = "index.php";

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['dash'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Dashboard
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light ">
										<?php if($_SESSION['user_status'] != 4 and $_SESSION['user_status'] != 5) { ?>
										<a class="btn btn-azure" >
											<i class="fa fa-check-square-o"></i>  <?=$approve[0]['TotalPo'];?> PO Need Approval
										</a>

										<a class="btn btn-dark-azure" >
											<i class="fa fa fa-credit-card"></i>  <?=$verify[0]['TotalPo'];?> PO Need Verification
										</a>
										<?php } ?>

										<?php if($_SESSION['user_status'] != 3 and $_SESSION['user_status'] != 5) { ?>
										<a class="btn btn-dark-green" >
											<i class="fa fa-ship"></i>  <?=$ship[0]['TotalPo'];?> PO Ready to Ship
										</a>
										<?php } ?>

										<?php if($_SESSION['user_status'] != 4 and $_SESSION['user_status'] != 3) { ?>
										<a class="btn btn-dark-blue" >
											<i class="fa fa-exclamation-triangle"></i>  <?=$exp[0]['TotalPo'];?> Products Below Six Month Expired
										</a>
										<?php } ?>


									</div>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													
												</div>
											</div>
										</div>
									
									<div class="table-responsive">									
											<?php 
												$cat="";
												if(is_array($datas)) { 
												foreach($datas as $data) { 
												$stok[]= $data['product_stok'] ;
												$cat .="'".$data['product_name']."'," ;	} }

												$bln=""; if(is_array($month_sales)) { 
												foreach($month_sales as $data) { 
												$jual[]= $data['TotalSales'] ;
												$bln .="'".$data['SalesMonth']."'," ;	} }

												$prod=""; if(is_array($produk_sales)) { 
												foreach($produk_sales as $data) { 
												$qty[]= $data['TotalSales'] ;
												$prod .="'".$data['product_name']."<br>(".$data['product_satuan'].")'," ;	} }

												$year=""; if(is_array($year_sales)) { 
												foreach($year_sales as $data) { 
												$sell[]= $data['TotalSales'] ;
												$year .="'".$data['SalesYear']."'," ;	} }
											?>												
									</div>
										<?php if($_SESSION['user_status'] == 5) { $hid = "hidden";}?>
										<div class="row <?=$hid;?>">
											<div class="col-md-12">
											<div class="col-md-6 col-sm-12" id="tahun" style="height:400px;"></div>											
											<div class="col-md-6 col-sm-12" id="penjualan" style="height:400px;"></div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
											<div class="col-md-6 col-sm-12" id="produk_sales" style="height:400px;"></div>
											<div class="col-md-6 col-sm-12" id="coba" style="height:400px;"></div>											
											</div>
										</div>

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Product : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no product right now!
											</div>
										</div>
									</div>
									<?php }?>
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/highchart/highcharts.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});

					$(function () { 
					    var myChart = Highcharts.chart('coba', {
					        chart: {
					            type: 'column'					            
					        },
					        title: {
					            text: 'Stok Produk'
					        },
					          xAxis: {
           					 categories: [<?php echo $cat ?>]
       						 },
       						 yAxis: {
           					 title: "Stok Produk"
       						 },
					        
					        series: [{
					        	name : "Stok",
					            data: [<?php echo join($stok, ',') ?>],
         						pointStart: 0,
         						color: '#FF00FF'
         						
					        }]
					    });
					});

						$(function () { 
					    var myChart = Highcharts.chart('penjualan', {
					        chart: {
					            type: 'column'
					        },
					        title: {
					            text: 'Penjualan Tahun <?=Date("Y");?> (USD)'
					        },
					          xAxis: {
           					 categories: [<?php echo $bln ?>]
       						 },
       						 yAxis: {
           					 title: "Penjualan per Bulan"
       						 },
					        
					        series: [{
					        	name : "Penjualan",
					            data: [<?php echo join($jual, ',') ?>],
         						pointStart: 0,
         						color : '#00e6e6'
         						
					        }]
					    });
					});

				$(function () { 
					    var myChart = Highcharts.chart('produk_sales', {
					        chart: {
					            type: 'column'
					        },
					        title: {
					            text: 'Penjualan Produk'
					        },
					          xAxis: {
           					 categories: [<?php echo $prod ?>]
       						 },
       						 yAxis: {
           					 title: "Produk"
       						 },
					        
					        series: [{
					        	name : "Quantity",
					            data: [<?php echo join($qty, ',') ?>],
         						pointStart: 0,
         						color : '#884dff'
         						
					        }]
					    });
				});

			$(function () { 
					    var myChart = Highcharts.chart('tahun', {
					        chart: {
					            type: 'column'
					        },
					        title: {
					            text: 'Penjualan Per Tahun (USD)'
					        },
					          xAxis: {
           					 categories: [<?php echo $year ?>]
       						 },
       						 yAxis: {
           					 title: "Tahun"
       						 },
					        
					        series: [{
					        	name : "Penjualan",
					            data: [<?php echo join($sell, ',') ?>],
         						pointStart: 0,
         						color : '#0099cc'
         						
					        }]
					    });
				});

			});

		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

					//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>