<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_order.php");
$curpage="order";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['order'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									<img class="img-responsive" src="<?=$global['logo-agb'];?>">
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['order'];?>">
										Add New Purchase Order
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">ADD PURCHASE ORDER</span></h4>
								</div>
								<form name="updateProfile" action="insert.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left">Customer Name <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="name" type="text" class="form-control" placeholder="customer name"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left">Company Name <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company" name="company_name" type="text" class="form-control" placeholder="company name"  />
		                                    				<div id="error-company" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up3">
															<div class="form-label bold text-left">Address <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<input id="input-address1" name="address1" type="text" class="form-control"  placeholder="address *" />
		                                    				<div id="error-address1" class="is-error"></div>
														</div>
													</div>
												
													<div class="row">
														<div class="col-xs-12 up1">
															<input id="input-city" name="city" type="text" class="form-control" placeholder="city *" />
		                                    				<div id="error-city" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-province" name="province" type="text" class="form-control" placeholder="province *"  />
		                                    				<div id="error-province" class="is-error"></div>
														</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-zip" name="zip" type="text" class="form-control" placeholder="zip code *" />
		                                    				<div id="error-zip" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-country" name="country" type="text" class="form-control" placeholder="country *"  />
		                                    				<div id="error-country" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up3">
															<div class="form-label bold text-left">Email Address <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-email1" name="email1" type="text" class="form-control" placeholder="email address *" />
		                                    				<div id="error-email1" class="is-error"></div>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> E-mail to receive contact form and order</span></small>
														</div>
													</div>
													
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Phone Number <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-phone1" name="phone1" type="text" class="form-control" placeholder="phone number *" />
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class="panel-heading border-light">
														<h4 class="panel-title">PO Data</span></h4>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Term <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-phone1" name="term" type="text" class="form-control" placeholder="term" />
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Payment Term <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<select class="form-control" id="payterm" name="pay_term">
																<option value="At Sight L/C">At Sight L/C</option>
																<option value="Restricted L/C">Restricted L/C</option>
																<option value="Red Clause L/C">Red Clause L/C</option>
																<option value="Transferable L/C">Transferable L/C</option>
															</select>
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">FOB <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-phone1" name="fob" type="text" class="form-control" placeholder="nama pelabuhan" />
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Packing <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<select class="form-control" id="fow" name="pack">																
																<option value="Wood Container">Wood Container</option>
																<option value="Plastic Container">Plastic Container</option>																
															</select>
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Jasa Forwarding <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<select class="form-control" id="fow" name="fow">
																<?php if(is_array($forwards)) { $num=1;foreach($forwards as $data) { ?>
																<option value="<?=$data['id_forwarding'];?>"><?=$data['forwarding_name'];?></option>
																<?php }}?>
															</select>
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<div class ="row">							
														<div class="col-xs-6 up15">
															<div class="form-label bold text-left">
															Barang <span class="symbol required"></div>
														</div>
														<div class="col-xs-6 up15">
															<div class="form-label bold text-left">
															Kuantitas <span class="symbol required"></div>
														</div>
														
													</div>
													<div class="row"> 
														
														<div class="col-sm-12 col-xs-12 up1">
															<div id="productField" class="input_fields_wrap">
														
																<div class="col-sm-6 col-xs-12 up1">
																<select class="form-control" id="sel1" name="barang[]">
																   <?=$products;?>
															  	</select>
															  	</div>
															  	<div class="col-sm-4 col-xs-12 up1">
															  		<input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty">
															  	</div>
															  	
															  	<br><br><br>							
															</div>								
														</div>						
													</div>					
												
												

												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['order'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="button" class="btn btn-info add_field_button">
														<i class="fa fa-plus"></i> Add Product 
													</button>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Insert
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>

	<script type="text/javascript">
			$(document).ready(function() {

				 	var max_fields      = 10; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				    var select			= '<div> <div class="col-sm-6 col-xs-12 up1"> <select class="form-control" id="sel1" name="barang[]">';
					var pilihan			= " <?=$products;?> </select> </div>";
				    var qty 			= '<div class="col-sm-6 col-xs-12 up1"><input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty"></div>';
				    var hapus			= '<a href="#" class="remove_field">Remove</a> </div> ';
				   
				    
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				        e.preventDefault();
				        if(x < max_fields){ //max input box allowed
				            x++; //text box increment
				            $(wrapper).append(select+pilihan+qty+hapus); //add input box
				        }
				    });
				    
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
			});
		</script>
	<script type="text/javascript">
			function validateForm(){
				var company_name = $("#input-company_name").val();
				var address1 = $("#input-address1").val();
				var province = $("#input-province").val();
				var city = $("#input-city").val();
				var zip = $("#input-zip").val();
				var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				if(address1 != ""){
					$("#error-address1").html("");
					$("#error-address1").hide();
					$("#input-address1").removeClass("input-error");
				} else {
					$("#error-address1").show();
					$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-address1").addClass("input-error");
					return false;
				}
				if(city != ""){
					$("#error-city").html("");
					$("#error-city").hide();
					$("#input-city").removeClass("input-error");
				} else {
					$("#error-city").show();
					$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-city").addClass("input-error");
					return false;
				}
				if(province != ""){
					$("#error-province").html("");
					$("#error-province").hide();
					$("#input-province").removeClass("input-error");
				} else {
					$("#error-province").show();
					$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-province").addClass("input-error");
					return false;
				}
				if(zip != ""){
					$("#error-zip").html("");
					$("#error-zip").hide();
					$("#input-zip").removeClass("input-error");
				} else {
					$("#error-zip").show();
					$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-zip").addClass("input-error");
					return false;
				}
				if(country != ""){
					$("#error-country").html("");
					$("#error-country").hide();
					$("#input-country").removeClass("input-error");
				} else {
					$("#error-country").show();
					$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-country").addClass("input-error");
					return false;
				}
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

		</script>

	</body>
	<!-- end: BODY -->
	</html>