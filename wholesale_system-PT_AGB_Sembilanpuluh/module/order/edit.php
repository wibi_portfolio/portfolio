<?php 
include("../../packages/require.php");
include("../../controller/controller_order_edit.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php

$curpage="order";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['order'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY --> 
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									<img class="img-responsive" src="<?=$global['logo-agb'];?>">
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['order'];?>">
										Purchase Order Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['customer_name'];?>'s PO #<?=$O_id;?> Detail
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<div class="row">
										<div class="col-sm-3 col-xs-12 hidden-xs">
											<a href="<?=$path['order'];?>" type="reset" class="btn btn-default">
												<i class="fa fa-arrow-circle-left"></i> Back
											</a>
										</div>
										<div class="col-sm-6 col-xs-12 text-center">
											<h4 class="panel-title bold"><?=$datas[0]['customer_name'];?>'s Purchase Order Detail</h3>
										</div>
									</div>
								</div>
								<form name="editCustomer" action="edit.php?action=edit" enctype="multipart/form-data" method="post" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row up2">
						                        <div class="col-xs-12 text-center">
						                            <h4 class="hidden-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h4>
						                            <h5 class="visible-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h5>
						                        </div>
						                    </div>

						                    <div class="row up2">
                            					<div class="col-xs-12">
                            						<h5 class="up15 bold">Status: <?=convert_status($datas[0]['po_status']);?>  </h5>      
                                					
                                                    <h5 class="up15 bold">
                                                    
                                                    </h5>
                                                </div>
                    						</div>

                    						<div class="row up3">
                    							<div class="col-sm-6 col-xs-12">
                    								<div class="thumbnail">
	                    								<div class="well bold">Customer Data</div>
	                    								<div style="padding: 0 20px 5px 20px;">
		                    								<div><span class="bold">Full Name</span> : <?=$datas[0]['customer_name'];?></div>
		                    								<div class="up1"><span class="bold">Company</span> : <?=$datas[0]['customer_company'];?></div>
		                    								<div class="up1"><span class="bold">Phone</span> : <?=$datas[0]['customer_phone'];?></div>
		                    								<div class="up1"><span class="bold">E-Mail</span> : <?=$datas[0]['customer_email'];?></div>
	                    									<div class="up1"><span class="bold">Address</span> :</div>
		                    								<address style="padding: 0 20px 5px 20px;margin: 0;">
		                                                        <?=$datas[0]['customer_address'];?><br>	                                                        
		                                                        <?=$datas[0]['customer_city'].". ".$datas[0]['customer_state'].".";?><br>
		                                                        <?=$datas[0]['customer_country'].". ".$datas[0]['customer_zip'].".";?>
		                                                  	</address>
	                    								</div>                 								
		                    								
	                                                  
                    								</div>
                    							</div>
                    							<div class="col-sm-6 col-xs-12">
                    								<div class="thumbnail">
	                    								<div class="well bold">Purchase Order Detail</div>
	                    								<div style="padding: 0 20px 20px 20px;">
		                    								<div><span class="bold">Term </span> : <?=$datas[0]['po_term'];?></div>
		                    								<div class="up1"><span class="bold">Payment Term</span> : <?=$datas[0]['po_pay_term'];?></div>
		                    								<div class="up1"><span class="bold">FOB</span> : <?=$datas[0]['pengiriman_fob'];?></div>
		                    								<div class="up1"><span class="bold">Packing</span> : <?=$datas[0]['pengiriman_packing'];?></div>
		                    								<?php if($datas[0]['payment_LC'] != null && $datas[0]['payment_LC'] != ""){ ?>
		                    								<div class="up1"><span class="bold">L/C No</span> : <?=$datas[0]['payment_LC'];?></div>
		                    								<?php } ?>
		                    								<?php if($datas[0]['payment_bank'] != null && $datas[0]['payment_bank'] != ""){ ?>
		                    								<div class="up1"><span class="bold">Issuing Bank</span> : <?=$datas[0]['payment_bank'];?></div>
		                    								<?php } ?>
		                    								<?php if($datas[0]['payment_applicant'] != null && $datas[0]['payment_applicant'] != ""){ ?>
		                    								<div class="up1"><span class="bold">LC Applicant</span> : <?=$datas[0]['payment_applicant'];?></div>
		                    								<?php } ?>
	                    									
	                    								</div> 
                                                  	</div>
                    							</div>
                    						</div>
                    						
                    						<?php if($datas[0]['counter_product'] != 0){ ?>
											<div style="text-align:right;margin: 30px 0 10px 0;">
												Total Product : <span class="label label-info"><?=$datas[0]['counter_product'];?></span>
											</div>
											<div class="table-responsive">
												<table class="table table-striped table-bordered">
												  	<tr>
												  		<th>#</th>
												  		<th>Product ID</th>
												  		<th>Product Name</th>
												  		<th>QTY</th>
												  		<th>Price (USD)</th>
												  		<th>Line Total (USD)</th>
												  	</tr>
												  	<?php $total_weight="";$num=1; for($i=0; $i<$datas[0]['counter_product']; $i++){?>
												  	<tr class="info">
												  		<td><?=$num;?></td>
												  		<td>#<?=$datas[0][$i]['atpp_productID'];?></td>
												  		<td><?=$datas[0][$i]['product_code']." - ".$datas[0][$i]['product_name'];?></td>
												  		<td><?=$datas[0][$i]['atpp_qty']." ".$datas[0][$i]['product_satuan'];?></td>
												  		<td> <?=number_format(floatval($datas[0][$i]['product_sell']),2,'.',',')." / ".$datas[0][$i]['product_satuan'];?></td>
												  		<td class="bold"> <?=number_format(floatval($datas[0][$i]['atpp_price']),2,'.',',');?></td>
												  		<?php $total_weight += ($datas[0][$i]['atpp_qty']);?>
												  	</tr>
												  	<?php $num++;}?>
												  <!--
												  	<tr>
												  		<td colspan="5" class="bold text-right">Total Weight </td>
												  		<td class="bold"> <?=$total_weight." Kg";?></td>
												  	</tr>
												  	-->
												  	<tr class="success">
												  		<td colspan="5" class="bold text-right">Total Amount</td>
												  		<td class="bold">USD <?=number_format(floatval($datas[0]['po_totalAmount']),2,'.',',');?></td>
												  	</tr>
												  
												</table>
											
											</div>
											<?php } ?>
										</div>
									</div>
									<div class="panel-footer up3">
										<div class="row">
											<input type="hidden" id="po_id" name="PO_ID" value="<?=$datas[0]['po_ID'];?>">
											<input type="hidden" id="po_name" name="pO_name" value="<?=$datas[0]['po_name'];?>">
											
											<input type="hidden" id="customer_id" name="customer_ID" value="<?=$datas[0]['customer_ID'];?>">
											<div class="col-xs-12 pad0 text-right">

												<div class="btn-group hidden-xs ">

													<!--Cancel order hanya bisa dilakukan pada status awaiting payment (1) / verify payment(2) -->
													<?php if(($datas[0]['po_status'] == 2 or $datas[0]['po_status'] == 1) and $_SESSION['user_status'] != 4 ){ ?>
													<a href="javascript:void(0)" class="btn btn-danger" onclick="cancelOrder();">
														<i class="fa fa-remove fa fa-white"></i> Cancel Order
													</a>
													<?php } ?>
													<?php if($datas[0]['po_status'] == 3 and $_SESSION['user_status'] != 3){ ?>
													<a href="#panel-resi" class="btn btn-info" data-toggle="modal" data-target="#panel-resi">
														<i class="fa fa-ship"></i> Bill of Lading
													</a>
													<?php } ?>
													<?php if($datas[0]['po_status'] == 4){ ?>
													<a href="<?=$path['shipping-edit'].$datas[0]['po_ID']."&po=".$datas[0]['po_name'];?>" class="btn btn-info">
														<i class="fa fa-ship"></i> View Shipping
													</a>
													<?php } ?>
													<?php if($datas[0]['po_status'] == 2 and $_SESSION['user_status'] != 4){ ?>
													<a href="#panel-note" class="btn btn-primary" data-toggle="modal" data-target="#panel-note">
														<i class="fa fa-money"></i> Verify payment														
													</a>
													<?php } ?>												
													<?php if($datas[0]['po_status'] == 1 and $_SESSION['user_status'] != 4){ ?>
													<a href="javascript:void(0)" class="btn btn-success" onclick="approve();">
														<i class="fa fa-thumbs-o-up fa fa-white"></i> Approve PO
													</a>											
													<?php } ?>

												</div>
												
												<div class="btn-group-vertical visible-xs">

													<!--Cancel order hanya bisa dilakukan pada status awaiting payment (1) / verify payment(2) -->
													<?php if($datas[0]['po_status'] == 2 or $datas[0]['po_status'] == 1){ ?>
													<a href="javascript:void(0)" class="btn btn-danger" onclick="cancelOrder();">
														<i class="fa fa-remove fa fa-white"></i> Cancel Order
													</a>
													<?php } ?>

													<a href="#panel-resi" class="btn btn-info" data-toggle="modal" data-target="#panel-resi">
														<i class="fa fa-ship"></i> Bill of Lading
													</a>

													<a href="#panel-note" class="btn btn-primary" data-toggle="modal" data-target="#panel-note">
														<i class="fa fa-money"></i> Verify payment														
													</a>

													<!--jika po_status = 5 /completed maka button PAID-NOT PAID akan di hidden -->
													
														<a href="javascript:void(0)" class="btn btn-success" onclick="approve();">
															<i class="fa fa-thumbs-o-up fa fa-white"></i> Approve PO
														</a>
														
													
													
													
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data order in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->

			<!-- start: PANEL Bill of Lading MODAL FORM -->
				<div class="modal fade" id="panel-resi" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Input Bill of Lading</h4>
							</div>
							<form name="addResi" action="edit.php?action=ship" enctype="multipart/form-data" method="post" onsubmit="return validateResi();" >
								<div class="modal-body">
									<div class="row">
										<div class="col-sm-4 col-xs-12 up1"><strong>Bill of Lading  <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="PO_resi_modal" type="text" name="bol" class="form-control" placeholder="bill of lading" />
											<div id="error-resi" class="is-error"></div>
											<input type="hidden" name="PO_ID" value="<?=$datas[0]['po_ID'];?>" />
											<input type="hidden" name="PO_name" value="<?=$datas[0]['po_name'];?>" />
											<input type="hidden" name="Cust_name" value="<?=$datas[0]['customer_fname'];?>" />
											<input type="hidden" name="Cust_email" value="<?=$datas[0]['customer_email'];?>" />
										</div>
										<div class="col-sm-4 col-xs-12 up1"><strong>Charge <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="PO_resi_cas" type="text" name="cas" class="form-control" placeholder="charge" />
																						
										</div>
									</div>
								</div>
								<div class="modal-footer f5-bg">
									<div class="btn-group">
										<button type="reset" class="btn btn-default" data-dismiss="modal">
											<i class="fa fa-times"></i> Cancel
										</button>
										<button type="submit" class="btn btn-success">
											<i class="fa fa-check fa fa-white"></i> Confirm Bill of Lading
										</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL RESI MODAL FORM -->

				<!-- start: PANEL NOTE MODAL FORM -->
				<div class="modal fade" id="panel-note" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Verify Payment</h4>
							</div>
							<form name="addResi" action="edit.php?action=payment" enctype="multipart/form-data" method="post" onsubmit="return validateNote();" >
								<div class="modal-body">
									<div class="row">
										<div class="col-sm-4 col-xs-12 up1"><strong>L/C No <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="no_lc" type="text" name="lc" class="form-control" placeholder="no lc" />
											<div id="error-note" class="is-error"></div>
											<input type="hidden" name="PO_ID" value="<?=$datas[0]['po_ID'];?>" />
											<input type="hidden" name="customer_ID" value="<?=$datas[0]['customer_ID'];?>" />
										</div>
										<div class="col-sm-4 col-xs-12 up1"><strong>Issuing Bank <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="no_lc" type="text" name="bank" class="form-control" placeholder="issuing bank" />
											<div id="error-note" class="is-error"></div>											
										</div>
										<div class="col-sm-4 col-xs-12 up1"><strong>Nama Applicant <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="no_lc" type="text" name="applicant" class="form-control" placeholder="applicant name" />
											<div id="error-note" class="is-error"></div>											
										</div>
										<div class="col-sm-4 col-xs-12 up1"><strong>Date Issued <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="conf_date" type="date" name="date_issued" class="form-control" placeholder="applicant name" />
											<div id="error-note" class="is-error"></div>											
										</div>
									</div>
								</div>
								<div class="modal-footer f5-bg">
									<div class="btn-group">
										<button type="reset" class="btn btn-default" data-dismiss="modal">
											<i class="fa fa-times"></i> Cancel
										</button>
										<button type="submit" class="btn btn-success">
											<i class="fa fa-check fa fa-white"></i> Submit
										</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL NOTE MODAL FORM -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">

		function transferPOid(x){
			var currentPOid = $(x).attr("data-poid");
			var currentPOresi = $(x).attr("data-poresi");
			var currentPOName = $(x).attr("data-poName");
			var currentCustName = $(x).attr("data-custName");
			var currentCustEmail = $(x).attr("data-custEmail");
			$('#PO_ID_modal').val(currentPOid);
			$('#PO_resi_modal').val(currentPOresi);
			$('#PO_name_modal').val(currentPOName);
			$('#Cust_name_modal').val(currentCustName);
			$('#Cust_email_modal').val(currentCustEmail);			
			
				
			<?php if($datas[0]['po_status'] == 3 or $datas[0]['po_status'] == 4 ) {	?>		
			$('#PO_resi_modal').attr("disabled", false);
			$('#btn-submit').attr("disabled", false);
			<?php } else { ?>
			$('#PO_resi_modal').attr("disabled", true);
			<?php } ?>			     
		}

		function validateResi(){
                var resi = $("#PO_resi_modal").val();
                
                if(resi != ""){
                    $("#error-resi").html("");
                    $("#error-resi").hide();
                    $("#PO_resi_modal").removeClass("input-error");
                } else {
                    $("#error-resi").show();
                    $("#error-resi").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#PO_resi_modal").addClass("input-error");
                    return false;
                }
            }
        function validateNote(){
                var note = $("#input-note").val();
                
                if(note != ""){
                    $("#error-note").html("");
                    $("#error-note").hide();
                    $("#input-note").removeClass("input-error");
                } else {
                    $("#error-note").show();
                    $("#error-note").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-note").addClass("input-error");
                    return false;
                }
            }
		//function confirmation cancel order
		function cancelOrder(){
			var po_id = $("#po_id").val();
			var po_name = $("#po_name").val();
			swal({
				title: "Are you sure?",
				text: "you want to cancel order "+po_name+"",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Cancel Order ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "edit.php?action=cancel&po_id="+po_id;
				} else {
	                   //nothing
	               }
	           });
		}
		//function confirmation set as paid
		function approve(){
			var po_id = $("#po_id").val();
			var po_name = $("#po_name").val();			
			var cust_id = $("#customer_id").val();
			swal({
				title: "Are you sure ?",
				text: "Approve Purchase Order with Code "+po_name+" ",
				type: "success",
				showCancelButton: true,
				confirmButtonColor: "#ec971f",
				confirmButtonText: "Yes, Approve ",
				cancelButtonText: "Cancel",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "edit.php?action=approve&po_id="+po_id+"&customer_id="+cust_id;
				} else {
	                   //nothing
	               }
	           });
		}
		//function confirmation set as paid
		function setNotPaid(){
			var po_id = $("#po_id").val();
			var po_name = $("#po_name").val();
			var po_paid = $("#po_paid").val();
			var cust_id = $("#customer_id").val();
			swal({
				title: "Are you sure ? ",
				text: "you want to change the payment status for the order "+po_name+" as NOT PAID",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#ec971f",
				confirmButtonText: "Yes, Set as NOT Paid ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "edit.php?action=paid&po_id="+po_id+"&po_isPaid="+po_paid+"&customer_id="+cust_id;
				} else {
	                   //nothing
	               }
	           });
		}
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		<?php if($message != "") { ?>
            //use session here for alert success/failed
            var alertText = "<?=$message;?>"; //teks for alert
            
                <?php if($alert != "success"){ ?>
                    //error alert
                    errorAlert(alertText);
                <?php } else { ?>
                    //success alert
                    successAlert(alertText); 
                <?php } ?>
             
            <?php } ?> 

		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}
		</script>

	</body>
	<!-- end: BODY -->
	</html>