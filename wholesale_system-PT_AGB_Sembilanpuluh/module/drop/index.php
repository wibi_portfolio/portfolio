<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_drop.php");
	$curpage="drop";
	$page_name = "index.php";

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['drop'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Discard Barang Masuk
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
									
									</div>
									

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Data : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
									
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>Kode Masuk</th>
												<th>Tanggal Discard</th>
												<th>Barang</th>
												<th>Jumlah </th>											
												<th>Kondisi</th>
																																			
												
											</tr>
											<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											<tr>
												<td><?=($O_page-1)*10+$num;?>.</td>
												<td><?=$data['brg_kode_name'];?></td>	
												<td><?php 
													date_default_timezone_set("Asia/Jakarta");
													echo date('j/M/y',strtotime($data['drop_date']));?>											
												</td>
												<td><?=$data['product_code']."-".$data['product_name'];?>	
												</td>												
												<td><?=$data['drop_qty']." ".$data['product_satuan'];?></td>
												
												<td><?=$data['drop_kondisi'];?></td>					
												
												
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="6" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
									</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1"."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Data : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no data right now!
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>



		
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	
		<script type="text/javascript">
			

			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			
		</script>
	</body>
	<!-- end: BODY -->
</html>