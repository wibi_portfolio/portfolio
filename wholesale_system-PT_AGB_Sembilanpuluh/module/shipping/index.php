<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_shipping.php");
	$curpage="shipping";
	$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['shipping'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Shipping Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
												<tr>
													<th class="text-center">#</th>
													
													<th>Ship Date</th>
													<th>PO Code</th>
													<th>Bill of Lading</th>
													<th>FOB</th>
													<th>Action</th>
												</tr>
												<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
												<tr>
													<td class="text-center"><?=($O_page-1)*10+$num;?>.</td>
													<td><?php 
													date_default_timezone_set("Asia/Jakarta");
													echo date('j/M/Y',strtotime($data['pengiriman_date']));?></td>
													<td><?=$data['po_name'];?></td>
													<td><?=$data['bill_of_lading'];?></td>
													<td><?=$data['pengiriman_fob'];?></td>
													
													<td>
														<div class="text-center">
															<a href="<?=$path['shipping-edit'].$data['pengiriman_ref_po']."&po=".$data['po_name'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['pengiriman_id'];?>', '<?=$data['po_name'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														</div>
													</td>
												</tr>
												<?php $num++;} } else { ?>
												<tr class="warning">
													<td colspan="6" class="bold">There is no data right now!</td>
												</tr>
												<?php } ?>
											</table>
										</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(5*($batch-1));
												$nextLimit = 5 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if($O_page > 1){
													echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
													?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php 
												}
												if($total_page > 1 && $O_page != $total_page){
													echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
										<!-- end pagination -->
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

		
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>
		<script type="text/javascript">
			function validateForm(){
				var title = $("#input-title").val();
				
				if(title != ""){
					$("#error-title").html("");
					$("#error-title").hide();
					$("#input-title").removeClass("input-error");
				} else {
					$("#error-title").show();
					$("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-title").addClass("input-error");
					return false;
				}
			}
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>