<?php 
include("../../packages/require.php");
include("../../controller/controller_incoming_drop.php");
include("../../packages/productFunction.php");//RECURSIVE CATEGORY
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php

$curpage="incoming";


?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['incoming'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									<img class="img-responsive" src="<?=$global['logo-agb'];?>">
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['incoming'];?>">
										Discard Product
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['brg_kode_name'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Discard Product <span class="text-bold">(<?=$datas[0]['product_name'];?>)</span></h4>
								</div>
								<form name="editProduct" action="drop.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
									<div class="panel-body">
										<div class="form-body">
											
											
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Kode Masuk<span class="symbol required"></span></strong></div>
												<div class="col-sm-4 col-xs-12 up1">
													<input id="input-name" name="name" type="text" class="form-control" placeholder="product name"  value="<?=$datas[0]['brg_kode_name'];?>" disabled />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nama Produk <span class="symbol required"></span></strong></div>
												<div class="col-sm-4 col-xs-12 up1">
													<input id="input-name" name="name" type="text" class="form-control" placeholder="product name"  value="<?=$datas[0]['product_name'];?>" disabled/>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Discard Quantity<span class="symbol required"></span></strong></div>
												<div class="col-sm-4 col-xs-12 up1">
													<input id="input-name" name="qty" type="text" class="form-control" placeholder="qty" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Kondisi<span class="symbol required"></span></strong></div>
												<div class="col-sm-4 col-xs-12 up1">
													<select class="form-control" name="cond">
														<option value="Expired">Expired</option>
														<option value="Rusak">Rusak</option>
													</select>
												</div>
											</div>
																				
										
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['at_id'];?>">
											<input type="hidden" name="prod" value="<?=$datas[0]['product_ID'];?>">
											
										
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['incoming'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
												
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Discard
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['incoming'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Discard
															</button>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

	<?php include("../../packages/footer-js.php");?>
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script type="text/javascript">
		function validateForm(){
                var name = $("#input-name").val();
                var price = $("#input-price").val();
                var weight = $("#input-weight").val();
                var numFormat = /^[0-9.,\b]+$/;
                var weightFormat = /^[0-9.,\b]+$/;
                var category = $("#input-category").val();
                
                if (category != "") {
                    $("#error-category").html("");
                    $("#error-category").hide();
                    $("#input-category").removeClass("input-error");
				} else {
                    $("#error-category").show();
                    $("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-category").addClass("input-error");
                    return false;
				}
                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
                if(price != ""){
                    if(price.match(numFormat)){
                        $("#error-price").html("");
                        $("#error-price").hide();
                        $("#input-price").removeClass("input-error");
                    } else {
                        $("#error-price").show();
                        $("#error-price").html("<i class='fa fa-warning'></i> This field must contain number only.");
                        $("#input-price").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-price").show();
                    $("#error-price").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-price").addClass("input-error");
                    return false;
                }
                if(weight != ""){
                    if(weight.match(weightFormat)){
                        $("#error-weight").html("");
                        $("#error-weight").hide();
                        $("#input-weight").removeClass("input-error");
                    } else {
                        $("#error-weight").show();
                        $("#error-weight").html("<i class='fa fa-warning'></i> This field must contain number, comma(,), and dot(.) only.");
                        $("#input-weight").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-weight").show();
                    $("#error-weight").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-weight").addClass("input-error");
                    return false;
                }
            }
	
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
			<?php if($alert != "success"){ ?>
				//error alert
				errorAlert(alertText);
			<?php } else { ?>
				//success alert
				successAlert(alertText); 
			<?php } ?>
			 
		<?php } ?>

		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}

	
	</script>

</body>
<!-- end: BODY -->
</html>