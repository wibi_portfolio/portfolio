<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_incoming.php");
	$curpage="incoming";
	$page_name = "index.php";

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['incoming'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Menejemen Barang Masuk
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
									<?php if($_SESSION['user_status'] == 5 or $_SESSION['user_status'] == 1) { ?>
										<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
											<i class="fa fa-plus"></i> Add Barang Masuk
										</a>
									</div>
									<?php } ?>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Data : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
									
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>Kode Masuk</th>
												<th>Tanggal Masuk</th>
												<th>Barang</th>
												<th>Jumlah masuk</th>											
												<th>Expired</th>
												<th>Produksi</th>																							
												<th>Action</th>
											</tr>
											<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											<tr>
												<td><?=($O_page-1)*10+$num;?>.</td>
												<td><?=$data['brg_kode_name'];?></td>	
												<td><?php 
													date_default_timezone_set("Asia/Jakarta");
													echo date('j/M/y',strtotime($data['brg_date']));?>											
												</td>
												<td><?=$data['product_code']."-".$data['product_name'];?>
													<?php if(check_expired($data['at_expired'])){ ?>
													<a class="tooltips" data-placement="top" data-original-title="Expired less than 6 month" ><span class="label label-danger"> <i class="fa fa-warning fa-white"></i></span></a>
													<?php }?>
												</td>												
												<td><?=$data['at_jumlah']." ".$data['product_satuan'];?></td>
												<td><?php 
													date_default_timezone_set("Asia/Jakarta");
													echo date('j/M/y',strtotime($data['at_expired']));?>													
												</td>
												<td><?=$data['farm_kota'];?></td>											
												
												<td>
													<div class="text-center">
														<a href="<?=$path['product-edit'].$data['product_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View"><i class="fa fa-trash-o fa fa-edit"></i></a>
														<!--														
														<a href="javascript:void(0)"  onclick="confirmDelete('<?=$data['at_id'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														-->
														<a href="<?=$path['incoming-drop'].$data['at_id'];?>" class="btn btn-xs btn-red tooltips" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
													</div>
												</td>
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="6" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
									</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1"."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Data : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no data right now!
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
	<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header form-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title form-title">Add Barang Masuk</h4>
				</div>
				<form name="addPhoto" action="index.php?action=add" enctype="multipart/form-data"  method="post" onsubmit="return validateFormPhoto();">
					<div class="modal-body">
						<div class ="row">
							
							<div class="col-sm-4 col-xs-12 up1 form-label text-left"><strong>Barang </strong></div>
							<div class="col-sm-2 col-xs-12 up1 form-label text-left"><strong>Kuantitas </strong></div>
							<div class="col-sm-3 col-xs-12 up1 form-label text-left"><strong>Expired </strong></div>
							<div class="col-sm-3 col-xs-12 up1 form-label text-left"><strong>Produksi </strong></div>
						</div>
						<div class="row"> 
							
							<div class="col-sm-12 col-xs-12 up1">
								<div id="productField" class="input_fields_wrap">
							
									<div class="col-sm-4">
									<select class="form-control" id="sel1" name="barang[]">
									   <?=$products;?>
								  	</select>
								  	</div>
								  	<div class="col-sm-2">
								  	<input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty">
								  	</div>
								  	<div class="col-sm-3">
								  	<input id="conf_date" name="exp[]" type="date" class="form-control" />
								  	</div>
								  	<div class="col-sm-3">
								  		<select class="form-control" id="kota" name="kota[]">
											<?=$farm;?>
										</select>
								  	</div>
								  	<br><br><br>							
								</div>								
							</div>						
						</div>
					</div>
					<div class="modal-footer f5-bg">
						
						<div class="btn-group">
							<button type="reset" class="btn btn-default" data-dismiss="modal">
								<i class="fa fa-times"></i> Cancel
							</button>
							<button type="button" class="btn btn-info add_field_button">
								<i class="fa fa-plus"></i> Add Product
							</button>
							<button type="submit" class="btn btn-success">
								<i class="fa fa-upload fa fa-white"></i> Submit
							</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->


		
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});


				 	var max_fields      = 10; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				    var select			= '<div> <div class="col-sm-4"> <select class="form-control" id="sel1" name="barang[]">';
					var pilihan			= " <?=$products;?> </select> </div>";
				    var qty 			= '<div class="col-sm-2"><input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty"></div>';
				    var hapus			= '<a href="#" class="remove_field">Remove</a> </div> ';
				    var tgl 			= '<div class="col-sm-3"><input id="conf_date" name="exp[]" type="date" class="form-control" /></div>';
				    var select_kota		= '<div class="col-sm-3"><select class="form-control" id="kota" name="kota[]">';
					var pilihan_kota	= " <?=$farm;?> </select> </div>";
				    
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				        e.preventDefault();
				        if(x < max_fields){ //max input box allowed
				            x++; //text box increment
				            $(wrapper).append(select+pilihan+qty+tgl+select_kota+pilihan_kota+hapus); //add input box
				        }
				    });
				    
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
			});

			
		</script>
		<script type="text/javascript">
			function validateForm(){
            
                var date = $("#conf_date").val();
                var qty = $("#input-weight").val();               
                var numFormat = /^[0-9]+$/;               
                
                
                if(date != ""){                  
                    $("#conf_date").removeClass("input-error");
                } else {                 
                    $("#conf_date").addClass("input-error");
                    return false;
                }
                if(qty > 0){
                    if(qty.match(numFormat)){                        
                        $("#input-weight").removeClass("input-error");
                    } else {                        
                        $("#input-weight").addClass("input-error");
                        return false;
                    }
                } else {                   
                    $("#input-weight").addClass("input-error");
                    return false;
                }                
               
            }


			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

					//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>