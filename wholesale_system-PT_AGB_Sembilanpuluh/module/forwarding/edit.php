<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_forwarding_detail.php");
$curpage="forwarding";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['forwarding'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									<img class="img-responsive" src="<?=$global['logo-agb'];?>">
									
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['forwarding'];?>">
										Forwarding Service Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['forwarding_name'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Forwarding Service <span class="text-bold">(<?=$datas[0]['forwarding_name'];?>)</span></h4>
								</div>
								<form name="editPortfolio" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Perusahaan <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-title" name="title" type="text" class="form-control" placeholder="perusahaan" value="<?=$datas[0]['forwarding_name'];?>" />
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Person in Charge <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-title" name="pic" type="text" class="form-control" placeholder="nama pic" value="<?=$datas[0]['forwarding_pic'];?>" />
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											  <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Alamat <span class="symbol required"></span></strong></div>
                                            <div class="col-sm-6 col-xs-12 up1">
                                                <textarea id="input-desc" name="alamat" class="tinymce form-control" rows="5" placeholder="alamat"><?=$datas[0]['forwarding_address'];?></textarea>
                                                <div id="error-desc" class="is-error"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Telpon <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <input id="input-title" name="telpon" type="text" class="form-control" placeholder="telpon" value="<?=$datas[0]['forwarding_phone'];?>"/>
                                                <div id="error-title" class="is-error"></div>
                                            </div>
                                        </div>
			                              
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['id_forwarding'];?>">
											<div class="col-sm-2 col-xs-12 pad0">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['id_forwarding'];?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['forwarding'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['forwarding'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Update
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
		function validateForm(){
			var title = $("#input-title").val();

			if(title != ""){
				$("#error-title").html("");
				$("#error-title").hide();
				$("#input-title").removeClass("input-error");
			} else {
				$("#error-title").show();
				$("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
				$("#input-title").addClass("input-error");
				return false;
			}
		}
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert

		//success alert
		//successAlert(alertText); 

		//error alert
		//errorAlert(alertText); 

		//function confirmation delete
		function confirmDelete(num, text){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num+"&title="+text;
				} else {
	                   //nothing
	               }
	           });
		}
	</script>

</body>
<!-- end: BODY -->
</html>