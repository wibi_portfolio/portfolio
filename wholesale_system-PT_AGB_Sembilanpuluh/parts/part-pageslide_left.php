<a class="closedbar inner hidden-sm hidden-xs" href="#">

</a>

<nav id="pageslide-left" class="pageslide inner">

	<div class="navbar-content">

		<!-- start: SIDEBAR -->

		<div class="main-navigation left-wrapper transition-left">

			<div class="navigation-toggler hidden-sm hidden-xs">

				<a href="#main-navbar" class="sb-toggle-left">

				</a>

			</div>

			<div class="user-profile border-top padding-horizontal-10 block">

				<div class="inline-block hide">

					<img src="<?=$global['absolute-url-admin'];?>assets/images/avatar-1.jpg" alt="">

				</div>

				<div class="inline-block">

					<h5 class="no-margin"> Welcome </h5>

					<h4 class="no-margin"> <?=convert_role($_SESSION['user_status']);?> </h4>
					

				</div>

			</div>

			<!-- start: MAIN NAVIGATION MENU -->

			<ul class="main-navigation-menu">				

				<li <?php if($curpage == "dashboard") { ?>class="active"<?php } ?>>

							<a href="<?=$path['dash'];?>">

								<span class="title"><i class="fa fa-bar-chart"></i>Dashboard </span>

							</a>					

				</li>
			

				<li <?php if($curpage == "product") { ?>class="active"<?php } ?>>

							<a href="<?=$path['product'];?>">

								<span class="title"><i class="fa fa-folder"></i>Products </span>

							</a>					

				</li>

				<?php if($_SESSION['user_status'] != 4 AND $_SESSION['user_status'] != 3) { ?>
				<li <?php if($curpage == "farm") { ?>class="active"<?php } ?>>

							<a href="<?=$path['farm'];?>">

								<span class="title"><i class="fa fa-tree"></i>  Tempat Produksi </span>

							</a>					

				</li>

				<li <?php if($curpage == "incoming") { ?>class="active"<?php } ?>>

							<a href="<?=$path['incoming'];?>">

								<span class="title"><i class="fa fa-cart-arrow-down"></i>  Barang Masuk </span>

							</a>					

				</li>
				<li <?php if($curpage == "drop") { ?>class="active"<?php } ?>>

							<a href="<?=$path['drop'];?>">

								<span class="title"><i class="fa fa-trash"></i> Discard Barang </span>

							</a>					

				</li>
				<?php } ?>

				<?php if($_SESSION['user_status'] != 5) { ?>

				<li <?php if($curpage == "forwarding") { ?>class="active"<?php } ?>>

							<a href="<?=$path['forwarding'];?>">

								<span class="title"><i class="fa fa-suitcase"></i>  Forwarding </span>

							</a>					

				</li>

				<li <?php if($curpage == "customer") { ?>class="active"<?php } ?>>

					<a href="<?=$path['customer'];?>"><i class="fa fa-users"></i> <span class="title"> Customer </span></a>

				</li>

				<!--
				<li <?php if($curpage == "confirmation") { ?>class="active"<?php } ?>>

					<a href="<?=$path['confirmation'];?>"><i class="fa fa-check-square"></i> <span class="title"> Confirmation </span></a>

				</li>
				-->


				<li <?php if($curpage == "order") { ?>class="active"<?php } ?>>

					<a href="<?=$path['order'];?>"><i class="fa fa-shopping-cart"></i> <span class="title"> Purchase Order </span></a>

				</li>

				
				<li <?php if($curpage == "shipping") { ?>class="active"<?php } ?>>

					<a href="<?=$path['shipping'];?>"><i class="fa fa-ship"></i> <span class="title"> Shipping </span></a>

				</li>
				<?php } ?>			
		
				
				<!--
				<li <?php if($curpage == "content") { ?>class="active"<?php } ?>>

					<a href="javascript:void(0)"><i class="fa fa-book"></i> <span class="title"> Content </span><i class="icon-arrow"></i> </a>

					<ul class="sub-menu">

						<li>

							<a href="<?=$path['content'];?>">

								<span class="title"><i class="fa fa-book"></i>  About Us </span>

							</a>

						</li>

						<li>

							<a href="<?=$path['content-return'];?>">

								<span class="title"><i class="fa fa-book"></i>  Return </span>

							</a>

						</li>

						<li>

							<a href="<?=$path['content-order'];?>">

								<span class="title"><i class="fa fa-book"></i>  How To Order </span>

							</a>

						</li>

						<li>

							<a href="<?=$path['content-faq'];?>">

								<span class="title"><i class="fa fa-book"></i>  FAQ </span>

							</a>

						</li>

						<li>

							<a href="<?=$path['content-email'];?>">

								<span class="title"><i class="fa fa-book"></i>  E-Mail </span>

							</a>

						</li>

					</ul>

				</li>

				-->
				<?php if($_SESSION['user_status'] == 1) { ?>
				<li <?php if($curpage == "user") { ?>class="active"<?php } ?>>

					<a href="<?=$path['user'];?>"><i class="fa fa-user"></i> <span class="title"> Users </span></a>

				</li>
				<?php } ?>

				

			</ul>

			<!-- end: MAIN NAVIGATION MENU -->

		</div>

		<!-- end: SIDEBAR -->

	</div>

	<div class="slide-tools">

		<div class="col-xs-6 text-left no-padding">

			<a class="btn btn-sm status" href="#">

				Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>

			</a>

		</div>

		<div class="col-xs-6 text-right no-padding">

			<a class="btn btn-sm log-out text-right" href="<?=$path['logout'];?>">

				<i class="fa fa-power-off"></i> Log Out

			</a>

		</div>

	</div>

</nav>