<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/Shipping.php");
$obj_shipping = new Shipping();
require_once("../../model/Forwarding.php");
$obj_forward = new Forwarding();


if(!isset($_GET['action'])){
    $obj_con->up();
    

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $O_po = mysql_real_escape_string(check_input($_GET['po']));

    $datas = $obj_shipping->get_by_id($O_id);
    $forwards = $obj_forward->get_data() ;
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();

        $N_shippingID = mysql_real_escape_string(check_input($_POST['Shipping_ID']));
        $N_shippingCity = mysql_real_escape_string(check_input($_POST['Shipping_city']));
        $N_shippingCost1 = mysql_real_escape_string(check_input($_POST['Shipping_cost_1']));
        $N_shippingCost2 = mysql_real_escape_string(check_input($_POST['Shipping_cost_2']));
        $N_shippingCost3 = mysql_real_escape_string(check_input($_POST['Shipping_cost_3']));
        $N_shippingCost4 = mysql_real_escape_string(check_input($_POST['Shipping_cost_4']));


        $result = $obj_shipping->update_data($N_shippingID, $N_shippingCity, $N_shippingCost1, $N_shippingCost2, $N_shippingCost3, $N_shippingCost4);
        if($result <= 0){
            $message = "There has been error on shipping creation.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "$N_shippingCity has been successfully updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
      
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    } 
}
?> 