<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/farm.php");
$obj_farm = new Farm(); 


if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();    

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_farm->get_data_detail($O_id);
    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_name = mysql_real_escape_string(check_input($_POST['title']));        
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_telp = mysql_real_escape_string(check_input($_POST['telpon']));
        $N_kota = mysql_real_escape_string(check_input($_POST['kota']));

        $result = $obj_farm->update_data($N_id, $N_name, $N_alamat, $N_telp, $N_kota);

        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Farm<i><b>'" . $N_name . "'</b></i> has been succesfully edited.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }   

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>