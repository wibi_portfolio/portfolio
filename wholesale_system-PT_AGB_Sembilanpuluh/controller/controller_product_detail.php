<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/Product.php");
$obj_product = new Product();


if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
 

    $datas = $obj_product->get_data_detail($O_id);
   

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $message = "";
        $N_id = mysql_real_escape_string(check_input($_POST['id']));        
        $N_name = mysql_real_escape_string(check_input($_POST['name']));       
        $N_price = mysql_real_escape_string(check_input($_POST['price']));
        $N_profit = mysql_real_escape_string(check_input($_POST['profit']));
        //$N_stok = mysql_real_escape_string(check_input($_POST['stok']));        
        $N_satuan = mysql_real_escape_string(check_input($_POST['satuan']));      
       
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        //start update product
        $result = $obj_product->update_data($N_id, $N_name, $N_satuan, $N_price, $N_profit, $N_publish);

        if($result == 1){
            $message .= "Changes on <i><b>'" . $N_name . "'</b></i> has been saved successfully.<br><br>";

            $_SESSION['alert'] = "success";
        }else{
            $message .= "There is no change in product<br><br>";
            $_SESSION['alert'] = "error";
        }
        //end update product       
                 

        $_SESSION['status'] = $message;
        header("Location:".$path['product']);
        $obj_con->down();

    }
}
?>