<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

$file_json = "../../../uploads/json/content_about_us.json";

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_brief = $json['brief'];
    $J_visi = $json['visi'];

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']); 
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();

        $N_brief = mysql_real_escape_string(check_input($_POST['brief']));
        $N_visi = mysql_real_escape_string(check_input($_POST['visi']));
        
        $data['brief'] = $N_brief;
        $data['visi'] = $N_visi;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Content About Us has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Content About Us failed to update!";
            $_SESSION['alert'] = "error";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?> 