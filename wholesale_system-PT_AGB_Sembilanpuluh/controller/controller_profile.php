<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

$file_json = "../../../uploads/json/company_profile.json";

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_company = $json['company_name'];
    $J_address1 = $json['address1'];
   // $J_address2 = $json['address2'];
    $J_city = $json['city'];
    $J_province = $json['province'];
    $J_zip = $json['zip'];
    $J_country = $json['country'];
   // $J_bbm = $json['bbm'];
    $J_email1 = $json['email1'];
   // $J_email2 = $json['email2'];
    $J_phone1 = $json['phone1'];
   // $J_phone2 = $json['phone2'];
   // $J_fax = $json['fax'];
    $J_website = $json['website'];
   // $J_promo = $json['promo'];
    $J_bank = $json['bank'];

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();

        $N_company = mysql_real_escape_string(check_input($_POST['company_name']));
        $N_address1 = mysql_real_escape_string(check_input($_POST['address1']));
       // $N_address2 = mysql_real_escape_string(check_input($_POST['address2']));
        $N_city = mysql_real_escape_string(check_input($_POST['city']));
        $N_province = mysql_real_escape_string(check_input($_POST['province']));
        $N_zip = mysql_real_escape_string(check_input($_POST['zip']));
        $N_country = mysql_real_escape_string(check_input($_POST['country']));
       // $N_bbm = mysql_real_escape_string(check_input($_POST['bbm']));
        $N_email1 = mysql_real_escape_string(check_input($_POST['email1']));
        //$N_email2 = mysql_real_escape_string(check_input($_POST['email2']));
        $N_phone1 = mysql_real_escape_string(check_input($_POST['phone1']));
        //$N_phone2 = mysql_real_escape_string(check_input($_POST['phone2']));
        //$N_fax = mysql_real_escape_string(check_input($_POST['fax']));
        $N_website = mysql_real_escape_string(check_input($_POST['website']));
        //$N_promo = mysql_real_escape_string(check_input($_POST['promo']));
        $N_bank = mysql_real_escape_string(check_input($_POST['bank']));
        
        $data['company_name'] = $N_company;
        $data['address1'] = $N_address1;
        //$data['address2'] = $N_address2;
        $data['city'] = $N_city;
        $data['province'] = $N_province;
        $data['zip'] = $N_zip;
        $data['country'] = $N_country;
        //$data['bbm'] = $N_bbm;
        $data['email1'] = $N_email1;
        //$data['email2'] = $N_email2;
        $data['phone1'] = $N_phone1;
       // $data['phone2'] = $N_phone2;
        //$data['fax'] = $N_fax;
        $data['website'] = $N_website;
       // $data['promo'] = $N_promo;
        $data['bank'] = $N_bank;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Company profile has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Company profile failed to update!";
            $_SESSION['alert'] = "error";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?> 