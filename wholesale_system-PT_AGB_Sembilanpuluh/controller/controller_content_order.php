<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

$file_json = "../../../uploads/json/content_order.json";

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_content = $json['content'];

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']); 
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();

        $N_content = mysql_real_escape_string(check_input($_POST['content']));
        
        $data['content'] = $N_content;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Content How To Order has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Content How To Order failed to update!";
            $_SESSION['alert'] = "error";
        }

        $_SESSION['status'] = $message;
        header("Location:order.php");
        $obj_con->down();
    }
}
?> 