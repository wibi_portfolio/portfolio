<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/PO.php");
$obj_po = new PO(); 
require_once("../../model/Customer.php");
$obj_cust = new Customer();
require_once("../../model/Product.php");
$obj_brg = new Product();
require_once("../../model/Forwarding.php");
$obj_forward = new Forwarding();
require_once("../../model/Shipping.php");
$obj_ship = new Shipping();      


if(!isset($_GET['action'])){
    $obj_con->up();
    
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    if(isset($_GET['id'])){
        $O_cust = mysql_real_escape_string(check_input($_GET['id']));
    }

    $datas = $obj_po->get_order_history($O_page);
    $products= $obj_brg->get_product_list();
    $forwards = $obj_forward->get_data() ;


    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

 

    $obj_con->down();
} else if(isset($_GET['action'])){
        if($_GET['action'] == "add"){
        $obj_con->up();
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_company = mysql_real_escape_string(check_input($_POST['company_name']));
        $N_address1 = mysql_real_escape_string(check_input($_POST['address1']));       
        $N_city = mysql_real_escape_string(check_input($_POST['city']));
        $N_province = mysql_real_escape_string(check_input($_POST['province']));
        $N_zip = mysql_real_escape_string(check_input($_POST['zip']));
        $N_country = mysql_real_escape_string(check_input($_POST['country']));       
        $N_email1 = mysql_real_escape_string(check_input($_POST['email1']));     
        $N_phone1 = mysql_real_escape_string(check_input($_POST['phone1'])); 
        $N_term = mysql_real_escape_string(check_input($_POST['term']));
        $N_pay = mysql_real_escape_string(check_input($_POST['pay_term']));  
        $N_fob = mysql_real_escape_string(check_input($_POST['fob']));
        $N_fow = mysql_real_escape_string(check_input($_POST['fow']));
        $N_pack = mysql_real_escape_string(check_input($_POST['pack']));   

        $cust_id = $obj_cust->insert_cust($N_name, $N_email1, $N_phone1, $N_company, $N_address1, $N_city, $N_province, $N_country, $N_zip);    
        $N_po_name = $obj_po->generate_po_code();
        $result = $obj_po->create_po($N_po_name, $cust_id, $N_term, $N_pay); 
      
        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){

            $pengiriman = $obj_ship->insert_data($result, $N_fow, $N_fob, $N_pack);

            //insert at po
            $at = count($_POST['barang']);
            $N_total =0;
            for($c=0; $c < $at; $c++){
                $price = $obj_po->get_price($_POST['barang'][$c]);
               
                $N_subtotal = $price  * $_POST['qty'][$c] ;
                
                $N_total = $N_total + $N_subtotal ;                
                $save_at = $obj_po->create_atpo($result, $_POST['barang'][$c], $_POST['qty'][$c], $N_subtotal);
                //$sold    = $obj_brg->sold_stok($_POST['barang'][$c], $_POST['qty'][$c]);
                $N_subtotal = 0 ;

            } 

            $add_total = $obj_po->add_total($result, $N_total);

            $message = "Data has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
    else if($_GET['action'] == "addpo"){
        $obj_con->up();
               
        $N_term = mysql_real_escape_string(check_input($_POST['term']));
        $N_pay = mysql_real_escape_string(check_input($_POST['pay_term']));  
        $N_fob = mysql_real_escape_string(check_input($_POST['fob']));
        $N_fow = mysql_real_escape_string(check_input($_POST['fow']));
        $N_pack = mysql_real_escape_string(check_input($_POST['pack']));
        $N_cust = mysql_real_escape_string(check_input($_POST['id']));    

         
        $N_po_name = $obj_po->generate_po_code();
        $result = $obj_po->create_po($N_po_name, $N_cust, $N_term, $N_pay); 
      
        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){

            $pengiriman = $obj_ship->insert_data($result, $N_fow, $N_fob, $N_pack);

            //insert at po
            $at = count($_POST['barang']);
            $N_total =0;
            for($c=0; $c < $at; $c++){
                $price = $obj_po->get_price($_POST['barang'][$c]);
               
                $N_subtotal = $price  * $_POST['qty'][$c] ;
                
                $N_total = $N_total + $N_subtotal ;                
                $save_at = $obj_po->create_atpo($result, $_POST['barang'][$c], $_POST['qty'][$c], $N_subtotal);
                //$sold    = $obj_brg->sold_stok($_POST['barang'][$c], $_POST['qty'][$c]);
                $N_subtotal = 0 ;

            } 

            $add_total = $obj_po->add_total($result, $N_total);

            $message = "Data has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }

    else if($_GET['action'] == "resi"){

        $file_json = "../../../uploads/json/content_email.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
    
        $J_register = $json['register'];
        $J_order = $json['order'];
        $J_shipment = $json['shipment'];

        function send_email($custEmail, $custName, $orderPoResi, $orderPoName, $textShipment){
        //mail
        $to = $custEmail; 
        $subject = "Shipment Complete for order $orderPoName";

        $message = "
        <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>
        <script src='//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'></script>
        </head>
        <body>
        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border: solid 2px #f2786b;'>
            <tr>
                <td style='padding: 40px 30px 40px 30px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tr style='padding: 20px 0 20px 0;'>
                            <td>
                                Dear $custName,<br/>
                                We would like to inform you that your order <b>$orderPoName</b> has been shipped.<br/>
                                The tracking number is <b>$orderPoResi</b>.
                            </td>
                        </tr>
                        <tr>
                            <td style='padding: 20px 0 30px 0;'>
                                $textShipment
                            </td>
                        </tr>
                        <tr>
                            <td style='padding: 20px 0 30px 0;'>
                                Please do not hesitate to contact us if you require any assistance.<br/>
                                Thank you,<br/><br/>
                                Odetta.<br/><br/><br/>
                                *this e-mail completes your order
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <tr>
            <td>
                <table style='width:100%;'>
                    <tr style='background-color:#f2786b;'>
                        <td style='padding: 10px 30px 10px 30px;' >
                            <h3 style='color:#fff;'>
                            Thank you <br/>
                            Do not forget to Subscribe to our newsletter to get the latest info.
                            </h3>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        </body>
        </html>
        ";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= "From: <donotreply@odettastyle.com>" . "\r\n";

        $result = mail($to,$subject,$message,$headers);
        return $result; 
        }

        $obj_con->up();
        $headerURL = "index.php";
        $N_orderPoID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_orderPoResi = mysql_real_escape_string(check_input($_POST['PO_resi']));
        $N_orderPoName = mysql_real_escape_string(check_input($_POST['PO_name']));
        $N_custEmail = mysql_real_escape_string(check_input($_POST['Cust_email']));
        $N_custName = mysql_real_escape_string(check_input($_POST['Cust_name']));


        if($N_orderPoResi == '0'){
            //$headerURL = "index.php?action=cancel&po_ID=$N_orderPoID";
            $headerURL = "index.php";
        }
        else if($N_orderPoResi == '1'){
            $N_orderPoResi = 'PICK UP';
        }
        else{
            $N_orderPoResi = strtoupper($N_orderPoResi);
        }

        $result = $obj_po->update_resi($N_orderPoID, $N_orderPoResi, 4);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $result = send_email($N_custEmail, $N_custName, $N_orderPoResi, $N_orderPoName, $J_shipment);
            $message = "The resi order has been updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }

}
?>