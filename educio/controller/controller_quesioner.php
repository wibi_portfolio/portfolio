<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();

require_once("../../model/quesioner.php");
$obj_quesioner = new quesioner(); 


if(!isset($_GET['action'])){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
   
    $datas = $obj_quesioner->get_data_by_page($O_page);
    $opsi = $obj_quesioner->get_answer_list();
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{

        $total_data = 0;
        $total_page = 0;
    }
   
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        $message = "";

        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));       
        $N_no = mysql_real_escape_string(check_input($_POST['no']));
        $N_type = mysql_real_escape_string(check_input($_POST['tipe']));
        $N_opsi = mysql_real_escape_string(check_input($_POST['opsi']));
       
       
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        

        $result = $obj_quesioner->insert_data($N_desc, $N_no, $N_type, $N_opsi, $N_publish);

        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Question has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }       
             
        $direct = $path['quesioner'] ;
        $_SESSION['status'] = $message;
        echo "<script> window.location.href='$direct'; </script>";
        //header("Location : $direct");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

        $result = $obj_quesioner->delete_data($O_id);
      
        if($result <= 0){
            $message .= "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "Question has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>