<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/kuesioner.php");
$obj_quesioner = new kuesioner(); 


if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
 

    $datas = $obj_quesioner->get_data_detail($O_id);
    $list = $obj_quesioner->get_church_list();
    
   

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $message = "";

        $N_id = mysql_real_escape_string(check_input($_POST['id']));        
        

       
         $N_nama = mysql_real_escape_string(check_input($_POST['nama']));       
        $N_umur = mysql_real_escape_string(check_input($_POST['umur']));
        $N_gender = mysql_real_escape_string(check_input($_POST['gender']));
        $N_sekolah = mysql_real_escape_string(check_input($_POST['sekolah']));

        $N_gereja = mysql_real_escape_string(check_input($_POST['gereja']));       
        $N_kelas = mysql_real_escape_string(check_input($_POST['kelas']));
        $N_guru = mysql_real_escape_string(check_input($_POST['guru']));
        $N_hp = mysql_real_escape_string(check_input($_POST['hp']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));

        //start update responden
        $result = $obj_quesioner->update_data($N_id, $N_nama, $N_umur, $N_gender, $N_sekolah, $N_gereja, $N_kelas, $N_guru, $N_hp, $N_email);

        if($result == 1){
            $message .= "Changes on responden has been saved successfully.<br><br>";

            $_SESSION['alert'] = "success";
        }else{
            $message .= "There is no change in responden data <br><br>";
            $_SESSION['alert'] = "error";
        }
        //end update product       
                 

        $_SESSION['status'] = $message;
        header("Location:".$path['responden-edit'].$N_id);
        $obj_con->down();

    }
}
?>