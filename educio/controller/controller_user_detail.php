<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();

require_once("../../model/User.php");
$obj_user = new User(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_user->get_data_detail($O_id);
    //var_dump($datas);



     if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
        $N_username = mysql_real_escape_string(check_input($_POST['username'])); 
        //$N_password = mysql_real_escape_string(check_input($_POST['password']));      
        $N_newpassword = mysql_real_escape_string(check_input($_POST['new_password']));

        if(isset($_POST['role'])){
        $N_role = mysql_real_escape_string(check_input($_POST['role']));}
        else{
            $N_role = 2;
        }
        

        if($N_newpassword != ""){
             $result = $obj_user->update_data($N_id, $N_email, $N_username, $N_newpassword, $N_role);
        } else {
             $result = $obj_user->update_data_np($N_id, $N_email, $N_username, $N_role);
        }

        //var_dump($result);

       
        if($result <= 0){
            $message = "User <i><b>'" . $N_username . "'</b></i> failed to be updated.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "User <i><b>'" . $N_username . "'</b></i> is successfully updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
    
        $_SESSION['status'] = $message;
        header("Location:".$path['user-edit'].$N_id);
        $obj_con->down();
    }
}
?>