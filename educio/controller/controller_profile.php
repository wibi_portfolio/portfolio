<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();

$file_json = "../../assets/json/company_profile.json";

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_title = $json['title'];
    $J_sub = $json['sub'];
 
    $J_pengantar = $json['pengantar'];

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $obj_con->up();

        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_sub = mysql_real_escape_string(check_input($_POST['sub']));
       
        $N_pengantar = check_input($_POST['pengantar']);
        
        $data['title'] = $N_title;
        $data['sub'] = $N_sub;
      
        $data['pengantar'] = $N_pengantar;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Title has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Title failed to update!";
            $_SESSION['alert'] = "error";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?> 