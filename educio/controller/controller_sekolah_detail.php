<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/sekolah.php");
$obj_sekolah = new sekolah(); 

 require_once("../../model/provinsi.php");
$obj_provinsi = new provinsi();

require_once("../../model/kota.php");
$obj_kota = new kota();
require_once("../../model/kecamatan.php");
$obj_kecamatan = new kecamatan();
require_once("../../model/prospek.php");
$obj_prospek = new prospek();


if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
 

    $datas = $obj_sekolah->get_data_detail($O_id);
    $provinsi = $obj_provinsi->get_provinsi(); 
    $kota = $obj_kota->get_kota($datas[0]['prov']); 
    $kecamatan=$obj_kecamatan->get_kecamatan($datas[0]['kot']);   
    
   

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();
        $message = "";

        $N_id = mysql_real_escape_string(check_input($_POST['id']));        
        

        $N_nama = mysql_real_escape_string(check_input($_POST['nama']));       
        $N_nisp = mysql_real_escape_string(check_input($_POST['nisp']));
        $N_status = mysql_real_escape_string(check_input($_POST['status']));
        $N_sekolah = mysql_real_escape_string(check_input($_POST['sekolah']));
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_prov = mysql_real_escape_string(check_input($_POST['prov']));
        $N_kota = mysql_real_escape_string(check_input($_POST['kota']));
        $N_kec = mysql_real_escape_string(check_input($_POST['kec']));
        $N_telp= mysql_real_escape_string(check_input($_POST['telp']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
       
        $N_stakon = mysql_real_escape_string(check_input($_POST['stakon']));
        $N_penerima = mysql_real_escape_string(check_input($_POST['penerima']));
        $N_kepsek = mysql_real_escape_string(check_input($_POST['kepsek']));
        $N_web = mysql_real_escape_string(check_input($_POST['web']));
        $N_fax = mysql_real_escape_string(check_input($_POST['fax']));

        if(isset($_POST['minat'])){
            $N_minat = mysql_real_escape_string(check_input($_POST['minat']));
        }else{
            $N_minat = 0;
        }

        if(isset($_POST['uang'])){
            $N_uang = mysql_real_escape_string(check_input($_POST['uang']));
        }else{
            $N_uang = 0;
        }

        if(isset($_POST['ruang'])){
            $N_ruang = mysql_real_escape_string(check_input($_POST['ruang']));
        }else{
            $N_ruang = 0;
        }

$result = $obj_sekolah->update_data($N_id, $N_nama, $N_nisp, $N_sekolah, $N_status, $N_alamat, $N_prov, $N_kota, $N_kec, $N_telp, $N_email, $N_stakon, $N_penerima, $N_kepsek,$N_web,$N_fax, $N_minat, $N_uang, $N_ruang);
        
            if(isset($_POST['desc']) and !empty($_POST['desc'])){
                $del = $obj_prospek->delete_keterangan($N_id) ;
                $at = count($_POST['desc']);
                for($c=0; $c < $at; $c++){
                    if (strlen($_POST['desc'][$c]) > 0 && strlen(trim($_POST['desc'][$c])) != 0)                                   
                    $save_ket = $obj_prospek->save_keterangan($N_id, $_POST['desc'][$c]) ;
        
                   
                }
            }
       

        if($result == 1){
            $save_log=$obj_sekolah->save_log($N_id, $_SESSION['admin_id'], "edit");
            $message .= "Changes on school has been saved successfully.<br><br>";

            if($N_stakon == 1){
                $_SESSION['diangkat'] = $_SESSION['diangkat'] + 1;
            }else if ($N_stakon != 0){
                $_SESSION['telp'] = $_SESSION['telp'] + 1 ;
            }

            $_SESSION['alert'] = "success";
        }else{
            $message .= "There is no change in school data <br><br>";
            $_SESSION['alert'] = "error";
        }
        //end update product       
                 

        $_SESSION['status'] = $message;
        header("Location:".$path['sekolah-edit'].$N_id);
        $obj_con->down();

    } else if($_GET['action'] == "prospek"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));

        $N_nama = mysql_real_escape_string(check_input($_POST['nama']));       
        $N_nisp = mysql_real_escape_string(check_input($_POST['nisp']));
        $N_status = mysql_real_escape_string(check_input($_POST['status']));
        $N_sekolah = mysql_real_escape_string(check_input($_POST['sekolah']));
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_prov = mysql_real_escape_string(check_input($_POST['prov']));
        $N_kota = mysql_real_escape_string(check_input($_POST['kota']));
        $N_kec = mysql_real_escape_string(check_input($_POST['kec']));
        $N_telp= mysql_real_escape_string(check_input($_POST['telp']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
       
        $N_stakon = mysql_real_escape_string(check_input($_POST['stakon']));
        $N_penerima = mysql_real_escape_string(check_input($_POST['penerima']));
        $N_kepsek = mysql_real_escape_string(check_input($_POST['kepsek']));
        $N_web = mysql_real_escape_string(check_input($_POST['web']));
        $N_fax = mysql_real_escape_string(check_input($_POST['fax']));

        if(isset($_POST['minat'])){
            $N_minat = mysql_real_escape_string(check_input($_POST['minat']));
        }else{
            $N_minat = 0;
        }

        if(isset($_POST['uang'])){
            $N_uang = mysql_real_escape_string(check_input($_POST['uang']));
        }else{
            $N_uang = 0;
        }

        if(isset($_POST['ruang'])){
            $N_ruang = mysql_real_escape_string(check_input($_POST['ruang']));
        }else{
            $N_ruang = 0;
        }
                
        //start update responden
        $result2 = $obj_sekolah->update_data($N_id, $N_nama, $N_nisp, $N_sekolah, $N_status, $N_alamat, $N_prov, $N_kota, $N_kec, $N_telp, $N_email, $N_stakon, $N_penerima, $N_kepsek,$N_web,$N_fax, $N_minat, $N_uang, $N_ruang);
        if($result2){
            $save_log_update=$obj_sekolah->save_log($N_id, $_SESSION['admin_id'], "edit");
        }else{
            $result = 0 ;
        }


        if(isset($_POST['desc'])){
                $at = count($_POST['desc']);
                $del = $obj_prospek->delete_keterangan($N_id) ;
                for($c=0; $c < $at; $c++){
                    if (strlen($_POST['desc'][$c]) > 0 && strlen(trim($_POST['desc'][$c])) != 0)                               
                    $save_ket = $obj_prospek->save_keterangan($N_id, $_POST['desc'][$c]) ;                   
                }
        }
                  
        $result = $obj_sekolah->set_prospek($O_id);      
      

        if($result <= 0){
            $message .= "Something is wrong with your submission <br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $save_log=$obj_sekolah->save_log($O_id, $_SESSION['admin_id'], "prospek");
            $_SESSION['prospek'] = $_SESSION['prospek'] + 1 ;
            $message .= "Changes on school has been saved successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:".$path['sekolah-edit'].$O_id);
        $obj_con->down();

    }else if($_GET['action'] == "prospek_update"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

         $save_log=$obj_sekolah->save_log($O_id, $_SESSION['admin_id'], "update prospek");

        if($save_log){
            $result = $obj_prospek->update_prospek($O_id);
        }else{
            $result = 0;
        }

        if($result <= 0){
            $message .= "Something is wrong with your submission <br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "Changes on school has been saved successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:".$path['sekolah-edit'].$O_id);
        $obj_con->down();
    }
}
?>