<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/Incoming.php");
$obj_in = new Incoming();
require_once("../../model/Product.php");
$obj_brg = new Product();
require_once("../../model/Farm.php");
$obj_farm = new Farm();    


if(!isset($_GET['action'])){
    $obj_con->up();
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    
    $datas = $obj_in->get_data_by_page($O_page);
    $products= $obj_in->get_product_list(); 
    $farm = $obj_farm->get_list();
    
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{

        $total_data = 0;
        $total_page = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_barang = mysql_real_escape_string(check_input($_POST['title']));        
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_telp = mysql_real_escape_string(check_input($_POST['telpon']));

        $result = $obj_in->insert_data($_SESSION['admin_id']);

        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){

            //insert at barang
            $at = count($_POST['barang']);
            for($c=0; $c < $at; $c++){
                $save_at = $obj_in->insert_data_at($result, $_POST['barang'][$c], $_POST['qty'][$c], $_POST['exp'][$c], $_POST['kota'][$c]);
                $brg_add = $obj_brg->add_stok($_POST['barang'][$c], $_POST['qty'][$c]) ;
            } 

            $message = "Data has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
      
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        if(isset($_GET['title'])){
            $O_title = mysql_real_escape_string(check_input($_GET['title']));    
        }else{
            $O_title = $O_id;
        }
        
        $result = $obj_in->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Data has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>