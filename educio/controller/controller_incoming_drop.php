<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/Incoming.php");
$obj_in = new Incoming();
require_once("../../model/Product.php");
$obj_brg = new Product();
   


if(!isset($_GET['action'])){
    $obj_con->up();    
    
    $O_id = mysql_real_escape_string(check_input($_GET['id']));   
     
    $datas = $obj_in->get_data_detail($O_id);   
  

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_qty = mysql_real_escape_string(check_input($_POST['qty']));        
        $N_kond = mysql_real_escape_string(check_input($_POST['cond']));
        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_prod = mysql_real_escape_string(check_input($_POST['prod']));

        $result = $obj_in->insert_drop($N_id, $N_qty, $N_kond);

        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){
            //remove stok           
            $brg_drop = $obj_brg->sold_stok($N_prod, $N_qty) ;          

            $message = "Discard success.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
      
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    } 
}
?>