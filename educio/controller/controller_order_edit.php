<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/PO.php");
$obj_po = new PO();
require_once("../../model/Payment.php");
$obj_pay = new Payment(); 
require_once("../../model/Shipping.php");
$obj_ship = new Shipping(); 
require_once("../../model/Product.php");
$obj_pro = new Product(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_po->get_order_detail($O_id);

    //var_dump($datas);
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }

    $obj_con->down();

} else if(isset($_GET['action'])){


    $file_json = "../../../uploads/json/content_email.json";
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_register = $json['register'];
    $J_order = $json['order'];
    $J_shipment = $json['shipment'];

	if($_GET['action'] == "resi"){
        function send_email($custEmail, $custName, $orderPoResi, $orderPoName, $textShipment){
        //mail
        $to = $custEmail; 
        $subject = "Shipment Complete for order $orderPoName";

        $message = "
        <!DOCTYPE html>
        <html>
        <head>
        <title></title>
        <link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'>
        <script src='//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'></script>
        </head>
        <body>
        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border: solid 2px #f2786b;'>
            <tr>
                <td style='padding: 40px 30px 40px 30px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                        <tr style='padding: 20px 0 20px 0;'>
                            <td>
                                Dear $custName,<br/>
                                We would like to inform you that your order <b>$orderPoName</b> has been shipped.<br/>
                                The tracking number is <b>$orderPoResi</b>.
                            </td>
                        </tr>
                        <tr>
                            <td style='padding: 20px 0 30px 0;'>
                                $textShipment
                            </td>
                        </tr>
                        <tr>
                            <td style='padding: 20px 0 30px 0;'>
                                Please do not hesitate to contact us if you require any assistance.<br/>
                                Thank you,<br/><br/>
                                Odetta.<br/><br/><br/>
                                *this e-mail completes your order
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <tr>
            <td>
                <table style='width:100%;'>
                    <tr style='background-color:#f2786b;'>
                        <td style='padding: 10px 30px 10px 30px;' >
                            <h3 style='color:#fff;'>
                            Thank you <br/>
                            Do not forget to Subscribe to our newsletter to get the latest info.
                            </h3>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        </body>
        </html>
        ";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . '\r\n';
        $headers .= "Content-type:text/html;charset=UTF-8" . '\r\n';

        // More headers
        $headers .= "From: <donotreply@wholesalesystem.esy.es>" . '\r\n';

        $result = mail($to,$subject,$message,$headers);
        return $result; 
        }

        $obj_con->up();
        $N_orderPoID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_orderPoResi = mysql_real_escape_string(check_input($_POST['PO_resi']));
        $N_orderPoName = mysql_real_escape_string(check_input($_POST['PO_name']));
        $N_custEmail = mysql_real_escape_string(check_input($_POST['Cust_email']));
        $N_custName = mysql_real_escape_string(check_input($_POST['Cust_name']));
        $headerURL = "edit.php?id=$N_orderPoID";

        if($N_orderPoResi == '0'){
            $headerURL = "index.php?action=cancel&po_ID=$N_orderPoID";
        }
        else if($N_orderPoResi == '1'){
            $N_orderPoResi = 'PICK UP';
        }
        else{
            $N_orderPoResi = strtoupper($N_orderPoResi);
        }

        $result = $obj_po->update_resi($N_orderPoID, $N_orderPoResi,5);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $result = send_email($N_custEmail, $N_custName, $N_orderPoResi, $N_orderPoName, $J_shipment);
            $message = "The resi order has been updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    else if($_GET['action'] == "payment"){
        $obj_con->up();
        $N_poID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_lc = mysql_real_escape_string(check_input($_POST['lc']));
        $N_bank = mysql_real_escape_string(check_input($_POST['bank']));
        $N_applicant = mysql_real_escape_string(check_input($_POST['applicant']));
        $N_date = mysql_real_escape_string(check_input($_POST['date_issued']));
        $headerURL = "edit.php?id=$N_poID";

        $result = $obj_pay->insert_data($N_poID, $N_lc, $N_applicant, $N_bank, $N_date);
        if($result <= 0){
            $message = "There has been error when saving.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $stat_pay = $obj_po->update_po_status($N_poID,'3');
            $message = "Payment has been verified.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }else if($_GET['action'] == "ship"){
        $obj_con->up();
        $N_poID = mysql_real_escape_string(check_input($_POST['PO_ID']));
        $N_bol = mysql_real_escape_string(check_input($_POST['bol']));
        $N_cas = mysql_real_escape_string(check_input($_POST['cas']));
       
        $headerURL = "edit.php?id=$N_poID";

        $result = $obj_ship->insert_bol($N_poID, $N_bol, $N_cas);
        if($result <= 0){
            $message = "There has been error when saving.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $stat_pay = $obj_po->update_po_status($N_poID,'4');
            $message = "Bill of Lading has been saved.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    else if($_GET['action'] == "cancel"){
        $obj_con->up();
        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));
        $headerURL = "edit.php?id=$N_poID";

        $result = $obj_po->cancel_order($N_poID);
        if($result <= 0){
            $message = "There has been error when updating the order.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "The order has been canceled succesfully.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
    
    else if($_GET['action'] == "approve"){
        $obj_con->up();
        $N_poID = mysql_real_escape_string(check_input($_GET['po_id']));
        $N_poPaid = mysql_real_escape_string(check_input($_GET['po_isPaid']));
        $headerURL = "edit.php?id=$N_poID";

       
        $result = $obj_po->update_po_status($N_poID,'2');
        $reduce_stok = $obj_pro->reduce_stock($N_poID);
        if($result <= 0){
            $message = "There has been error when updating the order.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "The order has been approved.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
        $_SESSION['status'] = $message;
        header("Location: $headerURL");
        $obj_con->down();
    }
}

?> 