<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();

require_once("../../model/kuesioner.php");
$obj_kuesioner = new kuesioner(); 


if(!isset($_GET['action'])){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    
    if(isset($_GET['id'])){
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $responden = $obj_kuesioner->get_data_detail($O_id );
    }
   // $datas = $obj_kuesioner->get_data_by_page($O_page);
    $gereja = $obj_kuesioner->get_gereja_list();
    $question = $obj_kuesioner->get_pertanyaan();
    //var_dump($question);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{

        $total_data = 0;
        $total_page = 0;
    }
   
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        $message = "";

        $N_nama = mysql_real_escape_string(check_input($_POST['nama']));       
        $N_umur = mysql_real_escape_string(check_input($_POST['umur']));
        $N_gender = mysql_real_escape_string(check_input($_POST['gender']));
        $N_sekolah = mysql_real_escape_string(check_input($_POST['sekolah']));

        $N_gereja = mysql_real_escape_string(check_input($_POST['gereja']));       
        $N_kelas = mysql_real_escape_string(check_input($_POST['kelas']));
        $N_guru = mysql_real_escape_string(check_input($_POST['guru']));
        $N_hp = mysql_real_escape_string(check_input($_POST['hp']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
       
       
    
        

        $result = $obj_kuesioner->insert_data($N_nama, $N_umur, $N_gender, $N_sekolah, $N_gereja,$N_kelas,$N_guru,$N_hp,$N_email);

        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){

            $question = $obj_kuesioner->get_pertanyaan();
            $total_score = 0;
            $inatensi=0;
            $hiperaktifitas=0;
            $impulsifitas=0;
            $none=0;

            if(is_array($question)) { foreach($question as $data) {
                    $kode=$data['id_q'];
                     
                    $simpan_nilai=$obj_kuesioner->insert_data_at($result, $kode, $_POST["ans_$kode"]);
                    
                    if($data['type'] != "none"){
                        $total_score =$total_score + $_POST["ans_$kode"];
                    }

                    if($data['type']=="inatensi"){
                        $inatensi=$inatensi+$_POST["ans_$kode"];
                    }
                    else if($data['type']=="hiperaktifitas"){
                        $hiperaktifitas=$hiperaktifitas+$_POST["ans_$kode"];
                    }
                    else if($data['type']=="impulsifitas"){
                        $impulsifitas=$impulsifitas+$_POST["ans_$kode"];
                    }
                    else{
                        $none++;
                    }

            }}

            $adhd = $obj_kuesioner->hasil_kuis($total_score);

            $hasil = $obj_kuesioner->update_hasil($result, $inatensi, $hiperaktifitas, $impulsifitas, $adhd, $total_score);

            $message = "Respond has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }       
             
        $direct = $path['kuesioner-hasil'].$result;
        $_SESSION['status'] = $message;
        echo "<script> window.location.href='$direct'; </script>";
        //header("Location : $direct");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

        $result = $obj_quesioner->delete_data($O_id);
      
        if($result <= 0){
            $message .= "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "Question has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>