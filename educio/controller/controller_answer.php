<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();

require_once("../../model/answer.php");
$obj_answer = new answer(); 


if(!isset($_GET['action'])){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
   
    $datas = $obj_answer->get_data_by_page($O_page);
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{

        $total_data = 0;
        $total_page = 0;
    }
   
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        $message = "";

        $N_nama = mysql_real_escape_string(check_input($_POST['nama']));    
       
       
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        

        $result = $obj_answer->insert_data($N_nama, $N_publish);

        if(!$result){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result){

            $at = count($_POST['opsi']);
            for($c=0; $c < $at; $c++){
                $save_at = $obj_answer->insert_at_data($result, $_POST['opsi'][$c]);                
            } 

            $message = "Answer has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }       
             
        $direct = $path['answer'] ;
        $_SESSION['status'] = $message;
        echo "<script> window.location.href='$direct'; </script>";
        //header("Location : $direct");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

        $result = $obj_answer->delete_data($O_id);
      
        if($result <= 0){
            $message .= "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "Question has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>