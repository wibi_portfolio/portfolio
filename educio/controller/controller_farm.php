<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/Farm.php");
$obj_farm = new Farm(); 

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $datas = $obj_farm->get_data();    
    if(is_array($datas)){
        $total_data = $datas[0]['total_data'] ;
    }else{
        $total_data = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_name = mysql_real_escape_string(check_input($_POST['title'])); 
        $N_kota = mysql_real_escape_string(check_input($_POST['kota']));        
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_telp = mysql_real_escape_string(check_input($_POST['telpon']));

        $result = $obj_farm->insert_data($N_kota, $N_alamat, $N_telp, $N_name);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Farm <i><b>'" . $N_name . "'</b></i> has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
      //var_dump($result);
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        if(isset($_GET['title'])){
            $O_title = mysql_real_escape_string(check_input($_GET['title']));    
        }else{
            $O_title = $O_id;
        }
        
        $result = $obj_farm->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Farm <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>