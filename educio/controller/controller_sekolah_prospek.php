<?php 
require_once("../../model/Connection.php");
$obj_con = new Connection();
require_once("../../model/sekolah.php");
$obj_sekolah = new sekolah(); 
require_once("../../model/provinsi.php");
$obj_provinsi = new provinsi();
require_once("../../model/kota.php");
$obj_kota = new kota();
require_once("../../model/kecamatan.php");
$obj_kecamatan = new kecamatan();
require_once("../../model/prospek.php");
$obj_prospek = new prospek();



include('../../parser/simple_html_dom.php');




if(!isset($_GET['action'])){
    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice
    $O_page = ((isset($_GET['page']))) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;

    


    if(isset($_GET['prov'])){
         $O_prov =  mysql_real_escape_string(check_input($_GET['prov'])) ;      
    }
    else{
        $O_prov = "";
    }

    if(isset($_GET['kota'])){
         $O_kota =  mysql_real_escape_string(check_input($_GET['kota'])) ; 
         $data_kota =  $obj_kota->get_kota($O_prov);

    }
    else{
        $O_kota = "";
    }

    if(isset($_GET['kec'])){
        $O_kec =  mysql_real_escape_string(check_input($_GET['kec'])) ; 
        $data_kec =  $obj_kecamatan->get_kecamatan($O_kota);     
    }
    else{
        $O_kec = "";
    }
   
    if(isset($_GET['tingkat'])){
         $O_tingkat=  mysql_real_escape_string(check_input($_GET['tingkat'])) ;      
    }
    else{
        $O_tingkat = "";
    }

    if(isset($_GET['keyword'])){
         $O_keyword=  mysql_real_escape_string(check_input($_GET['keyword'])) ;      
    }
    else{
        $O_keyword = "";
    }
    
    if(isset($_GET['user'])){
         $O_user=  mysql_real_escape_string(check_input($_GET['user'])) ;      
    }
    else{
        $O_user= "";
    }

     if(isset($_GET['ket'])){
         //$O_ket =  mysql_real_escape_string(check_input($_GET['ket'])) ;
                $O_ket = "";
                $S_ket =  array();
                $at = count($_GET['ket']);
                for($c=0; $c < $at; $c++){
                    $s_ket[] = $_GET['ket'][$c] ;
                    if($_GET['ket'][$c] =="M"){$O_ket.= " AND minat = '1' "; $o_m = 1;}
                    if($_GET['ket'][$c] =="U"){$O_ket .= " AND uang = '1' "; $o_u = 1;}
                    if($_GET['ket'][$c] =="R"){$O_ket .= " AND ruang = '1' "; $o_r = 1;}                 
                }                
            
    }
    else{
        $O_ket = "";
    }

    if(isset($_GET['tgl'])){
        $O_tgl =  mysql_real_escape_string(check_input($_GET['tgl'])) ; 
         
    }
    else{
        $O_tgl = "";
    }
   //$obj_sekolah->get_data_sekolah("016002");
   
    
   
    $datas = $obj_prospek->get_data_by_page($O_page,$O_prov,$O_kota,$O_kec,$O_tingkat, $O_user,$O_ket,$O_keyword, $O_tgl);
    $provinsi = $obj_provinsi->get_provinsi();
    $datas_user = $obj_prospek->get_data_user();


   // ini_set('max_execution_time', 3000);

    /*foreach ($provinsi as $data) {
        $obj_kota->get_data_kota($data['kode'],$data['id']);
        # code...
    }

    */
    //var_dump($provinsi);
     
   
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{

        $total_data = 0;
        $total_page = 0;
    }
   
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }   

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        $message = "";

        $N_nama = mysql_real_escape_string(check_input($_POST['nama']));       
        $N_nisp = mysql_real_escape_string(check_input($_POST['nisp']));
        $N_status = mysql_real_escape_string(check_input($_POST['status']));
        $N_sekolah = mysql_real_escape_string(check_input($_POST['sekolah']));
        $N_alamat = mysql_real_escape_string(check_input($_POST['alamat']));
        $N_prov = mysql_real_escape_string(check_input($_POST['prov']));
        $N_kota = mysql_real_escape_string(check_input($_POST['kota']));
        $N_kec = mysql_real_escape_string(check_input($_POST['kec']));
        $N_telp= mysql_real_escape_string(check_input($_POST['telp']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
       
       
        
        

        $result = $obj_sekolah->insert_data($N_nama, $N_nisp, $N_sekolah, $N_status, $N_alamat, $N_prov, $N_kota, $N_kec, $N_telp, $N_email);
        
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "School has been succesfully added.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }       
             
        $direct = $path['sekolah'] ;
        $_SESSION['status'] = $message;
        echo "<script> window.location.href='$direct'; </script>";
        //header("Location : $direct");
        $obj_con->down();

    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

        $result = $obj_sekolah->delete_data($O_id);
      
        if($result <= 0){
            $message .= "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "School has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();

    }else if($_GET['action'] == "responden"){
        $obj_con->up();
        $message = "";
        $O_id = mysql_real_escape_string(check_input($_GET['id']));                  

        $result = $obj_quesioner->set_responden($O_id,1);
      
        if($result <= 0){
            $message .= "Something is wrong with your submission <br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message .= "Responden has been add successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>