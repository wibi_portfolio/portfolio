<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1'); 
session_start();
require_once("back_config.php");
require_once("check_input.php");
   function convert_status($status){
        $result = "";
        if($status==2){
            $result='<span class="label label-warning">Tidak Diangkat</span>' ;
        }elseif($status == 3) {
            $result='<span class="label label-danger">Error</span>' ;             
        }elseif($status == 1) {
            $result='<span class="label label-success">Diangkat</span>' ;                            
        }else {
            $result='<span class="label label-default">Belum dicek</span>' ;
        } 
        return $result ;                                       
    }

     function convert_prospek($status){
        $result = "";
        if($status==0){
            $result='<span class="label label-default">Belum ditetapkan</span>' ;
        }elseif($status == -1) {
            $result='<span class="label label-danger">Tidak Prospek</span>' ;             
        }elseif($status == 1) {
            $result='<span class="label label-success">Prospek</span>' ;                            
        }
        return $result ;                                       
    }

     function convert_role($status){
        $result = "";

        if($status == 2) {
            $result='Staff' ;                            
        }else {
            $result='Administrator' ;
        } 
        return $result ;                                       
    }

    function check_expired($tgl){
        $result = false ;
        $exp = new DateTime($tgl);
        $now = new DateTime();
        $exp->modify('-6 month');

        $result = ($exp < $now);
        return $result ;
    }
?>