<?php 
$global['absolute-url'] = "http://localhost/educio/";
$global['tes-url'] = $global['absolute-url'];
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['img-url'] = $global['absolute-url'];
$global['absolute-url-admin'] = $global['absolute-url'];
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url-admin']."assets/images/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."images/logo/logo-mobile.png";
$global['logo-desktop'] = $global['absolute-url']."images/logo/logo-desktop.png";
$global['logo-agb']=$global['absolute-url']."assets/images/logo.jpg";
$global['logo-t']=$global['absolute-url']."assets/images/logo_trans.png";

$path['home'] = $global['absolute-url'];
$path['login'] = $global['absolute-url-admin']."index.php";
$path['logout'] = "?action=logout";

$seo['company-name'] = "Educio Telemarketing";
$global['copyright'] = "&copy; Educio ".date('Y')." All rights reserved.";
$global['powered'] = date('Y')." &copy; Powered by <a id='copyright-link' href='' target='_blank'>Educio</a>.";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";



// module user
$title['user'] = $seo['company-name']." | Module User Admin";

$path['user'] = $global['absolute-url-admin']."module/user/index.php";
$path['user-edit'] = $global['absolute-url-admin']."module/user/edit.php?id=";
// dashboard
$title['dash'] = $seo['company-name']." | Dashboard";

$path['dash'] = $global['absolute-url-admin']."module/dashboard/index.php";

// module guru
$title['guru'] = $seo['company-name']." | Module Data Guru";

$path['guru'] = $global['absolute-url-admin']."module/guru/index.php";
$path['guru-edit'] = $global['absolute-url-admin']."module/guru/edit.php?id=";

// module prospek
$title['karyawan'] = $seo['company-name']." | Module Record Karyawan";

$path['karyawan'] = $global['absolute-url-admin']."module/karyawan/index.php";
$path['karyawan-list'] = $global['absolute-url-admin']."module/karyawan/list.php?id=";
$path['karyawan-ket'] = $global['absolute-url-admin']."module/karyawan/detail.php?id=";

// module prospek
$title['prospek'] = $seo['company-name']." | Module Prospek Sekolah";

$path['prospek'] = $global['absolute-url-admin']."module/prospek/index.php";
$path['prospek-edit'] = $global['absolute-url-admin']."module/prospek/edit.php?id=";

// module sekolah
$title['sekolah'] = $seo['company-name']." | Module Data Sekolah";

$path['sekolah'] = $global['absolute-url-admin']."module/sekolah/index.php";
$path['sekolah-add'] = $global['absolute-url-admin']."module/sekolah/insert.php";
$path['sekolah-edit'] = $global['absolute-url-admin']."module/sekolah/edit.php?id=";

// module gereja
$title['gereja'] = $seo['company-name']." | Module Gereja";

$path['gereja'] = $global['absolute-url-admin']."module/gereja/index.php";
$path['gereja-add'] = $global['absolute-url-admin']."module/gereja/insert.php";
$path['gereja-edit'] = $global['absolute-url-admin']."module/gereja/edit.php?id=";

// module responden
$title['responden'] = $seo['company-name']." | Module Responden";

$path['responden'] = $global['absolute-url-admin']."module/responden/index.php";
$path['responden-add'] = $global['absolute-url-admin']."module/responden/insert.php";
$path['responden-edit'] = $global['absolute-url-admin']."module/responden/edit.php?id=";
// drop
$title['drop'] = $seo['company-name']." | Discard Barang Masuk";

$path['drop'] = $global['absolute-url-admin']."module/drop/index.php";

// module barang masuk
$title['incoming'] = $seo['company-name']." | Module Barang Masuk";

$path['incoming'] = $global['absolute-url-admin']."module/incoming/index.php";
$path['incoming-edit'] = $global['absolute-url-admin']."module/incoming/edit.php?id=";
$path['incoming-drop'] = $global['absolute-url-admin']."module/incoming/drop.php?id=";

// module customer
$title['customer'] = $seo['company-name']." | Module Customer";

$path['customer'] = $global['absolute-url-admin']."module/customer/index.php";
$path['customer-edit'] = $global['absolute-url-admin']."module/customer/edit.php?id=";
$path['customer-address'] = $global['absolute-url-admin']."module/customer/address.php?id=";
$path['customer-order'] = $global['absolute-url-admin']."module/customer/order.php?id=";
$path['customer-wishlist'] = $global['absolute-url-admin']."module/customer/wishlist.php?id=";

// module category
$title['category'] = $seo['company-name']." | Module Category";

$path['category'] = $global['absolute-url-admin']."module/category/index.php";
$path['category-edit'] = $global['absolute-url-admin']."module/category/edit.php?id=";

// module coupon
$title['coupon'] = $seo['company-name']." | Module Coupon";
$path['coupon'] = $global['absolute-url-admin']."module/coupon/index.php";
$path['coupon-edit'] = $global['absolute-url-admin']."module/coupon/edit.php?id=";

// module quesioner
$title['kuesioner'] = $seo['company-name']." | Module Kuesioner";
$path['kuesioner'] = $global['tes-url']."index.php";
$path['kuesioner-hasil'] = $global['tes-url']."hasil.php?id=";


// module quesioner
$title['quesioner'] = $seo['company-name']." | Module Question";

$path['quesioner'] = $global['absolute-url-admin']."module/quesioner/index.php";
$path['quesioner-edit'] = $global['absolute-url-admin']."module/quesioner/edit.php?id=";
$path['quesioner-add'] = $global['absolute-url-admin']."module/quesioner/insert.php";

// module answer
$title['answer'] = $seo['company-name']." | Module Answer";

$path['answer'] = $global['absolute-url-admin']."module/answer/index.php";
$path['answer-edit'] = $global['absolute-url-admin']."module/answer/edit.php?id=";
$path['answer-add'] = $global['absolute-url-admin']."module/answer/insert.php";

// module order history
$title['order'] = $seo['company-name']." | Module Order History";

$path['order'] = $global['absolute-url-admin']."module/order/index.php";
$path['order-add'] = $global['absolute-url-admin']."module/order/insert.php";
$path['order-edit'] = $global['absolute-url-admin']."module/order/edit.php?id=";
$path['order-addpo'] = $global['absolute-url-admin']."module/order/insertpo.php?id=";

// module confirmation
$title['confirmation'] = $seo['company-name']." | Module Payment Confirmation";

$path['confirmation'] = $global['absolute-url-admin']."module/confirmation/index.php";

// module social
$title['social'] = $seo['company-name']." | Module Social Media";

$path['social'] = $global['absolute-url-admin']."module/social/index.php";

// module shipping
$title['shipping'] = $seo['company-name']." | Module Shipping";

$path['shipping'] = $global['absolute-url-admin']."module/shipping/index.php";
$path['shipping-edit'] = $global['absolute-url-admin']."module/shipping/edit.php?id=";



// module content
$title['content'] = $seo['company-name']." | Module Content";

$path['content'] = $global['absolute-url-admin']."module/content/index.php";
$path['content-return'] = $global['absolute-url-admin']."module/content/return.php";
$path['content-order'] = $global['absolute-url-admin']."module/content/order.php";
$path['content-faq'] = $global['absolute-url-admin']."module/content/faq.php";
$path['content-email'] = $global['absolute-url-admin']."module/content/email.php";

// module profile
$title['profile'] = $seo['company-name']." | Module Profile";

$path['profile'] = $global['absolute-url-admin']."module/profile/index.php";

// module user
$title['user'] = $seo['company-name']." | Module User Administrator";

$path['user'] = $global['absolute-url-admin']."module/user/index.php";
$path['user-edit'] = $global['absolute-url-admin']."module/user/edit.php?id=";
?>