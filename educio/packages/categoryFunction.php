<?php

//function recursive for category list select box in module/category/index.php
function category_list($parent=0, $stripe=""){
    require_once("../../../model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name FROM T_CATEGORY WHERE category_parentID = '$parent' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);
    
    if($parent == 0){
        $stripe = "";
    }else{
        $stripe .= "-";
    }

    if($count > 0){
        while($row = mysql_fetch_assoc($sql)){
            $space = ($stripe != "") ? "&nbsp;" : "";
            echo "<option value='".$row['category_ID']."'>".$stripe.$space.$row['category_name']."</option>";   
            category_list($row['category_ID'], $stripe); //panggil diri sendiri
        }       
    }

    $obj_con->down();
}

//function recursive for select category list in module/category/edit.php
function select_category_list($parent=0, $stripe="", $data_parent){
    require_once("../../../model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name FROM T_CATEGORY WHERE category_parentID = '$parent' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);
    
    if($parent == 0){
        $stripe = "";
    }else{
        $stripe .= "-";
    }

    if($count > 0){
        while($row = mysql_fetch_assoc($sql)){
            $space = ($stripe != "") ? "&nbsp;" : "";
            $selected = ($row['category_ID'] == $data_parent) ? "selected=selected" : "";
            echo "<option ".$selected." value='".$row['category_ID']."'>".$stripe.$space.$row['category_name']."</option>";   
            select_category_list($row['category_ID'], $stripe, $data_parent); //panggil diri sendiri
        }       
    }

    $obj_con->down();
}

//function recursive for category breadcrumb in module/category/edit.php
function category_breadcrumb($id){
    require_once("../../../model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_name, category_parentID FROM T_CATEGORY WHERE category_ID = '$id'";
    $sql = mysql_query($query);
    $row = mysql_fetch_assoc($sql);

    $name = $row['category_name'];  
    if($row['category_parentID'] == 0){
        echo "<a href='edit.php?id=".$id."'>".$name."</a>";
    }else{
        echo category_breadcrumb($row['category_parentID'])."<li><a href='edit.php?id=".$id."'>".$name."</a></li>";
    }

    $obj_con->down();
}
?>