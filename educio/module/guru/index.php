<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_guru.php");
	$curpage="guru";
	$page_name = "index.php";
	$kla_name = "&klasis=$O_klasis";
	$gre_name = "&gereja=$O_gereja";
	

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['guru'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.min.css" rel="stylesheet" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Teacher Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<div class="row">
											<form action="" method="GET">

												<select name="klasis" class="js-example-basic-single col-sm-5 col-xs-12 pad1">
													<option value="">Seluruh  Klasis</option>
					                                <option value="Jakarta Barat" <?php if($O_klasis == "Jakarta Barat"){ echo "selected"; }?>>Jakarta Barat</option>
					                                <option value="Jakarta Timur" <?php if($O_klasis == "Jakarta Timur"){ echo "selected"; }?>>Jakarta Timur</option>
					                                <option value="Jakarta Selatan" <?php if($O_klasis == "Jakarta Selatan"){ echo "selected"; }?>>Jakarta Selatan</option>
					                                <option value="Jakarta Utara" <?php if($O_klasis == "Jakarta Utara"){ echo "selected"; }?>>Jakarta Utara</option>
					                                <option value="Bandung" <?php if($O_klasis == "Bandung"){ echo "selected"; }?>>Bandung</option>
					                                <option value="Cirebon" <?php if($O_klasis == "Cirebon"){ echo "selected"; }?>>Cirebon</option>
					                                <option value="Banten" <?php if($O_klasis == "Banten"){ echo "selected"; }?>>Banten</option>
					                                <option value="Priangan" <?php if($O_klasis == "Priangan"){ echo "selected"; }?>>Priangan</option>
					                                <option value="Jakarta 1" <?php if($O_klasis == "Jakarta 1"){ echo "selected"; }?>>Jakarta 1</option>
					                                <option value="Jakarta 2" <?php if($O_klasis == "Jakarta 2"){ echo "selected"; }?>>Jakarta 2</option>
					                            </select>
											
												<select class="js-example-basic-single col-sm-5 col-xs-12 pad1" id="gereja" name="gereja">
													<option value="">Seluruh Gereja</option>
														<?php if(is_array($list)) { foreach($list as $data) { ?>
						                                <option value="<?=$data['id_g'];?>" <?php if($O_gereja == $data['id_g']){ echo "selected"; }?>><?=$data['nama'];?></option>
						                         		<?php }} ?>
												</select>
										

											
												

											
											<div class="col-sm-2 col-xs-12 pad1  text-center">

													<button type="submit" class="btn btn-primary">
														Filter <i class="fa fa-search"></i> 
													</button>
												
											</div>

											</form>
										
										</div>
									


									</div>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">

												<div style="text-align:right;margin: 10px 0;">
													<h5> Total guru: <span class="label label-primary"><?=$total_data;?></span> </h5>
												</div>
											</div>
										</div>
									
									<div class="row">
										<?php if(is_array($datas)) {  ?>
										<table id="guru_table" class="display" cellspacing="0" width="100%">
											<thead>
											<tr>
												<th>#</th>
												<th>Nama</th>
												<th>No. Hp</th>
												<th>Email</th>	
												<th>Gereja</th>
												<th>Klasis</th>												
												
											</tr>
											</thead>
											<tbody>
											<?php 												
												if(is_array($datas)) { $num=1;foreach($datas as $data) { 												
												
											?>
											
											<tr>
												<td><?=$num;?>.</td>
												<td><?=$data['guru'];?></td>
												<td><?=$data['hp'];?></td>
												<td><?=$data['email'];?></td>												
												<td><?=$data['namag'];?></td>
												<td><?=$data['klasis'];?></td>
									
											</tr>
											
											<?php $num++;} }  ?>
												</tbody>
											
										</table>
										<?php } ?>
									</div>

					
										

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
										
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total guru : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no responden right now!
											</div>
										</div>
									</div>
									<?php }?>
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add New Question</h4>
								</div>
								<form name="addProduct" action="index.php?action=add" enctype="multipart/form-data" method="post" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Pertanyaan <span class="symbol required"></span></strong> </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<textarea id="input-desc" name="desc" class="summernote form-control" rows="5" placeholder="Question"></textarea>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nomor urut <span class="symbol required"></span></strong> </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="no" type="text" class="form-control" required placeholder="Nomor urut" />
		                                    </div>
		                                </div>

		                                 <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Type <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="tipe" class="form-control">
					                                <option value="radio">Radio</option>
					                                <option value="text">Text</option>
					                            </select>
					                        </div>
					                   	</div>

					                   	 <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Opsi Jawaban <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="opsi" class="form-control">
					                                <?=$opsi;?>
					                            </select>
					                        </div>
					                   	</div>

		                                <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="publish" class="form-control">
					                                <option value="1">Publish</option>
					                                <option value="0">Not Publish</option>
					                            </select>
					                        </div>
					                   	</div>
		                              
		                             
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/js/select2.min.js"></script>
			<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.js"></script>
	
		<!-- export data tables -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.flash.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/jszip.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/pdfmake.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/vfs_fonts.js"></script>

		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.html5.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.print.min.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				//$(".js-example-basic-single").select2();
				$(".fancybox").fancybox({
					padding : 0
				});	

			});	


				 $('#guru_table').DataTable( {
    					responsive: true,
    					dom: 'Bfrtip',
    					lengthMenu: [
				            [ 10, 25, 50],
				            [ '10 rows', '25 rows', '50 rows']
				        ],
				        buttons: [
				            'pageLength', 'excel', 'pdf', 'print'
				        ]
    					

				} );

			$('.js-example-basic-single').selectize({
			    create: true,
			    sortField: 'text'
			});			
		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

					//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>