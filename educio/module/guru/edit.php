<?php 
include("../../packages/require.php");
include("../../controller/controller_responden_detail.php");

include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php

$curpage="responden";


?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['responden'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.min.css" rel="stylesheet" />
	
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
									<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
									</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['responden'];?>">
										Responden Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['nama'];?>
									
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Responden <span class="text-bold">(<?=$datas[0]['nama'];?>)</span></h4>
								</div>
								<form name="editProduct" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
									<div class="panel-body">
										<div class="form-body" style="color:#131516;">
											<?php if($_SESSION['user_status'] != 1) {$disabled = "disabled"; } else {$disabled ="";} ?>
											
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nama Anak </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="nama" type="text" class="form-control" placeholder="Nama Anak"  value="<?=$datas[0]['nama'];?>"/>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Usia anak  </strong></div>
												<div class="col-sm-3 col-xs-12 up1">
		                                    		<div class="input-group">
																
																<input id="input-price" type="text" class="form-control" name="umur" value="<?=$datas[0]['usia'];?>">
																<span class="input-group-addon" style="color:#000;background-color: #ddd;border-color: #ddd;">tahun</span>
															</div>
		                                    	</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Jenis kelamin </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<div class="radio">
																<label style="color:#131516;"><input type="radio" name="gender" value="Laki-laki" <?php if($datas[0]['gender'] == "Laki-laki"){ echo "checked"; }?>>Laki-laki</label>
															</div>
															<div class="radio">
																<label style="color:#131516;"><input type="radio" name="gender" value="Perempuan" <?php if($datas[0]['gender'] == "Perempuan"){ echo "checked"; }?>>Perempuan</label>
		                                    				</div>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Tingkat Sekolah </strong></div>
												<div class="col-sm-4 col-xs-12 up1">
															<select class="form-control" name="sekolah">
																<option value="1 SD" <?php if($datas[0]['tingkat_sekolah'] == "1 SD"){ echo "selected"; }?>>1 SD</option>
																<option value="2 SD" <?php if($datas[0]['tingkat_sekolah'] == "2 SD"){ echo "selected"; }?>>2 SD</option>
																<option value="3 SD" <?php if($datas[0]['tingkat_sekolah'] == "3 SD"){ echo "selected"; }?>>3 SD</option>
																<option value="4 SD" <?php if($datas[0]['tingkat_sekolah'] == "4 SD"){ echo "selected"; }?>>4 SD</option>
																<option value="5 SD" <?php if($datas[0]['tingkat_sekolah'] == "5 SD"){ echo "selected"; }?>>5 SD</option>
																<option value="6 SD" <?php if($datas[0]['tingkat_sekolah'] == "6 SD"){ echo "selected"; }?>>6 SD</option>

															</select>
															
													
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div><br>


											

			                                 <div class="row">
						                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Gereja Kristen Indonesia </strong></div>
						                        
						                            <select name="gereja" class="col-sm-6 col-xs-12 " id="gereja" name="gereja">
						                            	<?php if(is_array($list)) { foreach($list as $data) { ?>
						                                <option value="<?=$data['id_g'];?>" <?php if($datas[0]['gki'] == $data['id_g']){ echo "selected"; }?>><?=$data['nama'];?></option>
						                         		<?php }} ?>
						                            </select>
						                       
						                   	</div>

						                   	<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Kelas Sekolah Minggu </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="kelas" type="text" class="form-control" placeholder="Kelas Sekolah Minggu" value="<?=$datas[0]['kelas'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Guru Sekolah Minggu </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="guru" type="text" class="form-control" placeholder="nama guru"   value="<?=$datas[0]['guru'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>No HP/WA Guru </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="hp" type="text" class="form-control" placeholder="No. HP/WA Guru"  value="<?=$datas[0]['hp'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>


											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Email  </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input id="input-email1" name="email" type="text" class="form-control" placeholder="alamat email *"  value="<?=$datas[0]['email'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Total Score </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<?=$datas[0]['total_score'];?>
												</div>
											</div>
						
											<div class="row">
					                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Subskala Inatensi </strong> </div>
					                                    <div class="col-sm-8 col-xs-12 up1">
					                                     	<?php echo $datas[0]['inatensi']." (".$obj_quesioner->skala($datas[0]['inatensi']).")";?>
					                                    </div>
					                         </div>

					                                <div class="row">
					                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Subskala Hiperaktifitas/ Impulsifitas  </strong> </div>
					                                    <div class="col-sm-8 col-xs-12 up1">
					                                    	 <?=$datas[0]['hiper']." (".$obj_quesioner->skala($datas[0]['hiper']).") / ".$datas[0]['impulsif']." (".$obj_quesioner->skala($datas[0]['impulsif']).")";?>
					                                    </div>
					                                </div>

						                   <div class="row">														
															<div class="col-sm-4 col-xs-12 up1 form-label" style="color:#131516;">
																<strong>Hasil 		</strong>								
														
															</div>
															<div class="col-sm-8 col-xs-12 up1 bold">
																<?=$datas[0]['hasil'];?>
															</div>
														
													</div>
																				
											
											
									


										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['id_res'];?>">
											<input type="hidden" name="url" value="<?=$path['responden-edit'].$O_id;?>">
											<div class="col-sm-4 col-xs-12 pad0">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['id_res'];?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['responden'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-chevron-circle-left"></i> Back
													</a>
												
													<button type="submit" class="btn btn-success">
														<i class="fa fa-pencil-square-o fa fa-white"></i> Edit
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['responden'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-chevron-circle-left"></i> Back
															</a>
															
															<button type="submit" class="btn btn-success">
																<i class="fa fa-pencil-square-o fa fa-white"></i> Edit
															</button>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

	<?php include("../../packages/footer-js.php");?>
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/js/select2.min.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script type="text/javascript">
			$(document).ready(function() {

			    $("#gereja").select2();
			});
		</script>
	<script type="text/javascript">
		function validateForm(){
                var name = $("#input-name").val();
                var price = $("#input-price").val();
                var weight = $("#input-weight").val();
                var numFormat = /^[0-9.,\b]+$/;
                var weightFormat = /^[0-9.,\b]+$/;
                var category = $("#input-category").val();
                
                if (category != "") {
                    $("#error-category").html("");
                    $("#error-category").hide();
                    $("#input-category").removeClass("input-error");
				} else {
                    $("#error-category").show();
                    $("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-category").addClass("input-error");
                    return false;
				}
                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
                if(price != ""){
                    if(price.match(numFormat)){
                        $("#error-price").html("");
                        $("#error-price").hide();
                        $("#input-price").removeClass("input-error");
                    } else {
                        $("#error-price").show();
                        $("#error-price").html("<i class='fa fa-warning'></i> This field must contain number only.");
                        $("#input-price").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-price").show();
                    $("#error-price").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-price").addClass("input-error");
                    return false;
                }
                if(weight != ""){
                    if(weight.match(weightFormat)){
                        $("#error-weight").html("");
                        $("#error-weight").hide();
                        $("#input-weight").removeClass("input-error");
                    } else {
                        $("#error-weight").show();
                        $("#error-weight").html("<i class='fa fa-warning'></i> This field must contain number, comma(,), and dot(.) only.");
                        $("#input-weight").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-weight").show();
                    $("#error-weight").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-weight").addClass("input-error");
                    return false;
                }
            }
	
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
			<?php if($alert != "success"){ ?>
				//error alert
				errorAlert(alertText);
			<?php } else { ?>
				//success alert
				successAlert(alertText); 
			<?php } ?>
			 
		<?php } ?>

		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}

	
	</script>

</body>
<!-- end: BODY -->
</html>