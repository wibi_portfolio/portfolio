<?php 
	include("../../packages/require.php");
	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_dashboard.php");
	$curpage="dashboard";
	$page_name = "index.php";

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['dash'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		
		
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />

	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1 class="display-4">
										Educio <small class="text-muted"> Telemarketing </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Dashboard
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light ">
										

									</div>

								
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													
												</div>
											</div>
										</div>
									
																	
											<?php 
												
												if(is_array($datas)) {
												$prov = "";
												$prospek = array();
												$tdk = array();
												$blm = array();
												$loop= 0;

												foreach($datas as $data) { 
													$prospek[$loop]= $data['num_pros'] ;
													$tdk[$loop]= $data['num_tdk'] ;
													$blm[$loop]= $data['num_blm'] ;
													$prov[$loop] = "'".$data['nama']."'" ;
													$loop++;	
												} }

											?>
									
									<div class="row">
									<div class="col-sm-10 col-xs-12 text-center">
									</div>
										<div class="col-sm-2 col-xs-12 text-center">

													<button id="prints" class="btn btn-secondary btn-sm">
														Print Chart <i class="fa fa-print"></i> 
													</button>
												
										</div>
									</div>
										<div class="row" >
											
																	
											<div class="col-md-12 col-sm-12" id="chart_prov"  ></div>
											
										</div>
										

										
								
									                											
									  <div class="row">

									  	<h3 class="display-3 text-center pad2" style="color:#131516;">
											
										</h3>
									  </div>


								

										<div class="panel-heading border-light">
											<h4 class="panel-title"></span></h4>
										</div>

							
									</div>
									
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/highchart/highcharts.js"></script>
		
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/highchart/exporting.js"></script>
		<script  src="<?=$global['absolute-url-admin'];?>assets/plugins/highchart/offline-exporting.js"></script>
	
	
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/js/select2.min.js"></script>
		<!-- export data tables -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.flash.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/jszip.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/pdfmake.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/vfs_fonts.js"></script>

		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.html5.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.print.min.js"></script>

		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>
		
		
		
		
		<!-- export data tables -->
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});

			});

				//$(".js-example-basic-single").select2();


					

					    var myChartg = Highcharts.chart('chart_prov', {
					    	
					        chart: {
					            type: 'bar'
					        },
					        title: {
					            text: 'Data Prospek per Provinsi'
					        },
					          xAxis: {
           					 categories: [<?php echo join($prov, ',') ?>]
       						 },
       						 yAxis: {
           					 title: "Jumlah Sekolah"
       						 },
					        
					        series: [
					        {
					        	name : "Prospek",
					            data: [<?php echo join($prospek, ',') ?>],
         						
         						
					        },
					        {
					        	name : "Tidak Prospek",
					            data: [<?php echo join($tdk, ',') ?>],
         						
         						
					        }
					        
					        ],
					           navigation: {
						        buttonOptions: {
						            enabled: false
						        }
						    }     


					        
					    });

						$('#prints').click(function () {
							    myChartg.print();
						});
					
			

		

		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

					//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>