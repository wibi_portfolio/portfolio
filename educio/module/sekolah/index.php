<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_sekolah.php");
	$curpage="sekolah";
	$page_name = "index.php";
	$p_prov = "&prov=".$O_prov;
	$p_kot = "&kota=".$O_kota;
	$p_kec = "&kec=".$O_kec;
	$p_tingkat = "&tingkat=".$O_tingkat;
	$p_status= "&status=".$O_status;
	
	$p_key = "&keyword=".$O_keyword;

	if(is_array($s_ket)){		
		$p_ket = http_build_query(array("ket"=>$s_ket)) ;		
	}else{
		$p_ket = "";
	}
	
	

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['sekolah'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.min.css" rel="stylesheet" />

		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1 class="display-4">
										Educio <small class="text-muted"> Telemarketing </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
									<h3>	School Report </h3>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<div class="row">
											<form action="" method="GET">
												<div class="row">

														<div class="col-sm-4 col-xs-12 up1">
															<div class="row text-center" style="color:#1c5cb5;">
																<strong>Provinsi</strong>	
															</div>
															<div class="row up1">
															<select class="js-example-basic-single" id="prov" name="prov" style="width:100%">
															<option value="">pilih provinsi</option>
																<?php if(is_array($provinsi)) { $num=1;foreach($provinsi as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']==$O_prov){echo "selected";}?>><?=$data['nama'];?></option>
																<?php } }?>																
															</select>
															</div>
		                                    				
		                                    			</div>

														<div class="col-sm-4 col-xs-12 up1">

															<div class="row text-center" style="color:#1c5cb5;">
																<strong>Kabupaten/Kota	</strong>
															</div>
															<div class="row up1">
														
																<select id="select_kota" class="js-example-basic-single" name="kota" style="width:100%">
																	<option value="">pilih kabupaten/Kota</option>	
																	<?php if(is_array($data_kota)) { $num=1;foreach($data_kota as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']== $O_kota){echo "selected";}?> ><?=$data['nama'];?></option>
																	<?php } }?>																
																</select>
															</div>
			                                    			
		                                    				</div>
											
														<div class="col-sm-4 col-xs-12 up1">
															<div class="row text-center" style="color:#1c5cb5;">
																<strong>Kecamatan</strong>
																	
															</div>
															<div class="row up1">
														
															<select class="js-example-basic-single" name="kec" id="kec" style="width:100%">
																<option value="">pilih kecamatan</option>
																<?php if(is_array($data_kec)) { $num=1;foreach($data_kec as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']== $O_kec){echo "selected";}?>><?=$data['nama'];?></option>
																<?php } }?>	
																
															</select>
															</div>
		                                    				
		                                    			</div>
		                                    	</div>
										

											<div class="row">
												

											<div class="col-sm-4 col-xs-12 up2">
												<div class="row text-center" style="color:#1c5cb5;">
													<strong>Tingkat Sekolah</strong>
												</div>
												<div class="row up1">
												<select class="js-example-basic-single" name="tingkat" style="width:100%">
													<option value="">semua tingkat</option>
													<option value="SD" <?php if($O_tingkat == "SD"){ echo "selected"; }?>>SD</option>
													<option value="SMP" <?php if($O_tingkat == "SMP"){ echo "selected"; }?>>SMP</option>
													<option value="SMA" <?php if($O_tingkat == "SMA"){ echo "selected"; }?>>SMA</option>
													
												</select>
												</div>
											</div>

											<div class="col-sm-4 col-xs-12 up2">
												<div class="row text-center" style="color:#1c5cb5;">
													<strong>Status Kontak</strong>
												</div>
												<div class="row up1">
												<select class="js-example-basic-single" name="status" style="width:100%">
													<option value="">semua status</option>
													<option value="0" <?php if($O_status == "0"){ echo "selected"; }?>>Belum dicek</option>
													<option value="1" <?php if($O_status == "1"){ echo "selected"; }?>>Diangkat</option>
													<option value="2" <?php if($O_status == "2"){ echo "selected"; }?>>Tidak Diangkat</option>
													<option value="3" <?php if($O_status == "3"){ echo "selected"; }?>>Error</option>
													
												</select>
												</div>
											</div>

											<div class="col-sm-4 col-xs-12 up2">
												<div class="row text-center" style="color:#1c5cb5;">
													<strong>Keterangan</strong>
												</div>
												<div class="row up1">
												<select class="js-example-basic-multiple" name="ket[]" style="width:100%" multiple="multiple" >
													<option value="">semua keterangan</option>
													<option value="M" <?php if($o_m){ echo "selected"; }?>>Minat</option>
													<option value="U" <?php if($o_u){ echo "selected"; }?>>Uang</option>
													<option value="R" <?php if($o_r){ echo "selected"; }?>>Ruang</option>
													
													
												</select>
												</div>
											</div>									
											

										</div>

									

										<div class="row up2">
											<div class="col-sm-4 col-xs-12"></div>
											<div class="col-sm-4 col-xs-12 text-center">
													<div class="col-sm-6 col-xs-6 pad0">												
														<input  name="keyword" type="text" class="" placeholder="kata kunci"  value="<?=$O_keyword;?>" />
														
													</div>
													<div class="col-sm-6 col-xs-6 pad0">
													<button type="submit" class="btn btn-primary btn-sm">
															Cari Sekolah <i class="fa fa-search"></i> 
														</button>
													</div>											
												
											</div>
											<div class="col-sm-4 col-xs-12"></div>
										</div>
											</form>
										
										</div>
									


									</div>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">

												<div style="text-align:right;margin: 10px 0;">
													<h5> Total sekolah: <span class="label label-primary"><?=$total_data;?></span> </h5>
												</div>
											</div>
										</div>
									
									<div class="row">
										<?php 												
												if(is_array($datas)) { 												
												
											?>
										<table class="display" cellspacing="0" width="100%" id="tabel_responden">
										<thead>
											<tr>
												<th>No</th>
												<th>NPSN</th>
												<th>Nama Sekolah</th>
												<th>Tingkat</th>
												<th>Status</th>	
												<th>Alamat</th>
												<th>No. telp</th>
												<th>Status Kontak</th>
												<th>Keterangan </th>
												

												
																									
												
												<?php if($_SESSION['user_status'] != 4) { ?>											
												<th>Action</th>
												<?php } ?>
											</tr>
											</thead>
											<tbody>
											<?php 												
												if(is_array($datas)) { $num=1;foreach($datas as $data) { 												
												
											?>
											<tr>
												<td><?=($O_page-1)*10+$num;?>.</td>
												
												<td><?=$data['nisp'];?></td>
												<td><?=$data['nama'];?></td>
												<td><?=$data['tingkat'];?></td>
												<td><?=$data['status'];?></td>

												<td><?=$data['alamat'];?></td>	
												<td><?php echo $data['fax'];?></td>
												<td><?=convert_status($data['stat_telp']);?></td>	
												<td><?php if($data['minat']) { ?> <span class="badge">M</span> <?php } ?>
												<?php if($data['uang']) { ?> <span class="badge">U</span> <?php } ?>
												<?php if($data['ruang']) { ?> <span class="badge">R</span> <?php } ?>
												</td>					
											
												
											

																
												
												
												<?php if($_SESSION['user_status'] != 4) { ?>
												<td>
													<div class="btn-group-vertical up1" role="group">
														
														<a href="<?=$path['sekolah-edit'].$data['id'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View">Detail <i class="fa fa-eye fa fa-white"></i></a>
														
														
													</div>
												</td>
												<?php } ?>
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="7" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
											</tbody>
										</table>
										<?php } ?>
									</div>

									
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1".$p_prov.$p_kot.$p_kec.$p_tingkat.$p_status.$p_ket.$p_key."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1).$p_prov.$p_kot.$p_kec.$p_tingkat.$p_status.$p_ket.$p_key."'><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>

													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon.$p_prov.$p_kot.$p_kec.$p_tingkat.$p_status.$p_ket.$p_key;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1).$p_prov.$p_kot.$p_kec.$p_tingkat.$p_status.$p_ket.$p_key."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page.$p_prov.$p_kot.$p_kec.$p_tingkat.$p_status.$p_ket.$p_key."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}

											
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
										
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total sekolah : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no data right now!
											</div>
										</div>
									</div>
									<?php }?>
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/js/select2.min.js"></script>

			<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.js"></script>
	
		<!-- export data tables -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.flash.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/jszip.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/pdfmake.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/vfs_fonts.js"></script>

		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.html5.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.print.min.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				//$(".js-example-basic-single").select2();
				$(".fancybox").fancybox({
					padding : 0
				});	

				var kondkot = "<?php if($data['id']==$O_prov){echo 'selected';}?>";

			$('#prov').change(function(){
					$.getJSON('kota.php',{id:$(this).val()}, function(json){
						$('#select_kota').html('<option value="">semua kabupaten/Kota</option>');
						$.each(json, function(index, row) {
							$('#select_kota').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});


				$('#select_kota').change(function(){
					$.getJSON('select_kecamatan.php',{id:$(this).val()}, function(json){
						$('#kec').html('<option value="">semua kecamatan</option>');
						$.each(json, function(index, row) {
							$('#kec').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});

			});


	


				 $('#tabel_responden').DataTable( {
    					responsive: true,
    					 "paging":   false,
				        "ordering": false,
				        "info":     false,
				        "searching": false
    					

				} );

			$('.js-example-basic-single').select2();
			$('.js-example-basic-multiple').selectize({
				  
				});					
		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}


			//function confirmation delete
			function confirmSet(num){
				swal({
					title: "Are you sure?",
					text: "Responden ini akan dijadikan Sampel Observasi Bahan Ajar",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=responden&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>