<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_sekolah.php");
	$curpage="tsekolah";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['sekolah'];?></title>
	<?php include("../../packages/module-head.php");?>
	
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.css" rel="stylesheet" />
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />
	
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1 class="display-4">
										Educio <small class="text-muted"> Telemarketing </small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									
										Add New School data
								
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h3 class="display-4 text-center">Tambah Data Sekolah
									</h3>
									

								</div>
								<form name="updateProfile" action="?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
												
													<div class="panel-heading border-light pad1">
														<h5 class="panel-title" style="color:#131516;">Identitas Sekolah: </span></h5>
													</div>

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;"> Nama Sekolah <span class="symbol required"></span></div>
														</div>
												
														<div class="col-sm-6 col-xs-12 up2">
															<input id="input-company_name" name="nama" type="text" class="form-control" placeholder="Nama Sekolah" required />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">NPSN <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<input id="input-company_name" name="nisp" type="text" class="form-control" placeholder="NPSN Sekolah" required />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
												

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Tingkat Sekolah <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<select class="form-control" name="sekolah">
																<option value="1 SD">SD</option>
																<option value="2 SD">SMP</option>
																<option value="3 SD">SMA</option>														

															</select>
															
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
													

													<div class="row">
														<div class="col-sm-3 col-xs-12 up1">
															<div class="form-label bold" style="color:#131516;">Status <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<select class="form-control" name="status">
																<option value="NEGERI">NEGERI</option>
																<option value="SWASTA">SWASTA</option>																													

															</select>															
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

												

																								

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Alamat <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<input id="input-company_name" name="alamat" type="text" class="form-control" placeholder="alamat sekolah" required />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

												

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Provinsi <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
															<select class="js-example-basic-single" id="prov" name="prov" style="width:100%">
															<option value="">Pilih Provinsi..</option>
																<?php if(is_array($provinsi)) { $num=1;foreach($provinsi as $data) { ?>
																	<option value="<?=$data['id'];?>"><?=$data['nama'];?></option>
																<?php } }?>																
															</select>
		                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Kabupaten / Kota <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
																<select id="select_kota" class="js-example-basic-single" name="kota" style="width:100%">
																	<option value="">pilih Kabupaten/Kota..</option>																
																</select>

			                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Kecamatan <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
															<select class="js-example-basic-single" name="kec" id="kec" style="width:100%">
																<option value="0">pilih kecamatan..</option>
																
															</select>
		                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">No. Telpon <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">									
																
																<input id="input-price" type="text" class="form-control" name="telp" placeholder="Nomor telpon" required>																
															
														</div>
													</div>
									
									

													<div class="row">
														<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Email </div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<input id="input-email1" name="email" type="text" class="form-control" placeholder="alamat email" />
		                                    				<div id="error-email1" class="is-error"></div>
															
														</div>
													</div>

												<div class="row">
												<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Website </div>
														</div>
												
												<div class="col-sm-6 col-xs-12 up2 ">
													<input id="input-email1" name="web" type="text" class="form-control" placeholder="halaman web"/>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>


											<div class="row">
												<div class="col-sm-3 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Nama Kepala Sekolah </div>
														</div>
												
												<div class="col-sm-6 col-xs-12 up2 ">
													<input  name="kepsek" type="text" class="form-control" placeholder="Nama Kepala Sekolah "  />
                                                	
												</div>
											</div>
												
													

												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-5 col-xs-6 text-left">
											
												<a href="<?=$path['sekolah'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-chevron-circle-left"></i> Back
													</a>
										
											</div>
											<div class="col-sm-offset-1 col-sm-5 col-xs-6 text-right">
												
												<div class="btn-group">
													
													
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Submit
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<script src="<?=$global['absolute-url-admin'];?>/assets/plugins/select2-master/dist/js/select2.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>/assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>

	
	<script type="text/javascript">


					//$('.js-example-basic-single').selectize({
					   // create: true,
					   // sortField: 'text'
					//});

					$('.js-example-basic-single').select2();


	$(document).ready(function(){
				$('#prov').change(function(){
					$.getJSON('kota.php',{id:$(this).val()}, function(json){
						$('#select_kota').html('<option value="">pilih Kabupaten/Kota..</option>');
						$.each(json, function(index, row) {
							$('#select_kota').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});


				$('#select_kota').change(function(){
					$.getJSON('select_kecamatan.php',{id:$(this).val()}, function(json){
						$('#kec').html('');
						$.each(json, function(index, row) {
							$('#kec').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});


			});

				



					$('.datepicker').datepicker({
					});

			function validateForm(){
				var company_name = $("#input-company_name").val();
				var address1 = $("#input-address1").val();
				var province = $("#input-province").val();
				var city = $("#input-city").val();
				var zip = $("#input-zip").val();
				var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				if(address1 != ""){
					$("#error-address1").html("");
					$("#error-address1").hide();
					$("#input-address1").removeClass("input-error");
				} else {
					$("#error-address1").show();
					$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-address1").addClass("input-error");
					return false;
				}
				if(city != ""){
					$("#error-city").html("");
					$("#error-city").hide();
					$("#input-city").removeClass("input-error");
				} else {
					$("#error-city").show();
					$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-city").addClass("input-error");
					return false;
				}
				if(province != ""){
					$("#error-province").html("");
					$("#error-province").hide();
					$("#input-province").removeClass("input-error");
				} else {
					$("#error-province").show();
					$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-province").addClass("input-error");
					return false;
				}
				if(zip != ""){
					$("#error-zip").html("");
					$("#error-zip").hide();
					$("#input-zip").removeClass("input-error");
				} else {
					$("#error-zip").show();
					$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-zip").addClass("input-error");
					return false;
				}
				if(country != ""){
					$("#error-country").html("");
					$("#error-country").hide();
					$("#input-country").removeClass("input-error");
				} else {
					$("#error-country").show();
					$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-country").addClass("input-error");
					return false;
				}
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

		</script>

	</body>
	<!-- end: BODY -->
	</html>