<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_karyawan.php");
	$curpage="karyawan";
	$page_name = "index.php";
	
	$p_key = "&keyword=".$O_keyword;
	$p_tgl = "&tgl=".$O_tgl;

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['prospek'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.min.css" rel="stylesheet" />

		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
		<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1 class="display-4">
										Educio <small class="text-muted"> Telemarketing </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
									<h3>	Record Karyawan </h3>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<div class="row">
											<form action="" method="GET">
												<div class="row">
												<div class="col-sm-4 col-xs-12 up1">
													<div class="col-sm-2 col-xs-12 pad0"><strong style="color:#131516;">Last action</strong></div>
													<div class="col-sm-7 col-xs-12 pad0">
													<input name="tgl" class="datepicker form-control" data-date-format="dd/mm/yyyy" value="<?=$O_tgl;?>"  />
													</div>
												</div>

												<div class="col-sm-4 col-xs-12 up1">
													<div class="col-sm-2 col-xs-12 pad0"><strong style="color:#131516;">Kata kunci</strong></div>
													<div class="col-sm-7 col-xs-12 pad0">
														<input  name="keyword" type="text" class="form-control" value="<?=$O_keyword;?>" />
													</div>
												</div>
												<div class="col-sm-2 col-xs-12 up1 text-left">
													<button type="submit" class="btn btn-primary btn-sm">
															Cari Record <i class="fa fa-search"></i> 
														</button>
												</div>
												</div>

																		

											</form>										
										</div>
									


									</div>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">

												<div style="text-align:right;margin: 10px 0;">
													<h5> Total karyawan: <span class="label label-primary"><?=$total_data;?></span> </h5>
												</div>
											</div>
										</div>
									
									<div class="row">
										<?php 												
												if(is_array($datas)) { 												
												
											?>
										<table class="display" cellspacing="0" width="100%" id="tabel_responden">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama</th>
												<th>Total telpon</th>
												<th>Total diangkat</th>
												<th>Total prospek</th>
												<th>Total edit</th>												
												<th>Last action</th>																					
												
												
											</tr>
											</thead>
											<tbody>
											<?php 												
												if(is_array($datas)) { $num=1;foreach($datas as $data) { 												
												
											?>
											<tr>
												<td><?=($O_page-1)*10+$num;?>.</td>
												
												
												<td><?=$data['nama_user'];?></td>
												<td> 
												<a href="<?=$path['karyawan-list'].$data['user_ID'].'&type=telp';?>">
												<span class="badge"><?=$data['num_telp'];?></span> sekolah
												</a>
												</td>
												<td> 
												<a href="<?=$path['karyawan-list'].$data['user_ID'].'&type=diangkat';?>">
												<span class="badge"><?=$data['num_diangkat'];?></span> sekolah
												</a>
												</td>
												<td> 
												<a href="<?=$path['karyawan-list'].$data['user_ID'].'&type=prospek';?>">
												<span class="badge"><?=$data['num_prospek'];?></span> sekolah
												</a>
												</td>
												<td>
												<a href="<?=$path['karyawan-list'].$data['user_ID'].'&type=edit';?>">
												<span class="badge"><?=$data['num_edit'];?></span> sekolah </a></td>
													
												
													
																
												
												<td><?php
													if($data['last_act']!=0){
													date_default_timezone_set("Asia/Jakarta");
													echo date('H:i j/M/y',strtotime($data['last_act']));
													}else{echo "not yet";
													}?>
												</td>
																							
												
											
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="7" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
											</tbody>
										</table>
										<?php } ?>
									</div>

									
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1".$p_key.$p_tgl."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1).$p_key.$p_tgl."'><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>

													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon.$p_key.$p_tgl;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1).$p_key.$p_tgl."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page.$p_key.$p_tgl."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}

											
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
										
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total sekolah : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no data right now!
											</div>
										</div>
									</div>
									<?php }?>
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>


					
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/js/select2.min.js"></script>

			<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.js"></script>
	
		<!-- export data tables -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.flash.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/jszip.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/pdfmake.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/vfs_fonts.js"></script>

		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.html5.min.js"></script>
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>assets/plugins/datatable/exp/buttons.print.min.js"></script>
		<script src="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				//$(".js-example-basic-single").select2();
				$(".fancybox").fancybox({
					padding : 0
				});			

			});


	


				 $('#tabel_responden').DataTable( {
    					responsive: true,
    					 "paging":   false,
				        "ordering": false,
				        "info":     false,
				        "searching": false
    					

				} );

			$('.js-example-basic-single').select2();

					$('.datepicker').datepicker({
					});					
		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}


			//function confirmation delete
			function confirmSet(num){
				swal({
					title: "Are you sure?",
					text: "Responden ini akan dijadikan Sampel Observasi Bahan Ajar",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=responden&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>