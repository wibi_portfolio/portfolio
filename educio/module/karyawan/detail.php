<?php 
include("../../packages/require.php");
include("../../controller/controller_sekolah_detail.php");

include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php

$curpage="karyawan";
$kar_id = $_GET['kar'];
$tipe = $_GET['type'];

?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['karyawan'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/selectize/dist/css/selectize.bootstrap2.css" rel="stylesheet"  type="text/css" />
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/select2-master/dist/css/select2.css" rel="stylesheet" />
	<link href="<?=$global['absolute-url-admin'];?>assets/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css" rel="stylesheet">

	
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
									<h1 class="display-4">
										Educio <small class="text-muted"> Telemarketing </small>
									</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['karyawan-list'].$kar_id.'&type='.$tipe;?>">
										Record
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['nama'];?>
									
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="text-left" style="color:#131516;">Profil Satuan Pendidikan / lembaga </h4>
								</div>
								<form name="editProduct" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
									<div class="panel-body">
										<div class="form-body" style="color:#131516;">
											<?php if($_SESSION['user_status'] != 1) {$disabled = "disabled"; } else {$disabled ="";} ?>
											
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nama Sekolah </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="nama" type="text" class="form-control" placeholder="Nama sekolah"  value="<?=$datas[0]['nama'];?>"/>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>NPSN </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="nisp" type="text" class="form-control" placeholder="NPSN sekolah"  value="<?=$datas[0]['nisp'];?>"/>
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

										

											

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Tingkat Sekolah </strong></div>
												<div class="col-sm-4 col-xs-12 up1">
															<select class="form-control" name="sekolah">
																<option value="SD" <?php if($datas[0]['tingkat'] == "SD"){ echo "selected"; }?>>SD</option>
																<option value="SMP" <?php if($datas[0]['tingkat'] == "SMP"){ echo "selected"; }?>>SMP</option>
																<option value="SMA" <?php if($datas[0]['tingkat'] == "SMA"){ echo "selected"; }?>>SMA</option>
																
															</select>
															
													
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Status </strong></div>
												<div class="col-sm-4 col-xs-12 up1">
															<select class="form-control" name="status">
																<option value="NEGERI" <?php if($datas[0]['status'] == "NEGERI"){ echo "selected"; }?>>NEGERI</option>
																<option value="SWASTA" <?php if($datas[0]['status'] == "SWASTA"){ echo "selected"; }?>>SWASTA</option>
																
																
															</select>
															
													
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
														<div class="col-sm-4 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Alamat <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<input id="input-company_name" name="alamat" type="text" class="form-control" value="<?=$datas[0]['alamat'];?>" placeholder="alamat sekolah" required />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
												</div>
											
											<div class="row">
														<div class="col-sm-4 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Provinsi <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
															<select class="js-example-basic-single" id="prov" name="prov" style="width:100%">
															<option value="">Pilih Provinsi..</option>
																<?php if(is_array($provinsi)) { $num=1;foreach($provinsi as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']==$datas[0]['prov']){echo "selected";}?>><?=$data['nama'];?></option>
																<?php } }?>																
															</select>
		                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

													<div class="row">
														<div class="col-sm-4 col-xs-12 up2">
															<div class="form-label bold " style="color:#131516;">Kabupaten / Kota <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
															<select id="select_kota" class="js-example-basic-single" name="kota" style="width:100%">
																	<option value="">pilih Kabupaten/Kota..</option>	
																		<?php if(is_array($kota)) { $num=1;foreach($kota as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']==$datas[0]['kot']){echo "selected";}?>><?=$data['nama'];?></option>
																<?php } }?>																
																</select>
		                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

													<div class="row">
														<div class="col-sm-4 col-xs-12 up2">
															<div class="form-label bold" style="color:#131516;">Kecamatan <span class="symbol required"></span></div>
														</div>
													
															<div class="col-sm-6 col-xs-12 up2">
														
															<select class="js-example-basic-single" name="kec" id="kec" style="width:100%">
																<option value="">pilih kecamatan..</option>
																	<?php if(is_array( $kecamatan)) { $num=1;foreach($kecamatan as $data) { ?>
																	<option value="<?=$data['id'];?>" <?php if($data['id']==$datas[0]['kec']){echo "selected";}?>><?=$data['nama'];?></option>
																<?php } }?>	
																
															</select>
		                                    				<div id="error-company_name" class="is-error"></div>
		                                    				</div>
														
													</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Email  </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input id="input-email1" name="email" type="text" class="form-control" placeholder="alamat email *"  value="<?=$datas[0]['email'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Website  </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input id="input-email1" name="web" type="text" class="form-control" placeholder="halaman web"  value="<?=$datas[0]['web'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>No Telp 1 </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input id="input-email1" name="fax" type="text" class="form-control" placeholder="No. telpon sekolah"  value="<?=$datas[0]['fax'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>
											


											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>No Telp 2 </strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-company_name" name="telp" type="text" class="form-control" placeholder="No. telpon/hp"  value="<?=$datas[0]['telp'];?>" />
                                                	<div id="error-name" class="is-error"></div>
												</div>
											</div>

												<div class="row">
														<div class="col-sm-4 col-xs-12 up2" >
															<div class="form-label bold" style="color:#131516;">Status Kontak <span class="symbol required"></span></div>
														</div>
													
														<div class="col-sm-6 col-xs-12 up2">
															<?=convert_status($datas[0]['stat_telp']);?>
															
														</div>
													</div>


											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Penerima </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input  name="penerima" type="text" class="form-control" placeholder="penerima "  value="<?=$datas[0]['penerima'];?>" />
                                                	
												</div>
											</div>

											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nama Kepala Sekolah </strong></div>
												<div class="col-sm-6 col-xs-12 up1 ">
													<input  name="kepsek" type="text" class="form-control" placeholder="Nama Kepala Sekolah "  value="<?=$datas[0]['kepsek'];?>" />
                                                	
												</div>
											</div>

											

											<div class="panel-heading border-light pad1">
														<h5 class="panel-title" style="color:#131516;"></h5>
													</div>
											


												<div class="row">
					                                    <div class="col-sm-4 col-xs-12 up2 form-label"><strong>Keterangan<span class="symbol required"></span></strong> </div>
					                                    <div class="col-sm-6 col-xs-12 up2">
					                                    	<div class="row">
					                                    		<?php if($datas[0]['minat']) { ?>
					                                    			<span class="label label-primary">Minat (M)</span> <?php }?>
					                                    		<?php if($datas[0]['uang']){ ?>
					                                    		<span class="label label-success"> Uang (U) </span> <?php }?>
					                                    		<?php if($datas[0]['ruang']){ ?>
					                                    		<span class="label label-danger">Ruang (R) </span>	<?php }?>
					                                    	</div>
					                                    	<?php if(isset($datas[0]['ket'])){ foreach($datas[0]['ket'] as $ket){ ?>
					                                    	<div class="row  up1">

					                                    	<textarea id="input-desc" name="desc[]" class="tinymce form-control" rows="4">
					                                    		
					                                    		<?php echo correctDisplay($ket['keterangan']);?>
					                                    	</textarea>
					                                    	</div>
					                                    	<?php }} ?>
					                                    </div>
					                                </div>

					                                
           



					                     

					                                 <div class="row">
					                                    <div class="col-sm-4 col-xs-12 up2 form-label"><strong>Prospek</strong> </div>
					                                    <div class="col-sm-6 col-xs-12 up3">
					                                    	<?=convert_prospek($datas[0]['prospek']);?>
					                                    </div>
					                                </div>
					                                
											
						

																		
											
											
									


										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['id'];?>">
											<input type="hidden" name="url" value="<?=$path['sekolah-edit'].$O_id;?>">
											<div class="col-sm-4 col-xs-12 pad0">
												<!--
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['id'];?>');">
														Delete
													</a>
												</div>
												-->
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['karyawan-list'].$kar_id.'&type='.$tipe;?>" type="reset" class="btn btn-default">
														<i class="fa fa-chevron-circle-left"></i> Back
													</a>
												
												
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['karyawan-list'].$kar_id.'&type='.$tipe;?>" type="reset" class="btn btn-default">
																<i class="fa fa-chevron-circle-left"></i> Back
															</a>
															
															
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

	<?php include("../../packages/footer-js.php");?>
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>/assets/plugins/select2-master/dist/js/select2.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>/assets/plugins/selectize/dist/js/standalone/selectize.min.js"></script>

	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
	<script src="../../packages/tinymce/tinymce.min.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	
		<script type="text/javascript">

		$('.js-example-basic-single').select2();

			$(document).ready(function(){
				$('#prov').change(function(){
					$.getJSON('kota.php',{id:$(this).val()}, function(json){
						$('#select_kota').html('<option value="">pilih Kabupaten/Kota..</option>');
						$.each(json, function(index, row) {
							$('#select_kota').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});


				$('#select_kota').change(function(){
					$.getJSON('select_kecamatan.php',{id:$(this).val()}, function(json){
						$('#kec').html('');
						$.each(json, function(index, row) {
							$('#kec').append('<option value='+row.id+'>'+row.nama+'</option>');
							
						});
						
					});
				});

					

				 


			});



					var max_fields      = 10; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				    var divi 			= '<div> <div class="row up2">';
					
				    var area 			= '<textarea name="desc[]" class="tinymce"> </textarea>';															
					
				    var hapus			= '</div><a href="" class="remove_field btn btn-danger btn-sm up1"><i class="fa fa-trash-o" aria-hidden="true"></i> hapus</a> </div> ';
				 
				   
				    
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				        e.preventDefault();
				        if(x < max_fields){ //max input box allowed
				            x++; //text box increment
				            $(wrapper).append($(divi+area+hapus)); //add input box
				          	
				            
				        }
				    });
				    
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				        e.preventDefault(); $(this).parent('div').remove();
				    })

			 
			    $('.datepicker').datepicker({
					});
			
			    $('.tinymce').summernote({
				  height: 100,   //set editable area's height
				  codemirror: { // codemirror options
				    theme: 'monokai'
				  }
				});


				function add2TextAreas() {			        
			        $(wrapper).append($(divi+area+hapus));	
			         $('.tinymce').summernote({
				  height: 100,   //set editable area's height
				  codemirror: { // codemirror options
				    theme: 'monokai'
				  }
				});		      
			        
			    }
		  
			

		</script>
	<script type="text/javascript">
		function validateForm(){
                var name = $("#input-name").val();           
                
              
                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
               
                
            }
	
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
			<?php if($alert != "success"){ ?>
				//error alert
				errorAlert(alertText);
			<?php } else { ?>
				//success alert
				successAlert(alertText); 
			<?php } ?>
			 
		<?php } ?>

		//function confirmation delete

		//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
			
			function confirmMinat(num){
				swal({
					title: "Are you sure?",
					text: "Status Minat (M) akan diubah",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "edit.php?action=minat&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}

			function confirmProspek(num){
				swal({
					title: "Apakah sekolah ini prospek?",
					text: "Sekolah ini akan dinyatakan prospek",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "edit.php?action=prospek&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}

			function confirmUang(num){
				swal({
					title: "Are you sure?",
					text: "Status Uang (U) akan diubah",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "edit.php?action=uang&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}

			function confirmRuang(num){
				swal({
					title: "Are you sure?",
					text: "Status Ruang (R) akan diubah",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#2db300",
					confirmButtonText: "OK ",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "edit.php?action=ruang&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
	
	</script>

</body>
<!-- end: BODY -->
</html>