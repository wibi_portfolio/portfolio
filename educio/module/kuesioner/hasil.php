<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_kuesioner.php");
$curpage="kuesioner";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['kuesioner'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['kuesioner'];?>">
										Add New Responden
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h3 class="display-4 text-center">Hasil Tes (ADD/ ADHD)
									</h3>
									<div class="row">
									<p> </p> 
									<p class="text-muted text-center">
									Dibuat berdasarkan kriteria DSM-5 dan pengukuran lain untuk ADD/ ADHD
									Ditinjau oleh John M. Grohol, Psy.D.
									</p>
									</div>

								</div>
								<form name="updateProfile" action="?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body" style="color:#131516;">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													
													<div class="row">
					                                    <div class="col-sm-4 col-xs-12 up1 "><strong>bernilai total  </strong> </div>
					                                    <div class="col-sm-8 col-xs-12 up1">
					                                    	: <?=$responden[0]['total_score'];?>
					                                    </div>
					                                </div>
					                                <div class="row">
					                                    <div class="col-sm-4 col-xs-12 up1 "><strong>Subskala Inatensi  </strong> </div>
					                                    <div class="col-sm-8 col-xs-12 up1">
					                                    	: <?php echo $responden[0]['inatensi']." (".$obj_kuesioner->skala($responden[0]['inatensi']).")";?>
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-4 col-xs-12 up1 "><strong>Subskala Hiperaktifitas/ Impulsifitas  </strong> </div>
					                                    <div class="col-sm-8 col-xs-12 up1">
					                                    	: <?=$responden[0]['hiper']." (".$obj_kuesioner->skala($responden[0]['hiper']).") / ".$responden[0]['impulsif']." (".$obj_kuesioner->skala($responden[0]['impulsif']).")";?>
					                                    </div>
					                                </div>

					                                <div class="row">														
															<div class="col-sm-4 col-xs-12 up1 bold" style="color:#131516;">
																Hasil 											
														
															</div>
															<div class="col-sm-8 col-xs-12 up1 bold">
																: <?=$responden[0]['hasil'];?>
															</div>
														
													</div>

													<div class="panel-heading border-light">
														<h5 class="panel-title" style="color:#131516;"> </span></h5>
													</div>
													<?php if($responden[0]['total_score'] <= 23 ){ ?>
													<div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Anda telah menjawab kuesioner pelaporan pribadi ini sesuai yang dianjurkan untuk menunjukkan bahwa tampaknya anak tidak menderita ADHD, atau terdapat gejala-gejala pada anak Anda yang berkaitan dengan ADHD. 
					                                    </div>
					                                    
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    	Anda sebaiknya tidak menganggap hasil ini sebagai sebuah diagnosa atau rekomendasi untuk perawatan ADHD. Hanya ahli kejiwaan atau dokter yang dapat membuat sebuah diagnosa.
					                                    </div>
					                                    
					                                </div>
					                                <?php } else if($responden[0]['total_score'] <= 35) { ?>


					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    	Berdasarkan tanggapan Anda tentang anak dalam ADD / ADHD screening kuis ini, anak tampaknya memiliki beberapa gejala yang konsisten dengan diagnosis kemungkinan gangguan perhatian defisit. Beberapa orang dengan skor serupa dengan Anda telah mencari pengobatan profesional untuk anak mereka untuk kekhawatiran ini, namun ada juga yang tidak.
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
Jika anak didiagnosis ADHD, kemungkinan akan menjadi jenis Gabungan, seperti yang ditunjukkan oleh gejala yang signifikan dari Inatensi dan hiperaktif / impulsif.
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    	<ul>
					                                    		<li>
Anda menyatakan beberapa gejala tidak tampak sebelum usia 12.
					                                    		</li>

					                                    		<li>
Anda menyatakan gejala-gejala terjadi dalam dua atau lebih lingkungan.
					                                    		</li>
					                                    	</ul>
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Untuk ADD atau ADHD yang didiagnosis oleh seorang ahli kesehatan mental, biasanya gejala terjadi dalam setidaknya dua lingkungan yang berbeda (seperti sekolah dan rumah, atau di tempat bermain dan rumah), dan berlangsung setidaknya 6 bulan. Gejala biasanya memburuk dalam situasi yang membutuhkan perhatian terus-menerus atau usaha mental, atau situasi yang membosankan.
					                                    </div>
					                                  </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
Anda sebaiknya tidak menganggap hasil ini sebagai diagnosis apapun, atau rekomendasi untuk pengobatan. Namun, mungkin bermanfaat bagi Anda untuk mencari diagnosis lebih lanjut dari dokter atau ahli kesehatan mental yang terlatih dalam rangka untuk mencegah kemungkinan gangguan perhatian defisit.
					                                    </div>
					                                 </div>

<?php } else if($responden[0]['total_score'] <= 41) { ?>

													<div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Berdasarkan tanggapan Anda tentang anak  dalam ADD / ADHD screening kuis ini, anak Anda muncul gejala yang mirip dengan anak-anak lain yang memiliki gejala ADD moderat. Beberapa orang dengan skor serupa dengan Anda telah mencari pengobatan ahli untuk anak mereka untuk kekhawatiran ini, terutama jika mereka merasa secara signifikan mengganggu kehidupan anak.
					                                    </div>
					                                </div>

					                                   <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
Jika anak didiagnosis ADHD, kemungkinan akan menjadi jenis Gabungan, seperti yang ditunjukkan oleh gejala yang signifikan dari Inatensi dan hiperaktif / impulsif.
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    	<ul>
					                                    		<li>
Anda menyatakan beberapa gejala tidak tampak sebelum usia 12.
					                                    		</li>

					                                    		<li>
Anda menyatakan gejala-gejala terjadi dalam dua atau lebih lingkungan.
					                                    		</li>
					                                    	</ul>
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Untuk ADD atau ADHD yang didiagnosis oleh seorang ahli kesehatan mental, biasanya gejala terjadi dalam setidaknya dua lingkungan yang berbeda (seperti sekolah dan rumah, atau di tempat bermain dan rumah), dan berlangsung setidaknya 6 bulan. Gejala biasanya memburuk dalam situasi yang membutuhkan perhatian terus-menerus atau usaha mental, atau situasi yang membosankan.
					                                    </div>
					                                  </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
Anda sebaiknya tidak menganggap hasil ini sebagai diagnosis apapun, atau rekomendasi untuk pengobatan. Namun, mungkin bermanfaat bagi Anda untuk mencari diagnosis lebih lanjut dari dokter atau ahli kesehatan mental yang terlatih dalam rangka untuk mencegah kemungkinan gangguan perhatian defisit.
					                                    </div>
					                                 </div>
<?php } else { ?>
													<div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Berdasarkan tanggapan Anda tentang anak dalam ADD / ADHD screening kuis ini, anak memiliki gejala yang mirip dengan anak-anak lain yang memiliki gejala ADD sedang hingga ADD berat. tanggapan serupa biasanya memenuhi syarat diagnosis ADHD atau ADD, dan banyak orangtua telah mencari pengobatan ahli untuk anak mereka untuk masalah kesehatan mental ini.
					                                    </div>
					                               </div>

					                                 <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
Jika anak didiagnosis ADHD, kemungkinan akan menjadi jenis Gabungan, seperti yang ditunjukkan oleh gejala yang signifikan dari Inatensi dan hiperaktif / impulsif.
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    	<ul>
					                                    		<li>
Anda menyatakan beberapa gejala tidak tampak sebelum usia 12.
					                                    		</li>

					                                    		<li>
Anda menyatakan gejala-gejala terjadi dalam dua atau lebih lingkungan.
					                                    		</li>
					                                    	</ul>
					                                    </div>
					                                </div>

					                                <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-left">
					                                    Untuk ADD atau ADHD yang didiagnosis oleh seorang ahli kesehatan mental, biasanya gejala terjadi dalam setidaknya dua lingkungan yang berbeda (seperti sekolah dan rumah, atau di tempat bermain dan rumah), dan berlangsung setidaknya 6 bulan. Gejala biasanya memburuk dalam situasi yang membutuhkan perhatian terus-menerus atau usaha mental, atau situasi yang membosankan.
					                                    </div>
					                                  </div>

<?php }  ?>
					                                <br> <br><br>

					                                 <div class="row">
					                                    <div class="col-sm-12 col-xs-12 up1 text-center">
					                                    	<strong>Hasil tes ini tidak diartikan sebagai sarana diagnosa.</strong> <br>
														Hanya ahli kesehatan atau ahli kejiwaan yang dapat membuat diagnosa tentang ADHD.

					                                    </div>
					                                    
					                                </div>
													

													<div class="row">
														<div class="col-xs-12 up1 text-center">
															<a href="<?=$path['kuesioner'];?>" type="reset" class="btn btn-azure">
																<i class="fa fa-check-square-o"></i> Selesai
															</a>
														</div>
													</div>
													
																								
												
												

												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													
													
													
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>

	<script type="text/javascript">
			$(document).ready(function() {

				 	var max_fields      = 10; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				    var select			= '<div> <div class="col-sm-6 col-xs-12 up1"> <select class="form-control" id="sel1" name="barang[]">';
					var pilihan			= " <?=$products;?> </select> </div>";
				    var qty 			= '<div class="col-sm-6 col-xs-12 up1"><input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty"></div>';
				    var hapus			= '<a href="#" class="remove_field">Remove</a> </div> ';
				   
				    
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				        e.preventDefault();
				        if(x < max_fields){ //max input box allowed
				            x++; //text box increment
				            $(wrapper).append(select+pilihan+qty+hapus); //add input box
				        }
				    });
				    
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
			});
		</script>
	<script type="text/javascript">
			function validateForm(){
				var company_name = $("#input-company_name").val();
				var address1 = $("#input-address1").val();
				var province = $("#input-province").val();
				var city = $("#input-city").val();
				var zip = $("#input-zip").val();
				var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				if(address1 != ""){
					$("#error-address1").html("");
					$("#error-address1").hide();
					$("#input-address1").removeClass("input-error");
				} else {
					$("#error-address1").show();
					$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-address1").addClass("input-error");
					return false;
				}
				if(city != ""){
					$("#error-city").html("");
					$("#error-city").hide();
					$("#input-city").removeClass("input-error");
				} else {
					$("#error-city").show();
					$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-city").addClass("input-error");
					return false;
				}
				if(province != ""){
					$("#error-province").html("");
					$("#error-province").hide();
					$("#input-province").removeClass("input-error");
				} else {
					$("#error-province").show();
					$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-province").addClass("input-error");
					return false;
				}
				if(zip != ""){
					$("#error-zip").html("");
					$("#error-zip").hide();
					$("#input-zip").removeClass("input-error");
				} else {
					$("#error-zip").show();
					$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-zip").addClass("input-error");
					return false;
				}
				if(country != ""){
					$("#error-country").html("");
					$("#error-country").hide();
					$("#input-country").removeClass("input-error");
				} else {
					$("#error-country").show();
					$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-country").addClass("input-error");
					return false;
				}
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

		</script>

	</body>
	<!-- end: BODY -->
	</html>