<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_kuesioner.php");
$curpage="kuesioner";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['kuesioner'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['kuesioner'];?>">
										Add New Responden
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h3 class="display-4 text-center">Tes terhadap Gangguan Pemusatan Perhatian dan Hiperaktifitas (ADD/ ADHD)
									</h3>
									<div class="row">
									<p> </p> 
									<p class="text-muted text-center">
									Dibuat berdasarkan kriteria DSM-5 dan pengukuran lain untuk ADD/ ADHD
									Ditinjau oleh John M. Grohol, Psy.D.
									</p>
									</div>

								</div>
								<form name="updateProfile" action="?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="bold text-left" style="color:#131516;">
																Petunjuk: <br>
																Jawablah pertanyaan di bawah ini mengenai bagaimana perilaku dan perasaan anak Anda selama 6 bulan belakangan ini. 
																<br>Jawablah secermat dan sejujur mungkin untuk mendapatkan hasil yang akurat.

															</div>
														</div>
													</div>
													<div class="panel-heading border-light">
														<h5 class="panel-title" style="color:#131516;">Identitas Responden: </span></h5>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;"> Nama Anak <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="nama" type="text" class="form-control" placeholder="Nama Anak"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Usia anak  <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<div class="input-group">
																
																<input id="input-price" type="text" class="form-control" name="umur">
																<span class="input-group-addon" style="color:#000;background-color: #ddd;border-color: #ddd;">tahun</span>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1" >
															<div class="form-label bold text-left" style="color:#131516;">Jenis kelamin <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<div class="radio">
																<label style="color:#131516;"><input type="radio" name="gender" value="Laki-laki">Laki-laki</label>
															</div>
															<div class="radio">
																<label style="color:#131516;"><input type="radio" name="gender" value="Perempuan">Perempuan</label>
		                                    				</div>
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Tingkat Sekolah <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="sekolah" type="text" class="form-control" placeholder="Tingkat Sekolah"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Gereja Kristen Indonesia <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<select class="form-control" id="gereja" name="gereja">
																<?=$gereja;?>
															</select>
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Kelas Sekolah Minggu <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="kelas" type="text" class="form-control" placeholder="Kelas Sekolah Minggu"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Guru Sekolah Minggu  <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="guru" type="text" class="form-control" placeholder="nama guru"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">No HP/WA Guru <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="hp" type="text" class="form-control" placeholder="No. HP/WA Guru"  />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>											

													<div class="row">
														<div class="col-xs-12 up3">
															<div class="form-label bold text-left" style="color:#131516;">Email <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-email1" name="email" type="text" class="form-control" placeholder="alamat email *" />
		                                    				<div id="error-email1" class="is-error"></div>
															
														</div>
													</div>
												
													<div class="panel-heading border-light">
														<h4 class="panel-title"></span></h4>
													</div>
													
													<?php

														if(is_array($question)) { $num=1;foreach($question as $data) { 												
												
													?>									
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;"> <?=$num.". ".$data['q_desc'];?>  <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															
																<?php if(is_array($data['opsi'])) { ;foreach($data['opsi'] as $ans) {  ?>
																<div class="radio">
																	<label style="color:#131516;"><input type="radio" name="ans_<?=$data['id_q'];?>" value="<?=$ans['value'];?>"><?=$ans['opsi'];?> </label>
																</div>
																<?php } }?>
															
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>

													<?php $num++; } } ?>													
												
												

												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['kuesioner'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Insert
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>

	<script type="text/javascript">
			$(document).ready(function() {

				 	var max_fields      = 10; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				    var select			= '<div> <div class="col-sm-6 col-xs-12 up1"> <select class="form-control" id="sel1" name="barang[]">';
					var pilihan			= " <?=$products;?> </select> </div>";
				    var qty 			= '<div class="col-sm-6 col-xs-12 up1"><input id="input-weight" type="text" class="form-control" name="qty[]" placeholder="Qty"></div>';
				    var hapus			= '<a href="#" class="remove_field">Remove</a> </div> ';
				   
				    
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				        e.preventDefault();
				        if(x < max_fields){ //max input box allowed
				            x++; //text box increment
				            $(wrapper).append(select+pilihan+qty+hapus); //add input box
				        }
				    });
				    
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
				        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
			});
		</script>
	<script type="text/javascript">
			function validateForm(){
				var company_name = $("#input-company_name").val();
				var address1 = $("#input-address1").val();
				var province = $("#input-province").val();
				var city = $("#input-city").val();
				var zip = $("#input-zip").val();
				var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				if(address1 != ""){
					$("#error-address1").html("");
					$("#error-address1").hide();
					$("#input-address1").removeClass("input-error");
				} else {
					$("#error-address1").show();
					$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-address1").addClass("input-error");
					return false;
				}
				if(city != ""){
					$("#error-city").html("");
					$("#error-city").hide();
					$("#input-city").removeClass("input-error");
				} else {
					$("#error-city").show();
					$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-city").addClass("input-error");
					return false;
				}
				if(province != ""){
					$("#error-province").html("");
					$("#error-province").hide();
					$("#input-province").removeClass("input-error");
				} else {
					$("#error-province").show();
					$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-province").addClass("input-error");
					return false;
				}
				if(zip != ""){
					$("#error-zip").html("");
					$("#error-zip").hide();
					$("#input-zip").removeClass("input-error");
				} else {
					$("#error-zip").show();
					$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-zip").addClass("input-error");
					return false;
				}
				if(country != ""){
					$("#error-country").html("");
					$("#error-country").hide();
					$("#input-country").removeClass("input-error");
				} else {
					$("#error-country").show();
					$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-country").addClass("input-error");
					return false;
				}
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

		</script>

	</body>
	<!-- end: BODY -->
	</html>