<?php 
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_quesioner.php");
	$curpage="quesioner";
	$page_name = "index.php";

?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['quesioner'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Quesioner Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
									<?php if($_SESSION['user_status'] == 1) { ?>	
										<a class="btn btn-light-azure" href="<?=$path['quesioner-add'];?>">
											<i class="fa fa-plus"></i> Add New Data
										</a>
									<?php } ?>
									</div>

									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Product : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
									
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>Nomor urut</th>
												<th>Pertanyaan</th>												
												<th>Status</th>
												<?php if($_SESSION['user_status'] != 4) { ?>											
												<th>Action</th>
												<?php } ?>
											</tr>
											<?php 												
												if(is_array($datas)) { $num=1;foreach($datas as $data) { 												
												
											?>
											<tr>
												<td><?=($O_page-1)*10+$num;?>.</td>
												<td><?=$data['no'];?></td>
												<td><?php echo charLength($data['q_desc'], 60);?> </td>

												<td><?php if($data['publish']){echo "Publish";}else{echo "Not Publish";}?></td>				
												
												
												<?php if($_SESSION['user_status'] != 4) { ?>
												<td>
													<div class="text-center">
														
														<a href="<?=$path['quesioner-edit'].$data['id_q'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View">View</a>
														
														<?php if($_SESSION['user_status'] ==1) { ?>
														<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['id_q'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														<?php } ?>	
													</div>
												</td>
												<?php } ?>
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="7" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
									</div>
										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1)."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
										
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Question : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no Question right now!
											</div>
										</div>
									</div>
									<?php }?>
									
									
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add New Question</h4>
								</div>
								<form name="addProduct" action="index.php?action=add" enctype="multipart/form-data" method="post" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Pertanyaan <span class="symbol required"></span></strong> </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<textarea id="input-desc" name="desc" class="summernote form-control" rows="5" placeholder="Question"></textarea>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Nomor urut <span class="symbol required"></span></strong> </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="no" type="text" class="form-control" required placeholder="Nomor urut" />
		                                    </div>
		                                </div>

		                                 <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Type <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="tipe" class="form-control">
					                                <option value="radio">Radio</option>
					                                <option value="text">Text</option>
					                            </select>
					                        </div>
					                   	</div>

					                   	 <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Opsi Jawaban <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="opsi" class="form-control">
					                                <?=$opsi;?>
					                            </select>
					                        </div>
					                   	</div>

		                                <div class="row">
					                        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
					                        <div class="col-sm-6 col-xs-12 up1">
					                            <select name="publish" class="form-control">
					                                <option value="1">Publish</option>
					                                <option value="0">Not Publish</option>
					                            </select>
					                        </div>
					                   	</div>
		                              
		                             
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});	
			});				
		
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

					//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>