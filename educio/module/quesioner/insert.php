<?php 
	include("../../packages/require.php");
	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_quesioner.php");
	$curpage="quesioner";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['quesioner'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										ADHD <small class="text-muted"> Test System </small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="<?=$path['quesioner'];?>">
											Questionnaire Management
										</a>
									</li>
									<li class="active">
										Add New Question
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-left">
										<h4 class="modal-title form-title">Add New Question</h4>
									</div>
									<div class="panel-body">
										<form name="addProduct" action="insert.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
											<div class="panel-body">
												<div class="form-body" style="color:#131516;">
													<div class="row">
					                                    <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Pertanyaan <span class="symbol required"></span></strong> </div>
					                                    <div class="col-sm-6 col-xs-12 up1">
					                                    	<textarea id="input-desc" name="desc" class="summernote form-control" rows="5" placeholder="Question"></textarea>
					                                    </div>
					                                </div>
					                                <div class="row">
					                                    <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Nomor urut <span class="symbol required"></span></strong> </div>
					                                    <div class="col-sm-6 col-xs-12 up1">
					                                    	<input name="no" type="text" class="form-control" required placeholder="Nomor urut" />
					                                    </div>
					                                </div>

					                                 <div class="row">
								                        <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Type <span class="symbol required"></span></strong></div>
								                        <div class="col-sm-6 col-xs-12 up1">
								                            <select name="tipe" class="form-control">
								                                <option value="inatensi">Inatensi</option>
								                                <option value="hiperaktifitas">Hiperaktifitas</option>
								                                <option value="impulsifitas">Impulsifitas</option>
								                                <option value="none">none</option>
								                            </select>
								                        </div>
								                   	</div>

								                   	<div class="row">
								                        <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Opsi Jawaban <span class="symbol required"></span></strong></div>
								                        <div class="col-sm-6 col-xs-12 up1">
								                            <select name="opsi" class="form-control">
								                                <?=$opsi;?>
								                            </select>
								                        </div>
								                   	</div>

					                                <div class="row">
								                        <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
								                        <div class="col-sm-6 col-xs-12 up1">
								                            <select name="publish" class="form-control">
								                                <option value="1">Publish</option>
								                                <option value="0">Not Publish</option>
								                            </select>
								                            <small><span class="help-block up05"><i class="fa fa-info-circle"></i> Pilih Publish jika ingin pertanyaan ditampilkan pada quisioner. Pilih Not Publish jika tidak</span></small>
								                        </div>
								                   	</div>														

													


												</div>
											</div>
											<div class="panel-footer">
												<div class="row">
													<div class="col-xs-12 pad0 text-right">
														<div class="btn-group text-right">
															<a href="<?=$path['quesioner'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
														
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Create
															</button>
														</div>
													</div>
												</div>
											</div>
										</form>

									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript">
			function validateForm(){
                var category = $("#input-category").val();
                var name = $("#input-name").val();
                var price = $("#input-price").val();
                var weight = $("#input-weight").val();
                var photo = $("#input-photo").val();
                var numFormat = /^[0-9]+$/;
                var weightFormat = /^[0-9.,\b]+$/;
                
                if (category != "") {
                    $("#error-category").html("");
                    $("#error-category").hide();
                    $("#input-category").removeClass("input-error");
				} else {
                    $("#error-category").show();
                    $("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-category").addClass("input-error");
                    return false;
				}
                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
                if(price != ""){
                    if(price.match(numFormat)){
                        $("#error-price").html("");
                        $("#error-price").hide();
                        $("#input-price").removeClass("input-error");
                    } else {
                        $("#error-price").show();
                        $("#error-price").html("<i class='fa fa-warning'></i> This field must contain number only.");
                        $("#input-price").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-price").show();
                    $("#error-price").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-price").addClass("input-error");
                    return false;
                }
                if(weight != ""){
                    if(weight.match(weightFormat)){
                        $("#error-weight").html("");
                        $("#error-weight").hide();
                        $("#input-weight").removeClass("input-error");
                    } else {
                        $("#error-weight").show();
                        $("#error-weight").html("<i class='fa fa-warning'></i> This field must contain number, comma(,), and dot(.) only.");
                        $("#input-weight").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-weight").show();
                    $("#error-weight").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-weight").addClass("input-error");
                    return false;
                }
                if(photo != ""){
                    $("#error-photo").html("");
                    $("#error-photo").hide();
                    $("#input-photo").removeClass("input-error");
                } else {
                    $("#error-photo").show();
                    $("#error-photo").html("<i class='fa fa-warning'></i> Please insert primary photo.");
                    $("#input-photo").addClass("input-error");
                    return false;
                }
            }
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>