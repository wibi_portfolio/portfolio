<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_profile.php");
$curpage="profile";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['profile'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1 class="display-4">
										ADHD <small class="text-muted"> Test System </small>
									</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['profile'];?>">
										Introduction Management
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Introduction Management</span></h4>
								</div>
								<form name="updateProfile" action="index.php?action=update" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left" style="color:#131516;">Title <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="title" type="text" class="form-control" placeholder="Title" value="<?=$J_title;?>" />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
																
												
													
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left" style="color:#131516;">Subtitle</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="sub" type="text" class="form-control" placeholder="subtitle" value="<?=$J_sub;?>" />
														</div>
													</div>
													
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left" style="color:#131516;">kata pengantar, petunjuk, dan penjelasan kuesioner</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1" style="color:#131516;">
															<textarea name="pengantar" class="summernote form-control" rows="5" placeholder="kata pengantar, petunjuk, dan penjelasan kuesioner" ><?=correctDisplay($J_pengantar);?></textarea>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> Teks Tersebut akan ditampilkan sebagai kata pengantar dalam kuesioner</span></small>
														</div>
													</div>

													
													

												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['profile'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<script type="text/javascript">
			function validateForm(){
				var company_name = $("#input-company_name").val();
				var address1 = $("#input-address1").val();
				var province = $("#input-province").val();
				var city = $("#input-city").val();
				var zip = $("#input-zip").val();
				var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				if(address1 != ""){
					$("#error-address1").html("");
					$("#error-address1").hide();
					$("#input-address1").removeClass("input-error");
				} else {
					$("#error-address1").show();
					$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-address1").addClass("input-error");
					return false;
				}
				if(city != ""){
					$("#error-city").html("");
					$("#error-city").hide();
					$("#input-city").removeClass("input-error");
				} else {
					$("#error-city").show();
					$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-city").addClass("input-error");
					return false;
				}
				if(province != ""){
					$("#error-province").html("");
					$("#error-province").hide();
					$("#input-province").removeClass("input-error");
				} else {
					$("#error-province").show();
					$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-province").addClass("input-error");
					return false;
				}
				if(zip != ""){
					$("#error-zip").html("");
					$("#error-zip").hide();
					$("#input-zip").removeClass("input-error");
				} else {
					$("#error-zip").show();
					$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-zip").addClass("input-error");
					return false;
				}
				if(country != ""){
					$("#error-country").html("");
					$("#error-country").hide();
					$("#input-country").removeClass("input-error");
				} else {
					$("#error-country").show();
					$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-country").addClass("input-error");
					return false;
				}
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

		</script>

	</body>
	<!-- end: BODY -->
	</html>