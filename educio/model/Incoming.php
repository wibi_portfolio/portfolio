<?php

class Incoming{
	private $table = "at_brg_masuk";
	private $itemPerPageAdmin = 10;
    private $join="LEFT JOIN t_barang_masuk ON at_ref_msk_id = brg_id";
    private $join_brg= "LEFT JOIN t_product ON at_ref_bgr_id = product_ID";
    private $join_farm= "LEFT JOIN t_farm ON at_ref_farm_id = farm_id";
    private $join_at = "LEFT JOIN at_brg_masuk ON drop_ref_at = at_id" ;
       

  
   
  
    //START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
             
        //get total data
        $result =0;
        $text_total = "SELECT at_id FROM $this->table";

        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);

        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM $this->table $this->join $this->join_brg $this->join_farm ORDER BY at_id DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }       

        if(is_array($result)){      
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }

        return $result;
    }

    public function get_product_list(){
        $result = "";

        $text = "SELECT * FROM t_product ORDER BY product_code ASC";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
         
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result .= "<option value='".$row['product_ID']."'>".$row['product_code']."-".$row['product_name']."</option>"  ;

                $loop++;
            }
        }
        //$result = $text;
        return $result;
    }

    public function generate_masuk_code(){
        $result = 0;
        
        $rand = strtoupper(substr(md5(microtime()),rand(0,26),4));

        $text = "SELECT brg_id FROM t_barang_masuk";
        $query = mysql_query($text);
        if($query){
            $total_data = mysql_num_rows($query);
            if($total_data > 0){
                $num = $total_data + 1;
                $result = "GS/".$rand."/".$num;
            } else {
                $result = "GS/".$rand."/1";
            }
        } else {
            $result = "GS/".$rand."/1";
        }
        return $result;
    }



	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table $this->join $this->join_brg  WHERE at_id = '$id'";

		$query = mysql_query($text);

		if(mysql_num_rows($query) >= 1){
			$result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
		}
        //$result = $text;
		return $result;
	}



	public function insert_data($usr){

		$result = 0;
        $kode = $this->generate_masuk_code() ;        
		$text = "INSERT INTO t_barang_masuk (brg_rev_usr_id, brg_kode_name) VALUES('$usr','$kode')";
		$query = mysql_query($text);
		if($query){
            $result = mysql_insert_id();
        }        
		return $result;	
    }

    public function insert_drop($id, $qty, $cond){

        $result = 0;
              
        $text = "INSERT INTO at_brg_drop (drop_ref_at, drop_qty, drop_kondisi, drop_date) 
        VALUES('$id','$qty','$cond', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }        
        return $result; 
    }

    public function insert_data_at($id_masuk, $id_brg, $jumlah, $expired, $kota){

        $result = 0;        
        $text = "INSERT INTO at_brg_masuk (at_ref_msk_id, at_ref_bgr_id, at_jumlah, at_expired, at_ref_farm_id) 
        VALUES('$id_masuk', '$id_brg', '$jumlah', '$expired', '$kota')";

        $query = mysql_query($text);
        if($query){
            $result = 1;
        }       
        return $result; 
    }


	



	public function update_data($id, $name, $stok, $satuan, $price, $profit, $publish){

		$result = 0;
		$text = "UPDATE $this->table SET product_name = '$name', product_price = '$price', product_publish = '$publish', 
         product_stok='$stok', product_satuan = '$satuan', product_persentase_profit = '$profit'  WHERE product_ID = '$id'";

		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}



	public function delete_data($id){
		$result = 0;      
		$text = "DELETE FROM $this->table WHERE at_id = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

    public function get_drop_by_page($page=1){
             
        //get total data
        $result =0;
        $text_total = "SELECT drop_id FROM at_brg_drop";

        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);

        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM at_brg_drop $this->join_at $this->join $this->join_brg  ORDER BY drop_id DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }       

        if(is_array($result)){      
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }

        return $result;
    }



//END FUNCTION FOR ADMIN PAGE

}

?>