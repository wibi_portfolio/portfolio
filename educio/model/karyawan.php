<?php

class karyawan{
	private $table = "log";      
    private $itemPerPageAdmin= 10;
  

  
    //START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1,  $keyword="", $tgl="", $id=""){
             
      
        if($keyword != ""){
            $cond_key = " AND nama_user LIKE '%$keyword%' ";
        } else {
            $cond_key = "";
        }

        if($tgl != ""){
            $tgl = str_replace('/', '-', $tgl);
            $tgl = date("Y-m-d", strtotime($tgl)); 
            $cond_tgl = " HAVING DATE_FORMAT(last_act,'%Y-%m-%d') = '$tgl' ";
        } else {
            $cond_tgl = "";
        }

        if($id != ""){
            $cond_id = " AND user_ID = '$id' ";
        } else {
            $cond_id = "";
        }



        $text_total = "SELECT user_ID, nama_user,
        (SELECT time FROM log WHERE rev_id_user = user_ID ORDER BY UNIX_TIMESTAMP(time) DESC LIMIT 0,1) AS last_act
        FROM t_user 
        WHERE user_status = 2  $cond_key $cond_tgl $cond_id";

        $query_total = mysql_query($text_total);
       
        $total_data = mysql_num_rows($query_total);
    
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT user_ID, nama_user,
        (SELECT time FROM log WHERE rev_id_user = user_ID ORDER BY UNIX_TIMESTAMP(time) DESC LIMIT 0,1) AS last_act
        FROM t_user  
        WHERE user_status = 2 $cond_key $cond_tgl $cond_id
        
        ORDER BY last_act DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                $text3 = "SELECT COUNT(id) as num_prospek
                FROM log 
                WHERE rev_id_user='".$row['user_ID']."' AND type='prospek'
                GROUP BY rev_id_user";

                $query3 = mysql_query($text3);

                $result[$loop]['num_prospek'] = 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_prospek'] = $row_child['num_prospek'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(id) as num_edit
                FROM log 
                WHERE rev_id_user='".$row['user_ID']."' AND type='edit'
                GROUP BY rev_id_user";

                $query3 = mysql_query($text3);

                $result[$loop]['num_edit'] = 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_edit'] = $row_child['num_edit'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(log.id) as num_telp
                FROM log LEFT JOIN sekolah ON rev_id_sekolah = sekolah.id
                WHERE rev_id_user='".$row['user_ID']."' AND type='edit'
                AND stat_telp != 0
                GROUP BY rev_id_user";

                $query3 = mysql_query($text3);

                $result[$loop]['num_telp'] = 0;
                if($query3){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_telp'] = $row_child['num_telp'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(log.id) as num_diangkat
                FROM log LEFT JOIN sekolah ON rev_id_sekolah = sekolah.id
                WHERE rev_id_user='".$row['user_ID']."' AND type='edit'
                AND stat_telp = 1
                GROUP BY rev_id_user";

                $query3 = mysql_query($text3);

                $result[$loop]['num_diangkat'] = 0;
                if($query3){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_diangkat'] = $row_child['num_diangkat'];         

                        $loop2++ ;
                    }
                }



                $loop++ ;
            }
        }       

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return  $result;

    } 


     public function get_data_user(){
        $result = 0;
        $text = "SELECT user_ID,nama_user FROM t_user";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){

            $result = array();

            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

     public function get_nama($id){
        $result = 0;

        $text = "SELECT nama_user FROM t_user WHERE user_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = "";
            while($row = mysql_fetch_assoc($query)){
                $result= $row['nama_user'];
            }
        }

        return $result;
    }   


    //START FUNCTION FOR ADMIN PAGE
    public function get_data_list($page=1, $prov="", $kota="", $kec="", $tingkat="", $user="", $ket="", $keyword="", $tgl="", $type="", $status=""){
             
        if($prov != ""){
            $cond_prov = " AND prov = '$prov' ";
        } else {
            $cond_prov = "";
        }

        if($kota !=""){
            $cond_kot = " AND kot = '$kota' ";
        } else {
            $cond_kot = "";
        }

        if($kec !=""){            
            $cond_kec = " AND kec = '$kec' ";
        } else {
            $cond_kec = "";
        }

        if($tingkat != ""){
            $cond_tingkat = " AND tingkat = '$tingkat' ";
        } else {
            $cond_tingkat = "";
        }

        if($user != ""){
            $cond_user = " AND rev_id_user = '$user' ";
        } else {
            $cond_user = "";
        }

        if($keyword != ""){
            $cond_key = " AND nama LIKE '%$keyword%' ";
        } else {
            $cond_key = "";
        }

        if($tgl != ""){
            $tgl = str_replace('/', '-', $tgl);
            $tgl = date("Y-m-d", strtotime($tgl)); 
            $cond_tgl = " AND DATE_FORMAT(time,'%Y-%m-%d') = '$tgl' ";
        } else {
            $cond_tgl = "";
        }

        if($type != ""){
            if($type=="diangkat"){
                $cond_type = " AND stat_telp = '1' AND type='edit' " ;
            }else if($type=="telp"){
                $cond_type = " AND stat_telp != '0' AND type='edit' " ;
            }else{
                $cond_type = " AND type = '$type' ";
            }
          
        } else {
            $cond_type = "";
        }



        if($ket != ""){
            if($ket =="M"){$cond_ket = " AND minat = '1' ";}
            if($ket =="U"){$cond_ket = " AND uang = '1' ";}
            if($ket =="R"){$cond_ket = " AND ruang = '1' ";}            
        } else {
            $cond_ket = "";
        }


        if($status != ""){
            $cond_status = " AND stat_telp = '$status' ";
        } else {
            $cond_status = "";
        }

        $text_total = "SELECT sekolah.id FROM log LEFT JOIN sekolah  ON rev_id_sekolah=sekolah.id LEFT JOIN t_user 
        ON  rev_id_user=user_ID

        WHERE sekolah.id != 0  $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_user $cond_ket $cond_key $cond_tgl $cond_type $cond_status";

        $query_total = mysql_query($text_total);

       
        $total_data = mysql_num_rows($query_total);
    
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT sekolah.id as id,nisp, nama, alamat, status, tingkat, telp, stat_telp, alamat,ket,
        nama_user, time, prospek, type
        FROM log LEFT JOIN sekolah ON rev_id_sekolah=sekolah.id LEFT JOIN t_user 
        ON  rev_id_user=user_ID
        WHERE sekolah.id != 0  $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_user $cond_ket $cond_key $cond_tgl $cond_type
        $cond_status
        ORDER BY time DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++ ;
            }
        }       

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return  $result;

    } 
  


}


?>