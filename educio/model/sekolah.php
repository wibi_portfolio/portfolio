<?php

class sekolah{
	private $table = "sekolah";      
    private $itemPerPageAdmin= 10;
  

  
    //START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1, $prov="", $kota="", $kec="", $tingkat="", $status="", $ket="", $keyword=""){
             
        if($prov != ""){
            $cond_prov = " AND prov = '$prov' ";
        } else {
            $cond_prov = "";
        }

        if($kota !=""){
            $cond_kot = " AND kot = '$kota' ";
        } else {
            $cond_kot = "";
        }

        if($kec !=""){            
            $cond_kec = " AND kec = '$kec' ";
        } else {
            $cond_kec = "";
        }

        if($tingkat != ""){
            $cond_tingkat = " AND tingkat = '$tingkat' ";
        } else {
            $cond_tingkat = "";
        }

        if($status != ""){
            $cond_status = " AND stat_telp = '$status' ";
        } else {
            $cond_status = "";
        }

        if($keyword != ""){
            $cond_key = " AND ( nama LIKE '%$keyword%' OR nisp LIKE '%$keyword%')";
        } else {
            $cond_key = "";
        }

        if($ket != ""){        
         
            $cond_ket = $ket;        
                      
        } else {
            $cond_ket = "";
        }

        if($keyword != "" or $prov != ""){
            $kond_not="";
        }else{
            $kond_not =" AND id = -123 ";
        }

        $text_total = "SELECT id FROM $this->table 
        WHERE prospek = 0  $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_status $cond_ket $cond_key $kond_not";

        $query_total = mysql_query($text_total);

       
        $total_data = mysql_num_rows($query_total);
    
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT *
        FROM $this->table 
        WHERE prospek = 0 $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_status $cond_ket $cond_key
        $kond_not
        ORDER BY LPAD(lower(fax), 10,0) DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++ ;
            }
        }       

        if(is_array($result)){  
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return  $result;

    }   


    public function correctDisplay($data) {
        $data = htmlspecialchars_decode(stripslashes($data), ENT_QUOTES);
        return $data;
    }



	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE id = '$id'";

		$query = mysql_query($text);

		if(mysql_num_rows($query) >= 1){
			$result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;

                $text3 = "SELECT *
                FROM sekolah_keterangan
                WHERE rev_id_sekolah='".$row['id']."' ORDER BY id";

                $query3 = mysql_query($text3);

                $result[$loop]['ket'] = array();
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['ket'][$loop2] = $row_child ;         

                        $loop2++ ;
                    }
                }                
                $loop++;
            }
		}
        //$result = $text;
		return $result;
	}

   



	public function insert_data($nama, $nisp, $tingkat, $status, $alamat, $prov, $kot, $kec, $telp, $email, $web, $kepsek){

		$result = 0;
       
          
		$text = "INSERT INTO ". $this->table. " (nama, nisp, tingkat, status, alamat, prov, kot, kec, telp, email, web, kepsek) 
         VALUES('$nama', '$nisp', '$tingkat', '$status', '$alamat', '$prov', '$kot', '$kec', '$telp','$email', '$web', '$kepsek')";

		$query = mysql_query($text);

		if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
		return  $result;	
    }

    public function save_log($id_sekolah, $id_user, $type){

        $result = 0;     
          
        $text = "INSERT INTO log (rev_id_sekolah, rev_id_user, type) 
         VALUES('$id_sekolah', '$id_user', '$type')";

        $query = mysql_query($text);

        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
        return  $result;    
    }


   
 
	public function update_data($id, $nama, $nisp, $tingkat, $status, $alamat, $prov, $kot, $kec, $telp, $email, $stakon, $penerima, $kepsek, $web, $fax, $minat, $uang, $ruang){

		$result = 0;      

		$text = "UPDATE $this->table SET nama = '$nama', nisp='$nisp', tingkat='$tingkat', status='$status', alamat='$alamat',
        prov='$prov', kot='$kot', kec='$kec', telp='$telp', email='$email', stat_telp='$stakon',
        penerima='$penerima', kepsek='$kepsek', web='$web', fax='$fax',
        minat = '$minat', uang = '$uang', ruang = '$ruang'
        WHERE   id = '$id'";

		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}

		return $result;
	}

     public function set_prospek($id){

        $result = 0;      

        $text = "UPDATE $this->table SET prospek = 1
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }

    public function update_minat($id){

        $result = 0;      

        $text = "UPDATE $this->table SET minat = NOT minat
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }

     public function update_uang($id){

        $result = 0;      

        $text = "UPDATE $this->table SET uang = NOT uang
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }

     public function update_ruang($id){

        $result = 0;      

        $text = "UPDATE $this->table SET ruang = NOT ruang
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }


    public function set_responden($id, $val){

        $result = 0;
        $text = "UPDATE $this->table SET responden = '$val'
        WHERE   id_res = '$id'";

        $query = mysql_query($text);
        if($query){
            $result = 1;
        }

        return $result;
    }

  

	public function delete_data($id){
        

		$result = 0;      
		$text = "DELETE FROM $this->table WHERE id = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

    public function insert_data_sekolah($npsn, $nama, $alamat, $kelurahan, $status, $prov, $kot, $kec){

        $result = 0;
       
          
        $text = "INSERT INTO ". $this->table. " (nama, nisp, status, alamat, prov, kot, kec, kel) 
         VALUES('$nama', '$npsn', '$status', '$alamat', '$prov', '$kot', '$kec', '$kelurahan')";

        $query = mysql_query($text);

        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
        return  $result;    
    }


    public function update_data_kontak($id, $tingkat, $telp, $email, $fax, $web){

        $result = 0;      

        $text = "UPDATE $this->table SET  tingkat='$tingkat', telp='$telp', email='$email',
        fax='$fax', web='$web' 
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }




    public function get_data_sekolah($kode, $id_prov, $id_kota, $id_kec){
        $url = "http://referensi.data.kemdikbud.go.id/index11.php?kode=".$kode."&level=3";
        $html = file_get_html($url);
        $count = 1;
        foreach($html->find('table[id=example]') as $noscript)
        {      

            foreach($noscript->find('tr') as $nos){
                
               
                if($count>1){
                    //echo "Ke-".$count."<br>";
                    $loop = -1;
                    $baris= array();
                   foreach($nos->find('td') as $nos2){
                        if($loop>=0){
                            $kolom = str_replace("&nbsp;", '', $nos2->plaintext);
                            $baris[$loop] = $kolom;
                        }

                        $loop++;
                    }
                   // $result = $this->insert_data_sekolah($baris[0], $baris[1], $baris[2], $baris[3], $baris[4], $id_prov, $id_kota, $id_kec);

                    echo $result."--".$baris[0]."  ".$baris[1]."  ".$baris[2]."  ".$baris[3]."  ".$baris[4]."  p:".$id_prov."   kot:".$id_kota."   kec:".$id_kec."<br>";                  
                }

                 $count++;
            }   
            

        }
       

    }


    public function get_kontak_sekolah($kode, $id_sekolah){
        $url = "http://referensi.data.kemdikbud.go.id/tabs.php?npsn=".$kode;
        $html = file_get_html($url);
        $count = 1;
        $kontak = array();
        foreach($html->find('div[id=tabs-1]') as $noscript)
        {      

            foreach($noscript->find('table[!width]') as $nos){
                       
               
                    $loop= 1 ;
                   foreach($nos->find('tr') as $nos2){
                        if($loop==14){
                            $kolom=1;
                            foreach($nos2->find('td') as $nos3){
                                if($kolom==4){
                                $kontak['jenjang']=$nos3->plaintext;
                                }
                                $kolom++;
                            }
                        }

                 

                        $loop++;
                    }                   
            }         

        }
        //kontak
         foreach($html->find('div[id=tabs-6]') as $noscript)
        {      

            foreach($noscript->find('table[!width]') as $nos){
                       
               
                    $loop= 1 ;
                   foreach($nos->find('tr') as $nos2){

                        if($loop==1){
                            $kolom=1;

                            foreach($nos2->find('td') as $nos3){
                                if($kolom==4){
                                $kontak['telp']=$nos3->plaintext;
                                }
                                $kolom++;
                            }
                        }

                        if($loop==2){
                            $kolom=1;

                            foreach($nos2->find('td') as $nos3){
                                if($kolom==4){
                                $kontak['fax']=$nos3->plaintext;
                                }
                                $kolom++;
                            }
                        }

                        if($loop==4){
                            $kolom=1;

                            foreach($nos2->find('td') as $nos3){
                                if($kolom==4){
                                $kontak['mail']=$nos3->plaintext;
                                }
                                $kolom++;
                            }
                        }

                        if($loop==5){
                            $kolom=1;

                            foreach($nos2->find('td') as $nos3){
                                if($kolom==4){
                                $kontak['web']=$nos3->plaintext;
                                }
                                
                                $kolom++;
                            }
                        }

                        $loop++;
                    }                   
            }         

        }
       
       $result = $this->update_data_kontak($id_sekolah, $kontak['jenjang'], $kontak['telp'], $kontak['mail'], $kontak['fax'], $kontak['web']);
       echo $result."-- ".$id_sekolah."  - ".$kode."<br>";
       //print_r($kontak) ;

    }

    public function get_sekolah_all(){
        $result = 0;

        $text = "SELECT id, nisp FROM $this->table WHERE telp = '' AND email = '' ORDER BY id ASC";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
        }
        //$result = $text;
        return $result;
    }


    public function get_provinsi(){
        $result = 0;

        $text = "SELECT * FROM provinces ORDER BY id ASC";

        $query = mysql_query($text);

        if($query){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++;
            }
        }
        //$result = $text;
        return $result;
    }


    public function get_kota($id){
        $result = 0;

        $text = "SELECT id, name FROM regencies WHERE province_id = '$id' ORDER BY id ASC";

        $query = mysql_query($text);

        if($query){
            $result = array();           
            while($row = mysql_fetch_array($query)){
                $result[] = array('id_kot'=>$row['id'],'name_kot'=>$row['name']);                
               
            }
        }
        //$result = $text;
        return $result;
    }


  



}

?>