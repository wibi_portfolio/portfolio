<?php

class prospek{
	private $table = "sekolah";      
    private $itemPerPageAdmin= 10;
  

  
    //START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1, $prov="", $kota="", $kec="", $tingkat="", $user="", $ket="", $keyword="", $tgl=""){
             
        if($prov != ""){
            $cond_prov = " AND prov = '$prov' ";
        } else {
            $cond_prov = "";
        }

        if($kota !=""){
            $cond_kot = " AND kot = '$kota' ";
        } else {
            $cond_kot = "";
        }

        if($kec !=""){            
            $cond_kec = " AND kec = '$kec' ";
        } else {
            $cond_kec = "";
        }

        if($tingkat != ""){
            $cond_tingkat = " AND tingkat = '$tingkat' ";
        } else {
            $cond_tingkat = "";
        }

        if($user != ""){
            $cond_user = " AND rev_id_user = '$user' ";
        } else {
            $cond_user = "";
        }

        if($keyword != ""){
            $cond_key = " AND ( nama LIKE '%$keyword%' OR nisp LIKE '%$keyword%') ";
        } else {
            $cond_key = "";
        }

        if($tgl != ""){
            $tgl = str_replace('/', '-', $tgl);
            $tgl = date("Y-m-d", strtotime($tgl)); 
            $cond_tgl = " AND DATE_FORMAT(time,'%Y-%m-%d') = '$tgl' ";
        } else {
            $cond_tgl = "";
        }



        if($ket != ""){
              $cond_ket = $ket;        
        } else {
            $cond_ket = "";
        }

        $text_total = "SELECT sekolah.id FROM $this->table LEFT JOIN log ON sekolah.id=rev_id_sekolah LEFT JOIN t_user 
        ON  rev_id_user=user_ID

        WHERE prospek != 0  AND type='prospek' $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_user $cond_ket $cond_key $cond_tgl
         GROUP BY sekolah.id";

        $query_total = mysql_query($text_total);

       
        $total_data = mysql_num_rows($query_total);
    
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT sekolah.id as id,nisp, nama, alamat, status, tingkat, telp, stat_telp, alamat,ket,
        nama_user, time, prospek, minat,uang, ruang, fax
        FROM $this->table LEFT JOIN log ON sekolah.id=rev_id_sekolah LEFT JOIN t_user 
        ON  rev_id_user=user_ID
        WHERE prospek != 0 AND type='prospek' $cond_prov
        $cond_kot $cond_kec $cond_tingkat $cond_user $cond_ket $cond_key $cond_tgl
        GROUP BY sekolah.id
        ORDER BY time DESC LIMIT $limitBefore, $this->itemPerPageAdmin";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){
            $result = array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row;                
                $loop++ ;
            }
        }       

        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return  $result;

    } 


     public function get_data_user(){
        $result = 0;
        $text = "SELECT user_ID,nama_user FROM t_user";

        $query = mysql_query($text);

        if(mysql_num_rows($query) >= 1){

            $result = array();

            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    } 


       public function update_prospek($id, $val){

        $result = 0;      

        $text = "UPDATE $this->table SET prospek = '$val'
        WHERE   id = '$id'";

        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }

        return $result;
    }

    public function save_keterangan($id_sekolah, $ket){

        $result = 0;     
          
        $text = "INSERT INTO sekolah_keterangan (rev_id_sekolah, keterangan) 
         VALUES('$id_sekolah', '$ket')";

        $query = mysql_query($text);

        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        
        return  $result;    
    }

    public function delete_keterangan($id){        

        $result = 0;      
        $text = "DELETE FROM sekolah_keterangan WHERE rev_id_sekolah = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
 


}


?>