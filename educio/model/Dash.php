<?php
class Dash{

	public function get_adhd_klasis(){
		$result = 0;
        $result = array();

        $text = "SELECT COUNT(id_res) as num_res,klasis
        FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
        WHERE total_score >= 42
        GROUP BY klasis ORDER BY klasis DESC";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            
            $result['adhd']= array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query)){
                $result['adhd'][$loop] = $row;         

                $loop++ ;
            }
        }

        $text2 = "SELECT COUNT(id_res) as num_res,klasis
        FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
        WHERE total_score >= 36 AND total_score <= 41
        GROUP BY klasis ORDER BY klasis DESC";

        $query2 = mysql_query($text2);
        if(mysql_num_rows($query2) >= 1){
           
            $result['moderat']= array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query2)){
                $result['moderat'][$loop] = $row;         

                $loop++ ;
            }
        }

        $text3 = "SELECT COUNT(id_res) as num_res,klasis
        FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
        WHERE total_score >= 24 AND total_score <= 35
        GROUP BY klasis ORDER BY klasis DESC";

        $query3 = mysql_query($text3);
        if(mysql_num_rows($query3) >= 1){
           
            $result['mungkin']= array();
            $loop = 0;
            while($row = mysql_fetch_assoc($query3)){
                $result['mungkin'][$loop] = $row;         

                $loop++ ;
            }
        }




        return $result;

	}

    public function get_prospek_prov(){
        $result = 0;
        

        $text = "SELECT id, nama
        FROM prov       
        ORDER BY id ASC";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();            
            $loop = 0;

            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row; 

                $text3 = "SELECT COUNT(sekolah.id) as num_pros
                FROM sekolah LEFT JOIN prov ON prov= prov.id 
                WHERE prov='".$row['id']."' AND prospek=1
                GROUP BY prov";

                $query3 = mysql_query($text3);
                $result[$loop]['num_pros']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_pros'] = $row_child['num_pros'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(sekolah.id) as num_tdk
                FROM sekolah LEFT JOIN prov ON prov= prov.id 
                WHERE prov='".$row['id']."' AND prospek=-1
                GROUP BY prov";

                $query3 = mysql_query($text3);
                $result[$loop]['num_tkd']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_tdk'] = $row_child['num_tdk'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(sekolah.id) as num_blm
                FROM sekolah LEFT JOIN prov ON prov= prov.id 
                WHERE prov='".$row['id']."' AND stat_telp=0
                GROUP BY prov";

                $query3 = mysql_query($text3);
                $result[$loop]['num_blm']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['num_blm'] = $row_child['num_blm'];         

                        $loop2++ ;
                    }
                }

         
                      


                $loop++ ;
            }
        }

      
        return $result;

    }

      public function get_adhd_per_sinode(){
        $result = 0;
        

        $text = "SELECT sinode
        FROM t_gereja       
        GROUP BY sinode ORDER BY sinode DESC";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();            
            $loop = 0;

            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row; 

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE sinode='".$row['sinode']."' AND total_score >= 24 AND total_score <= 35
                GROUP BY sinode";

                $query3 = mysql_query($text3);
                $result[$loop]['mungkin']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['mungkin'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE sinode='".$row['sinode']."' AND total_score >= 36 AND total_score <= 41
                GROUP BY sinode";

                $query3 = mysql_query($text3);
                $result[$loop]['moderat']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['moderat'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE sinode='".$row['sinode']."' AND total_score >= 42
                GROUP BY sinode";

                $query3 = mysql_query($text3);
                $result[$loop]['adhd']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['adhd'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE sinode='".$row['sinode']."'
                GROUP BY sinode";

                $query3 = mysql_query($text3);
                $result[$loop]['jml']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['jml'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }                          


                $loop++ ;
            }
        }

      
        return $result;

    }

    public function get_adhd_church_chart(){
        $result = 0;
        

        $text = "SELECT id_g, nama
        FROM t_gereja       
        GROUP BY nama ORDER BY id_g DESC";

        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();            
            $loop = 0;

            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row; 

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 24 AND total_score <= 35
                GROUP BY gki";

                $query3 = mysql_query($text3);
                $result[$loop]['mungkin']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['mungkin'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 36 AND total_score <= 41
                GROUP BY gki";

                $query3 = mysql_query($text3);
                $result[$loop]['moderat']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['moderat'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 42
                GROUP BY gki";

                $query3 = mysql_query($text3);
                $result[$loop]['adhd']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['adhd'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."'
                GROUP BY gki";

                $query3 = mysql_query($text3);
                $result[$loop]['jml']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['jml'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }                          


                $loop++ ;
            }
        }

      
        return $result;

    }


    public function get_adhd_per_church($sinode, $klasis){
        $result = 0;

        if($klasis != ""){
            $cond_klasis = " AND klasis = '$klasis' ";
        } else {
            $cond_klasis = "";
        }

        if($sinode !=""){
            $cond_sinode = " AND sinode = '$sinode' ";
        } else {
            $cond_sinode= "";
        }
        

        $text = "SELECT id_g, nama, sinode, klasis
        FROM t_gereja WHERE id_g != 0  $cond_sinode $cond_klasis
        ORDER BY klasis ASC";

        $query = mysql_query($text);
        if($query){
            $result = array();            
            $loop = 0;

            while($row = mysql_fetch_assoc($query)){
                $result[$loop] = $row; 

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 24 AND total_score <= 35
                GROUP BY id_g";

                $query3 = mysql_query($text3);
                $result[$loop]['mungkin']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['mungkin'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 36 AND total_score <= 41
                GROUP BY id_g";

                $query3 = mysql_query($text3);
                $result[$loop]['moderat']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['moderat'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }

                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."' AND total_score >= 42
                GROUP BY id_g";

                $query3 = mysql_query($text3);
                $result[$loop]['adhd']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['adhd'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }


                $text3 = "SELECT COUNT(id_res) as num_res
                FROM t_responden LEFT JOIN t_gereja ON gki=id_g 
                WHERE gki='".$row['id_g']."'
                GROUP BY id_g";

                $query3 = mysql_query($text3);
                $result[$loop]['jml']= 0;
                if(mysql_num_rows($query3) >= 1){                   
                    
                    $loop2 = 0;
                    while($row_child = mysql_fetch_assoc($query3)){
                        $result[$loop]['jml'] = $row_child['num_res'];         

                        $loop2++ ;
                    }
                }                          


                $loop++ ;
            }
        }

      
        return $result;

    }


    public function get_persen($a, $b){
        if($b==0){
           return 0 ; 
        }else{
        $hasil = $a/$b*100;
        $hasil = number_format((float)$hasil, 2, '.', '') ;
        return $hasil ;
        }
    }

	



}

?>