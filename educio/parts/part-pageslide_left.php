<?php
date_default_timezone_set("Asia/Jakarta");
$now = date('d/m/Y');
?>
<a class="closedbar inner hidden-sm hidden-xs" href="#">

</a>

<nav id="pageslide-left" class="pageslide inner">

	<div class="navbar-content">

		<!-- start: SIDEBAR -->

		<div class="main-navigation left-wrapper transition-left">

			<div class="navigation-toggler hidden-sm hidden-xs">

				<a href="#main-navbar" class="sb-toggle-left">

				</a>

			</div>

			<div class="user-profile border-top padding-horizontal-10 block">

				<div class="inline-block hide">

					<img src="<?=$global['absolute-url-admin'];?>assets/images/avatar-1.jpg" alt="">

				</div>

				<div class="inline-block">

					<h5 class="no-margin"> Welcome </h5>

					
						<h4 class="no-margin"> <?=ucwords($_SESSION['admin_username']);?> </h4>
						
						
				</div>


			</div>

			<!-- start: MAIN NAVIGATION MENU -->

			<ul class="main-navigation-menu">	
			
					
				<!--
				<li <?php if($curpage == "dashboard") { ?>class="active"<?php } ?>>

							<a href="<?=$path['dash'];?>">

								<span class="title"><i class="fa fa-bar-chart"></i>Dashboard </span>

							</a>					

				</li>

				-->
						
			

				<li <?php if($curpage == "sekolah") { ?>class="active"<?php } ?>>

							<a href="<?=$path['sekolah'];?>">

								<span class="title"><i class="fa fa-graduation-cap"></i>Data Sekolah </span>

							</a>					

				</li>

				<?php if($_SESSION['user_status']==1) { ?>
				<li <?php if($curpage == "prospek") { ?>class="active"<?php } ?>>

							<a href="<?=$path['prospek'];?>">

								<span class="title"><i class="fa fa-check-square-o"></i>Data Prospek</span>

							</a>					

				</li>

				<li <?php if($curpage == "tsekolah") { ?>class="active"<?php } ?>>

							<a href="<?=$path['sekolah-add'];?>">

								<span class="title"><i class="fa fa-pencil-square-o"></i>Tambah Data Sekolah </span>

							</a>					

				</li>

				<li <?php if($curpage == "karyawan") { ?>class="active"<?php } ?>>

							<a href="<?=$path['karyawan'];?>">

								<span class="title"><i class="fa fa-users"></i>Record Karyawan </span>

							</a>					

				</li>

				<?php }?>

				<?php if($_SESSION['user_status']!=1) { ?>

				<li <?php if($curpage == "karyawan") { ?>class="active"<?php } ?>>

							<a href="<?=$path['karyawan'].'?id='.$_SESSION['admin_id'];?>">

								<span class="title"><i class="fa fa-folder-open"></i>Record Kerja </span>

							</a>					

				</li>

				<?php } ?>

			
						
				<?php if($_SESSION['user_status'] == 1) { ?>

				
				<li <?php if($curpage == "user") { ?>class="active"<?php } ?>>

					<a href="<?=$path['user'];?>"><i class="fa fa-user"></i> <span class="title"> User Access </span></a>

				</li>
				<?php } ?>


				<?php if($_SESSION['user_status']!=1) { ?>

				<li <?php if($curpage == "") { ?>class="active"<?php } ?>>

							

				
				</li>
				<li <?php if($curpage == "") { ?>class="active"<?php } ?>>

						<br>	

				
				</li>

				<li <?php if($curpage == "") { ?>class="active"<?php } ?>>

							<a href="<?=$path['karyawan-list'].$_SESSION['admin_id'].'&type=telp&tgl='.$now;?>">

								<span class="title"><h4><span class="label label-primary"><?=$_SESSION['telp'];?></span> Ditelpon</h4></span>


							</a>

				
				</li>


				<li <?php if($curpage == "") { ?>class="active"<?php } ?>>

							<a href="<?=$path['karyawan-list'].$_SESSION['admin_id'].'&type=diangkat&tgl='.$now;?>">

								<span class="title"><h4><span class="label label-primary"><?=$_SESSION['diangkat'];?></span> Diangkat</h4></span>


							</a>

				
				</li>

				<li <?php if($curpage == "") { ?>class="active"<?php } ?>>

							<a href="<?=$path['karyawan-list'].$_SESSION['admin_id'].'&type=prospek&tgl='.$now;?>">

								<span class="title"><h4><span class="label label-primary"><?=$_SESSION['prospek'];?></span> Prospek</h4></span>


							</a>

				
				</li>

				<?php } ?>

				

			</ul>

			<!-- end: MAIN NAVIGATION MENU -->

		</div>

		<!-- end: SIDEBAR -->

	</div>

	<div class="slide-tools">

		<?php if(isset($_SESSION['user_status'])) { ?>
		<div class="col-xs-6 text-left no-padding">

			<a class="btn btn-sm status" href="#">

				Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>

			</a>

		</div>

		<div class="col-xs-6 text-right no-padding">

			<a class="btn btn-sm log-out text-right" href="<?=$path['logout'];?>">

				<i class="fa fa-power-off"></i> Log Out

			</a>

		</div>
		<?php } ?>

	</div>

</nav>